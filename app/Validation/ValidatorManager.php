<?php namespace App\Validation;

class ValidatorManager {

    private static $rules_manager_common_info = [
        'map'=>'url|max:250',                                       // Карта


        'address_dispatch_service'=>'max:250',                      //— Адрес диспетчерской службы
        'contact_telephone_dispatch_service'=>'max:250',            //— Контактные телефоны диспетчерской службы
        'mode_operation_dispatch_service'=>'max:250',               //— Режим работы диспетчерской службы
        'fax'=>'max:50',                                            //Факс
        'number_houses'=>'integer',                                 //Количество домов, находящихся в управлении , ед.
        'area_houses'=>'numeric',                                   //Площадь домов, находящихся в управлении, кв. м


        'responsible_for_filling'=>'max:100',                       // Ответственный за заполнение
        'full_name'=>'max:250',                                     // Полное наименование
        'short_name'=>'max:150',                                    // Краткое наименование
        'organization_form'=>'max:150',                             // Организационная форма
        'director'=>'max:150',                                      // Руководитель
        'inn'=>'numeric',                                           // ИНН
        'ogrn'=>'numeric',                                          // ОГРН или ОГРНИП
        'date_ogrn'=>'max:150',                                     // Дата присвоения ОГРН (ОГРНИП)
        'name_register_organ'=>'max:250',                           // Наименование органа, принявшего решение о регистрации
        'legal_address'=>'max:250',                                 // Юридический адрес
        'physical_address'=>'max:250',                              // Фактический адрес
        'mailing_address'=>'max:250',                               // Почтовый адрес
        'time_working'=>'max:150',                                  // Режим работы
        'phone'=>'max:25',                                          // Телефон
        'email_address'=>'email|min:6|max:30',                      // Электронный адрес
        'internet_site'=>'url|max:30',                              // Интернет сайт
        'share_in_authorized_capital_subject​​'=>'numeric',           // Доля участия в уставном капитале Субъекта РФ, %
        'share_in_authorized_capital_municipal'=>'numeric',         // Доля участия в уставном капитале муниципального образования, %
        'additional_information'=>'max:250',                        // Дополнительная информация
        'self_regulatory_organizations'=>'max:250',                 // Сведения об участии в саморегулируемых организациях
        'number_subject_where_organization_working'=>'integer',     // Количество Субъектов РФ, в которых организация осуществляет свою деятельность
        'number_municipal_where_organization_working'=>'integer',   // Количество муниципальных образований, в которых организация осуществляет свою деятельность
        'number_office'=>'integer',                                 // Количество офисов обслуживания граждан
        'regular_staffing'=>'integer',                              // Штатная численность на отчетную дату, чел.
        'regular_staffing_administrative'=>'integer',               // - административный персонал, чел.
        'regular_staffing_engineers'=>'integer',                    // - инженеры, чел.
        'regular_staffing_working'=>'integer',                      // - рабочий персонал, чел.
        'laid_off_during_period'=>'integer',                        // Уволено за отчетный период, чел.
        'laid_off_during_period_administrative'=>'integer',         // - административный персонал, чел.
        'laid_off_during_period_engineers'=>'integer',              // - инженеры, чел.
        'laid_off_during_period_working'=>'integer',                // - рабочий персонал, чел.
        'number_accidents'=>'integer',                              // Число несчастных случаев за отчетный период
        'number_administrative_responsibility'=>'integer',          // Число случаев привлечения организации к административной ответственности
        'copies_document_administrative_responsibility'=>'max:100', // Копии документов о применении мер административного воздействия, а также мер, принятых для устранения нарушений, повлекших применение административных санкций
        'members_of_board'=>'max:250',                              // Члены правления ТСЖ или ЖСК
        'members_audit_commission'=>'max:250',                      // Члены ревизионной комиссии
        'more_information'=>'max:250'                               // Дополнительные сведения в произвольной форме
    ];

    private static $rules_manager_housing = [
        'number_peoples'=>'integer',                                       // Число жителей в обслуживаемых домах
        'number_houses_end_time'=>'integer',                               // Количество домов под управлением на отчетную дату
        'number_houses_end_time_tsg'=>'integer',                           // - обслуживаемых ТСЖ
        'number_houses_end_time_tsg_and_organization'=>'integer',          // - обслуживаемых по договору между ТСЖ и управляющей организацией
        'number_houses_end_time_tsg_and_owner'=>'integer',                 // - обслуживаемых по договору между собственниками и управляющей организацией
        'number_houses_end_time_open_competition'=>'integer',              // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
        'number_houses_start_time'=>'integer',                             // Количество домов под управлением на начало периода
        'number_houses_start_time_tsg'=>'integer',                         // - обслуживаемых ТСЖ
        'number_houses_start_time_tsg_and_organization'=>'integer',        // - обслуживаемых по договору между ТСЖ и управляющей организацией
        'number_houses_start_time_tsg_and_owner'=>'integer',               // - обслуживаемых по договору между собственниками и управляющей организацией
        'number_houses_start_time_open_competition'=>'integer',            // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
        'common_area_houses_reporting_date'=>'numeric',                    // Общая площадь домов под управлением на отчетную дату, включая жилые и нежилые помещения, а также помещения общего пользования, тыс.кв.м.
        'common_area_houses_reporting_date_less_25'=>'numeric',            // - по домам до 25 лет
        'common_area_houses_reporting_date_26_50'=>'numeric',              // - по домам от 26 до 50 лет
        'common_area_houses_reporting_date_51_75'=>'numeric',              // - по домам от 51 до 75 лет
        'common_area_houses_reporting_date​_more_76'=>'numeric',            // - по домам 76 лет и более
        'common_area_houses_reporting_date_house_alarm'=>'numeric',        // - по аварийным домам
        'common_area_houses_start_period'=>'numeric',                      // Площадь домов под управлением на начало периода, тыс.кв.м.
        'common_area_houses_start_period_concluded_contract'=>'numeric',   // - изменение площади по заключенным договорам
        'common_area_houses_start_period_terminate_contract'=>'numeric',   // - изменение площади по расторгнутым договорам
        'average_term_service'=>'numeric',                                 // Средний срок обслуживания МКД, лет
        'average_term_service_less_25'=>'numeric',                         // - по домам до 25 лет
        'average_term_service_26_50'=>'numeric',                           // - по домам от 26 до 50 лет
        'average_term_service_51_75'=>'numeric',                           //  - по домам от 51 до 75 лет
        'average_term_service_more_76'=>'numeric',                         // - по домам 76 лет и более
        'average_term_service_house_alarm'=>'numeric'                      // - по аварийным домам
    ];

    private static $rules_manager_financial_highlights = [

        'start_time_reporting_period'=>'max:50',                          //Дата начала отчетного периода
        'end_time_reporting_period'=>'max:50',                            //Дата конца отчетного периода
        'total_debt'=>'numeric',                                           //Общая задолженность управляющей организации (индивидуального предпринимателя) перед ресурсоснабжающими организациями за коммунальные ресурсы, руб.
        'total_debt_heat_energy'=>'numeric',                               //— Общая задолженность по тепловой энергии, руб.
        'total_debt_heat_energy_for_heating'=>'numeric',                   //— Общая задолженность по тепловой энергии для нужд отопления, руб.
        'total_debt_heat_energy_for_water'=>'numeric',                     //— Общая задолженность по тепловой энергии для нужд горячего водоснабжения, руб.
        'total_debt_hot_water_supply'=>'numeric',                          //— Общая задолженность по горячей воде, руб.
        'total_debt_cold_water'=>'numeric',                                //— Общая задолженность по холодной воде, руб.
        'total_debt_sanitation'=>'numeric',                                //— Общая задолженность по водоотведению, руб.
        'total_debt_gas'=>'numeric',                                       //— Общая задолженность по поставке газа, руб.
        'total_debt_electricity'=>'numeric',                               //— Общая задолженность по электрической энергии, руб.
        'total_debt_for_other_resources'=>'numeric',                       //— Общая задолженность по прочим ресурсам (услугам), руб.



        'income_service'=>'numeric',                                       // Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб.
        'income_service_houses_less_25'=>'numeric',                        // - по домам до 25 лет
        'income_service_houses_26_50'=>'numeric',                          // - по домам от 26 до 50 лет
        'income_service_houses_51_75'=>'numeric',                          // - по домам от 51 до 75 лет
        'income_service_houses_more_76'=>'numeric',                        // - по домам 76 лет и более
        'income_service_houses_alarm'=>'numeric',                          // - по аварийным домам
        'income_common_property'=>'numeric',                               // Сумма доходов, полученных от использования общего имущества за отчетный период, тыс.руб.
        'income_common_property_less_25'=>'numeric',                       // - по домам до 25 лет
        'income_common_property_26_50'=>'numeric',                         // - по домам от 26 до 50 лет
        'income_common_property_51_75'=>'numeric',                         // - по домам от 51 до 75 лет
        'income_common_property_more_76'=>'numeric',                       // - по домам 76 лет и более
        'income_common_property_alarm'=>'numeric',                         // - по аварийным домам
        'income_utilities'=>'numeric',                                     // Доход, полученный за отчетный период от предоставления коммунальных услуг без учета коммунальных ресурсов, поставленных
                                                                // потребителям непосредственно поставщиками по прямым договорам, тыс.руб.
        'income_utilities_heating'=>'numeric',                             // - отопление
        'income_utilities_electricity'=>'numeric',                         // - электричество
        'income_utilities_gas'=>'numeric',                                 // - газ
        'income_utilities_hot_water_supply'=>'numeric',                    // - горячее водоснабжение
        'income_utilities_cold_water'=>'numeric',                          // - холодное водоснабжение
        'income_utilities_sanitation'=>'numeric',                          // - водоотведение
        'costs_service'=>'numeric',                                        // Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб.
        'costs_service_less_25'=>'numeric',                                // - по домам до 25 лет
        'costs_service_26_50'=>'numeric',                                  // - по домам от 26 до 50 лет
        'costs_service_51_75'=>'numeric',                                  // - по домам от 51 до 75 лет
        'costs_service_more_76'=>'numeric',                                // - по домам 76 лет и более
        'costs_service_alarm'=>'numeric',                                  // - по аварийным домам
        'costs_contract_service'=>'numeric',                               // Выплаты по искам по договорам управления за отчетный период, тыс.руб.
        'costs_contract_service_damage'=>'numeric',                        // - иски по компенсации нанесенного ущерба
        'costs_contract_service_unused_service'=>'numeric',                // - иски по снижению платы в связи с неоказанием услуг
        'costs_contract_service_non_delivery_resources'=>'numeric',        // - иски по снижению платы в связи с недопоставкой ресурсов
        'costs_resources_organization'=>'numeric',                         // Выплаты по искам ресурсоснабжающих организаций за отчетный период, тыс.руб.
        'costs_resources_organization_heating'=>'numeric',                 // - отопление
        'costs_resources_organization_electricity'=>'numeric',             // - электричество
        'costs_resources_organization_gas'=>'numeric',                     // - газ
        'costs_resources_organization_hot_water_supply'=>'numeric',        // - горячее водоснабжение
        'costs_resources_organization_cold_water'=>'numeric',              // - холодное водоснабжение
        'costs_resources_organization_sanitation'=>'numeric',              // - водоотведение
        'net_assets'=>'numeric'
    ];

    private static $rules_manager_arrears = [
        'past_account_owner_service_current_date'=>'numeric',                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб.
        'past_account_owner_service_current_date_less_25'=>'numeric',            // - по домам до 25 лет
        'past_account_owner_service_current_date_26_50'=>'numeric',              // - по домам от 26 до 50 лет
        'past_account_owner_service_current_date_51_75'=>'numeric',              // - по домам от 51 до 75 лет
        'past_account_owner_service_current_date_more_76'=>'numeric',            // - по домам 76 лет и более
        'past_account_owner_service_current_date_alarm'=>'numeric',              // - по аварийным домам
        'past_account_owner_service_start_period'=>'numeric',                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению на начало отчетного периода, тыс.руб.
        'past_account_owner_utilities_current_date'=>'numeric',                  // Просроченная задолженность собственников помещений и иных лиц, .... , за коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
        'past_account_owner_utilities_current_date_heating'=>'numeric',          // - отопление
        'past_account_owner_utilities_current_date_electricity'=>'numeric',      // - электричество
        'past_account_owner_utilities_current_date_gas'=>'numeric',              // - газ
        'past_account_owner_utilities_current_date_hot_water_supply'=>'numeric', // - горячее водоснабжение
        'past_account_owner_utilities_current_date_cold_water'=>'numeric',       // - холодное водоснабжение
        'past_account_owner_utilities_current_date_sanitation'=>'numeric',       // - водоотведение
        'past_account_owner_utilities_start_period'=>'numeric',                  // Просроченная задолженность собственников помещений и иных лиц, .... за коммунальные услуги на начало отчетного периода, тыс.руб.
        'past_account_organ_utilities_current_date'=>'numeric',                  // Просроченная задолженность организации за предоставленные коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
        'past_account_organ_utilities_current_date_heating'=>'numeric',          // - отопление
        'past_account_organ_utilities_current_date_electricity'=>'numeric',      // - электричество
        'past_account_organ_utilities_current_date_gas'=>'numeric',              // - газ
        'past_account_organ_utilities_current_date_hot_water_supply'=>'numeric', // - горячее водоснабжение
        'past_account_organ_utilities_current_date_cold_water'=>'numeric',       // - холодное водоснабжение
        'past_account_organ_utilities_current_date_sanitation'=>'numeric',       // - водоотведение
        'amount_collected_debt_service'=>'numeric',                              //  Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
                                                                      //  проживающих в помещениях на законных основаниях за услуги по управлению, тыс.руб.
        'amount_collected_debt_service_less_25'=>'numeric',                      // - по домам до 25 лет
        'amount_collected_debt_service_26_50'=>'numeric',                        // - по домам от 26 до 50 лет
        'amount_collected_debt_service_51_75'=>'numeric',                        // - по домам от 51 до 75 лет
        'amount_collected_debt_service_more_76'=>'numeric',                      // - по домам 76 лет и более
        'amount_collected_debt_service_alarm'=>'numeric',                        // - по аварийным домам
        'amount_collected_debt_utilities'=>'numeric',                            // Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
                                                                      // проживающих в помещениях на законных основаниях за предоставленные коммунальные услуги, тыс.руб.
        'amount_collected_debt_utilities_heating'=>'numeric',                    // - отопление
        'amount_collected_debt_utilities_electricity'=>'numeric',                // - электричество
        'amount_collected_debt_utilities_gas'=>'numeric',                        // - газ
        'amount_collected_debt_utilities_hot_water_supply'=>'numeric',           // - горячее водоснабжение
        'amount_collected_debt_utilities_cold_water'=>'numeric',                 // - холодное водоснабжение
        'amount_collected_debt_utilities_sanitation'=>'numeric'                  // - водоотведение
    ];

    private static $rules_manager_service_mkd = [
        'scope_repair'=>'numeric',                            // Объем работ по ремонту за отчетный период, тыс.руб.
        'scope_repair_less_25'=>'numeric',                    // - по домам до 25 лет
        'scope_repair_26_50'=>'numeric',                      // - по домам от 26 до 50 лет
        'scope_repair_51_75'=>'numeric',                      // - по домам от 51 до 75 лет
        'scope_repair_more_76'=>'numeric',                    // - по домам 76 лет и более
        'scope_repair_date_alarm'=>'numeric',                 // - по аварийным домам
        'scope_improvement'=>'numeric',                       // Объем работ по ремонту за отчетный период, тыс.руб.
        'scope_improvement_less_25'=>'numeric',               // - по домам до 25 лет
        'scope_improvement_26_50'=>'numeric',                 // - по домам от 26 до 50 лет
        'scope_improvement_51_75'=>'numeric',                 // - по домам от 51 до 75 лет
        'scope_improvement_more_76'=>'numeric',               // - по домам 76 лет и более
        'scope_improvement_date_alarm'=>'numeric',            // - по аварийным домам
        'volume_attracted_funds'=>'numeric',                  // Объем привлеченных средств за отчетный период, тыс.руб.
        'volume_attracted_funds_subsidies'=>'numeric',        // - Субсидии
        'volume_attracted_funds_credits'=>'numeric',          // - Кредиты
        'volume_attracted_funds_financing_leasing'=>'numeric',// - Финансирование по договорам лизинга
        'volume_attracted_funds_financing_energy'=>'numeric', // - Финансирование по энергосервисным договорам
        'volume_attracted_funds_earmarked'=>'numeric',        // - Целевые взносы жителей
        'volume_attracted_funds_other'=>'numeric',            // - Другие источники
        'paid_ru_indications'=>'numeric',                     // Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.
        'paid_ru_indications_heating'=>'numeric',             // - отопление
        'paid_ru_indications_electricity'=>'numeric',         // - электричество
        'paid_ru_indications_gas'=>'numeric',                 // - газ
        'paid_ru_indications_hot_water_supply'=>'numeric',    // - горячее водоснабжение
        'paid_ru_indications_cold_water'=>'numeric',          // - холодное водоснабжение
        'paid_ru_accounts'=>'numeric',                        //  Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.
        'paid_ru_accounts_heating'=>'numeric',                // - отопление
        'paid_ru_accounts_electricity'=>'numeric',            // - электричество
        'paid_ru_accounts_gas'=>'numeric',                   // - газ
        'paid_ru_accounts_hot_water_supply'=>'numeric',       // - горячее водоснабжение
        'paid_ru_accounts_cold_water'=>'numeric'             // - холодное водоснабжение
    ];



    public static function get_rules_manager_service_mkd_list() {
        return self::$rules_manager_service_mkd;
    }

    public static function get_rules_manager_arrears_list() {
        return self::$rules_manager_arrears;
    }


    public static function get_rules_manager_financial_highlights_list() {
        return self::$rules_manager_financial_highlights;
    }


    public static function get_rules_manager_housing_list() {
        return self::$rules_manager_housing;
    }

    public static function get_rules_manager_common_info_list() {
        return self::$rules_manager_common_info;
    }

}