<?php
namespace App\Services;

use App\Validation\ValidatorManager;
use App\Models\Home;
use App\Models\Manager\ManagerArrears;
use App\Models\Manager\ManagerCommonInformation;
use App\Models\Manager\ManagerFinancialHighlights;
use App\Models\Manager\ManagerHousing;
use App\Models\Manager\ManagerServiceMKD;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Schema\Blueprint;



class ManagerService
{

    public static function get_All_Info_For_One_Manager($params, $manager_id) {
        /*$manager_service_mkd=ManagerServiceMKD::getOne($manager_id);
        if ($manager_service_mkd instanceof \Exception)
        {
            //return 'Error in during execute ManagerServiceMKD::getOne(manager_id): '.$manager_service_mkd;
            $manager_service_mkd=NULL;
        }
        $manager_arrears=ManagerArrears::getOne($manager_id);
        if ($manager_arrears instanceof \Exception)
        {
            //return 'Error in during execute ManagerArrears::getOne(manager_id): '.$manager_arrears;
            $manager_arrears=NULL;
        }*/
        $manager_financial_highlights=ManagerFinancialHighlights::getOne($manager_id);
        if ($manager_financial_highlights instanceof \Exception)
        {
            //return 'Error in during execute ManagerFinancialHighlights::getOne(manager_id): '.$manager_financial_highlights;
            $manager_financial_highlights=NULL;
        }
        /*$manager_housing=ManagerHousing::getOne($manager_id);
        if ($manager_housing instanceof \Exception)
        {
            //return 'Error in during execute ManagerHousing::getOne(manager_id): '.$manager_housing;
            $manager_housing=NULL;
        }*/
        $manager_common_info=ManagerCommonInformation::getOne($manager_id);
        if ($manager_common_info instanceof \Exception)
        {
            //return 'Error in during execute ManagerCommonInformation::getOne($manager_id): '.$manager_common_info;
            $manager_common_info=NULL;
        }
        //$params=array_merge($params, array('manager_service_mkd' => $manager_service_mkd));
        //$params=array_merge($params, array('manager_arrears' => $manager_arrears));
        $params=array_merge($params, array('manager_financial_highlights' => $manager_financial_highlights));
        //$params=array_merge($params, array('manager_housing' => $manager_housing));
        $params=array_merge($params, array('manager_common_info' => $manager_common_info));
        return $params;
    }

    public static function get_NULL_Info_For_One_Manager($params) {
        $object= new ManagerCommonInformation();
        $params=array_merge($params, array('manager_common_info' => $object));
        //$object= new ManagerServiceMKD();
        //$params=array_merge($params, array('manager_service_mkd' => $object));
        //$object= new ManagerArrears();
        //$params=array_merge($params, array('manager_arrears' => $object));
        $object= new ManagerFinancialHighlights();
        $params=array_merge($params, array('manager_financial_highlights' => $object));
        //$object= new ManagerHousing();
        //$params=array_merge($params, array('manager_housing' => $object));
        return $params;
    }

    public static function get_All_Info_For_All_Manager($params,$managers) {
        $total_area=array();
        $number_all_houses=array();
        $number_all_poeple=array();

        foreach($managers as $manager){
            array_push($total_area,$manager["common_area_houses_reporting_date"]);
            array_push($number_all_houses,$manager["number_houses_end_time"]);
            array_push($number_all_poeple,$manager["regular_staffing"]);
        }
        $total_area=array_sum($total_area);
        $number_all_houses=array_sum($number_all_houses);
        $number_all_poeple=array_sum($number_all_poeple);
        $params=array_merge($params, array('number_managers' => count($managers)));
        $params=array_merge($params, array('total_area' => $total_area));
        $params=array_merge($params, array('number_all_houses' => $number_all_houses));
        $params=array_merge($params, array('number_all_poeple' => $number_all_poeple));
        return $params;
    }


    public static function get_Short_Info_For_Managers($managers_id){

        $managers=array();
        foreach($managers_id as $manager_id){
            $managers_common=ManagerCommonInformation::getShortInfoForOne($manager_id);
            if ($managers_common instanceof Exception || !( isset($managers_common) && count($managers_common)))
            {
                //return 'Error in during execute ManagerCommonInformation::getShortInfoForOne(manager_id): '.$managers_common;
                return NULL;
            }

            /*$managers_house=ManagerHousing::getShortInfoForOne($manager_id);
            if ($managers_house instanceof Exception || !( isset($managers_house) && count($managers_house)))
            {
                //return 'Error in during execute ManagerHousing::getShortInfoForOne(manager_id): '.$managers_house;
                return NULL;
            }
*/
            $manager=array(
                'id' => $managers_common[0]->id,
                'short_name' => $managers_common[0]->short_name,
                'inn' => $managers_common[0]->inn,
                'regular_staffing' => $managers_common[0]->regular_staffing,
                'number_houses_end_time' => $managers_common[0]->number_houses,
                'common_area_houses_reporting_date' =>$managers_common[0]->area_houses

            );
            array_push($managers,$manager);

        }
        return $managers;
    }

    public static function Validator_Managers($data_manager_common_info,$data_manager_financial_highlights){//,$data__manager_housing,$data_manager_arrears,$data_manager_service_mkd){

        $val=Validator::make($data_manager_common_info, ValidatorManager::get_rules_manager_common_info_list());
        if ($val->fails()){
            return $val->getMessageBag();
        }

        //$val=Validator::make($data__manager_housing, ValidatorManager::get_rules_manager_housing_list());
        //if ($val->fails()){
            //return $val->getMessageBag();
        //}

        $val=Validator::make($data_manager_financial_highlights, ValidatorManager::get_rules_manager_financial_highlights_list());
        if ($val->fails()){
            return $val->getMessageBag();
        }

        /*$val=Validator::make($data_manager_arrears, ValidatorManager::get_rules_manager_arrears_list());
        if ($val->fails()){
            return $val->getMessageBag();
        }

        $val=Validator::make($data_manager_service_mkd, ValidatorManager::get_rules_manager_service_mkd_list());
        if ($val->fails()){
            return $val->getMessageBag();
        }*/

        return "true";
    }

    /**
     * @param $id
     * @return array
     */
    public function getManagerInfoById($id) {
        $result = [];
        if ($id) {
            $managerCommonInformation = ManagerCommonInformation::where('id', $id)->firstOrFail();
            $result = [
                'id'            => $id,
                'short_name'    => $managerCommonInformation->short_name
            ];
        }

        return $result;
    }


    public static  function create_manager_license($id) {
        Schema::create("manager_license_$id", function(Blueprint $table)
        {
            $table->increments('id');
            $table->char('number_license',100)->nullable();             // Номер лицензии
            $table->char('date_receipt_license',100)->nullable();       // Дата получения лицензии
            $table->char('issuing_authority',200)->nullable();          // Орган, выдавший лицензию
            $table->char('documents_license',250)->nullable();          // Документ лицензии
        });
    }


    public static function getAll_manager_license($id)
    {
        try{
            $post = DB::table("manager_license_$id")->get();;
        } catch (\Exception $e){
            return null;
        }
        return $post;
    }

    public static function edit_manager_license($id,$data)
    {
        try{
            DB::table("manager_license_$id")->truncate();
            $post = DB::table("manager_license_$id")->insert($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function down_manager_license($id)
    {
        Schema::drop("manager_license_$id");
    }

}