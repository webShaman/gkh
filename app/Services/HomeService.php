<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Models\Home;
use App\Models\Home\Passport\GeneralInformation as PassportGeneralInformation;
use App\Models\Home\Passport\StructuralElements as PassportStructuralElements;
use App\Models\Home\Passport\EngineeringSystems as PassportEngineeringSystems;
use App\Models\Home\Passport\Elevators as PassportElevators;
use App\Models\Home\Passport\MeteringDevices as PassportMeteringDevices;
use App\Models\Home\Management\Organization as ManagementOrganization;
use App\Models\Home\Management\WorkPerformed as ManagementWorkPerformed;
use App\Models\Home\Management\PublicService as ManagementPublicService;
use App\Models\Home\Management\CommonProperty as ManagementCommonProperty;
use App\Models\Home\Management\Repair as ManagementRepair;
use App\Models\Home\Report\Information as ReportInformation;
use App\Models\Home\Report\WorkPerformed as ReportWorkPerformed;
use App\Models\Home\Report\Claim as ReportClaim;
use App\Models\Home\Report\Utilities as Utilities;
use App\Models\Home\Report\PublicService as ReportPublicService;
use App\Models\Home\Report\ClaimWork as ReportClaimWork;

use App\Models\Home\PassportGeneral;
use App\Models\Home\EngineeringSystems;
use App\Models\Home\Elevator;
use App\Models\Home\StructuralElements as SE;
use App\Models\Home\UtilityBills;
use App\Models\Home\CardManagement;
use App\Models\Home\CardRco;
use App\Models\Home\CommonProperty;
use App\Models\Manager\ManagerCommonInformation;
use App\Models\Area;
use App\Libs\PaginationUk;

class HomeService
{
    protected $wordsBeCut = [
        'ул', 'Ул', 'улица', 'Улица',
        'проспект',
        'переулок'
    ];
    /**
     * @param $areaName
     * @return array
     */
    public function getGrids($areaName = null) {
        $grids = array();
        if ($areaName) {
            $areaId = Area::where('name', $areaName)->pluck('id');
            $homes = Home::where('area_id', $areaId)->paginate(PaginationUk::$paginate);
        } else {
            /** @var Home $homes */
            $homes = Home::paginate(PaginationUk::$paginate);
        }

        foreach ($homes as $home) {
            $passportGeneral = PassportGeneralInformation::where('home_id', $home->id)->firstOrFail();
            $managerCommonInformation = ManagerCommonInformation::where('inn', $home->manager_inn)->first();

            $grids[] = array(
                'id' => $home->id,
                'name' => $home->address,
                'year' => $passportGeneral->year_commissioning,
                'total_area' => $passportGeneral->total_area,
                'residents' => $passportGeneral->residents_number,
                'manager' => $managerCommonInformation,
                'area' => $home->area()->getResults()->getAttributes(),
            );
        }

        return array(
            'grids'     => $grids,
            'render'    => with(new PaginationUk($homes))->render(),
        );
    }

    /**
     * @param $id
     * @return array
     */
    public function getHomeById($id){
        /** @var Home $home */
        $home = Home::find($id);

        /** @var PassportGeneral $passportGeneral */
        $passportGeneral = $home->passportGeneral()->getResults();

        /** @var EngineeringSystems $engineeringSystems */
        $engineeringSystems = $home->engineeringSystems()->getResults();

        /** @var SE $structuralElements */
        $structuralElements = $home->structuralElements1()->getResults();

        /** @var Elevator $elevator */
        $elevator = $home->elevator()->getResults();

        /** @var UtilityBills $utilityBills */
        $utilityBills = $home->utilityBills()->getResults();

        /** @var CardManagement $cardManagement */
        $cardManagement = $home->cardManagement()->getResults();

        /** @var CardRco $cardRco */
        $cardRco = $home->cardRco()->getResults();

        /** @var CommonProperty $commonProperty */
        $commonProperty = $home->commonProperty()->getResults();

        return array_merge(
            $home->getAttributes(),
            $passportGeneral ? $passportGeneral->getAttributes() : array(),
            $engineeringSystems ? $engineeringSystems->getAttributes() : array(),
            $structuralElements ? $structuralElements->getAttributes() : array(),
            $elevator ? $elevator->getAttributes() : array(),
            $utilityBills ? $utilityBills->getAttributes() : array(),
            $cardManagement ? $cardManagement->getAttributes() : array(),
            $cardRco ? $cardRco->getAttributes() : array(),
            $commonProperty ? $commonProperty->getAttributes() : array()
        );
    }

    public function getModelFillable()
    {
        $modelHome = new Home();
        $passportGeneralInformation = new PassportGeneralInformation();
        $passportStructuralElements = new PassportStructuralElements();
        $passportEngineeringSystems = new PassportEngineeringSystems();
        return [
            $modelHome::TABLE_NAME                      => $this->arrayFlip($modelHome->getFillable()),
            $passportGeneralInformation::TABLE_NAME     => $this->arrayFlip($passportGeneralInformation->getFillable()),
            $passportStructuralElements::TABLE_NAME     => $this->arrayFlip($passportStructuralElements->getFillable()),
            $passportEngineeringSystems::TABLE_NAME     => $this->arrayFlip($passportEngineeringSystems->getFillable())

        ];
    }

    /**
     * @param $array
     * @return array
     */
    protected function arrayFlip($array)
    {
        $result = [];
        foreach ($array as $value) {
            $result[$value] = '';
        }
        return $result;
    }

    /**
     * @param $id
     * @param array $filledHome
     * @return array
     */
    public function getHomeById2($id, $filledHome = []) {
        /** @var Home $home */

        $home = Home::find($id);
        //todo: это пиздец, придумать что-то покрасивее

         $result = [
            Home::TABLE_NAME => $home->getAttributes(),
            PassportGeneralInformation::TABLE_NAME => $home->passportGeneralInformation()->getResults() ? $home->passportGeneralInformation()->getResults()->getAttributes() : 'данные не заполненны',
            PassportStructuralElements::TABLE_NAME => $home->passportStructuralElements()->getResults() ? $home->passportStructuralElements()->getResults()->getAttributes() : 'данные не заполненны',
            PassportEngineeringSystems::TABLE_NAME => $home->passportEngineeringSystems()->getResults() ? $home->passportEngineeringSystems()->getResults()->getAttributes() : 'данные не заполненны',
            PassportElevators::TABLE_NAME => $home->passportElevators()->getResults() ? $home->passportElevators()->getResults()->getAttributes() : 'данные не заполненны',
            PassportMeteringDevices::TABLE_NAME => $home->passportMeteringDevices()->getResults() ? $home->passportMeteringDevices()->getResults()->getAttributes() : 'данные не заполненны',
            ManagementOrganization::TABLE_NAME => $home->managementOrganization()->getResults() ? $home->managementOrganization()->getResults()->getAttributes() : 'данные не заполненны',
            ManagementWorkPerformed::TABLE_NAME => $home->managementWorkPerformed()->getResults() ? $home->managementWorkPerformed()->getResults()->getAttributes() : 'данные не заполненны',
            ManagementPublicService::TABLE_NAME => $home->managementPublicService()->getResults() ? $home->managementPublicService()->getResults() : 'данные не заполненны', // связь многие к одному
            ManagementCommonProperty::TABLE_NAME => $home->managementCommonProperty()->getResults() ? $home->managementCommonProperty()->getResults()->getAttributes() : 'данные не заполненны',
            ManagementRepair::TABLE_NAME => $home->managementRepair()->getResults() ? $home->managementRepair()->getResults()->getAttributes() : 'данные не заполненны',
            ReportInformation::TABLE_NAME => $home->reportInformation()->getResults() ? $home->reportInformation()->getResults()->getAttributes() : 'данные не заполненны',
            ReportWorkPerformed::TABLE_NAME => $home->reportWorkPerformed()->getResults() ? $home->reportWorkPerformed()->getResults() : 'данные не заполненны', // связь многие к одному
            ReportClaim::TABLE_NAME => $home->reportClaim()->getResults() ? $home->reportClaim()->getResults()->getAttributes() : 'данные не заполненны',
            Utilities::TABLE_NAME => $home->reportUtilities()->getResults() ? $home->reportUtilities()->getResults()->getAttributes() : 'данные не заполненны',
            ReportPublicService::TABLE_NAME => $home->reportPublicService()->getResults() ? $home->reportPublicService()->getResults() : 'данные не заполненны', // связь многие к одному
            ReportClaimWork::TABLE_NAME => $home->reportClaimWork()->getResults() ? $home->reportClaimWork()->getResults()->getAttributes() : 'данные не заполненны',
        ];

        foreach ($result as $key => $value) {
            $homeArray[$key] = !is_array($value) && !empty($filledHome[$key])
                ? $homeArray[$key] = $filledHome[$key]
                : $value ;
        }

        return $homeArray;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getIdFindName($name) {
        return Area::where('name', $name)->pluck('id');
    }

    /**
     * поиск дома по улице
     *
     * @param $address
     * @return array
     */
    public function getHomeByStreet($address) {
        $address = $this->wordsValid($address);

        $areaService = new AreaService;
        $result = array();
        if ($address) {
            $homes = Home::where('address', 'LIKE', "%$address%")->paginate(PaginationUk::$paginate);
            $homesResult = array();
            /** @var  Home $home */
            foreach ($homes as $home) {
                $homesResult[] =  array_merge($home->getAttributes(), array(
                    'manager' => null,
                    'area' => $areaService->getAreaById($home->area_id),
                ));
            }
            $result = array(
                'homes'     => $homesResult,
                'render'    => with(new PaginationUk($homes))->render(),
            );
        }
        return $result;
    }

    protected function wordsValid($address) {
        $words = explode(" ", preg_replace('/ {2,}/',' ',$address));
        foreach ($words as $key => $word) {
            if (in_array($word, $this->wordsBeCut)) {
                unset($words[$key]);
            }
        }
        return $address = implode(' ', $words);
    }

    /**
     * @param null $area
     * @return int
     */
    public function getNumberHomes($area = null) {

        if ($area) {
            $areaService = new AreaService();
            $areaId = $areaService->getIdFindName($area);
            $numberHomes = Home::where('area_id', $areaId)->count();
        } else {
            $numberHomes = Home::all()->count();
        }
        return $numberHomes;
    }

    /**
     * @param null $area
     * @return mixed
     */
    public function getNumberTotalArea($area = null) {
        $passportGeneral = PassportGeneralInformation::TABLE_NAME;
        $homes = Home::TABLE_NAME;
        if ($area) {
            $areaService = new AreaService();
            $areaId = $areaService->getIdFindName($area);
            $numberResidentialArea = Home::join("{$passportGeneral}", "{$homes}.id", '=', "{$passportGeneral}.home_id")
                ->where('area_id', $areaId)
                ->sum("{$passportGeneral}.total_area");
        } else {
            $numberResidentialArea = Home::join("{$passportGeneral}", "{$homes}.id", '=', "{$passportGeneral}.home_id")
                ->sum("{$passportGeneral}.total_area");
        }

        return $numberResidentialArea;
    }

    /**
     * @param null $area
     * @return mixed
     */
    public function getNumberResident($area = null) {
        $passportGeneral = PassportGeneralInformation::TABLE_NAME;
        $homes = Home::TABLE_NAME;

        if ($area) {
            $areaService = new AreaService();
            $areaId = $areaService->getIdFindName($area);
            $numberResident = Home::join("{$passportGeneral}", "{$homes}.id", '=', "{$passportGeneral}.home_id")
                ->where('area_id', $areaId)
                ->sum("{$passportGeneral}.residents_number");
        } else {
            $numberResident = Home::join("{$passportGeneral}", "{$homes}.id", '=', "{$passportGeneral}.home_id")
                ->sum("{$passportGeneral}.residents_number");
        }

        return $numberResident;
    }

    /**
     * добавить дом
     *
     * @param array $data
     */
    public function addHome($data = array()) {
        $managerId = null;
        if ($data['home']['manager_inn'] != null) {
            $managerId = ManagerCommonInformation::where('inn', $data['home']['manager_inn'])->pluck('id');
            $data['home'] = array_merge($data['home'], array('manager_id' => $managerId ));
        }
        //dd($data);
        DB::transaction(function() use ($data) {
            $homeModel = new Home();
            // todo поменять  'home' -> $homeModel::TABLE_NAME
            $home = $homeModel->create($data['home']);
            foreach ($homeModel->hashOneConnections as $nameModel => $model) {
                if (!empty($data[$nameModel])) {
                    foreach($data[$nameModel] as $key => $value) {
                        $element[$key] = $value ? $value : null;
                    }
                    $data = array_merge($element, array('home_id' => $home->id));
                    /**@param $model \Illuminate\Database\Eloquent\Model */
                    $model::create($data);
                }
            }
        });
    }

    /**
     * редактировать дом
     *
     * @param array $data
     * @param $id
     */
    public function editHome ($data = array(), $id) {
        $managerInn = $data['home']['manager_inn'];
        $managerId = ManagerCommonInformation::where('inn', $managerInn)->pluck('id');
        $data['home'] = array_merge($data['home'], array('manager_id' => $managerId ));

        DB::transaction(function() use ($data, $id) {
            $homeModel = new Home;
            // todo поменять  'home' -> $homeModel::TABLE_NAME
            $homeModel->where('id', $id)->update($data['home']);
            foreach ($homeModel->hashOneConnections as $nameModel => $model) {
                if (!empty($data[$nameModel])) {
                    foreach($data[$nameModel] as $key => $value) {
                        $element[$key] = $value ? $value : null;
                    }
                    $this->updateModel(new $model, $element, $id);
                }
            }
        });
    }

    /**
     * @param Model $model
     * @param array $data
     * @param $id
     */
    protected function updateModel(Model $model, array $data, $id) {
        $structuralElements = new $model;

        $columns=[];
        foreach ($structuralElements->getFillable() as $column) {
            if (!empty($data[$column]) && $column != 'home_id') {
                $columns[$column] = $data[$column] != '' ? $data[$column] : null;
            }
        }
        // если нет таблице привязанной к дому то создаем её
        if (!$model::where('home_id', $id)->update($columns)) {
            $data = array_merge($columns, array('home_id' => $id));
            $model::create($data);
        }
    }
}