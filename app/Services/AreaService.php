<?php
namespace App\Services;

use App\Models\Area;

class AreaService
{
    /**
     * @return array
     */
    public function getAreas() {
        foreach(Area::all() as $area) {
            $areas[] = $area['original'];
        }
        return $areas;
    }

    /**
     * @param $id
     * @return array
     */
    public function getAreaById($id) {
        /** @var Area $area */
        $area = Area::find($id);
        return $area->getAttributes();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getIdFindName($name) {
        return Area::where('name', $name)->pluck('id');
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getAreaForName($name) {
        $this->getIdFindName($name);
        /** @var  Area $area */
        $area = Area::find($this->getIdFindName($name));
        return $area->getAttributes();
    }

}