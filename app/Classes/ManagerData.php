<?php namespace App\Classes;

class ManagerData {

    private static $manager_common_info = array(
        'map',                                           // Карта

        'address_dispatch_service',                      //— Адрес диспетчерской службы
        'contact_telephone_dispatch_service',            //— Контактные телефоны диспетчерской службы
        'mode_operation_dispatch_service',               //— Режим работы диспетчерской службы
        'fax',                                           //Факс
        'number_houses',                                 //Количество домов, находящихся в управлении , ед.
        'area_houses',                                   //Площадь домов, находящихся в управлении, кв. м

        'responsible_for_filling',                       // Ответственный за заполнение
        'full_name',                                     // Полное наименование
        'short_name',                                    // Краткое наименование
        'organization_form',                             // Организационная форма
        'director',                                      // Руководитель
        'inn',                                           // ИНН
        'ogrn',                                          // ОГРН или ОГРНИП
        'date_ogrn',                                     // Дата присвоения ОГРН (ОГРНИП)
        'name_register_organ',                           // Наименование органа, принявшего решение о регистрации
        'legal_address',                                 // Юридический адрес
        'physical_address',                              // Фактический адрес
        'mailing_address',                               // Почтовый адрес
        'time_working',                                  // Режим работы
        'phone',                                         // Телефон
        'email_address',                                 // Электронный адрес
        'internet_site',                                 // Интернет сайт
        'share_in_authorized_capital_subject​​',           // Доля участия в уставном капитале Субъекта РФ, %
        'share_in_authorized_capital_municipal',         // Доля участия в уставном капитале муниципального образования, %
        'additional_information',                        // Дополнительная информация
        'self_regulatory_organizations',                 // Сведения об участии в саморегулируемых организациях
        'number_subject_where_organization_working',     // Количество Субъектов РФ, в которых организация осуществляет свою деятельность
        'number_municipal_where_organization_working',   // Количество муниципальных образований, в которых организация осуществляет свою деятельность
        'number_office',                                 // Количество офисов обслуживания граждан
        'regular_staffing',                              // Штатная численность на отчетную дату, чел.
        'regular_staffing_administrative',               // - административный персонал, чел.
        'regular_staffing_engineers',                    // - инженеры, чел.
        'regular_staffing_working',                      // - рабочий персонал, чел.
        'laid_off_during_period',                        // Уволено за отчетный период, чел.
        'laid_off_during_period_administrative',         // - административный персонал, чел.
        'laid_off_during_period_engineers',              // - инженеры, чел.
        'laid_off_during_period_working',                // - рабочий персонал, чел.
        'number_accidents',                              // Число несчастных случаев за отчетный период
        'number_administrative_responsibility',          // Число случаев привлечения организации к административной ответственности
        'copies_document_administrative_responsibility', // Копии документов о применении мер административного воздействия, а также мер, принятых для устранения нарушений, повлекших применение административных санкций
        'members_of_board',                              // Члены правления ТСЖ или ЖСК
        'members_audit_commission',                      // Члены ревизионной комиссии
        'more_information'                               // Дополнительные сведения в произвольной форме
        );

    private static $manager_housing = array(
        'number_peoples',                                       // Число жителей в обслуживаемых домах
        'number_houses_end_time',                               // Количество домов под управлением на отчетную дату
        'number_houses_end_time_tsg',                           // - обслуживаемых ТСЖ
        'number_houses_end_time_tsg_and_organization',          // - обслуживаемых по договору между ТСЖ и управляющей организацией
        'number_houses_end_time_tsg_and_owner',                 // - обслуживаемых по договору между собственниками и управляющей организацией
        'number_houses_end_time_open_competition',              // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
        'number_houses_start_time',                             // Количество домов под управлением на начало периода
        'number_houses_start_time_tsg',                         // - обслуживаемых ТСЖ
        'number_houses_start_time_tsg_and_organization',        // - обслуживаемых по договору между ТСЖ и управляющей организацией
        'number_houses_start_time_tsg_and_owner',               // - обслуживаемых по договору между собственниками и управляющей организацией
        'number_houses_start_time_open_competition',            // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
        'common_area_houses_reporting_date',                    // Общая площадь домов под управлением на отчетную дату, включая жилые и нежилые помещения, а также помещения общего пользования, тыс.кв.м.
        'common_area_houses_reporting_date_less_25',            // - по домам до 25 лет
        'common_area_houses_reporting_date_26_50',              // - по домам от 26 до 50 лет
        'common_area_houses_reporting_date_51_75',              // - по домам от 51 до 75 лет
        'common_area_houses_reporting_date​_more_76',            // - по домам 76 лет и более
        'common_area_houses_reporting_date_house_alarm',        // - по аварийным домам
        'common_area_houses_start_period',                      // Площадь домов под управлением на начало периода, тыс.кв.м.
        'common_area_houses_start_period_concluded_contract',   // - изменение площади по заключенным договорам
        'common_area_houses_start_period_terminate_contract',   // - изменение площади по расторгнутым договорам
        'average_term_service',                                 // Средний срок обслуживания МКД, лет
        'average_term_service_less_25',                         // - по домам до 25 лет
        'average_term_service_26_50',                           // - по домам от 26 до 50 лет
        'average_term_service_51_75',                           //  - по домам от 51 до 75 лет
        'average_term_service_more_76',                         // - по домам 76 лет и более
        'average_term_service_house_alarm'                      // - по аварийным домам
        );

    private static $manager_financial_highlights = array(

        'start_time_reporting_period',                          //Дата начала отчетного периода
        'end_time_reporting_period',                            //Дата конца отчетного периода
        'total_debt',                                           //Общая задолженность управляющей организации (индивидуального предпринимателя) перед ресурсоснабжающими организациями за коммунальные ресурсы, руб.
        'total_debt_heat_energy',                               //— Общая задолженность по тепловой энергии, руб.
        'total_debt_heat_energy_for_heating',                   //— Общая задолженность по тепловой энергии для нужд отопления, руб.
        'total_debt_heat_energy_for_water',                     //— Общая задолженность по тепловой энергии для нужд горячего водоснабжения, руб.
        'total_debt_hot_water_supply',                          //— Общая задолженность по горячей воде, руб.
        'total_debt_cold_water',                                //— Общая задолженность по холодной воде, руб.
        'total_debt_sanitation',                                //— Общая задолженность по водоотведению, руб.
        'total_debt_gas',                                       //— Общая задолженность по поставке газа, руб.
        'total_debt_electricity',                               //— Общая задолженность по электрической энергии, руб.
        'total_debt_for_other_resources',                       //— Общая задолженность по прочим ресурсам (услугам), руб.

        'income_service',                                       // Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб.
        'income_service_houses_less_25',                        // - по домам до 25 лет
        'income_service_houses_26_50',                          // - по домам от 26 до 50 лет
        'income_service_houses_51_75',                          // - по домам от 51 до 75 лет
        'income_service_houses_more_76',                        // - по домам 76 лет и более
        'income_service_houses_alarm',                          // - по аварийным домам
        'income_common_property',                               // Сумма доходов, полученных от использования общего имущества за отчетный период, тыс.руб.
        'income_common_property_less_25',                       // - по домам до 25 лет
        'income_common_property_26_50',                         // - по домам от 26 до 50 лет
        'income_common_property_51_75',                         // - по домам от 51 до 75 лет
        'income_common_property_more_76',                       // - по домам 76 лет и более
        'income_common_property_alarm',                         // - по аварийным домам
        'income_utilities',                                     // Доход, полученный за отчетный период от предоставления коммунальных услуг без учета коммунальных ресурсов, поставленных
                                                                // потребителям непосредственно поставщиками по прямым договорам, тыс.руб.
        'income_utilities_heating',                             // - отопление
        'income_utilities_electricity',                         // - электричество
        'income_utilities_gas',                                 // - газ
        'income_utilities_hot_water_supply',                    // - горячее водоснабжение
        'income_utilities_cold_water',                          // - холодное водоснабжение
        'income_utilities_sanitation',                          // - водоотведение
        'costs_service',                                        // Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб.
        'costs_service_less_25',                                // - по домам до 25 лет
        'costs_service_26_50',                                  // - по домам от 26 до 50 лет
        'costs_service_51_75',                                  // - по домам от 51 до 75 лет
        'costs_service_more_76',                                // - по домам 76 лет и более
        'costs_service_alarm',                                  // - по аварийным домам
        'costs_contract_service',                               // Выплаты по искам по договорам управления за отчетный период, тыс.руб.
        'costs_contract_service_damage',                        // - иски по компенсации нанесенного ущерба
        'costs_contract_service_unused_service',                // - иски по снижению платы в связи с неоказанием услуг
        'costs_contract_service_non_delivery_resources',        // - иски по снижению платы в связи с недопоставкой ресурсов
        'costs_resources_organization',                         // Выплаты по искам ресурсоснабжающих организаций за отчетный период, тыс.руб.
        'costs_resources_organization_heating',                 // - отопление
        'costs_resources_organization_electricity',             // - электричество
        'costs_resources_organization_gas',                     // - газ
        'costs_resources_organization_hot_water_supply',        // - горячее водоснабжение
        'costs_resources_organization_cold_water',              // - холодное водоснабжение
        'costs_resources_organization_sanitation',              // - водоотведение
        'net_assets'
        );

    private static $manager_arrears = array(
        'past_account_owner_service_current_date',                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб.
        'past_account_owner_service_current_date_less_25',            // - по домам до 25 лет
        'past_account_owner_service_current_date_26_50',              // - по домам от 26 до 50 лет
        'past_account_owner_service_current_date_51_75',              // - по домам от 51 до 75 лет
        'past_account_owner_service_current_date_more_76',            // - по домам 76 лет и более
        'past_account_owner_service_current_date_alarm',              // - по аварийным домам
        'past_account_owner_service_start_period',                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению на начало отчетного периода, тыс.руб.
        'past_account_owner_utilities_current_date',                  // Просроченная задолженность собственников помещений и иных лиц, .... , за коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
        'past_account_owner_utilities_current_date_heating',          // - отопление
        'past_account_owner_utilities_current_date_electricity',      // - электричество
        'past_account_owner_utilities_current_date_gas',              // - газ
        'past_account_owner_utilities_current_date_hot_water_supply', // - горячее водоснабжение
        'past_account_owner_utilities_current_date_cold_water',       // - холодное водоснабжение
        'past_account_owner_utilities_current_date_sanitation',       // - водоотведение
        'past_account_owner_utilities_start_period',                  // Просроченная задолженность собственников помещений и иных лиц, .... за коммунальные услуги на начало отчетного периода, тыс.руб.
        'past_account_organ_utilities_current_date',                  // Просроченная задолженность организации за предоставленные коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
        'past_account_organ_utilities_current_date_heating',          // - отопление
        'past_account_organ_utilities_current_date_electricity',      // - электричество
        'past_account_organ_utilities_current_date_gas',              // - газ
        'past_account_organ_utilities_current_date_hot_water_supply', // - горячее водоснабжение
        'past_account_organ_utilities_current_date_cold_water',       // - холодное водоснабжение
        'past_account_organ_utilities_current_date_sanitation',       // - водоотведение
        'amount_collected_debt_service',                              //  Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
                                                                      //  проживающих в помещениях на законных основаниях за услуги по управлению, тыс.руб.
        'amount_collected_debt_service_less_25',                      // - по домам до 25 лет
        'amount_collected_debt_service_26_50',                        // - по домам от 26 до 50 лет
        'amount_collected_debt_service_51_75',                        // - по домам от 51 до 75 лет
        'amount_collected_debt_service_more_76',                      // - по домам 76 лет и более
        'amount_collected_debt_service_alarm',                        // - по аварийным домам
        'amount_collected_debt_utilities',                            // Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
                                                                      // проживающих в помещениях на законных основаниях за предоставленные коммунальные услуги, тыс.руб.
        'amount_collected_debt_utilities_heating',                    // - отопление
        'amount_collected_debt_utilities_electricity',                // - электричество
        'amount_collected_debt_utilities_gas',                        // - газ
        'amount_collected_debt_utilities_hot_water_supply',           // - горячее водоснабжение
        'amount_collected_debt_utilities_cold_water',                 // - холодное водоснабжение
        'amount_collected_debt_utilities_sanitation'                  // - водоотведение
        );

    private static $manager_service_mkd = array(
        'scope_repair',                            // Объем работ по ремонту за отчетный период, тыс.руб.
        'scope_repair_less_25',                    // - по домам до 25 лет
        'scope_repair_26_50',                      // - по домам от 26 до 50 лет
        'scope_repair_51_75',                      // - по домам от 51 до 75 лет
        'scope_repair_more_76',                    // - по домам 76 лет и более
        'scope_repair_date_alarm',                 // - по аварийным домам
        'scope_improvement',                       // Объем работ по ремонту за отчетный период, тыс.руб.
        'scope_improvement_less_25',               // - по домам до 25 лет
        'scope_improvement_26_50',                 // - по домам от 26 до 50 лет
        'scope_improvement_51_75',                 // - по домам от 51 до 75 лет
        'scope_improvement_more_76',               // - по домам 76 лет и более
        'scope_improvement_date_alarm',            // - по аварийным домам
        'volume_attracted_funds',                  // Объем привлеченных средств за отчетный период, тыс.руб.
        'volume_attracted_funds_subsidies',        // - Субсидии
        'volume_attracted_funds_credits',          // - Кредиты
        'volume_attracted_funds_financing_leasing',// - Финансирование по договорам лизинга
        'volume_attracted_funds_financing_energy', // - Финансирование по энергосервисным договорам
        'volume_attracted_funds_earmarked',        // - Целевые взносы жителей
        'volume_attracted_funds_other',            // - Другие источники
        'paid_ru_indications',                     // Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.
        'paid_ru_indications_heating',             // - отопление
        'paid_ru_indications_electricity',         // - электричество
        'paid_ru_indications_gas',                 // - газ
        'paid_ru_indications_hot_water_supply',    // - горячее водоснабжение
        'paid_ru_indications_cold_water',          // - холодное водоснабжение
        'paid_ru_accounts',                        //  Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.
        'paid_ru_accounts_heating',                // - отопление
        'paid_ru_accounts_electricity',            // - электричество
        'paid_ru_accounts_gas',                   // - газ
        'paid_ru_accounts_hot_water_supply',       // - горячее водоснабжение
        'paid_ru_accounts_cold_water'             // - холодное водоснабжение
        );
    private static $manager_license = array(
        "number_license",                         // Номер лицензии
        "date_receipt_license",                   // Дата получения лицензии
        "issuing_authority",                      // Орган, выдавший лицензию
        "documents_license",                      // Документ лицензии
    );

    public static function get_manager_service_mkd_list() {
        return self::$manager_service_mkd;
    }

     public static function get_manager_arrears_list() {
        return self::$manager_arrears;
    }


    public static function get_manager_financial_highlights_list() {
        return self::$manager_financial_highlights;
    }


    public static function get_manager_housing_list() {
        return self::$manager_housing;
    }

    public static function get_manager_common_info_list() {
        return self::$manager_common_info;
    }
    public static function get_manager_license() {
        return self::$manager_license;
    }
}