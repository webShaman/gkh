<?php namespace App\Classes;

class Pages {
    // основное меню
    const MAIN          = 'main';                   // главная
    const ASSOCIATION   = 'association';            // ассоциация
    const MY_HOUSE      = 'myhouse';                // мой дом
        const AREA_HOME     = 'areaHome';           // мой дом -> районы
            const VIEW_HOME     = 'viewHome';       // мой дом -> районы -> дом
    const MY_MANAGER    = 'mymanager';              // мой управляющий
        const AREA_MANAGER  = 'areaManager';        // мой управляющий -> районы
            const VIEW_MANAGER  = 'viewManager';    // мой управляющий -> районы -> менеджер
    const OVERHAUL      = 'overhaul';               // капитальный ремонт
    const TERRITORY     = 'territory';              // территория жизни
    const NEWS          = 'news';                   // новости
    const PARTNERS      = 'partners';               // партнеры
    const ABOUT         = 'about';               	// о нас
    const STANDARD      = 'standard';				// стандарт раскрытия
    const HELP     	    = 'help';					// стандарт раскрытия
    const SUPPORT     	= 'support';				// техническая поддержка
    const CINEMA     	= 'cinema';					// техническая поддержка
    const EXEMPLARY    	= 'exemplary';				// дома образцового содержания
    const SITERULES    	= 'siterules';				// правила пользования сайтом
    const POLICY    	= 'policy';				    // политика конфиденциальности
    //-----------------

    //друге страници
    const AREA          = 'area';

    // поиск
    const SEARCH        = 'search';
        const MANAGER       = 'manager';
        const HOUSE         = 'house';

    // авторизация
    const AUTH              = 'auth';            // авторизация
    const AUTH_REGISTER     = 'auth/register';   // регистрация
    const AUTH_LOGIN        = 'auth/login';      // авторизация пользователя
    const CHANGE            = 'change';          // авторизация пользователя
    const CHANGE_PERSONAL_DATA  = 'change/personal_data';   // авторизация пользователя
    const CHANGE_PASSWORD   = 'change/password';      // авторизация пользователя
    const PASSWORD          = 'password';        // пароль
    const PASSWORD_EMAIL    = 'password/email';  // сбросить пароль
    const PASSWORD_RESET    = 'password/reset';  // сбросить пароль
    const HOME = 'home';

    const PERSONAL          = 'personal';
        const PERSONAL_HOUSE    = 'personalHouse';
            const PERSONAL_HOUSE_ADD = 'personalHouseAdd';
        const PERSONAL_MANAGER    = 'personalManager';
            const PERSONAL_MANAGER_ADD = 'personalManagerAdd';
//    const PERSONAL          = 'personal';
//    const PERSONAL          = 'personal';
//    const PERSONAL          = 'personal';
//

    /**
     *  Массив с параметрами страниц
     *  route       - переход
     *  name_ru     - отображается в меню
     *  active      - по умолчанию false становится true при переходе по этой менюшке
     *  position    - позиция в меню, если не учавствует в меню ставится false
     *  link        - родительская кнопка (в принципе уже не нужно, буду выпиливать)
     */
    private $pages = array(
        self::MAIN => array(
            'route' => '',
            'name_ru' => 'Главная',
            'title' => 'ЖКХ',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 1,
            'parent' => null,
        ),
        self::ASSOCIATION => array(
            'route' => self::ASSOCIATION,
            'name_ru' => 'Ассоциация ЖКХ',
            'title' => 'Ассоциация ЖКХ',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 2,
            'parent' => self::MAIN,
        ),
        self::OVERHAUL => array(
            'route' =>  self::OVERHAUL,
            'name_ru' => 'Капитальный ремонт',
            'title' => 'Капитальный ремонт',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 5,
            'parent' => self::MAIN,
        ),
        self::ABOUT => array(
            'route' => self::ABOUT,
            'name_ru' => 'О проекте',
            'title' => 'О проекте',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::NEWS => array(
            'route' => self::NEWS,
            'name_ru' => 'Новости',
            'title' => 'Новости',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 7,
            'parent' => self::MAIN,
        ),
        self::TERRITORY => array(
            'route' => self::TERRITORY,
            'name_ru' => 'Территория жизни',
            'title' => 'Территория жизни',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 6,
            'parent' => self::MAIN,
        ),
        self::PARTNERS => array(
            'route' => self::PARTNERS,
            'name_ru' => 'Партнеры',
            'title' => 'Партнеры',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 8,
            'parent' => self::MAIN,
        ),
        self::STANDARD => array(
            'route' => self::STANDARD,
            'name_ru' => 'Стандарт раскрытия',
            'title' => 'Стандарт раскрытия',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::HELP => array(
            'route' => self::HELP,
            'name_ru' => 'Справка',
            'title' => 'Справка',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::SITERULES => array(
            'route' => self::SITERULES,
            'name_ru' => 'Правила пользования сайтом',
            'title' => 'Правила пользования сайтом',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::POLICY => array(
            'route' => self::POLICY,
            'name_ru' => 'Политика конфиденциальности',
            'title' => 'Политика конфиденциальности',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::CINEMA => array(
            'route' => self::CINEMA,
            'name_ru' => 'Кинозал ЖКХ',
            'title' => 'Кинозал ЖКХ',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::EXEMPLARY => array(
            'route' => self::EXEMPLARY,
            'name_ru' => 'Дома образцового содержания',
            'title' => 'Дома образцового содержания',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::SUPPORT => array(
            'route' => self::SUPPORT,
            'name_ru' => 'Техническая поддержка',
            'title' => 'Техническая поддержка',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::MY_HOUSE => array(
            'route' => self::MY_HOUSE,
            'name_ru' => 'Мой дом',
            'title' => 'Мой дом',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 3,
            'parent' => self::MAIN,
        ),
        self::MY_MANAGER => array(
            'route' => self::MY_MANAGER,
            'name_ru' => 'Мой управляющий',
            'title' => 'Мой управляющий',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => 4,
            'parent' => self::MAIN,
        ),
        self::AREA_MANAGER => array(
            'route' => self::AREA,
            'name_ru' => 'Район',
            'title' => 'Район',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => self::MY_MANAGER,
            'active' => false,
            'position' => null,
            'resolution' => array(
                'avtozavodsky', 'kanavinsky', 'leninsky', 'moscowsky',
                'nizhegosky', 'prioksky', 'sovietsky', 'sormovsky'
            ),
            'parent' => self::MY_MANAGER,
        ),
        self::VIEW_MANAGER => array(
            'route' => '',
            'name_ru' => 'Компания',
            'title' => 'Компания',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => self::AREA_MANAGER,
            'active' => false,
            'position' => null,
            'parent' => self::AREA_MANAGER,
        ),
        self::AUTH => array(
            'route' => self::AUTH,
            'name_ru' => 'Авторизация',
            'title' => 'Авторизация',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::AUTH_REGISTER => array(
            'route' => self::AUTH_REGISTER,
            'name_ru' => 'Регистрация',
            'title' => 'Регистрация',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::AUTH_LOGIN => array(
            'route' => self::AUTH_LOGIN,
            'name_ru' => 'Авторизация пользователя',
            'title' => 'Авторизация пользователя',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::CHANGE => array(
            'route' => self::CHANGE,
            'name_ru' => 'Изменения',
            'title' => 'Изменения',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::CHANGE_PERSONAL_DATA => array(
            'route' => self::CHANGE_PERSONAL_DATA,
            'name_ru' => 'Изменить персональные данные',
            'title' => 'Изменить персональные данные',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::CHANGE_PASSWORD => array(
            'route' => self::CHANGE_PASSWORD,
            'name_ru' => 'Изменить пароль',
            'title' => 'Изменить пароль',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::PASSWORD_EMAIL => array(
            'route' => self::PASSWORD_EMAIL,
            'name_ru' => 'Мой управляющий',
            'title' => 'Мой управляющий',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::PASSWORD_RESET => array(
            'route' => self::PASSWORD_RESET,
            'name_ru' => 'Сбросить пароль',
            'title' => 'Сбросить пароль',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::HOUSE => array(
            'route' => '',
            'name_ru' => 'Результат поиска',
            'title' => 'Результат поиска',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => self::SEARCH,
            'active' => false,
            'position' => null,
        ),
        self::MANAGER => array(
            'route' => '',
            'name_ru' => 'Результат поиска',
            'title' => 'Результат поиска',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => self::SEARCH,
            'active' => false,
            'position' => null,
        ),
        self::AREA_HOME => array(
            'route' => self::AREA,
            'name_ru' => 'Район',
            'title' => 'Район',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => self::MY_HOUSE,
            'active' => false,
            'position' => null,
            'resolution' => array(
                'avtozavodsky', 'kanavinsky', 'leninsky', 'moscowsky',
                'nizhegosky', 'prioksky', 'sovietsky', 'sormovsky'
            ),
            'parent' => self::MY_HOUSE,
        ),
        self::VIEW_HOME => array(
            'route' => '',
            'name_ru' => 'Дом',
            'title' => 'Дом',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => self::AREA_HOME,
            'active' => false,
            'position' => null,
            'parent' => self::AREA_HOME,
        ),
        self::HOME => array(
            'route' => '',
            'name_ru' => 'Авторизация',
            'title' => 'Авторизация',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
        ),
        self::PERSONAL => array(
            'route' => self::PERSONAL,
            'name_ru' => 'Личный кабинет',
            'title' => 'Личный кабинет',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::MAIN,
        ),
        self::PERSONAL_HOUSE => array(
            'route' => self::MY_HOUSE,
            'name_ru' => 'Дома',
            'title' => 'Дома',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::PERSONAL,
        ),
        self::PERSONAL_HOUSE_ADD => array(
            'route' => self::PERSONAL_HOUSE,
            'name_ru' => 'Добавление и редактированние домов',
            'title' => 'Добавление и редактированние домов',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::PERSONAL_HOUSE,
        ),
        self::PERSONAL_MANAGER => array(
            'route' => self::MY_MANAGER,
            'name_ru' => 'Организации',
            'title' => 'Организации',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::PERSONAL,
        ),
        self::PERSONAL_MANAGER_ADD => array(
            'route' => self::PERSONAL_MANAGER_ADD,
            'name_ru' => 'Добавление и редактированние организаций',
            'title' => 'Добавление и редактированние организаций',
            'heading' => '',
            'description' => '',
            'keywords' => '',
            'robots' => '',
            'link' => null,
            'active' => false,
            'position' => null,
            'parent' => self::PERSONAL_MANAGER,
        ),

    );

    /**
     * @param $name
     * @param $link
     * @param $area
     * @return array
     */
    public function getPageOnLink($name, $link, $area = null) {
        if (array_key_exists($name, $this->pages)
            && $this->pages[$name]['link'] == $link
        ) {
            $result = array_merge($this->pages[$name], $this->getBreadcrumb($name, $area));
            return $result;
        } else  {
            abort(404);
        }
    }

    /**
     * @param $name
     * @return array
     */
    protected function getBreadcrumb($name, $area) {
        $page = $this->pages[$name];
        if (empty($page['parent'])
            || $page['parent'] == null
        ) {
            $result = null;
        } else {
            $result = array();
            $this->getParent($name, $result);
            $result = array_reverse($result);
            $route = '';
            foreach ($result as $key => &$page) {
                if ($page['route'] == self::AREA) $page['route'] = $area;

                if ($key < 2) {
                    $route = $page['route'];
                } else {
                    $route = $route . '/' . $page['route'];
                }

                $page['route'] = $route;
            }
        }
       // dd($result);
        return array('breadcrumb' => $result);
    }

    /**
     * @param $name
     * @param $result
     */
    protected function getParent($name, &$result) {
        $page = $this->pages[$name];
        $result[] = ['route' => $page['route'], 'name_ru' => $page['name_ru'], 'count' => count($result)];
        if ($page['parent'] != null) {
            $this->getParent($page['parent'], @$result);
        }
    }

    /**
     * @param $name
     * @param $resolution
     * @return bool
     */
    public function isPageResolution($name, $resolution) {
        if (isset($this->pages[$name]['resolution'])
            && in_array($resolution, $this->pages[$name]['resolution'])
        ) {
            return  true;
        } else  {
            abort(404);
        }
    }

    /**
     * собираем менюшку
     *
     * @param $pageName
     * @return mixed
     */
    public function getMenu($pageName) {
        foreach ($this->pages as $name => $value) {
            if ($value['position'] != 0){
                $pages[$name] = $value['position'];
            }
        }
        asort($pages);
        foreach ($pages as $key => $value) {
            $menu[$key] = $this->pages[$key];
        }

        if (array_key_exists($pageName, $menu)) {
            $menu[$pageName]['active'] = true;
        }

        return $menu;
    }

    /**
     * @param $page
     * @param $link
     * @param null $area
     * @return array
     */
    public static function init($page, $link, $area = null) {
        $pages = new Pages;
        $pageAttribute = $pages->getPageOnLink($page, $link, $area);
        return array_merge($pageAttribute, array('menu' => $pages->getMenu($page)));
    }
}