<?php namespace App\Libs;

use  Illuminate\Pagination\BootstrapThreePresenter;


/**
 * Class PaginationUk
 * @package App\Libs
 *
 */
class PaginationUk extends  BootstrapThreePresenter {

    static $paginate = 20;

    public function render() {
        if ($this->hasPages())
        {
            return sprintf(
                '<ul class="uk-pagination">%s %s %s</ul>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            );
        }

        return '';
    }
}