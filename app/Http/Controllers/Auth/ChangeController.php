<?php namespace App\Http\Controllers\Auth;

use App\User;
use App\Classes\Pages;
use App\Services\Registrar;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class ChangeController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPersonal() {
        $page = Pages::CHANGE_PERSONAL_DATA;
        $model = "auth.personal_data";
        $params = Pages::init($page, null);
        $params = array_merge($params,array("page" => $page));
        if(Auth::check()){
            $user=Auth::user();
            $params = array_merge($params, ['email' => $user->email, 'name' => $user->name]);
        }

        return view($model, $params);
    }

    public function getPassword() {
        $page = Pages::CHANGE_PASSWORD;
        $model = "auth.change_password";
        $params = Pages::init($page, null);
        $params = array_merge($params,array("page" => $page));

        return view($model, $params);
    }

    protected function getFailedLoginMessage()
    {
        return 'Ошибка ввода персональных данных.';
    }

    public function postPersonal(Request $request) {
        $page = Pages::CHANGE_PERSONAL_DATA;
        $model = "auth.personal_data";
        $params = Pages::init($page, null);
        $params = array_merge($params,array("page" => $page));
        $user=Auth::user();
        $email=$request->only('email');
        if($user->email != $email['email']){
            $this->validate($request, ['_token' => 'required','name' => 'required|max:255', 'email' => 'required|email|max:255|unique:users', 'password' => 'required|min:6']);
        }else{
            $this->validate($request, ['_token' => 'required','name' => 'required|max:255', 'password' => 'required|min:6']);
            $email=null;
        }
        $credentials = $request->only('password');
        $credentials = array_merge($credentials, ['email' => $user->email]);
        if (Auth::validate($credentials)) {
            if($email!=null){
                User::edit($user->id,$request->only('name','email'));
            }else{
                User::edit($user->id,$request->only('name'));
            }
            $params = array_merge($params,$request->only('name','email'));
            return view($model, $params);
        }

        $params = array_merge($params, $request->only('name', 'email'));
        return view($model, $params)->withErrors(array('email' => $this->getFailedLoginMessage()));
    }

    public function postPassword(Request $request) {
        $page = Pages::CHANGE_PASSWORD;
        $model = "auth.change_password";
        $params = Pages::init($page, null);
        $params = array_merge($params,array("page" => $page));
        $this->validate($request, ['_token' => 'required', 'old_password' => 'required|min:6', 'password' => 'required|same:password_confirmation|min:6', 'password_confirmation' => 'required|same:password|min:6']);
        $user=Auth::user();
        $old_pass = $request->only('old_password');
        $credentials=['password'=>$old_pass['old_password']];
        $credentials = array_merge($credentials, ['email' => $user->email]);
        if (Auth::validate($credentials)) {
            $pass=$request->only('password');
            User::edit($user->id,[ 'password' => bcrypt($pass['password'])]);
            return view($model, $params);
        }

        return view($model, $params);
    }

}