<?php namespace App\Http\Controllers\Auth;

use App\Classes\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
	}

    public function getEmail() {
        $page = Pages::PASSWORD_EMAIL;
        $model = "auth.password";
        $params = Pages::init($page, null);
        return view($model, $params);
    }

    public function getReset($token = null)
    {
        $page = Pages::PASSWORD_RESET;
        $model = "auth.reset";
        $params = Pages::init($page, null);

        if (is_null($token)) {
            throw new NotFoundHttpException();
        }
        $params=array_merge($params,array('token' => $token));
        return view($model,$params);
    }

    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }
        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/personal';
    }

}
