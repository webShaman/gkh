<?php namespace App\Http\Controllers\Gkh;

use App\Classes\ManagerData;
use App\Http\Controllers\Controller;
use App\Classes\Pages;
use App\Services\ManagerService;
use App\Models\Manager\ManagerCommonInformation;
use App\Models\Manager\ManagerFinancialHighlights;
use App\Models\Manager\ManagerLicense;
use App\Models\Home;
use App\Services\HomeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Forms\House\Change\ChangeForm;

class PersonalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function myHouse() {
        $page = Pages::PERSONAL_HOUSE;
        $homeService = new HomeService;
        $params = Pages::init($page, null);
        $model = "layout.body.personal.{$params['route']}";

        $grid = $homeService->getGrids();
        $params = array_merge($params, array(
            'grid' => isset($grid['grids']) ? $grid['grids'] : '',
            'render' => isset($grid['render']) ? $grid['render'] : '',
            'numberHomes' => $homeService->getNumberHomes(),
            'numberResidentialArea' => $homeService->getNumberTotalArea(),
            'numberResidents' => $homeService->getNumberResident(),
        ));

        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function personal() {
        $page = Pages::PERSONAL;
        $params = Pages::init($page, null);
        $model = "layout.body.personal";

        return view($model, $params);
    }

    /**
     * Добавление и редактированние дома
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\View\View
     */
    public function addHouse(Request $request, $id = null) {
        $page = Pages::PERSONAL_HOUSE_ADD;
        $params = Pages::init($page, null);
        $data = $request->all();
        $homeService = new HomeService;

        $addHouseForm = new ChangeForm();

      ///  dd($homeService->getModelFillable());
        $params = array_merge($params, array('home' => $homeService->getModelFillable()));
        // если пришел id то редактируем
        if ($id) {
            $params = array_merge($params, array('home' => $homeService->getHomeById2($id, $params['home'])));
        }

        // сохранение формы
        if ($request->get('_token')) {
            if ($request->get('edit')) {
                $homeService->editHome($data, $request->get('edit'));
                $massage = "Запись обновленна.";
            } else {
                $homeService->addHome($data);
                $massage = "Запись добавленна.";
            }
            $params = array_merge($params, array('massage' => $massage));
        }

        $model = "layout.body.personal.add.house";
        return view($model, $params);
    }

    public function my_manager() {

        $page = Pages::PERSONAL_MANAGER;
        $params = Pages::init($page, null);
        $managers_id=ManagerCommonInformation::getAllId();
        if ($managers_id instanceof \Exception || !(isset($managers_id) && count($managers_id)))
        {
            //return 'error ManagerCommonInformation::getAllId(): '.$managers_id;
            $managers_id=NULL;
        }else{

            $managers_tmp=array();
            foreach($managers_id as $manager_id){
                array_push($managers_tmp,$manager_id->id);
            }

            $managers_id=array_unique($managers_tmp);
            $number_managers_id=count($managers_id);
            $managers=ManagerService::get_Short_Info_For_Managers($managers_id);
            $params=array_merge($params,array('number_managers' => $number_managers_id));
            if ($managers instanceof \Exception || !(isset($managers) && count($managers)))
            {
                //return 'error ManagerService::get_Short_Info_For_Managers($managers_id): '.$managers;
                $managers=NULL;
            }else{
                ManagerService::get_All_Info_For_All_Manager($params,$managers);
            }


            $params=array_merge($params, array('managers' => $managers));
        }

        $model = "layout.body.personal.mymanager";

        return view($model, $params);
    }

    public function add_or_edit_manager($manager_id=null,$oldValue=null,$resultSaveData=null) {
        $page = Pages::PERSONAL_MANAGER_ADD;
        $params = Pages::init($page, null);
        if($manager_id == NULL && $oldValue==null){
             $params = array_merge($params, array('add_or_edit'=>'add'));
             $params=ManagerService::get_NULL_Info_For_One_Manager($params);
        }else if($oldValue==null){
            $params = array_merge($params, array('add_or_edit'=>'edit'));
            $params=ManagerService::get_All_Info_For_One_Manager($params,$manager_id);
            $tables_one=ManagerService::getAll_manager_license($manager_id);
            if($tables_one != Null){
                $params=array_merge($params,array('tables_one'=>$tables_one));
            }
        }else if($oldValue!=null){
            $tables_one=ManagerService::getAll_manager_license($manager_id);
            if($tables_one != Null){
                $params=array_merge($params,array('tables_one'=>$tables_one));
            }
            $params = array_merge($params, array('add_or_edit'=>'old'));
            $params = array_merge($params, $oldValue);
        }

        if($resultSaveData!=null){
            $params = array_merge($params,array('resultSaveData'=>$resultSaveData));
        }

        $model = "layout.body.personal.add_or_edit.manager";

        return view($model, $params);
    }

    public function add_or_edit_manager_post($manager_id=null){
        $oldValue=array();
        $data_license=array();
        $data_license_1=array();
        $resultSaveData='true';
        $data_manager_common_info=Input::only(ManagerData::get_manager_common_info_list());
        //$data_manager_housing=Input::only(ManagerData::get_manager_housing_list());
        $data_manager_financial_highlights=Input::only(ManagerData::get_manager_financial_highlights_list());
        //$data_manager_arrears=Input::only(ManagerData::get_manager_arrears_list());
        //$data_manager_service_mkd=Input::only(ManagerData::get_manager_service_mkd_list());
        //$data_license=Input::only(ManagerData::get_manager_license());
        $data_licenses=Input::get();
        $result=0;
        $delete=0;
         foreach (array_keys($data_licenses) as $data){
            foreach (ManagerData::get_manager_license() as $name){
                if (strncasecmp($name,$data,strlen($name))==0){
                    $data_license_1=array_merge($data_license_1,[$name=>$data_licenses[$data]]);
                    if($data_licenses[$data] != ""){
                        $delete=1;
                    }
                    $result=$result+1;
                }
            }
            if($result == 4){
                if($delete==1){
                    array_push($data_license,$data_license_1);
                }
                $result=0;
                $delete=0;
            }
        }


        $result=ManagerService::Validator_Managers($data_manager_common_info,$data_manager_financial_highlights);//,$data_manager_housing,$data_manager_arrears,$data_manager_service_mkd);

        if($manager_id == NULL && $result=="true"){
            $oldValue=NULL;
            $data_manager_common_info=array_merge(array('area_id'=>'1'),$data_manager_common_info);
            $result=ManagerCommonInformation::add($data_manager_common_info);
            if ($result instanceof \Exception)
            {
                return 'error ManagerCommonInformation::add($data_manager_common_info)'.$result;
            }

            $manager_id=$result->id;
            //$data_manager_arrears=array_merge(array('manager_id'=>$manager_id),$data_manager_arrears);
            //$result=ManagerArrears::add($data_manager_arrears);
            //if ($result instanceof \Exception)
            //{
            //    return 'error ManagerArrears::add($data_manager_arrears)'.$result;
            //}

            $data_manager_financial_highlights=array_merge(array('manager_id'=>$manager_id),$data_manager_financial_highlights);
            $result=ManagerFinancialHighlights::add($data_manager_financial_highlights);
            if ($result instanceof \Exception)
            {
                return 'error ManagerFinancialHighlights::add($data_manager_financial_highlights)'.$result;
            }

            ManagerService::create_manager_license($manager_id);

            //$data_manager_housing=array_merge(array('manager_id'=>$manager_id),$data_manager_housing);
            //$result=ManagerHousing::add($data_manager_housing);
            //if ($result instanceof \Exception)
            //{
            //    return 'error ManagerHousing::add($data__manager_housing)'.$result;
            //}

            //$data_manager_service_mkd=array_merge(array('manager_id'=>$manager_id),$data_manager_service_mkd);
            //$result=ManagerServiceMKD::add($data_manager_service_mkd);
            //if ($result instanceof \Exception)
            //{
            //    return 'error ManagerServiceMKD::add($data_manager_service_mkd)'.$result;
            //}
        }else if($result=="true"){
            $oldValue=NULL;
            $result=ManagerCommonInformation::edit($manager_id,$data_manager_common_info);
            if ($result instanceof \Exception)
            {
                return 'error ManagerCommonInformation::edit($data_manager_common_info)'.$result;
            }

            //$result=ManagerArrears::edit($manager_id,$data_manager_arrears);
            //if ($result instanceof \Exception)
            //{
            //    return 'error ManagerArrears::edit($data_manager_arrears)'.$result;
            //}

            $result=ManagerFinancialHighlights::edit($manager_id,$data_manager_financial_highlights);
            if ($result instanceof \Exception)
            {
                return 'error ManagerFinancialHighlights::edit($data_manager_financial_highlights)'.$result;
            }

            ManagerService::edit_manager_license($manager_id,$data_license);

            //$result=ManagerHousing::edit($manager_id,$data_manager_housing);
            //if ($result instanceof \Exception)
            //{
            //    return 'error ManagerHousing::edit($data__manager_housing)'.$result;
            //}

            //$result=ManagerServiceMKD::edit($manager_id,$data_manager_service_mkd);
            //if ($result instanceof \Exception)
            //{
            //    return 'error ManagerServiceMKD::edit($data_manager_service_mkd)'.$result;
            //}
        }else{
            $resultSaveData='false';
            $object= new ManagerCommonInformation($data_manager_common_info);
            $oldValue=array_merge($oldValue, array('manager_common_info' => $object));
            //$object= new ManagerServiceMKD($data_manager_service_mkd);
            //$oldValue=array_merge($oldValue, array('manager_service_mkd' => $object));
            //$object= new ManagerArrears($data_manager_arrears);
            //$oldValue=array_merge($oldValue, array('manager_arrears' => $object));
            $object= new ManagerFinancialHighlights($data_manager_financial_highlights);
            $oldValue=array_merge($oldValue, array('manager_financial_highlights' => $object));
            //$object= new ManagerHousing($data_manager_housing);
            //$oldValue=array_merge($oldValue, array('manager_housing' => $object));
        }

        return self::add_or_edit_manager($manager_id,$oldValue,$resultSaveData);
    }

}
