<?php namespace App\Http\Controllers\Gkh;

use App\Http\Controllers\Controller;
use App\Classes\Pages;
use App\Services\AreaService;
use App\Services\ManagerService;
use App\Models\Area;
use App\Models\Home;
use App\Services\HomeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Models\Home\Passport\GeneralInformation;
use App\Models\Manager\ManagerCommonInformation;
use App\Forms\House\View\ViewForm as houseViewForm;


class IndexController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function main()
    {
        $page = Pages::MAIN;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);
        return view($model, $params);
    }
    /**
     * @return \Illuminate\View\View
     */
    public function association()
    {
        $page = Pages::ASSOCIATION;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    /**
     * @return \Illuminate\View\View
     */
    public function about()
    {
        $page = Pages::ABOUT;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }


    /**
     * @return \Illuminate\View\View
     */
    public function myhouse()
    {
        $page = Pages::MY_HOUSE;

        $model = 'layout.body.' . $page;
        $params = Pages::init($page, null);

        $homeService = new HomeService;
        $params = array_merge($params, array(
            'numberHomes' => $homeService->getNumberHomes(),
            'numberResidentialArea' => $homeService->getNumberTotalArea(),
            'numberResidents' => $homeService->getNumberResident(),
        ));
        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function mymanager()
    {
        $page = Pages::MY_MANAGER;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        $managers_id_base=ManagerCommonInformation::getAllId()->toArray();
        if(count($managers_id_base)){
            $managers_id=array();

            foreach($managers_id_base as $managers_id_tmp){
                array_push($managers_id,$managers_id_tmp["id"]);
            }
            $managers=ManagerService::get_Short_Info_For_Managers($managers_id);
            $params=ManagerService::get_All_Info_For_All_Manager($params,$managers);
        }

        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function area()
    {
        $page = Pages::AREA;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function overhaul()
    {
        $page = Pages::OVERHAUL;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function news()
    {
        $page = Pages::NEWS;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);
        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function standard()
    {
        $page = Pages::STANDARD;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);
        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function partners()
    {
        $page = Pages::PARTNERS;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function territory()
    {
        $page = Pages::TERRITORY;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function help()
    {
        $page = Pages::HELP;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function exemplary()
    {
        $page = Pages::EXEMPLARY;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function siterules()
    {
        $page = Pages::SITERULES;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function policy()
    {
        $page = Pages::POLICY;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function cinema()
    {
        $page = Pages::CINEMA;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function support()
    {
        $page = Pages::SUPPORT;
        $model = 'layout.body.' . $page;

        $params = Pages::init($page, null);

        return view($model, $params);
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function search(Request $request)
    {
        $page="house";
        $date = $request->all();
        $model = 'layout.body.search';
        $params = array_merge(Pages::init($page, Pages::SEARCH), array('page' => $page, 'homes' => null));
        $homeService = new HomeService;

        if (isset($date['_token'])) {
            Session::put('searchHome', $date['search']);
            $search = $homeService->getHomeByStreet($date['search']);
            $params = array_merge($params, array(
                'homes'     => isset($search['homes']) ? $search['homes'] : null,
                'render'    => isset($search['render']) ? $search['render'] : null,
            ));
        }
        if (Session::has('searchHome')) {
            $search = Session::get('searchHome');
            $search = $homeService->getHomeByStreet($search);
            $params = array_merge($params, array(
                'homes'     => isset($search['homes']) ? $search['homes'] : null,
                'render'    => isset($search['render']) ? $search['render'] : null,
            ));
        }
        return view($model, $params);
    }

    public function get_searchManager()
    {
        $model = 'layout.body.search';
        $params = array_merge(Pages::init(Pages::MANAGER, Pages::SEARCH), array('page' => Pages::MANAGER, 'homes' => null));

        return view($model,$params);
    }

    public function post_searchManager()
    {
        $model = 'layout.body.search';
        $params = array_merge(Pages::init(Pages::MANAGER, Pages::SEARCH), array('page' => Pages::MANAGER, 'homes' => null));
        $managers_id=array();
        $managers_id_inn=array();
        $managers_id_short=array();
        $managers_id_full=array();

        $input_text=Input::only(['search','_token']);

        if (is_null($input_text['_token']) || is_null($input_text['search'])) {
            return view($model,$params);
        }

        $managers_id_inn=ManagerCommonInformation::get_Manager_Id_By_Inn($input_text['search']);
        $managers_id_short=ManagerCommonInformation::get_Manager_Id_By_ShortName($input_text['search']);
        $managers_id_full=ManagerCommonInformation::get_Manager_Id_By_FullName($input_text['search']);

        if(count($managers_id_inn))
        {
            foreach ($managers_id_inn->toArray() as $id_inn){
                array_push($managers_id,$id_inn["id"]);
            }
        }

        if(count($managers_id_short))
        {
            foreach ($managers_id_short->toArray() as $id_short){
                array_push($managers_id,$id_short["id"]);
            }
        }

        if(count($managers_id_full))
        {
            foreach ($managers_id_full->toArray() as $id_full){
                array_push($managers_id,$id_full["id"]);
            }
        }

        $managers_id=array_unique($managers_id);
        $managers=ManagerService::get_Short_Info_For_Managers($managers_id);
        $params=array_merge($params,array('managers' => $managers));

        if(! count($managers_id))
        {
            $params=array_merge($params,array('managers' => null));
        }

        return view($model, $params);
    }

    /**
     * @param $area
     * @param null $homeId
     * @return \Illuminate\View\View
     */
    public function home($area, $homeId = null)
    {
        $params = array();
        $homeService = new HomeService;
        $areaService = new AreaService;

        // переходим к просмотру таблици домов
        if ($homeId == null) {
            $pages = new Pages();
            // убеждаемся что пришла корретная страница
            if ($pages->isPageResolution(Pages::AREA_HOME, $area)) {
                $grid = $homeService->getGrids($area);
                $params = array(
                    'area' => $areaService->getAreaForName($area),
                    'grid' => isset($grid['grids']) ? $grid['grids'] : '',
                    'render' => isset($grid['render']) ? $grid['render'] : '',
                    'numberHomes' => $homeService->getNumberHomes($area),
                    'numberResidentialArea' => $homeService->getNumberTotalArea($area),
                    'numberResidents' => $homeService->getNumberResident($area),
                );
                $model = 'layout.body.myhouse.area';
                $link = Pages::MY_HOUSE;
                $page = Pages::AREA_HOME;
            }
        // переходим к просмотру дома
        } else {
            $managerService = new ManagerService;

            $home = $homeService->getHomeById2($homeId);
            $houseViewForm = new houseViewForm;
            $params = [
                'home'      => $home,
                'forms'     => $houseViewForm->bind($home),
                'manager'   => $managerService->getManagerInfoById($home['homes']['manager_id'])
            ];
            $model = 'layout.body.myhouse.home';
            $link = Pages::AREA_HOME;
            $page = Pages::VIEW_HOME;
        }

        $params = array_merge($params, Pages::init($page, $link, $area));

        return view($model, $params);
    }

    /**
     * @param $area
     * @param null $manager
     * @return \Illuminate\View\View
     */
    public function manager($area, $manager = null)
    {
        $params = array();
        if ($manager == null) {
            $pages = new Pages();
            if ($pages->isPageResolution(Pages::AREA_MANAGER, $area)) {
                $model = 'layout.body.mymanager.area';
                $link = Pages::MY_MANAGER;
                $page = Pages::AREA_MANAGER;
                $params = array('area' => $area);
            }

            $area_id=Area::getIdByName($area);
            $area_ru=Area::getNameRuByName($area)->toArray();
            $area_ru_rod=Area::getNameRuRodByName($area)->toArray();
            $params =array_merge($params, array('area_ru' => $area_ru[0]["name_ru"]));
            $params =array_merge($params, array('area_ru_rod' => $area_ru_rod[0]["name_ru_rod"]));
            if ($area_id instanceof \Exception || !(isset($area_id) && count($area_id)))
            {
                //return 'Error in during execute Area::getIdByName(area): $area_id';
            }else {
                $managers_id=Home::getAllManagersIdForArea($area_id[0]->id);
                if ($managers_id instanceof \Exception || !(isset($managers_id) && count($managers_id)))
                {
                    //return 'Error in during execute Home::getAllManagersIdForArea(area_id[0]->id): '.$managers_id;
                }else{
                    $managers_tmp=array();
                    foreach($managers_id as $manager_id){
                        if($manager_id->manager_id){
                            array_push($managers_tmp,$manager_id->manager_id);
                        }
                    }
                    $managers_id=array_unique($managers_tmp);
                    $number_managers_id=count($managers_id);

                    $managers=ManagerService::get_Short_Info_For_Managers($managers_id);
                    $params=array_merge($params,array('managers' => $managers));
                    $params=array_merge($params,array('number_managers' => $number_managers_id));

                    $params=ManagerService::get_All_Info_For_All_Manager($params,$managers);

                }
            }

        } else {
            $model = 'layout.body.mymanager.manager';
            $link = Pages::AREA_MANAGER;
            $page = Pages::VIEW_MANAGER;

            $manager_id=$manager;
            $params=ManagerService::get_All_Info_For_One_Manager($params,$manager_id);
            $homes=Home::get_All_Home_For_Manager($manager_id);

            $grids = array();
            if ($homes) {
                foreach ($homes as $home) {
                    $passportGeneral = GeneralInformation::where('home_id', $home->id)->firstOrFail();
                    $area_home=Area::getNameById($home->area_id);
                    $area_ru_home=Area::getNameRuById($home->area_id);

                    $grids[] = array(
                        'id' => $home->id,
                        'area' =>$area_home,
                        'area_ru' =>$area_ru_home,
                        'address' => $home->address,
                        'year' => $passportGeneral->year_commissioning,
                        'total_area' => $passportGeneral->total_area,
                        'residents' => $passportGeneral->residents_number,
                    );
                }
            }

            $params=array_merge($params,array('grids' => $grids));
            $tables_one=ManagerService::getAll_manager_license($manager_id);
            if($tables_one != Null){
                $params=array_merge($params,array('tables_one'=>$tables_one));
            }
           /* return array(
                'grids'     => $grids,
                'render'    => with(new PaginationUk($homes))->render(),
            );
*/



        }

        $params = array_merge($params, Pages::init($page, $link, $area));

        return view($model, $params);
    }
}
