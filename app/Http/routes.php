<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//-------------- index --------------------------------------
Route::get('/',             'Gkh\IndexController@main');
Route::get('/association',  'Gkh\IndexController@association');
Route::get('/myhouse',      'Gkh\IndexController@myHouse');
Route::get('/area',         'Gkh\IndexController@area');
Route::get('/mymanager',    'Gkh\IndexController@mymanager');
Route::get('/overhaul',     'Gkh\IndexController@overhaul');
Route::get('/news',         'Gkh\IndexController@news');
Route::get('/partners',     'Gkh\IndexController@partners');
Route::get('/territory',    'Gkh\IndexController@territory');
Route::get('/search',       'Gkh\IndexController@search');
Route::get('/about',        'Gkh\IndexController@about');
Route::get('/standard',     'Gkh\IndexController@standard');
Route::get('/help',     	'Gkh\IndexController@help');
Route::get('/support',     	'Gkh\IndexController@support');
Route::get('/cinema',     	'Gkh\IndexController@cinema');
Route::get('/exemplary',    'Gkh\IndexController@exemplary');
Route::get('/siterules',    'Gkh\IndexController@siterules');
Route::get('/policy',       'Gkh\IndexController@policy');

Route::any('/search/house',    'Gkh\IndexController@search');
Route::get('/search/manager',  'Gkh\IndexController@get_searchManager');
Route::post('/search/manager',  'Gkh\IndexController@post_searchManager');
Route::any('/search/house',     ['as' => 'house',   'uses' => 'Gkh\IndexController@search']);


Route::get('/myhouse/{area}/{house?}', 'Gkh\IndexController@home');
Route::get('/mymanager/{area}/{Manager?}', 'Gkh\IndexController@Manager');

Route::get('home', 'HomeController@index');

//-------------- personal ----------------------------------
Route::get('/personal/myhouse',      'Gkh\PersonalController@myHouse');
Route::get('/personal',      'Gkh\PersonalController@personal');
//Route::any('/personal/house/add',      'Gkh\PersonalController@addHouse');
Route::any('/personal/house/add/{id?}',      'Gkh\PersonalController@addHouse');
Route::get('/personal/mymanager',      'Gkh\PersonalController@my_manager');
Route::get('/personal/mymanager/add_or_edit/{manager_id?}',      'Gkh\PersonalController@add_or_edit_manager');
Route::post('/personal/mymanager/add_or_edit/{manager_id?}',      'Gkh\PersonalController@add_or_edit_manager_post');
//----------------------------------------------------------

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
    'change' => 'Auth\ChangeController',
]);
