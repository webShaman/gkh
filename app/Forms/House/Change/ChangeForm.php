<?php namespace App\Forms\House\Change;

use App\Forms\Form;
use App\Forms\House\Change\HomeForms;
use App\Forms\House\Change\Passport\GeneralInformationForms;
use App\Forms\House\Change\Passport\StructuralElementsForms;
use App\Forms\House\Change\Passport\EngineeringSystemsForms;

class ChangeForm extends Form
{
    /**
     * @param array $element
     */
    protected function addFormSelect(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;

        $this->form[$element['name']] = "

            <div class='uk-form-row'>
                <label class='uk-form-label'>{$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::select($element['name'], $element['select'], $value)}
                </div>
            </div>
        ";
    }

    /**
     * @param array $element
     */
    protected function addFormText(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;
        $danger = isset($element['731']) ? "<span class='uk-text-danger uk-text-small'>\"731\"</span>" : "";
        $sup = isset($element['sup']) ? "<sup>{$element['sup']}</sup>" : '';

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$danger} {$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::text($element['name'], $value, array('class' => 'uk-width-1-1', 'placeholder' => $element['label']))}
                </div>
            </div>
        ";
    }

    protected function addText(array $element) {
        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                {$element['label']}
            </div>
        ";
    }

    /**
     * @param array $element
     */
    protected function addFormTextarea(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;
        $danger = isset($element['731']) ? "<span class='uk-text-danger uk-text-small'>\"731\"</span>" : "";

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$danger} {$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::textarea($element['name'], $value, array('cols' => '30', 'rows' => '3', 'class' => 'uk-width-1-1', 'placeholder' => $element['label']))}
                </div>
            </div>
        ";
    }

    /**
     * @param array $element
     */
    protected function addFormDate(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::input('date', $element['name'], $value, array('class' => 'uk-width-1-1', 'placeholder' => $element['label']))}
                </div>
            </div>
        ";
    }

    /**
     * @return array
     */
    protected function getForm() {
        $result = '';

        foreach ($this->form as $value) {
            $result .= $value;
        }
        return $result;
    }

    /**
     * @param $home
     * @return array+
     */
    public function bind($home) {
        $homeForms = new HomeForms($home);
        $generalInformationForms = new GeneralInformationForms($home);
        $structuralElementsForms = new StructuralElementsForms($home);
        $engineeringSystemsForms = new EngineeringSystemsForms($home);

        return [
            $homeForms->formName => $homeForms->init(),
            $generalInformationForms->formName => $generalInformationForms->init(),
            $structuralElementsForms->formName => $structuralElementsForms->init(),
            $engineeringSystemsForms->formName => $engineeringSystemsForms->init(),
        ];
    }

}