<?php namespace App\Forms\House\Change;

use App\Models\Home;
use App\Forms\House\Change\ChangeForm;

class HomeForms extends ChangeForm
{
    public $formName = Home::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Home::TABLE_NAME]) ? $data[Home::TABLE_NAME] : null;
    }

    public function init() {

        $this->addFormSelect(array(
            'label' => 'Район',
            'name'  => 'area_id',
            'select' => Home::$area_id,
        ));

        $this->addFormText(array(
            'label' => 'Улица и номер дома',
            'name'  => 'address',
        ));

        $this->addFormText(array(
            'label' => 'ИНН управляющей организации',
            'name'  => 'manager_inn',
        ));

        $this->addFormTextarea(array(
            'label' => '<a target="_blank" href="https://tech.yandex.ru/maps/tools/constructor/">Конструктор карт</a>',
            'name'  => 'map',
        ));


        return $this->getForm();
    }
}