<?php namespace App\Forms\House\Change\Passport;

use App\Models\Home;
use App\Forms\House\Change\ChangeForm;
use App\Models\Home\Passport\GeneralInformation;

class GeneralInformationForms extends ChangeForm
{
    public $formName = GeneralInformation::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[GeneralInformation::TABLE_NAME]) ? $data[GeneralInformation::TABLE_NAME] : null;
    }

    public function init() {

        $this->addFormText(array(
            'number' => 1,
            'label'  => 'Год постройки',
            'name'   => 'year_built',
        ));

        $this->addFormDate(array(
            'number' => 2,
            'label'  => 'Год ввода в эксплуатацию',
            'name'   => 'year_commissioning',
        ));

        $this->addFormText(array(
            'number' => 3,
            'label'  => 'Серия, тип проекта',
            'name'   => 'series',
        ));
        $this->addFormText(array(
            'number' => 4,
            'label'  => 'Тип дома',
            'name'   => 'type',
        ));
        $this->addFormText(array(
            'number' => 5,
            'label'  => 'Описание местоположения',
            'name'   => 'description_position',
        ));
        $this->addFormText(array(
            'number' => 6,
            'label'  => 'Индивидуальное наименование дома',
            'name'   => 'name_house',
        ));
        $this->addFormText(array(
            'number' => 7,
            'label'  => 'Способ формирования фонда капитального ремонта',
            'name'   => 'method_overhaul',
        ));
        $this->addText(array(
            'number' => 8,
            'label'  => 'Количество этажей:',
            'class'  => 'uk-text-bold',
            'name'   => '8',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => ' - наибольшее, ед.',
            'name'   => 'floors_number_max',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => ' - наименьшее, ед.',
            'name'   => 'floors_number_min',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => 9,
            'label'  => 'Количество подъездов, ед',
            'name'   => 'entrances_number',
        ));
        $this->addFormText(array(
            'number' => 10,
            'label'  => 'Количество лифтов, ед',
            'name'   => 'lifts_number',
        ));
        $this->addFormText(array(
            'number' => 11,
            'label'  => 'Количество помещений, в том числе:',
            'name'   => 'rooms_number',
            'class'  => 'uk-text-bold',
        ));
        $this->addFormText(array(
            'label'  => ' - жилых, ед.',
            'name'   => 'rooms_number_living',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => ' - нежилых, ед.',
            'name'   => 'rooms_number_no_living',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => 12,
            'label'  => 'Общая площадь дома, в том числе, кв.м:',
            'name'   => 'total_area',
            'class'  => 'uk-text-bold',
        ));
        $this->addFormText(array(
            'label'  => ' - общая площадь жилых помещений, кв.м',
            'name'   => 'total_area_living',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => ' - общая площадь нежилых помещений, кв.м',
            'name'   => 'total_area_no_living',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => ' - общая площадь помещений, входящих в состав общего имущества, кв.м',
            'name'   => 'total_area_common_property',
            'dash'   => true,
        ));
        $this->addText([
            'number' => 13,
            'label'  => 'Общие сведения о земельном участке, на котором расположен многоквартирный дом:',
            'class'  => 'uk-text-bold',
            'name'   => '13',
        ]);
        $this->addFormText(array(
            'label'  => ' - площадь земельного участка, входящего в состав общего имущества в многоквартирном доме, кв.м',
            'name'   => 'parcel_area',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => ' - площадь парковки в границах земельного участка, кв.м',
            'name'   => 'parcel_parking_area',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => 14,
            'label'  => 'кадастровый номер',
            'name'   => 'cadastral_number',
        ));
        $this->addFormText(array(
            'number' => 15,
            'label'  => 'Класс энергетической эффективности',
            'name'   => 'energy_efficiency_class',
        ));
        $this->addText([
            'number' => 14,
            'label'  => 'Общие сведения о земельном участке, на котором расположен многоквартирный дом:',
            'class'  => 'uk-text-bold',
            'name'   => '14',
        ]);
        $this->addFormText(array(
            'label'  => ' - детская площадка',
            'name'   => 'beautification_playground_children',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => ' - спортивная площадка',
            'name'   => 'beautification_playground_sports',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => ' - другое',
            'name'   => 'beautification_other',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => 15,
            'label'  => 'Дополнительная информация',
            'name'   => 'additional_information',
        ));

        return $this->getForm();
    }
}