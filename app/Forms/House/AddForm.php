<?php namespace App\Forms\House;

use Illuminate\Html\FormFacade;
use App\Models\Home;
use App\Forms\Form;

class AddForm extends Form {

    public function init() {

        $this->addFormSelect(array(
            'label' => 'Район',
            'name'  => 'area_id',
            'select' => Home::$area_id,
        ));

        $this->addFormText(array(
            'label' => 'Улица и номер дома',
            'name'  => 'address',
        ));

        $this->addFormText(array(
            'label' => 'Домом управляет',
            'name'  => 'manager_inn',
        ));

        $this->addFormText(array(
            'label' => 'Серия, тип проекта',
            'name'  => 'series',
        ));

        $this->addFormTextarea(array(
            'label' => 'Описание местоположения',
            'name'  => 'description_position',
        ));

        $this->addFormTextarea(array(
            'label' => 'Индивидуальное наименование дома',
            'name'  => 'individual_name',
        ));

        $this->addFormTextarea(array(
            'label' => 'Тип жилого дома',
            'name'  => 'type_home',
        ));

        $this->addFormDate(array(
            'label' => 'Год ввода в эксплуатацию',
            'name'  => 'year_commissioning',
        ));

        $this->addFormTextarea(array(
            'label' => 'Материал стен',
            'name'  => 'wall_material',
        ));

        $this->addFormText(array(
            'label' => 'Тип перекрытий',
            'name'  => 'type_floors',
        ));

        $this->addFormText(array(
            'label' => 'Этажность',
            'name'  => 'number_storeys',
        ));

        $this->addFormText(array(
            'label' => 'Количество подъездов',
            'name'  => 'number_entrances',
        ));

        $this->addFormText(array(
            'label' => 'Количество лифтов',
            'name'  => 'number_lifts',
        ));

        $this->addFormText(array(
            'label' => 'Общая площадь, м2',
            'name'  => 'total_area',
            '731' => true,
        ));

        $this->addFormText(array(
            'label' => 'Площадь жилых помещений всего, м2',
            'name'  => 'residential_area',
        ));

        $this->addFormText(array(
            'label' => '- Частная',
            'name'  => 'residential_area_private',
        ));

        $this->addFormText(array(
            'label' => '- Муниципальная',
            'name'  => 'residential_area_municipal',
        ));

        $this->addFormText(array(
            'label' => '- Государственная',
            'name'  => 'residential_area_state',
        ));
        /**/
        $this->addFormText(array(
            'label' => 'Площадь нежилых помещений, м2',
            'name'  => '​​residential_non_area',
        ));

        $this->addFormText(array(
            'label' => 'Площадь участка, м2',
            'name'  => 'land_area',
        ));

        $this->addFormText(array(
            'label' => 'Площадь придомовой территории, м2',
            'name'  => 'territory_area',
        ));

        $this->addFormText(array(
            'label' => 'Инвентарный номер',
            'name'  => 'inventory_number',
        ));

        $this->addFormText(array(
            'label' => 'Кадастровый номер участка',
            'name'  => 'cadastral_number',
        ));

        $this->addFormText(array(
            'label' => 'Количество квартир',
            'name'  => 'apartments_number',
        ));

        $this->addFormText(array(
            'label' => 'Количество жителей',
            'name'  => 'residents_number',
        ));

        $this->addFormText(array(
            'label' => 'Количество лицевых счетов',
            'name'  => 'accounts_number',
        ));
        $this->addFormTextarea(array(
            'label' => 'Конструктивные особенности дома',
            'name'  => 'design_features',
            '731' => true,
        ));
        $this->addFormText(array(
            'label' => '- фактический удельный расход, Вт/М3Сград',
            'name'  => 'thermal_characteristic_actual',
        ));
        $this->addFormText(array(
            'label' => '- нормативный удельный расход, Вт/М3Сград',
            'name'  => 'thermal_characteristic_regulatory',
        ));

        $this->addFormText(array(
            'label' => 'Класс энергоэффективности',
            'name'  => 'energy_efficiency',
        ));
        $this->addFormDate(array(
            'label' => 'Дата проведения энергетического аудита',
            'name'  => 'date_energy_audit',
        ));
        $this->addFormDate(array(
            'label' => 'Дата начала приватизации',
            'name'  => 'date_privatization',
        ));
        $this->addFormText(array(
            'label' => 'Общая степень износа',
            'name'  => 'wear',
            '731' => true,
        ));
        $this->addFormText(array(
            'label' => 'Степень износа фундамента',
            'name'  => 'wear_foundation',
            '731' => true,
        ));
        $this->addFormText(array(
            'label' => 'Степень износа несущих стен',
            'name'  => 'wear_wall',
            '731' => true,
        ));
        $this->addFormText(array(
            'label' => 'Степень износа перекрытий',
            'name'  => 'wear_overlap',
            '731' => true,
        ));

        /** -- КОНСТРУКТИВНЫЕ ЭЛЕМЕНТЫ ДОМА -- */
        // фасад
        $this->addFormText(array(
            'label' => 'Площадь фасада общая, м2',
            'name'  => 'facade_area_total',
        ));

        $this->addFormText(array(
            'label' => 'Площадь фасада оштукатуренная, м2',
            'name'  => 'facade_area_plastered',
        ));

        $this->addFormText(array(
            'label' => 'Площадь фасада неоштукатуренная, м2',
            'name'  => 'facade_area_not_plastered',
        ));

        $this->addFormText(array(
            'label' => 'Площадь фасада панельная, м2',
            'name'  => 'facade_area_panel',
        ));

        $this->addFormText(array(
            'label' => 'Площадь фасада, облицованная плиткой, м2',
            'name'  => 'facade_area_tiles',
        ));

        $this->addFormText(array(
            'label' => 'Площадь фасада, облицованная сайдингом, м2',
            'name'  => 'facade_area_siding',
        ));

        $this->addFormText(array(
            'label' => 'Площадь фасада деревянная, м2',
            'name'  => 'facade_area_wooden',
        ));

        $this->addFormText(array(
            'label' => 'Площадь утепленного фасада с отделкой декоративной штукатуркой, м2',
            'name'  => 'facade_area_thermal_finish',
        ));

        $this->addFormText(array(
            'label' => 'Площадь утепленного фасада с отделкой плиткой, м2',
            'name'  => 'facade_area_thermal_tiles',
        ));

        $this->addFormText(array(
            'label' => 'Площадь утепленного фасада с отделкой сайдингом, м2',
            'name'  => 'facade_area_thermal_siding',
        ));

        $this->addFormText(array(
            'label' => 'Площадь отмостки, м2',
            'name'  => 'facade_area_otmostka',
        ));

        $this->addFormText(array(
            'label' => 'Площадь остекления мест общего пользования (дерево) , м2',
            'name'  => 'facade_area_glazing_common',
        ));

        $this->addFormText(array(
            'label' => 'Площадь остекления мест общего пользования (пластик) , м2',
            'name'  => 'facade_area_glazing_public',
        ));

        $this->addFormText(array(
            'label' => 'Площадь индивидуального остекления (дерево) , м2',
            'name'  => 'facade_area_individual_glazing_wood',
        ));

        $this->addFormText(array(
            'label' => 'Площадь индивидуального остекления (пластик) , м2',
            'name'  => 'facade_area_individual_glazing_plastic',
        ));

        $this->addFormText(array(
            'label' => 'Площадь металлических дверных заполнений, м2',
            'name'  => 'facade_area_metal_door',
        ));

        $this->addFormText(array(
            'label' => 'Площадь иных дверных заполнений, м2',
            'name'  => 'facade_area_wood_door',
        ));

        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта',
            'name'  => 'facade_year_renovation',
        ));
        // кровля
        $this->addFormText(array(
            'label' => 'Площадь кровли общая, м2',
            'name'  => 'roof_area_total',
        ));
        $this->addFormText(array(
            'label' => 'Площадь кровли шиферная скатная, м2',
            'name'  => 'roof_area_ramp_slate',
        ));
        $this->addFormText(array(
            'label' => 'Площадь кровли металлическая скатная, м2',
            'name'  => 'roof_area_ramp_metal',
        ));
        $this->addFormText(array(
            'label' => 'Площадь кровли иная скатная, м2',
            'name'  => 'roof_area_ramp_different',
        ));
        $this->addFormText(array(
            'label' => 'Площадь кровли плоская, м2',
            'name'  => 'roof_area_flat',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта кровли',
            'name'  => 'roof_year_renovation',
        ));
        // подвал
        $this->addFormText(array(
            'label' => 'Сведения о подвале',
            'name'  => 'basement_Information',
        ));
        $this->addFormText(array(
            'label' => 'Площадь подвальных помещений (включая помещения подвала и техподполье, если оно требует ремонта) , м2',
            'name'  => 'basement_area_total',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта подвальных помещений',
            'name'  => 'basement_year_renovation',
        ));
        //Помещения общего пользования
        $this->addFormText(array(
            'label' => 'Площадь помещений общего пользования, м2',
            'name'  => 'common_area_total',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего ремонта помещений общего пользования',
            'name'  => 'common_year_renovation',
        ));
        // мусоропроводы
        $this->addFormText(array(
            'label' => 'Количество мусоропроводов в доме',
            'name'  => 'chutes_number',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего ремонта мусоропроводов',
            'name'  => 'chutes_year_renovation',
        ));
        // -- ИНЖЕНЕРНЫЕ СИСТЕМЫ --
        // Система отопления
        $this->addFormText(array(
            'label' => 'Тип',
            'name'  => 'heating_system_type',
        ));
        $this->addFormText(array(
            'label' => 'Количество элеваторных узлов системы отопления',
            'name'  => 'heating_number_elevator_units',
        ));
        $this->addFormText(array(
            'label' => 'Длина трубопроводов системы отопления, м',
            'name'  => 'heating_length_pipelines',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта системы отопления',
            'name'  => 'heating_year_overhaul',
        ));
        $this->addFormText(array(
            'label' => 'Количество точек ввода отопления',
            'name'  => 'heating_number_dot_in',
        ));
        $this->addFormText(array(
            'label' => 'Количество узлов управления отоплением',
            'name'  => 'heating_number_control_units',
        ));
        $this->addFormText(array(
            'label' => 'Количество общедомовых приборов учета отопления',
            'name'  => 'heating_number_obschedomovyh',
        ));
        $this->addFormText(array(
            'label' => 'Отпуск отопления производится',
            'name'  => 'heating_leave',
        ));
        // Система горячего водоснабжения
        $this->addFormText(array(
            'label' => 'Тип',
            'name'  => 'hotwater_system_type',
        ));
        $this->addFormText(array(
            'label' => 'Длина трубопроводов системы горячего водоснабжения, м',
            'name'  => 'hotwater_length_pipelines',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта системы горячего водоснабжения',
            'name'  => 'hotwater_year_overhaul',
        ));
        $this->addFormText(array(
            'label' => 'Количество точек ввода горячей воды',
            'name'  => 'hotwater_number_dot_in',
        ));
        $this->addFormText(array(
            'label' => 'Количество узлов управления поставкой горячей воды',
            'name'  => 'hotwater_number_control_units',
        ));
        $this->addFormText(array(
            'label' => 'Количество общедомовых приборов учета горячей воды',
            'name'  => 'hotwater_number_obschedomovyh',
        ));
        $this->addFormText(array(
            'label' => 'Отпуск горячей воды производится',
            'name'  => 'hotwater_leave',
        ));
        // Система холодного водоснабжения
        $this->addFormText(array(
            'label' => 'Тип',
            'name'  => 'coldwater_system_type',
        ));
        $this->addFormText(array(
            'label' => 'Длина трубопроводов системы холодного водоснабжения, м',
            'name'  => 'coldwater_length_pipelines',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта системы холодного водоснабжения',
            'name'  => 'coldwater_year_overhaul',
        ));
        $this->addFormText(array(
            'label' => 'Количество точек ввода холодной воды',
            'name'  => 'coldwater_number_dot_in',
        ));
        $this->addFormText(array(
            'label' => 'Количество общедомовых приборов учета холодной воды',
            'name'  => 'coldwater_number_obschedomovyh',
        ));
        $this->addFormText(array(
            'label' => 'Отпуск холодной воды производится',
            'name'  => 'coldwater_leave',
        ));
        // Система водоотведения (канализации)
        $this->addFormText(array(
            'label' => 'Тип',
            'name'  => 'sewage_system_type',
        ));
        $this->addFormText(array(
            'label' => 'Длина трубопроводов системы водоотведения, м',
            'name'  => 'sewage_length_pipelines',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капитального ремонта системы водоотведения (канализации)',
            'name'  => 'sewage_year_overhaul',
        ));
        // Система электроснабжения
        $this->addFormText(array(
            'label' => 'Система электроснабжения',
            'name'  => 'electrical',
        ));
        $this->addFormText(array(
            'label' => 'Длина сетей в местах общего пользования, м',
            'name'  => 'electrical_length_commons',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капремонта системы электроснабжения',
            'name'  => 'electrical_year_overhaul',
        ));
        $this->addFormText(array(
            'label' => 'Количество точек ввода электричества',
            'name'  => 'electrical_number_dot_in',
        ));
        $this->addFormText(array(
            'label' => 'Количество общедомовых приборов учета электричества',
            'name'  => 'electrical_number_obschedomovyh',
        ));
        $this->addFormText(array(
            'label' => 'Отпуск электричества производится',
            'name'  => 'electrical_leave',
        ));
        // Система газоснабжения
        $this->addFormText(array(
            'label' => 'Вид системы газоснабжения',
            'name'  => 'gas_system_type',
        ));
        $this->addFormText(array(
            'label' => 'Длина сетей соответствующих требованиям',
            'name'  => 'gas_length_compliant',
        ));
        $this->addFormText(array(
            'label' => 'Длина сетей не соответствующих требованиям',
            'name'  => 'gas_length_non_compliant',
        ));
        $this->addFormDate(array(
            'label' => 'Год проведения последнего капремонта системы газоснабжения',
            'name'  => 'gas_year_overhaul',
        ));
        $this->addFormText(array(
            'label' => 'Количество точек ввода газа',
            'name'  => 'gas_number_dot_in',
        ));
        $this->addFormText(array(
            'label' => 'Количество общедомовых приборов учета газа',
            'name'  => 'gas_number_obschedomovyh',
        ));
        $this->addFormText(array(
            'label' => 'Отпуск газа производится',
            'name'  => 'gas_leave',
        ));
        // -- ЛИФТЫ --
        $this->addFormText(array(
            'label' => 'Лифты',
            'name'  => 'elevators',
        ));
        // КАРТОЧКА ДОГОВОР С УПРАВЛЯЮЩЕЙ ОРГАНИЗАЦИЕЙ
        $this->addFormTextarea(array(
            'label' => 'Тип договора управления',
            'name'  => 'management_type_contract',
        ));
        $this->addFormDate(array(
            'label' => 'Дата начала обслуживания дома',
            'name'  => 'management_date_start',
        ));
        $this->addFormDate(array(
            'label' => 'Плановая дата прекращения обслуживания дома',
            'name'  => 'management_date_end',
        ));
        $this->addFormTextarea(array(
            'label' => 'Выполняемые работы',
            'name'  => 'management_performed_works',
        ));
        $this->addFormTextarea(array(
            'label' => 'Выполнение обязательств',
            'name'  => 'management_execution_works',
        ));
        $this->addFormTextarea(array(
            'label' => 'Примечание',
            'name'  => 'management_note',
        ));
        $this->addFormTextarea(array(
            'label' => 'Стоимость услуг',
            'name'  => 'management_cost_services',
        ));
        $this->addFormTextarea(array(
            'label' => 'Средства ТСЖ или ЖСК',
            'name'  => 'management_means',
        ));
        $this->addFormTextarea(array(
            'label' => 'Условия оказания услуг ТСЖ или ЖСК',
            'name'  => 'management_terms_services',
        ));

        // УПРАВЛЕНИЕ ОБЩИМ ИМУЩЕСТВОМ
        $this->addFormTextarea(array(
            'label' => 'Поставщик отопления',
            'name' => 'rco_heating',
            '731' => true,
        ));
        $this->addFormTextarea(array(
            'label' => 'Поставщик электричества',
            'name' => 'rco_electricity',
            '731' => true,
        ));
        $this->addFormTextarea(array(
            'label' => 'Поставщик газа',
            'name' => 'rco_gas',
            '731' => true,
        ));
        $this->addFormTextarea(array(
            'label' => 'Поставщик горячей воды',
            'name' => 'rco_hot_water',
            '731' => true,
        ));
        $this->addFormTextarea(array(
            'label' => 'Поставщик холодной воды',
            'name' => 'rco_cold_water',
            '731' => true,
        ));
        $this->addFormTextarea(array(
            'label' => 'Поставщик водоотведения',
            'name' => 'rco_sanitation',
            '731' => true,
        ));

        // УПРАВЛЕНИЕ ОБЩИМ ИМУЩЕСТВОМ

        $this->addFormText(array(
            'label' => 'Доход от управления за отчетный период, тыс. руб.',
            'name'  => 'common_income',
        ));
        $this->addFormText(array(
            'label' => 'Доход от управления общим имуществом за отчетный период, тыс. руб.',
            'name'  => 'common_income_property',
        ));
        $this->addFormText(array(
            'label' => 'Расходы на управление за отчетный период, тыс. руб.',
            'name'  => 'common_costs',
        ));
        $this->addFormText(array(
            'label' => 'Задолженность собственников за услуги управления за отчетный период, тыс. руб.',
            'name'  => 'common_debt',
        ));
        $this->addFormText(array(
            'label' => 'Взыскано с собственников за услуги управления за отчетный период, тыс. руб.',
            'name'  => 'common_recover_owners',
        ));
        $this->addFormText(array(
            'label' => 'Выплаты по искам и договорам управления за отчетный период, тыс. руб.',
            'name'  => 'common_payment',
        ));
        $this->addFormText(array(
            'label' => 'иски по компенсации нанесенного ущерба',
            'name'  => 'common_claims',
        ));
        $this->addFormText(array(
            'label' => 'иски по снижению платы в связи с неоказанием услуг',
            'name'  => 'common_claims_refusal',
        ));
        $this->addFormText(array(
            'label' => 'иски по снижению платы в связи с недопоставкой ресурсов',
            'name'  => 'common_claims_shipment',
        ));
        $this->addFormText(array(
            'label' => 'Объем работ по ремонту за отчетный период, тыс. руб.',
            'name'  => 'common_work_repair',
        ));
        $this->addFormText(array(
            'label' => 'Объем работ по благоустройству за отчетный период, тыс. руб.',
            'name'  => 'common_work_improvement',
        ));
        $this->addFormText(array(
            'label' => 'Объем привлеченных средств за отчетный период, тыс. руб.',
            'name'  => 'common_attracted_funds',
        ));
        $this->addFormText(array(
            'label' => 'субсидии',
            'name'  => 'common_subsidy',
        ));
        $this->addFormText(array(
            'label' => 'кредиты',
            'name'  => 'common_credits',
        ));
        $this->addFormText(array(
            'label' => 'финансирование по договорам лизинга',
            'name'  => 'common_financing_lease',
        ));
        $this->addFormText(array(
            'label' => 'финансирование по энергосервисным договорам',
            'name'  => 'common_financing_energy',
        ));
        $this->addFormText(array(
            'label' => 'целевые взносы жителей',
            'name'  => 'common_contributions',
        ));
        $this->addFormText(array(
            'label' => 'иные источники',
            'name'  => 'common_other_sources',
        ));

        // КОММУНАЛЬНЫЕ УСЛУГИ
        $this->addFormText(array(
            'label' => 'Доход от поставки КУ за отчетный период, тыс. руб.',
            'name'  => 'income',
        ));
        $this->addFormText(array(
            'label' => 'отопление',
            'name'  => 'income_heating',
        ));
        $this->addFormText(array(
            'label' => 'электричество',
            'name'  => 'income_electricity',
        ));
        $this->addFormText(array(
            'label' => 'газ',
            'name'  => 'income_gas',
        ));
        $this->addFormText(array(
            'label' => 'горячее водоснабжение',
            'name'  => 'income_hot_water',
        ));
        $this->addFormText(array(
            'label' => 'холодное водоснабжение',
            'name'  => 'income_cold_water',
        ));
        $this->addFormText(array(
            'label' => 'водоотведение',
            'name'  => 'income_sanitation',
        ));
        $this->addFormText(array(
            'label' => 'Задолженность собственников за КУ за отчетный период, тыс. руб.',
            'name'  => 'debt',
        ));
        $this->addFormText(array(
            'label' => 'отопление',
            'name'  => 'debt_heating',
        ));
        $this->addFormText(array(
            'label' => 'электричество',
            'name'  => 'debt_electricity',
        ));
        $this->addFormText(array(
            'label' => 'газ',
            'name'  => 'debt_gas',
        ));
        $this->addFormText(array(
            'label' => 'горячее водоснабжение',
            'name'  => 'debt_hot_water',
        ));
        $this->addFormText(array(
            'label' => 'холодное водоснабжение',
            'name'  => 'debt_cold_water',
        ));
        $this->addFormText(array(
            'label' => 'водоотведение',
            'name'  => 'debt_sanitation',
        ));
        $this->addFormText(array(
            'label' => 'Взыскано с собственников за КУ за отчетный период, тыс. руб.',
            'name'  => 'charged',
        ));
        $this->addFormText(array(
            'label' => 'отопление',
            'name'  => 'charged_heating',
        ));
        $this->addFormText(array(
            'label' => 'электричество',
            'name'  => 'charged_electricity',
        ));
        $this->addFormText(array(
            'label' => 'газ',
            'name'  => 'charged_gas',
        ));
        $this->addFormText(array(
            'label' => 'горячее водоснабжение',
            'name'  => 'charged_hot_water',
        ));
        $this->addFormText(array(
            'label' => 'холодное водоснабжение',
            'name'  => 'charged_cold_water',
        ));
        $this->addFormText(array(
            'label' => 'водоотведение',
            'name'  => 'charged_sanitation',
        ));
        $this->addFormText(array(
            'label' => 'Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс. руб.',
            'name'  => 'paid',
        ));
        $this->addFormText(array(
            'label' => 'отопление',
            'name'  => 'paid_heating',
        ));
        $this->addFormText(array(
            'label' => 'электричество',
            'name'  => 'paid_electricity',
        ));
        $this->addFormText(array(
            'label' => 'газ',
            'name'  => 'paid_gas',
        ));
        $this->addFormText(array(
            'label' => 'горячее водоснабжение',
            'name'  => 'paid_hot_water',
        ));
        $this->addFormText(array(
            'label' => 'холодное водоснабжение',
            'name'  => 'paid_cold_water',
        ));
        $this->addFormText(array(
            'label' => 'Оплачено ресурсов по счетам на общедомовые нужды за отчетный период, тыс. руб.',
            'name'  => 'paid_resources',
        ));
        $this->addFormText(array(
            'label' => 'отопление',
            'name'  => 'paid_resources_heating',
        ));
        $this->addFormText(array(
            'label' => 'электричество',
            'name'  => 'paid_resources_electricity',
        ));
        $this->addFormText(array(
            'label' => 'газ',
            'name'  => 'paid_resources_gas',
        ));
        $this->addFormText(array(
            'label' => 'горячее водоснабжение',
            'name'  => 'paid_resources_hot_water',
        ));
        $this->addFormText(array(
            'label' => 'холодное водоснабжение',
            'name'  => 'paid_resources_cold_water',
        ));
        $this->addFormText(array(
            'label' => 'водоотведение',
            'name'  => 'paid_resources_sanitation',
        ));
        $this->addFormText(array(
            'label' => 'водоотведение',
            'name'  => 'paid_sanitation',
        ));


        return $this->getForm();
    }

    /**
     * @param array $element
     */
    protected function addFormSelect(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::select($element['name'], $element['select'], $value)}
                </div>
            </div>
        ";
    }

    /**
     * @param array $element
     */
    protected function addFormText(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;
        $danger = isset($element['731']) ? "<span class='uk-text-danger uk-text-small'>\"731\"</span>" : "";
        $sup = isset($element['sup']) ? "<sup>{$element['sup']}</sup>" : '';

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$danger} {$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::text($element['name'], $value, array('class' => 'uk-width-1-1', 'placeholder' => $element['label']))}
                </div>
            </div>
        ";
    }

    /**
     * @param array $element
     */
    private function addFormTextarea(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;
        $danger = isset($element['731']) ? "<span class='uk-text-danger uk-text-small'>\"731\"</span>" : "";

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$danger} {$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::textarea($element['name'], $value, array('cols' => '30', 'rows' => '3', 'class' => 'uk-width-1-1', 'placeholder' => $element['label']))}
                </div>
            </div>
        ";
    }

    /**
     * @param array $element
     */
    private function addFormDate(array $element) {
        $value = isset($this->data[$element['name']]) ? $this->data[$element['name']] : null;

        $this->form[$element['name']] = "
            <div class='uk-form-row'>
                <label class='uk-form-label'>{$element['label']}</label>
                <div class='uk-form-controls'>
                    {$this::input('date', $element['name'], $value, array('class' => 'uk-width-1-1', 'placeholder' => $element['label']))}
                </div>
            </div>
        ";
    }
}