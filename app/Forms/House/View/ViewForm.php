<?php namespace App\Forms\House\View;

use App\Forms\Form;
use App\Forms\House\View\Passport\GeneralInformationForms;
use App\Forms\House\View\Passport\StructuralElementsForms;
use App\Forms\House\View\Passport\EngineeringSystemsForms;
use App\Forms\House\View\Passport\ElevatorForms;
use App\Forms\House\View\Passport\MeteringDevicesForms;
use App\Forms\House\View\Management\OrganizationForms;
use App\Forms\House\View\Management\WorkPerformedForms;
use App\Forms\House\View\Management\PublicServiceForms;
use App\Forms\House\View\Management\CommonPropertyForms;
use App\Forms\House\View\Management\RepairForms;
use App\Forms\House\View\Report\InformationForms;
use App\Forms\House\View\Report\WorkPerformedForms as ReportWorkPerformedForms;
use App\Forms\House\View\Report\ClaimForms;
use App\Forms\House\View\Report\UtilitiesForms;
use App\Forms\House\View\Report\PublicServiceForms as ReportPublicServiceForms;
use App\Forms\House\View\Report\ClaimWorkForms;

class ViewForm extends Form
{
    /**
     * @param array $element
     * @param bool $string
     * @param null $data
     * @return mixed
     */
    protected function addFormText(array $element, $string = false, $data = null) {
        $number = isset($element['number']) ? $element['number'] : '';
        $dash = isset($element['dash']) ? '&mdash; ' : '';
        $name = isset($element['name']) ? $element['name'] : null;
        $class = isset($element['class']) ? 'class="' . $element['class'] . '"' : '';
        $data = $data ? $data : $this->data;

        if (array_key_exists($element['name'], $data)) {
            $value = $data[$element['name']] ? $data[$element['name']] : 'нет информации';
        } else {
            $value = '';
        }
        $this->form[$name] = "
            <tr>
                <td><span>{$number}</span></td>
                <td {$class}><span>{$dash}{$element['label']}</span></td>
                <td><span>{$value}</span></td>
            </tr>
        ";
        if ($string) {
            $result = $this->form[$name];
            unset($this->form[$name]);
            return $result;
        }
    }

    /**
     * @param array $element
     */
    protected function addFormTr(array $element) {
        $this->form[] = "
            <tr>
                <td>???</td>
                <td>???</td>
                <td>???</td>
                <td>???</td>
            </tr>
        ";
    }

    /**
     * @param array $element
     */
    protected function addElementTable(array $element) {
        $service = $element['service'];
        $name = isset($element['name']) ? $element['name'] : null;
        $value = $this->data[$element['name']] ? $this->data[$element['name']] : 'нет информации';
        $this->form[$name] = "
        <tr class='tm-tr-bg-grey'>
            <td>{$element['label']}</td>
            <td>{$value}</td>
            <td class='uk-text-right'>
                <a data-uk-toggle=\"{target:'#service_{$service}', animation:'uk-animation-fade'}\" class='uk-text-small'><i class='uk-icon-chevron-down'></i> Подробнее</a>
            </td>
        </tr>
        <tr>
            <td id='service_{$service}' class='uk-hidden' colspan='3'>
                <table class='tm-table-strip tm-table-strip-align'>
                    <tbody>
                        <tr><td height='5'></td></tr>
                        <tr>
                            <td><span></span></td>
                            <td class='uk-width-2-3'><span>История стоимости работы / услуги</span></td>
                            <td><span><a href='#service_{$service}m' data-uk-modal='{center:true}'>Подробнее</a></span></td>
                        </tr>
                        <tr><td height='5'></td></tr>
                    </tbody>
                </table>
            </td>
        </tr>
        ";
    }

    /**
     * @return array
     */
    protected function getForm() {
        $result = '';

        foreach ($this->form as $value) {
            $result .= $value;
        }
        return $result;
    }

    /**
     * @param $home
     * @return array+
     */
    public function bind($home) {
        $generalInformationForms = new GeneralInformationForms($home);
        $structuralElementsForms = new StructuralElementsForms($home);
        $engineeringSystemsForms = new EngineeringSystemsForms($home);
        $elevatorForms = new ElevatorForms($home);
        $meteringDevicesForms = new MeteringDevicesForms($home);
        $organizationForms = new OrganizationForms($home);
        $workPerformedForms = new WorkPerformedForms($home);
        $publicServiceForms = new PublicServiceForms($home);
        $commonPropertyForms = new CommonPropertyForms($home);
        $repairForms = new RepairForms($home);
        $informationForms = new InformationForms($home);
        $reportWorkPerformedForms = new ReportWorkPerformedForms($home);
        $claimForms = new ClaimForms($home);
        $utilitiesForms = new UtilitiesForms($home);
        $reportPublicServiceForms = new ReportPublicServiceForms($home);
        $claimWorkForms = new ClaimWorkForms($home);
        return [
            $generalInformationForms->formName => $generalInformationForms->init(),
            $structuralElementsForms->formName => $structuralElementsForms->init(),
            $engineeringSystemsForms->formName => $engineeringSystemsForms->init(),
            $elevatorForms->formName => $elevatorForms->init(),
            $meteringDevicesForms->formName => $meteringDevicesForms->init(),
            $organizationForms->formName => $organizationForms->init(),
            $workPerformedForms->formName => $workPerformedForms->init(),
            $publicServiceForms->formName => $publicServiceForms->init(),
            $commonPropertyForms->formName => $commonPropertyForms->init(),
            $repairForms->formName => $repairForms->init(),
            $informationForms->formName => $informationForms->init(),
            $reportWorkPerformedForms->formName => $reportWorkPerformedForms->init(),
            $claimForms->formName => $claimForms->init(),
            $utilitiesForms->formName => $utilitiesForms->init(),
            $reportPublicServiceForms->formName => $reportPublicServiceForms->init(),
            $claimWorkForms->formName => $claimWorkForms->init(),
        ];
    }
}