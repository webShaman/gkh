<?php namespace App\Forms\House\View\Passport;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Passport\EngineeringSystems;

class EngineeringSystemsForms extends ViewForm
{
    public $formName = EngineeringSystems::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[EngineeringSystems::TABLE_NAME]) ? $data[EngineeringSystems::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText(array(
            'number' => 1,
            'class'  => 'uk-text-bold',
            'label'  => 'Система электроснабжения',
            'name'   => '1',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы электроснабжения',
            'name'   => 'electrical_type',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Количество вводов в дом, ед.',
            'name'   => 'electronic_number_entries',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 2,
            'class'  => 'uk-text-bold',
            'label'  => 'Система теплоснабжения',
            'name'   => '2',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы теплоснабжения',
            'name'   => 'heating_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 3,
            'class'  => 'uk-text-bold',
            'label'  => 'Система горячего водоснабжения',
            'name'   => '3',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы горячего водоснабжения',
            'name'   => 'hot_water_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 4,
            'class'  => 'uk-text-bold',
            'label'  => 'Система холодного водоснабжения',
            'name'   => '4',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы холодного водоснабжения',
            'name'   => 'cold_water_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 5,
            'class'  => 'uk-text-bold',
            'label'  => 'Система водоотведения',
            'name'   => '5',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы водоотведения',
            'name'   => 'drainage_type',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Объем выгребных ям, куб. м.',
            'name'   => 'cesspools_volume',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 6,
            'class'  => 'uk-text-bold',
            'label'  => 'Система газоснабжения',
            'name'   => '6',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы газоснабжения',
            'name'   => 'gas_supply_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 7,
            'class'  => 'uk-text-bold',
            'label'  => 'Cистема вентиляции',
            'name'   => '7',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы вентиляции',
            'name'   => 'ventilation_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 8,
            'class'  => 'uk-text-bold',
            'label'  => 'Система пожаротушения',
            'name'   => '8',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы пожаротушения',
            'name'   => 'fire_extinguishing_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 9,
            'class'  => 'uk-text-bold',
            'label'  => 'Система водостоков',
            'name'   => '9',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип системы водостоков',
            'name'   => 'drains_type',
            'dash'   => true,
        ));

        return $this->getForm();
    }
}