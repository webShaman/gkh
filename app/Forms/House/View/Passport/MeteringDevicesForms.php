<?php namespace App\Forms\House\View\Passport;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Passport\MeteringDevices;

class MeteringDevicesForms extends ViewForm
{
    public $formName = MeteringDevices::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[MeteringDevices::TABLE_NAME]) ? $data[MeteringDevices::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        return $this->getForm();
    }
}