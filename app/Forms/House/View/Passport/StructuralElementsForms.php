<?php namespace App\Forms\House\View\Passport;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Passport\StructuralElements;

class StructuralElementsForms extends ViewForm
{
    public $formName = StructuralElements::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[StructuralElements::TABLE_NAME]) ? $data[StructuralElements::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText(array(
            'number' => 1,
            'class'  => 'uk-text-bold',
            'label'  => 'Фундамент',
            'name'   => '1',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Тип фундамента',
            'name'   => 'foundation_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 2,
            'class'  => 'uk-text-bold',
            'label'  => 'Стены и перекрытия',
            'name'   => '2',
        ));
        $this->addFormText(array(
            'label'  => 'Тип перекрытий',
            'name'   => 'overlap_type',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => 'Материал несущих стен',
            'name'   => 'material_bearing_walls',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 3,
            'class'  => 'uk-text-bold',
            'label'  => 'Подвал',
            'name'   => '3',
        ));
        $this->addFormText(array(
            'label'  => 'Площадь подвала по полу, кв.м',
            'name'   => 'basement_area',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 4,
            'class'  => 'uk-text-bold',
            'label'  => 'Мусоропроводы',
            'name'   => '4',
        ));
        $this->addFormText(array(
            'label'  => 'Тип мусоропровода',
            'name'   => 'chutes_type',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => 'Количество мусоропроводов, ед.',
            'name'   => 'chutes_number',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 5,
            'class'  => 'uk-text-bold',
            'label'  => 'Фасады',
            'name'   => '5',
        ));
        $this->addFormText(array(
            'label'  => 'Тип фасада',
            'name'   => 'facade_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 6,
            'class'  => 'uk-text-bold',
            'label'  => 'Крыши',
            'name'   => '6',
        ));
        $this->addFormText(array(
            'label'  => 'Тип крыши',
            'name'   => 'roof_type',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => 'Тип кровли',
            'name'   => 'loft_type',
            'dash'   => true,
        ));

        $this->addFormText(array(
            'number' => 7,
            'class'  => 'uk-text-bold',
            'label'  => 'Иное оборудование / конструктивный элемент',
            'name'   => '7',
        ));
        $this->addFormText(array(
            'label'  => 'Вид оборудования / конструктивного элемента',
            'name'   => 'equipment_type',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'label'  => 'Описание дополнительного оборудования / конструктивного элемента',
            'name'   => 'equipment_description',
            'dash'   => true,
        ));

        return $this->getForm();
    }
}