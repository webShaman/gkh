<?php namespace App\Forms\House\View\Passport;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Passport\Elevators;

class ElevatorForms extends ViewForm
{
    public $formName = Elevators::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Elevators::TABLE_NAME]) ? $data[Elevators::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormTr([
        ]);
        return $this->getForm();
    }
}