<?php namespace App\Forms\House\View\Report;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Report\ClaimWork;

class ClaimWorkForms extends ViewForm
{
    public $formName = ClaimWork::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[ClaimWork::TABLE_NAME]) ? $data[ClaimWork::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText(array(
            'number' => 1,
            'label'  => 'Направлено претензий потребителям-должникам',
            'name'   => 'sent_claims',
        ));
        $this->addFormText(array(
            'number' => 2,
            'label'  => 'Направлено исковых заявлений',
            'name'   => 'sent_suits',
        ));
        $this->addFormText(array(
            'number' => 3,
            'label'  => 'Получено денежных средств по результатам претензионно-исковой работы',
            'name'   => 'obtained_funds',
        ));

        return $this->getForm();
    }
}