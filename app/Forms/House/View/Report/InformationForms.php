<?php namespace App\Forms\House\View\Report;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Report\Information;

class InformationForms extends ViewForm
{
    public $formName = Information::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Information::TABLE_NAME]) ? $data[Information::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText(array(
            'number' => 1,
            'label'  => 'Ававнсовые платежи потребителей (на начало периода), руб.',
            'name'   => 'charges_begin',
        ));
        $this->addFormText(array(
            'number' => 2,
            'label'  => 'Переходящие остатки денежных средств (на начало периода), руб.',
            'name'   => 'carryover_begin',
        ));
        $this->addFormText(array(
            'number' => 3,
            'label'  => 'Задолженность потребителей (на начало периода), руб.',
            'name'   => 'debt_begin',
        ));
        $this->addFormText(array(
            'number' => 4,
            'class'  => 'uk-text-bold',
            'label'  => 'Начислено за услуги (работы) по содержанию и текущему ремонту, в том числе:',
            'name'   => '4',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'за содержание дома, руб.',
            'name'   => 'charged_contents',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'за текущий ремонт, руб.',
            'name'   => 'charged_maintenance',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'за услуги управления, руб.',
            'name'   => 'charged_management',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => 5,
            'label'  => 'Получено денежных средств, в том числе:',
            'class'  => 'uk-text-bold',
            'name'   => '5',
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'денежных средств от собственников/нанимателей помещений, руб.',
            'name'   => 'obtained_cash_owners',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'целевых взносов от собственников/нанимателей помещений, руб.',
            'name'   => 'obtained_contributions_owners',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'субсидий, руб.',
            'name'   => 'obtained_subsidies_owners',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'денежных средств от использования общего имущества, руб.',
            'name'   => 'obtained_common_property_owners',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'прочие поступления, руб.',
            'name'   => 'obtained_other_income',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => 6,
            'label'  => 'Всего денежных средств с учетом остатков, руб.',
            'name'   => 'total_cash',
        ));
        $this->addFormText(array(
            'number' => 7,
            'label'  => 'Авансовые платежи потребителей (на конец периода), руб.',
            'name'   => 'charges_end',
        ));
        $this->addFormText(array(
            'number' => 8,
            'label'  => 'Переходящие остатки денежных средств (на конец периода), руб.',
            'name'   => 'carryover_end',
        ));
        $this->addFormText(array(
            'number' => 9,
            'label'  => 'Задолженность потребителей (на конец периода), руб.',
            'name'   => 'debt_end',
        ));

        return $this->getForm();
    }
}