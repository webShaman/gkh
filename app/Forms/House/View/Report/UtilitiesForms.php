<?php namespace App\Forms\House\View\Report;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Report\Utilities;

class UtilitiesForms extends ViewForm
{
    public $formName = Utilities::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Utilities::TABLE_NAME]) ? $data[Utilities::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText(array(
            'number' => 1,
            'label'  => 'Авансовые платежи потребителей (на начало периода), руб.',
            'name'   => 'advance_payments_start',
        ));
        $this->addFormText(array(
            'number' => 2,
            'label'  => 'Переходящие остатки денежных средств (на начало периода), руб.',
            'name'   => 'carryover_payments_start',
        ));
        $this->addFormText(array(
            'number' => 3,
            'label'  => 'Задолженность потребителей (на начало периода), руб.',
            'name'   => 'debt_start',
        ));
        $this->addFormText(array(
            'number' => 4,
            'label'  => 'Авансовые платежи потребителей (на конец периода), руб.',
            'name'   => 'advance_payments_end',
        ));
        $this->addFormText(array(
            'number' => 5,
            'label'  => 'Переходящие остатки денежных средств (на конец периода), руб.',
            'name'   => 'carryover_payments_end',
        ));
        $this->addFormText(array(
            'number' => 6,
            'label'  => 'Задолженность потребителей (на конец периода), руб.',
            'name'   => 'debt_end',
        ));
        $this->addFormText(array(
            'number' => 7,
            'label'  => 'Количество поступивших претензий, ед.',
            'name'   => 'claim_number_incoming',
        ));
        $this->addFormText(array(
            'number' => 8,
            'label'  => 'Количество удовлетворенных претензий, ед.',
            'name'   => 'claim_number_satisfied',
        ));
        $this->addFormText(array(
            'number' => 9,
            'label'  => 'Количество претензий, в удовлетворении которых отказано, ед.',
            'name'   => 'claim_number_denied',
        ));
        $this->addFormText(array(
            'number' => 10,
            'label'  => 'Сумма произведенного перерасчета, руб.',
            'name'   => 'recalculation',
        ));


        return $this->getForm();
    }
}