<?php namespace App\Forms\House\View\Report;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Report\PublicService;

class PublicServiceForms extends ViewForm
{
    public $formName = PublicService::TABLE_NAME;

    /**
     * @param null $data
     */
    public function __construct($data = null)
    {
        $this->data = isset($data[PublicService::TABLE_NAME]) ? $data[PublicService::TABLE_NAME] : null;
    }

    /**
     * @return array
     */
    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }


        $this->addForm();

        return $this->getForm();
    }

    protected function addForm() {

        foreach($this->data as $key => $data) {
            $key++;
            $data = $data->getAttributes();
            $viewPublic = $data['view_public'];
            $factProviding = $data['fact_providing'] ? $data['fact_providing'] : 'нет информации';
            $chargesCustomers = $data['charges_customers'] ? $data['charges_customers'] : 'нет информации';
            $unitMeasure = $data['unit_measure'] ? $data['unit_measure'] : 'нет информации';

            $this->form[] = "
                <tr class='tm-tr-bg-grey'>
                    <td>{$viewPublic}</td>
                    <td>{$factProviding}</td>
                    <td>{$unitMeasure}</td>
                    <td>{$chargesCustomers}</td>
                    <td class='uk-text-right'><a data-uk-toggle=\"{target:'#volume_service_{$key}', animation:'uk-animation-fade'}\" class='uk-text-small'>
                        <i class='uk-icon-chevron-down'></i> Подробнее</a>
                    </td>
                </tr>
                <tr>
                    <td id='volume_service_{$key}' class='uk-hidden' colspan='6'>
                        <table class='tm-table-strip tm-table-strip-align'>
                            <tbody>
                            "
                . $this->addFormText([
                    'number' => 1,
                    'label'  => 'Общий объем потребления, нат. показ.',
                    'name'   => 'total_consumption',
                ], true, $data)
                . $this->addFormText([
                    'number' => 2,
                    'label'  => 'Оплачено потребителями, руб.',
                    'name'   => 'paid_consumers',
                ], true, $data)
                . $this->addFormText([
                    'number' => 3,
                    'label'  => 'Задолженность потребителей, руб.',
                    'name'   => 'debt_consumers',
                ], true, $data)
                . $this->addFormText([
                    'number' => 4,
                    'label'  => 'Начислено поставщиком (поставщиками) коммунального ресурса, руб.',
                    'name'   => 'accrued_supplier',
                ], true, $data)
                . $this->addFormText([
                    'number' => 5,
                    'label'  => 'Оплачено поставщику (поставщикам) коммунального ресурса, руб.',
                    'name'   => 'paid_supplier',
                ], true, $data)
                . $this->addFormText([
                    'number' => 6,
                    'label'  => 'Задолженность перед поставщиком (поставщиками) коммунального ресурса, руб.',
                    'name'   => 'payable_suppliers',
                ], true, $data)
                . $this->addFormText([
                    'number' => 7,
                    'label'  => 'Суммы пени и штрафов, уплаченные поставщику(поставщикам) коммунального ресурса, руб.',
                    'name'   => 'fines',
                ], true, $data)
                . $this->addFormText([
                    'number' => 8,
                    'label'  => 'Суммы пени и штрафов, уплаченные поставщику(поставщикам) коммунального ресурса, руб.',
                    'name'   => 'fines',
                ], true, $data)
                . $this->addFormText([
                    'number' => 9,
                    'label'  => 'Суммы пени и штрафов, уплаченные поставщику(поставщикам) коммунального ресурса, руб.',
                    'name'   => 'fines',
                ], true, $data)
                . $this->addFormText([
                    'number' => 10,
                    'label'  => 'Суммы пени и штрафов, уплаченные поставщику(поставщикам) коммунального ресурса, руб.',
                    'name'   => 'fines',
                ], true, $data)

                ."
                            </tbody>
                        </table>
                    </td>
                </tr>
            ";
        }
    }
}