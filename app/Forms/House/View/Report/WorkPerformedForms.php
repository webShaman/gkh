<?php namespace App\Forms\House\View\Report;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Report\WorkPerformed;

class WorkPerformedForms extends ViewForm
{
    public $formName = WorkPerformed::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[WorkPerformed::TABLE_NAME]) ? $data[WorkPerformed::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addForm();
        return $this->getForm();
    }

    protected function addForm() {
        foreach($this->data as $key => $data) {
            $key++;
            $data = $data->getAttributes();
            $name = $data['name'];
            $number = $data['number'] ? $data['number'] : 'нет информации';
            $cost = $data['cost'] ? $data['cost'] : 'нет информации';

            $this->form[] = "
                <tr class='tm-tr-bg-grey'>
                    <td>{$name}</td>
                    <td>{$cost}</td>
                    <td>{$number}</td>
                    <td class='uk-text-right'><a data-uk-toggle='{target:\"#work_performed_{$key}\", animation:\"uk-animation-fade\"}' class='uk-text-small'><i class='uk-icon-chevron-down'></i> Подробнее</a></td>
                </tr>
                <tr>
                    <td id='work_performed_{$key}' class='uk-hidden' colspan='4' style='padding: 15px 70px 0 70px;'>
                        <table class='uk-table tm-table-red'>
                            <thead>
                                <tr>
                                    <th>Наименование работы (услуги)<br/>в рамках выбранной работы (услуги)</th>
                                    <th>Периодичность предоставления</th>
                                    <th>Единица измерения</th>
                                    <th>Стоимость на единицу измерения (руб.)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>????</td>
                                    <td>????</td>
                                    <td>????</td>
                                    <td>????</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            ";
        }
    }

}