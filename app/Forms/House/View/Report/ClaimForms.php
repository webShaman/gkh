<?php namespace App\Forms\House\View\Report;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Report\Claim;

class ClaimForms extends ViewForm
{
    public $formName = Claim::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Claim::TABLE_NAME]) ? $data[Claim::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText(array(
            'number' => 1,
            'label'  => 'Количество поступивших претензий, ед.',
            'name'   => 'number_incoming',
        ));
        $this->addFormText(array(
            'number' => 2,
            'label'  => 'Количество удовлетворенных претензий, ед.',
            'name'   => 'number_satisfied',
        ));
        $this->addFormText(array(
            'number' => 3,
            'label'  => 'Количество претензий, в удовлетворении которых отказано, ед.',
            'name'   => 'number_denied',
        ));
        $this->addFormText(array(
            'number' => 4,
            'label'  => 'Сумма произведенного перерасчета, руб.',
            'name'   => 'recalculation',
        ));

        return $this->getForm();
    }
}