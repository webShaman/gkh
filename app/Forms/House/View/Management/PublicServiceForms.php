<?php namespace App\Forms\House\View\Management;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Management\PublicService;

class PublicServiceForms extends ViewForm
{
    public $formName = PublicService::TABLE_NAME;

    /**
     * @param null $data
     */
    public function __construct($data = null)
    {
        $this->data = isset($data[PublicService::TABLE_NAME]) ? $data[PublicService::TABLE_NAME] : null;
    }

    /**
     * @return array
     */
    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addForm();

        return $this->getForm();
    }

    protected function addForm() {
        foreach($this->data as $key => $data) {
            $key++;
            $data = $data->getAttributes();
            $viewPublic = $data['view_public'];
            $factProviding = $data['fact_providing'] ? $data['fact_providing'] : 'нет информации';
            $tariff = $data['tariff'] ? $data['tariff'] : 'нет информации';
            $unitMeasure = $data['unit_measure'] ? $data['unit_measure'] : 'нет информации';
            $person = $data['person'] ? $data['person'] : 'нет информации';
            $this->form[] = "
                <tr class='tm-tr-bg-grey'>
                    <td>{$viewPublic}</td>
                    <td>{$factProviding}</td>
                    <td>{$tariff}</td>
                    <td>{$unitMeasure}</td>
                    <td>{$person}</td>
                    <td class='uk-text-right'><a data-uk-toggle=\"{target:'#utilities_{$key}', animation:'uk-animation-fade'}\" class='uk-text-small'>
                        <i class='uk-icon-chevron-down'></i> Подробнее</a>
                    </td>
                </tr>
                <tr>
                    <td id='utilities_{$key}' class='uk-hidden' colspan='6'>
                        <table class='tm-table-strip tm-table-strip-align'>
                            <tbody>
                            "
                . $this->addFormText([
                    'number' => 1,
                    'label'  => 'ИНН лица, осуществляющего поставку коммунального ресурса',
                    'name'   => 'inn',
                ], true, $data)
                . $this->addFormText([
                    'number' => 2,
                    'label'  => 'Дополнительная информация о лице, осуществляющего поставку коммунального ресурса',
                    'name'   => 'additional_information',
                ], true, $data)
                . $this->addFormText([
                    'number' => 3,
                    'label'  => 'Основание предоставления услуги',
                    'name'   => 'base_services',
                ], true, $data)
                . $this->addFormText([
                    'number' => 4,
                    'label'  => 'Реквизиты договора на поставку коммунального ресурса',
                    'name'   => 'details_contract',
                ], true, $data)
                . $this->addFormText([
                    'number' => 5,
                    'label'  => 'Нормативно-правовой акт, устанавливающий тариф',
                    'name'   => 'act_tariff',
                ], true, $data)
                . $this->addFormText([
                    'number' => 6,
                    'label'  => 'Дата начала действия тарифа',
                    'name'   => 'date_start_tariff',
                ], true, $data)
                . $this->addFormText([
                    'number' => 7,
                    'label'  => 'Описание дифференциации тарифов в случаях, предусмотренных законодательством Российской Федерации о государственном регулировании цен (тарифов)',
                    'name'   => 'differentiation',
                ], true, $data)
                . $this->addFormText([
                    'number' => 8,
                    'label'  => 'История стоимости услуги',
                    'name'   => 'cost_history',
                ], true, $data)
                . $this->addFormText([
                    'number' => 9,
                    'label'  => 'Норматив потребления коммунальной услуги в жилых помещениях:',
                    'class'  => 'uk-text-bold',
                    'name'   => '9',
                ], true, $data)
                . $this->addFormText([
                    'number' => '',
                    'label'  => 'Норматив потребления коммунальной услуги в жилых помещениях',
                    'name'   => 'dwellings_utilities',
                    'dash'   => true,
                ], true, $data)
                . $this->addFormText([
                    'number' => '',
                    'label'  => 'Единица измерения норматива потребления услуги',
                    'name'   => 'dwellings_unit',
                    'dash'   => true,
                ], true, $data)
                . $this->addFormText([
                    'number' => '',
                    'label'  => 'Дополнительно',
                    'name'   => 'dwellings_advanced',
                    'dash'   => true,
                ], true, $data)
                . $this->addFormText([
                    'number' => 10,
                    'label'  => 'Норматив потребления коммунальной услуги на общедомовые нужды:',
                    'class'  => 'uk-text-bold',
                    'name'   => '10',
                ], true, $data)
                . $this->addFormText([
                    'number' => '',
                    'label'  => 'Норматив потребления коммунальной услуги на общедомовые нужды',
                    'name'   => 'obschedomovye_utilities',
                    'dash'   => true,
                ], true, $data)
                . $this->addFormText([
                    'number' => '',
                    'label'  => 'Единица измерения норматива потребления услуги',
                    'name'   => 'obschedomovye_unit',
                    'dash'   => true,
                ], true, $data)
                . $this->addFormText([
                    'number' => '',
                    'label'  => 'Дополнительно',
                    'name'   => 'obschedomovye_advanced',
                    'dash'   => true,
                ], true, $data)
                . $this->addFormText([
                    'number' => 11,
                    'label'  => 'Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги',
                    'name'   => 'regulatory_act',
                    'dash'   => true,
                ], true, $data)
                ."
                            </tbody>
                        </table>
                    </td>
                </tr>
            ";
        }
    }
}