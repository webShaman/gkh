<?php namespace App\Forms\House\View\Management;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Management\CommonProperty;

class CommonPropertyForms extends ViewForm
{
    public $formName = CommonProperty::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[CommonProperty::TABLE_NAME]) ? $data[CommonProperty::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText([
            'number' => 1,
            'label'  => 'Информация',
            'name'   => 'info',
        ]);
        return $this->getForm();
    }
}