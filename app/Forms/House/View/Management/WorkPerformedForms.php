<?php namespace App\Forms\House\View\Management;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Management\WorkPerformed;

class WorkPerformedForms extends ViewForm
{
    public $formName = WorkPerformed::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[WorkPerformed::TABLE_NAME]) ? $data[WorkPerformed::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addElementTable([
            'service' => 1,
            'label'  => 'Работы по содержанию помещений, входящих в состав общего имущества в многоквартирном доме',
            'name'   => 'maintenance_premises',
        ]);
        $this->addElementTable([
            'service' => 2,
            'label'  => 'Работы по обеспечению вывоза бытовых отходов',
            'name'   => 'export_waste',
        ]);
        $this->addElementTable([
            'service' => 3,
            'label'  => 'Работы по содержанию и ремонту лифта (лифтов) в многоквартирном доме',
            'name'   => 'maintenance_elevator',
        ]);
        $this->addElementTable([
            'service' => 4,
            'label'  => 'Проведение дератизации и дезинсекции помещений, входящих в состав общего имущества в многоквартирном доме',
            'name'   => 'disinfestation',
        ]);
        $this->addElementTable([
            'service' => 5,
            'label'  => 'Работы по содержанию и ремонту конструктивных элементов (несущих конструкций и ненесущих конструкций) многоквартирных домов',
            'name'   => 'structural_elements',
        ]);
        $this->addElementTable([
            'service' => 6,
            'label'  => 'Работы по содержанию и ремонту оборудования и систем инженерно-технического обеспечения, входящих в состав общего имущества в многоквартирном доме)',
            'name'   => 'maintenance_equipment',
        ]);
        $this->addElementTable([
            'service' => 7,
            'label'  => 'Работы по содержанию земельного участка с элементами озеленения и благоустройства, иными объектами, предназначенными для обслуживания и эксплуатации многоквартирного дома',
            'name'   => 'content_land',
        ]);
        $this->addElementTable([
            'service' => 8,
            'label'  => 'Обеспечение устранения аварий на внутридомовых инженерных системах в многоквартирном доме',
            'name'   => 'eliminate_accidents',
        ]);
        $this->addElementTable([
            'service' => 9,
            'label'  => 'Работы (услуги) по управлению многоквартирным домом',
            'name'   => 'management',
        ]);

        return $this->getForm();
    }
}