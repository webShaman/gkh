<?php namespace App\Forms\House\View\Management;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Management\Organization;

class OrganizationForms extends ViewForm
{
    public $formName = Organization::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Organization::TABLE_NAME]) ? $data[Organization::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText([
            'number' => 1,
            'label'  => 'Дата начала управления',
            'name'   => 'date_start',
        ]);
        $this->addFormText([
            'number' => 2,
            'label'  => 'Основание управления',
            'name'   => 'ground_control',
        ]);
        $this->addFormText([
            'number' => 3,
            'label'  => 'Документ, подтверждающий выбранный способ управления:',
            'class'  => 'uk-text-bold',
            'name'   => '3',
        ]);
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Наименование документа',
            'name'   => 'document_name',
            'dash'   => true,
        ));
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Дата и номер документа',
            'name'   => 'date_number_document',
            'dash'   => true,
        ));
        $this->addFormText([
            'number' => 4,
            'label'  => 'Договор управления:',
            'class'  => 'uk-text-bold',
            'name'   => '4',
        ]);
        $this->addFormText(array(
            'number' => '',
            'label'  => 'Дата заключения договора управления',
            'name'   => 'date_contract',
            'dash'   => true,
        ));
        return $this->getForm();
    }
}