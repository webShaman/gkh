<?php namespace App\Forms\House\View\Management;

use App\Forms\House\View\ViewForm;
use App\Models\Home\Management\Repair;

class RepairForms extends ViewForm
{
    public $formName = Repair::TABLE_NAME;

    public function __construct($data = null) {
        $this->data = isset($data[Repair::TABLE_NAME]) ? $data[Repair::TABLE_NAME] : null;
    }

    public function init() {
        if (is_string($this->data)) {
            return $this->data;
        }

        $this->addFormText([
            'number' => 1,
            'label'  => 'Информация',
            'name'   => 'info',
        ]);
        return $this->getForm();
    }
}