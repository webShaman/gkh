<?php namespace App\Forms;

use Illuminate\Html\FormFacade;

class Form extends FormFacade
{
    /**
     * входные данные
     * @var array|null
     */
    protected $data = array();

    /**
     * выходные данные
     * @var array
     */
    protected $form = array();

    /**
     * @param null $data
     */
    public function __construct($data = null) {
        $this->data = isset($data['home']) ? $data['home'] : null;
    }

    /**
     * @return array
     */
    protected function getForm() {
        return $this->form;
    }
}