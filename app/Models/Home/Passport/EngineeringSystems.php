<?php namespace App\Models\Home\Passport;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineeringSystems
 * @package App\Models\Home\Passport
 */
class EngineeringSystems extends Model {

    const TABLE_NAME = 'homes_passport_engineering_systems';

    protected $fillable = array(
        'home_id',
        //    Система электроснабжения
        'electrical_type',              //    Тип системы электроснабжения
        'electronic_number_entries',    //    Количество вводов в дом, ед.
        //    Система теплоснабжения
        'heating_type',                 //    Тип системы теплоснабжения
        //    Система горячего водоснабжения
        'hot_water_type',               //    Тип системы горячего водоснабжения
        //    Система холодного водоснабжения
        'cold_water_type',              //    Тип системы холодного водоснабжения
        //    Система водоотведения
        'drainage_type',                //    Тип системы водоотведения
        'cesspools_volume',             //    Объем выгребных ям, куб. м.
        //    Система газоснабжения
        'gas_supply_type',              //    Тип системы газоснабжения
        //    Cистема вентиляции
        'ventilation_type',             //    Тип системы вентиляции
        //    Система пожаротушения
        'fire_extinguishing_type',      //    Тип системы пожаротушения
        //    Система водостоков
        'drains_type',//    Тип системы водостоков
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}