<?php namespace App\Models\Home\Passport;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Elevators
 * @package App\Models\Home\Passport
 */
class Elevators extends Model {

    const TABLE_NAME = 'homes_passport_elevators';

    protected $fillable = array(
        'home_id',
        'elevator_number',              //    Номер лифта
        'entrance_number',              //    Номер подъезда
        'lift_type',                    //    Тип лифта
        'year_commissioning',           //    Год ввода в эксплуатацию
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}