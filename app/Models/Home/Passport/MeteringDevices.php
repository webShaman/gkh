<?php namespace App\Models\Home\Passport;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MeteringDevices
 * @package App\Models\Home\Passport
 */
class MeteringDevices extends Model {

    const TABLE_NAME = 'homes_passport_metering_devices';

    protected $fillable = array(
        'home_id',
        'resources_public_type',    //    Вид коммунального ресурса
        'meters_installed',         //    Наличие прибора учета
        'unit_measure',             //    Единица измерения
        'date_start_up',            //    Дата ввода в эксплуатацию
        'metering_device_type',     //    Тип прибора учета
        'date_checking_replacing',  //    Дата поверки / замены прибора учета
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}