<?php
namespace App\Models\Home\Passport;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StructuralElements
 * @package App\Models\Home\Passport
 */
class StructuralElements extends Model {

    const TABLE_NAME = 'homes_passport_structural_elements';

    protected $fillable = array(
        'home_id',
        //    Фундамент
        'foundation_type',              //    Тип фундамента
        //    Стены и перекрытия
        'overlap_type',                 //    Тип перекрытий
        'material_bearing_walls',       //    Материал несущих стен
        //    Подвал
        'basement_area',                //    Площадь подвала по полу, кв.м
        //    Мусоропроводы
        'chutes_type',                  //    Тип мусоропровода
        'chutes_number',                //    Количество мусоропроводов, ед.
        //    Фасады
        'facade_type',                  //    Тип фасада
        //    Крыши
        'roof_type',                    //    Тип крыши
        'loft_type',                    //    Тип кровли
        //    Иное оборудование / конструктивный элемент
        'equipment_type',               //    Вид оборудования / конструктивного элемента
        'equipment_description',        //    Описание дополнительного оборудования / конструктивного элемента
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}