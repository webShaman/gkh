<?php
namespace App\Models\Home\Passport;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GeneralInformation
 * @package App\Models\Home\Passport
 */
class GeneralInformation extends Model {

    const TABLE_NAME = 'homes_passport_general_information';

    protected $fillable = array(
        'home_id',
        'year_built',                               //    Год постройки
        'year_commissioning',                       //    Год ввода в эксплуатацию
        'residents_number',                         //    Количество жителей
        'series',                                   //    Серия, тип проекта
        'type',                                     //    Тип дома
        'description_position',                     //    Описание местоположения
        'name_house',                               //    Индивидуальное наименование дома
        'method_overhaul',                          //    Способ формирования фонда капитального ремонта
        'floors_number_max',                        //    наибольшее, ед.
        'floors_number_min',                        //    наименьшее, ед.
        'entrances_number',                         //    Количество подъездов, ед
        'lifts_number',                             //    Количество лифтов, ед
        'rooms_number',                             //    Количество помещений, в том числе:
        'rooms_number_living',                      //    жилых, ед.
        'rooms_number_no_living',                   //    нежилых, ед.
        'total_area',                               //    Общая площадь дома, в том числе, кв.м:
        'total_area_living',                        //    общая площадь жилых помещений, кв.м
        'total_area_no_living',                     //    общая площадь нежилых помещений, кв.м
        'total_area_common_property',               //    общая площадь помещений, входящих в состав общего имущества, кв.м
        'parcel_area',                              //    площадь земельного участка, входящего в состав общего имущества в многоквартирном доме, кв.м
        'parcel_parking_area',                      //    площадь парковки в границах земельного участка, кв.м
        'cadastral_number',                         //    кадастровый номер
        'energy_efficiency_class',                  //    Класс энергетической эффективности
        'beautification_playground_children',       //    детская площадка
        'beautification_playground_sports',         //    спортивная площадка
        'beautification_other',                     //    другое
        'additional_information',                   //    Дополнительная информация
    );
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}