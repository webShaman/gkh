<?php namespace App\Models\Home\Report;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineeringSystems
 * @package App\Models\Home\Passport
 */
class Information extends Model {

    const TABLE_NAME = 'homes_report_information';

    protected $fillable = array(
        'home_id',
        'charges_begin',                    //    1 Ававнсовые платежи потребителей (на начало периода), руб.
        'carryover_begin',                  //    2	Переходящие остатки денежных средств (на начало периода), руб.
        'debt_begin',                       //    3	Задолженность потребителей (на начало периода), руб.
        //    4	Начислено за услуги (работы) по содержанию и текущему ремонту, в том числе:
        'charged_contents',                 //    — за содержание дома, руб.
        'charged_maintenance',              //    — за текущий ремонт, руб.
        'charged_management',               //    — за услуги управления, руб.
        //    5	Получено денежных средств, в том числе:
        'obtained_cash_owners',             //    — денежных средств от собственников/нанимателей помещений, руб.
        'obtained_contributions_owners',    //    — целевых взносов от собственников/нанимателей помещений, руб.
        'obtained_subsidies_owners',        //    — субсидий, руб.
        'obtained_common_property_owners',  //    — денежных средств от использования общего имущества, руб.
        'obtained_other_income',            //    — прочие поступления, руб.
        'total_cash',                       //    6	Всего денежных средств с учетом остатков, руб.
        'charges_end',                      //    7	Авансовые платежи потребителей (на конец периода), руб.
        'carryover_end',                    //    8	Переходящие остатки денежных средств (на конец периода), руб.
        'debt_end',                         //    9	Задолженность потребителей (на конец периода), руб.
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}