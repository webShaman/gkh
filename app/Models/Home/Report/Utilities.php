<?php namespace App\Models\Home\Report;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineeringSystems
 * @package App\Models\Home\Passport
 */
class Utilities extends Model {

    const TABLE_NAME = 'homes_report_utilities';

    protected $fillable = array(
        'home_id',
        'advance_payments_start',       //    1 Авансовые платежи потребителей (на начало периода), руб.
        'carryover_payments_start',     //    2	Переходящие остатки денежных средств (на начало периода), руб.
        'debt_start',                   //    3	Задолженность потребителей (на начало периода), руб.
        'advance_payments_end',         //    4	Авансовые платежи потребителей (на конец периода), руб.
        'carryover_payments_end',       //    5	Переходящие остатки денежных средств (на конец периода), руб.
        'debt_end',                     //    6	Задолженность потребителей (на конец периода), руб.
        'claim_number_incoming',        //    7	Количество поступивших претензий, ед.
        'claim_number_satisfied',       //    8	Количество удовлетворенных претензий, ед.
        'claim_number_denied',          //    9	Количество претензий, в удовлетворении которых отказано, ед.
        'recalculation',                //    10 Сумма произведенного перерасчета, руб.
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}