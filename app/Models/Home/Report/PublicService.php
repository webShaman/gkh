<?php namespace App\Models\Home\Report;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Elevators
 * @package App\Models\Home\Passport
 */
class PublicService extends Model
{
    const TABLE_NAME = 'homes_report_public_service';

    const SEWAGE =      'sewage';
    const COLD_WATER =  'cold_water';
    const HOT_WATER =   'hot_water';
    const GAS =         'gas';
    const ELECTRIC =    'electric';
    const HEATING =     'heating';

    /** @var array  view_public  */
    public static $viewPublic = [
        self::SEWAGE        => 'Водоотведение',
        self::COLD_WATER    => 'Холодное водоснабжение',
        self::HOT_WATER     => 'Горячее водоснабжение',
        self::GAS           => 'Газоснабжение',
        self::ELECTRIC      => 'Электроснабжение',
        self::HEATING       => 'Отопление',
    ];

    protected $fillable = array(
        'home_id',
        'view_public',          //    Вид коммунальной услуги
        'fact_providing',       //    Факт предоставления услуги
        'unit_measure',         //    Единица измерения
        'charges_customers',    //    Начислено потребителям (руб.)

        'total_consumption',    //    1	Общий объем потребления, нат. показ.
        'paid_consumers ',      //    2	Оплачено потребителями, руб.
        'debt_consumers ',      //    3	Задолженность потребителей, руб.
        'accrued_supplier',     //    4	Начислено поставщиком (поставщиками) коммунального ресурса, руб.
        'paid_supplier',        //    5	Оплачено поставщику (поставщикам) коммунального ресурса, руб.
        'payable_suppliers',    //    6	Задолженность перед поставщиком (поставщиками) коммунального ресурса, руб.
        'fines',                //    7	Суммы пени и штрафов, уплаченные поставщику(поставщикам) коммунального ресурса, руб.
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}