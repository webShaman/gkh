<?php namespace App\Models\Home\Report;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineeringSystems
 * @package App\Models\Home\Passport
 */
class ClaimWork extends Model {

    const TABLE_NAME = 'homes_report_claim_work';

    protected $fillable = array(
        'home_id',
        'sent_claims',      //    1	Направлено претензий потребителям-должникам
        'sent_suits',       //    2	Направлено исковых заявлений
        'obtained_funds',   //    3	Получено денежных средств по результатам претензионно-исковой работы
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}