<?php namespace App\Models\Home\Report;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineeringSystems
 * @package App\Models\Home\Passport
 */
class Claim extends Model {

    const TABLE_NAME = 'homes_report_claim';

    protected $fillable = array(
        'home_id',
        'number_incoming',          //    1	Количество поступивших претензий, ед.
        'number_satisfied',         //    2	Количество удовлетворенных претензий, ед.
        'number_denied',            //    3	Количество претензий, в удовлетворении которых отказано, ед.
        'recalculation',            //    4	Сумма произведенного перерасчета, руб.

    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}