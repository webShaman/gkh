<?php namespace App\Models\Home\Report;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineeringSystems
 * @package App\Models\Home\Passport
 */
class WorkPerformed extends Model {
    const TABLE_NAME = 'homes_report_work_performed';

    const MANAGEMENT            = 'management';
    const MAINTENANCE_PREMISES  = 'maintenance_premises';
    const EXPORT_WASTE          = 'export_waste';
    const STRUCTURAL_ELEMENTS   = 'structural_elements';
    const EQUIPMENT             = 'equipment';
    const ELEVATOR              = 'elevator';
    const ACCIDENTS             = 'accidents';
    const DISINFESTATION        = 'disinfestation';
    const CONTENT_LAND          = 'content_land';

    /** @var array  view_public  */
    public static $viewPublic = [
        self::MANAGEMENT                => 'Работы (услуги) по управлению многоквартирным домом',
        self::MAINTENANCE_PREMISES      => 'Работы по содержанию помещений, входящих в состав общего имущества в многоквартирном доме',
        self::EXPORT_WASTE              => 'Работы по обеспечению вывоза бытовых отходов',
        self::STRUCTURAL_ELEMENTS       => 'Работы по содержанию и ремонту конструктивных элементов (несущих конструкций и ненесущих конструкций) многоквартирных домов',
        self::EQUIPMENT                 => 'Работы по содержанию и ремонту оборудования и систем инженерно-технического обеспечения, входящих в состав общего имущества в многоквартирном доме)',
        self::ELEVATOR                  => 'Работы по содержанию и ремонту лифта (лифтов) в многоквартирном доме',
        self::ACCIDENTS                 => 'Обеспечение устранения аварий на внутридомовых инженерных системах в многоквартирном доме',
        self::DISINFESTATION            => 'Проведение дератизации и дезинсекции помещений, входящих в состав общего имущества в многоквартирном доме',
        self::CONTENT_LAND              => 'Работы по содержанию земельного участка с элементами озеленения и благоустройства, иными объектами, предназначенными для обслуживания и эксплуатации многоквартирного дома',
    ];

    protected $fillable = array(
        'home_id',
        'name',         //    Наименование работы
        'cost',         //    Годовая фактическая стоимость работ/услуг (руб.)
        'number',       //    Количество работ (ед.)
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}