<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class UtilityBills extends Model {

    const TABLE_NAME = 'homes_utility_bills';

    protected $fillable = array(
        'home_id',
        'income',                 //    Доход от поставки КУ за отчетный период, тыс. руб.
        'income_heating',         //    отопление
        'income_electricity',     //    электричество
        'income_gas',             //    газ
        'income_hot_water',       //    горячее водоснабжение
        'income_cold_water',      //    холодное водоснабжение
        'income_sanitation',      //    водоотведение
        'debt',                   //    Задолженность собственников за КУ за отчетный период, тыс. руб.
        'debt_heating',           //    отопление
        'debt_electricity',       //    электричество
        'debt_gas',               //    газ
        'debt_hot_water',         //    горячее водоснабжение
        'debt_cold_water',        //    холодное водоснабжение
        'debt_sanitation',        //    водоотведение
        'charged',                //    Взыскано с собственников за КУ за отчетный период, тыс. руб.
        'charged_heating',        //    отопление
        'charged_electricity',    //    электричество
        'charged_gas',            //    газ
        'charged_hot_water',      //    горячее водоснабжение
        'charged_cold_water',     //    холодное водоснабжение
        'charged_sanitation',     //    водоотведение
        'paid',                   //    Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс. руб.
        'paid_heating',           //    отопление
        'paid_electricity',       //    электричество
        'paid_gas',               //    газ
        'paid_hot_water',         //    горячее водоснабжение
        'paid_cold_water',        //    холодное водоснабжение
        'paid_sanitation',        //    водоотведение
        'paid_resources',             //    Оплачено ресурсов по счетам на общедомовые нужды за отчетный период, тыс. руб.
        'paid_resources_heating',     //    отопление
        'paid_resources_electricity', //    электричество
        'paid_resources_gas',         //    газ
        'paid_resources_hot_water',   //    горячее водоснабжение
        'paid_resources_cold_water',  //    холодное водоснабжение
        'paid_resources_sanitation',  //    водоотведение
    );
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }

}