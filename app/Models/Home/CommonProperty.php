<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class CommonProperty extends Model {

    const TABLE_NAME = 'homes_common_property';

    protected $fillable = array(
        'home_id',
        'common_income',              //    Доход от управления за отчетный период, тыс. руб.
        'common_income_property',     //    Доход от управления общим имуществом за отчетный период, тыс. руб.
        'common_costs',               //    Расходы на управление за отчетный период, тыс. руб.
        'common_debt',                //    Задолженность собственников за услуги управления за отчетный период, тыс. руб.
        'common_recover_owners',      //    Взыскано с собственников за услуги управления за отчетный период, тыс. руб.
        'common_payment',             //    Выплаты по искам и договорам управления за отчетный период, тыс. руб.
        'common_claims',              //    иски по компенсации нанесенного ущерба
        'common_claims_refusal',      //    иски по снижению платы в связи с неоказанием услуг
        'common_claims_shipment',     //    иски по снижению платы в связи с недопоставкой ресурсов
        'common_work_repair',         //    Объем работ по ремонту за отчетный период, тыс. руб.
        'common_work_improvement',    //    Объем работ по благоустройству за отчетный период, тыс. руб.
        'common_attracted_funds',     //    Объем привлеченных средств за отчетный период, тыс. руб.
        'common_subsidy',             //    субсидии
        'common_credits',             //    кредиты
        'common_financing_lease',     //    финансирование по договорам лизинга
        'common_financing_energy',    //    финансирование по энергосервисным договорам
        'common_contributions',       //    целевые взносы жителей
        'common_other_sources',       //    иные источники
    );
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }

}