<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class PassportGeneral extends Model {

    const TABLE_NAME = 'homes_passport_general';

    protected $fillable = array(
        'home_id',
        'series',
        'description_position',
        'individual_name',
        'type_home',
        'year_commissioning',
        'wall_material',
        'type_floors',
        'number_storeys',
        'number_entrances',
        'number_lifts',
        'total_area',
        'residential_area',
        'residential_area_private',
        'residential_area_municipal',
        'residential_area_state',
        '​​residential_non_area',
        'land_area',
        'territory_area',
        'inventory_number',
        'cadastral_number',
        'apartments_number',
        'residents_number',
        'accounts_number',
        'design_features',
        'thermal_characteristic_actual',
        'thermal_characteristic_regulatory',
        'energy_efficiency',
        'date_energy_audit',
        'date_privatization',
        'wear',
        'wear_foundation',
        'wear_wall',
        'wear_overlap',
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}