<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class StructuralElements extends Model {

    const TABLE_NAME = 'homes_structural_elements';

    protected $fillable = array(
        'home_id',
        'facade_area_total',
        'facade_area_plastered',
        'facade_area_not_plastered',
        'facade_area_panel',
        'facade_area_tiles',
        'facade_area_siding',
        'facade_area_wooden',
        'facade_area_thermal_finish',
        'facade_area_thermal_tiles',
        'facade_area_thermal_siding',
        'facade_area_otmostka',
        'facade_area_glazing_common',
        'facade_area_glazing_public',
        'facade_area_individual_glazing_wood',
        'facade_area_individual_glazing_plastic',
        'facade_area_metal_door',
        'facade_area_wood_door',
        'facade_year_renovation',
        'roof_area_total',
        'roof_area_ramp_slate',
        'roof_area_ramp_metal',
        'roof_area_ramp_different',
        'roof_area_flat',
        'roof_year_renovation',
        'basement_Information',
        'basement_area_total',
        'basement_year_renovation',
        'common_area_total',
        'common_year_renovation',
        'chutes_number',
        'chutes_year_renovation',
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}