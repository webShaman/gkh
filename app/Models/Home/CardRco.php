<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class CardRco extends Model {

    const TABLE_NAME = 'homes_card_rco';

    protected $fillable = array(
        'home_id',
        'rco_heating',        //    Поставщик отопления
        'rco_electricity',    //    Поставщик электричества
        'rco_gas',            //    Поставщик газа
        'rco_hot_water',      //    Поставщик горячей воды
        'rco_cold_water',     //    Поставщик холодной воды
        'rco_sanitation',     //    Поставщик водоотведения
    );
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }

}