<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class CardManagement extends Model {

    const TABLE_NAME = 'homes_card_management';

    protected $fillable = array(
        'home_id',
        'management_type_contract',       //    Тип договора управления
        'management_date_start',          //    Дата начала обслуживания дома
        'management_date_end',            //    Плановая дата прекращения обслуживания дома
        'management_performed_works',     //    Выполняемые работы
        'management_execution_works',     //    Выполнение обязательств
        'management_note',                //    Примечание
        'management_cost_services',       //    Стоимость услуг
        'management_means',               //    Средства ТСЖ или ЖСК
        'management_terms_services',      //    Условия оказания услуг ТСЖ или ЖСК
    );
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}