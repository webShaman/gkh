<?php namespace App\Models\Home\Management;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkPerformed
 * @package App\Models\Home\Management
 */
class WorkPerformed extends Model
{
    const TABLE_NAME = 'homes_manager_work_performed';

    protected $fillable = array(
        'home_id',
        'maintenance_premises ',        // Работы по содержанию помещений, входящих в состав общего имущества в многоквартирном доме
        'export_waste',                 // Работы по обеспечению вывоза бытовых отходов
        'maintenance_elevator',         // Работы по содержанию и ремонту лифта (лифтов) в многоквартирном доме
        'disinfestation',               // Проведение дератизации и дезинсекции помещений, входящих в состав общего имущества в многоквартирном доме
        'structural_elements',          // Работы по содержанию и ремонту конструктивных элементов (несущих конструкций и ненесущих конструкций) многоквартирных домов
        'maintenance_equipment',        // Работы по содержанию и ремонту оборудования и систем инженерно-технического обеспечения, входящих в состав общего имущества в многоквартирном доме)
        'content_land',                 // Работы по содержанию земельного участка с элементами озеленения и благоустройства, иными объектами, предназначенными для обслуживания и эксплуатации многоквартирного дома
        'eliminate_accidents',          // Обеспечение устранения аварий на внутридомовых инженерных системах в многоквартирном доме
        'management',                   // Работы (услуги) по управлению многоквартирным домом
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}