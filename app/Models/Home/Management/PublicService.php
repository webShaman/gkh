<?php namespace App\Models\Home\Management;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Elevators
 * @package App\Models\Home\Passport
 */
class PublicService extends Model
{
    const TABLE_NAME = 'homes_manager_public_service';

    const SEWAGE =      'sewage';
    const COLD_WATER =  'cold_water';
    const HOT_WATER =   'hot_water';
    const GAS =         'gas';
    const ELECTRIC =    'electric';
    const HEATING =     'heating';

    /** @var array  view_public  */
    public static $viewPublic = [
        self::SEWAGE        => 'Водоотведение',
        self::COLD_WATER    => 'Холодное водоснабжение',
        self::HOT_WATER     => 'Горячее водоснабжение',
        self::GAS           => 'Газоснабжение',
        self::ELECTRIC      => 'Электроснабжение',
        self::HEATING       => 'Отопление',
    ];

    protected $fillable = array(
        'home_id',
        'view_public',          //    Вид коммунальной услуги
        'fact_providing',       //    Факт предоставления услуги
        'tariff',               //    Тариф (цена) (руб.)
        'unit_measure',         //    Единица измерения
        'person',               //    Лицо, осуществляющее поставку коммунального ресурса

        'inn',                      //    1	ИНН лица, осуществляющего поставку коммунального ресурса
        'additional_information',   //    2	Дополнительная информация о лице, осуществляющего поставку коммунального ресурса
        'base_services',            //    3	Основание предоставления услуги
        'details_contract',         //    4	Реквизиты договора на поставку коммунального ресурса
        'act_tariff',               //    5	Нормативно-правовой акт, устанавливающий тариф
        'date_start_tariff',        //    6	Дата начала действия тарифа
        'differentiation',          //    7	Описание дифференциации тарифов в случаях, предусмотренных законодательством Российской Федерации о государственном регулировании цен (тарифов)
        'cost_history',             //    8	История стоимости услуги
        //    9	Норматив потребления коммунальной услуги в жилых помещениях:
        'dwellings_utilities',      //    — Норматив потребления коммунальной услуги в жилых помещениях
        'dwellings_unit',           //    — Единица измерения норматива потребления услуги
        'dwellings_advanced',       //    — Дополнительно
        //    10	Норматив потребления коммунальной услуги на общедомовые нужды:
        'obschedomovye_utilities',  //    — Норматив потребления коммунальной услуги на общедомовые нужды
        'obschedomovye_unit',       //    — Единица измерения норматива потребления услуги
        'obschedomovye_advanced',   //    — Дополнительно
        'regulatory_act',           //    11	Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}