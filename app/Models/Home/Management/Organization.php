<?php namespace App\Models\Home\Management;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Elevators
 * @package App\Models\Home\Passport
 */
class Organization extends Model
{
    const TABLE_NAME = 'homes_manager_organization';

    protected $fillable = array(
        'home_id',
        'date_start ',                  //  1  Дата начала управления
        'ground_control',               //  2  Основание управления
        // 3	Документ, подтверждающий выбранный способ управления:
        'document_name ',               // - Наименование документа
        'date_number_document',         // - Дата и номер документа
        // 4	Договор управления:
        'date_contract ',               // - Дата заключения договора управления
        // Копия договора управления
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}