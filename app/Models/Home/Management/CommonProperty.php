<?php namespace App\Models\Home\Management;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Elevators
 * @package App\Models\Home\Passport
 */
class CommonProperty extends Model
{
    const TABLE_NAME = 'homes_manager_common_property';

    protected $fillable = array(
        'home_id',
        'info',                        //  1  информация
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}