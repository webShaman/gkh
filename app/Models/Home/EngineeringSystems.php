<?php
namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class EngineeringSystems extends Model {

    const TABLE_NAME = 'homes_engineering_systems';

    protected $fillable = array(
        'home_id',
        'heating_system_type',
        'heating_number_elevator_units',
        'heating_length_pipelines',
        'heating_year_overhaul',
        'heating_number_dot_in',
        'heating_number_control_units',
        'heating_number_obschedomovyh',
        'heating_leave',
        'hotwater_system_type',
        'hotwater_length_pipelines',
        'hotwater_year_overhaul',
        'hotwater_number_dot_in',
        'hotwater_number_control_units',
        'hotwater_number_obschedomovyh',
        'hotwater_leave',
        'coldwater_system_type',
        'coldwater_length_pipelines',
        'coldwater_year_overhaul',
        'coldwater_number_dot_in',
        'coldwater_number_obschedomovyh',
        'coldwater_leave',
        'sewage_system_type',
        'sewage_length_pipelines',
        'sewage_year_overhaul',
        'electrical',
        'electrical_length_commons',
        'electrical_year_overhaul',
        'electrical_number_dot_in',
        'electrical_number_obschedomovyh',
        'electrical_leave',
        'gas_system_type',
        'gas_length_compliant',
        'gas_length_non_compliant',
        'gas_year_overhaul',
        'gas_number_dot_in',
        'gas_number_obschedomovyh',
        'gas_leave',
    );

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Home()
    {
        return $this->belongsTo('App\Models\Home');
    }
}