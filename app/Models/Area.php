<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpSpec\Exception\Exception;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class Area extends Model {
    const AVT = 'avtozavodsky';
    const KAN = 'kanavinsky';
    const LEN = 'leninsky';
    const MOS = 'moscowsky';
    const NIZ = 'nizhegosky';
    const PRI = 'prioksky';
    const SOV = 'sovietsky';
    const SOR = 'sormovsky';

    const AVT_RU = 'Автозаводский';
    const KAN_RU = 'Канавинский';
    const LEN_RU = 'Ленинский';
    const MOS_RU = 'Московский';
    const NIZ_RU = 'Нижегородский';
    const PRI_RU = 'Приокский';
    const SOV_RU = 'Советский';
    const SOR_RU = 'Сормовский';

    public static $arrays = array (
        self::AVT =>'Автозаводского',
        self::KAN =>'Канавинского',
        self::LEN =>'Ленинского',
        self::MOS =>'Московский',
        self::NIZ =>'Нижегородский',
        self::PRI =>'Приокский',
        self::SOV =>'Советский',
        self::SOR =>'Сормовский',
    );

    /**
     * @var string
     */
    protected $table = 'areas';

    public static function getIdByName($area)
    {
        try{
            $post = Area::where('name','=',$area)->get(['id']);
        } catch (Exception $e){
            return $e;
        }
        return $post;
    }

    public static function getNameById($area_id)
    {
        try{
            $post = Area::where('id','=',$area_id)->get(['name']);
        } catch (Exception $e){
            return $e;
        }
        return $post;
    }
    public static function getNameRuById($area_id)
    {
        try{
            $post = Area::where('id','=',$area_id)->get(['name_ru']);
        } catch (Exception $e){
            return $e;
        }
        return $post;
    }

    public static function getNameRuByName($area)
    {
    try{
        $post = Area::where('name','=',$area)->get(['name_ru']);
    } catch (Exception $e){
        return $e;
    }
    return $post;
    }

    public static function getNameRuRodByName($area)
    {
        try{
            $post = Area::where('name','=',$area)->get(['name_ru_rod']);
        } catch (Exception $e){
            return $e;
        }
        return $post;
    }

    public static function getNameRuDatByName($area)
    {
        try{
            $post = Area::where('name','=',$area)->get(['name_ru_dat']);
        } catch (Exception $e){
            return $e;
        }
        return $post;
    }

    /**
     * @param $name
     * @return mixed
     */
    static function getAreaOnName($name) {
        return self::$arrays[$name];
    }

    /**
     * @param $area_id
     * @return mixed
     */
    static function getNameRu_ById($area_id)
    {
        return self::where('id', $area_id)->first()->name_ru;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function home() {
        return $this->hasOne('App\Models\Home'); //area_id
    }
}