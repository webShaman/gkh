<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Area;
use PhpSpec\Exception\Exception;
use App\Models\Home\Passport\GeneralInformation;
use App\Models\Home\Passport\StructuralElements;
use App\Models\Home\Passport\EngineeringSystems;
use App\Models\Home\Passport\Elevators;
use App\Models\Home\Passport\MeteringDevices;
use App\Models\Home\Management\Organization;
use App\Models\Home\Management\WorkPerformed;
use App\Models\Home\Management\PublicService;
use App\Models\Home\Management\CommonProperty;
use App\Models\Home\Management\Repair;
use App\Models\Home\Report\Information;
use App\Models\Home\Report\WorkPerformed as ReportWorkPerformed;
use App\Models\Home\Report\Claim;
use App\Models\Home\Report\Utilities;
use App\Models\Home\Report\PublicService as ReportPublicService;
use App\Models\Home\Report\ClaimWork;

/**
 * Class Product
 * @package App\Models\OldMill
 */
class Home extends Model
{

    const TABLE_NAME = 'homes';

    protected $fillable = [
        'address',
        'area_id',
        'manager_id',
        'manager_inn',
        'map',
    ];

    public static $area_id = [
        '1' => Area::AVT_RU,
        '2' => Area::KAN_RU,
        '3' => Area::LEN_RU,
        '4' => Area::MOS_RU,
        '5' => Area::NIZ_RU,
        '6' => Area::PRI_RU,
        '7' => Area::SOV_RU,
        '8' => Area::SOR_RU,
    ];

    public $hashOneConnections = [
        GeneralInformation::TABLE_NAME => 'App\Models\Home\Passport\GeneralInformation',
        StructuralElements::TABLE_NAME => 'App\Models\Home\Passport\StructuralElements',
        EngineeringSystems::TABLE_NAME => 'App\Models\Home\Passport\EngineeringSystems',
        Elevators::TABLE_NAME => 'App\Models\Home\Passport\Elevators',
        MeteringDevices::TABLE_NAME => 'App\Models\Home\Passport\MeteringDevices',
        Organization::TABLE_NAME => 'App\Models\Home\Management\Organization',
        WorkPerformed::TABLE_NAME => 'App\Models\Home\Management\WorkPerformed',
        PublicService::TABLE_NAME => 'App\Models\Home\Management\PublicService',
        CommonProperty::TABLE_NAME => 'App\Models\Home\Management\CommonProperty',
        Repair::TABLE_NAME => 'App\Models\Home\Management\Repair',
        Information::TABLE_NAME => 'App\Models\Home\Report\Information',
        ReportWorkPerformed::TABLE_NAME => 'App\Models\Home\Report\WorkPerformed',
        Claim::TABLE_NAME => 'App\Models\Home\Report\Claim',
        Utilities::TABLE_NAME => 'App\Models\Home\Report\Utilities',
        ReportPublicService::TABLE_NAME => 'App\Models\Home\Report\PublicService',
        ClaimWork::TABLE_NAME => 'App\Models\Home\Report\ClaimWork',
        StructuralElements::TABLE_NAME => 'App\Models\Home\Passport\StructuralElements',
        EngineeringSystems::TABLE_NAME => 'App\Models\Home\Passport\EngineeringSystems',
        Elevators::TABLE_NAME => 'App\Models\Home\Passport\Elevators',
        MeteringDevices::TABLE_NAME => 'App\Models\Home\Passport\MeteringDevices',
        Organization::TABLE_NAME => 'App\Models\Home\Management\Organization',
        WorkPerformed::TABLE_NAME => 'App\Models\Home\Management\WorkPerformed',
        PublicService::TABLE_NAME => 'App\Models\Home\Management\PublicService',
        CommonProperty::TABLE_NAME => 'App\Models\Home\Management\CommonProperty',
        Repair::TABLE_NAME => 'App\Models\Home\Management\Repair',
        Information::TABLE_NAME => 'App\Models\Home\Report\Information',
        ReportWorkPerformed::TABLE_NAME => 'App\Models\Home\Report\WorkPerformed',
        Claim::TABLE_NAME => 'App\Models\Home\Report\Claim',
        Utilities::TABLE_NAME => 'App\Models\Home\Report\Utilities',
        ReportPublicService::TABLE_NAME => 'App\Models\Home\Report\PublicService',
        ClaimWork::TABLE_NAME => 'App\Models\Home\Report\ClaimWork',
    ];

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    public static function getAllManagersIdForArea($area_id)
    {
        try {
            $post = Home::where('area_id', '=', $area_id)->get(['manager_id']);
        } catch (Exception $e) {
            return $e;
        }
        return $post;

    }

    public static function get_All_Home_For_Manager($manager_id)
    {
        try {
            $post = Home::where('manager_id', '=', $manager_id)->get(['id', 'address', 'area_id']);
        } catch (Exception $e) {
            return $e;
        }
        return $post;
    }

    /**
     * @return GeneralInformation
     */
    public function passportGeneralInformation()
    {
        return $this->hasOne('App\Models\Home\Passport\GeneralInformation'); //home_id
    }

    /**
     * @return StructuralElements
     */
    public function passportStructuralElements()
    {
        return $this->hasOne('App\Models\Home\Passport\StructuralElements'); //home_id
    }

    /**
     * @return EngineeringSystems
     */
    public function passportEngineeringSystems()
    {
        return $this->hasOne('App\Models\Home\Passport\EngineeringSystems'); //home_id
    }

    /**
     * @return Elevators
     */
    public function passportElevators()
    {
        return $this->hasOne('App\Models\Home\Passport\Elevators'); //home_id
    }

    /**
     * @return MeteringDevices
     */
    public function passportMeteringDevices()
    {
        return $this->hasOne('App\Models\Home\Passport\MeteringDevices'); //home_id
    }

    /**
     * @return Organization
     */
    public function managementOrganization()
    {
        return $this->hasOne('App\Models\Home\Management\Organization'); //home_id
    }

    /**
     * @return WorkPerformed
     */
    public function managementWorkPerformed()
    {
        return $this->hasOne('App\Models\Home\Management\WorkPerformed'); //home_id
    }

    /**
     * @return PublicService
     */
    public function managementPublicService()
    {
        return $this->hasMany('App\Models\Home\Management\PublicService'); //home_id
    }

    /**
     * @return CommonProperty
     */
    public function managementCommonProperty()
    {
        return $this->hasOne('App\Models\Home\Management\CommonProperty'); //home_id
    }

    /**
     * @return Repair
     */
    public function managementRepair()
    {
        return $this->hasOne('App\Models\Home\Management\Repair'); //home_id
    }

    /**
     * @return Information
     */
    public function reportInformation()
    {
        return $this->hasOne('App\Models\Home\Report\Information'); //home_id
    }

    /**
     * @return ReportWorkPerformed
     */
    public function reportWorkPerformed()
    {
        return $this->hasMany('App\Models\Home\Report\WorkPerformed'); //home_id
    }

    /**
     * @return Claim
     */
    public function reportClaim()
    {
        return $this->hasOne('App\Models\Home\Report\Claim'); //home_id
    }

    /**
     * @return Utilities
     */
    public function reportUtilities()
    {
        return $this->hasOne('App\Models\Home\Report\Utilities'); //home_id
    }

    /**
     * @return ReportPublicService
     */
    public function reportPublicService()
    {
        return $this->hasMany('App\Models\Home\Report\PublicService'); //home_id
    }

    /**
     * @return ClaimWork
     */
    public function reportClaimWork()
    {
        return $this->hasOne('App\Models\Home\Report\ClaimWork'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area(){
        return $this->belongsTo('App\Models\Area');
    }

//***-------- устаревшие -------------------

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function elevator()
    {
        return $this->hasOne('App\Models\Home\Elevator'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function engineeringSystems()
    {
        return $this->hasOne('App\Models\Home\EngineeringSystems'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function passportGeneral()
    {
        return $this->hasOne('App\Models\Home\PassportGeneral'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function structuralElements1()
    {
        return $this->hasOne('App\Models\Home\StructuralElements'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function utilityBills()
    {
        return $this->hasOne('App\Models\Home\UtilityBills'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cardManagement()
    {
        return $this->hasOne('App\Models\Home\CardManagement'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cardRco()
    {
        return $this->hasOne('App\Models\Home\CardRco'); //home_id
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function commonProperty()
    {
        return $this->hasOne('App\Models\Home\CommonProperty'); //home_id
    }

}