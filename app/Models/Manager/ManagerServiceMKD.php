<?php namespace App\Models\Manager;
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.05.15
 * Time: 20:56
 */

use Illuminate\Database\Eloquent\Model;


class ManagerServiceMKD extends Model {

    public static $unguarded=true;

    protected $attributes = array(
        'scope_repair'=> NULL,                            // Объем работ по ремонту за отчетный период, тыс.руб.
        'scope_repair_less_25'=> NULL,                    // - по домам до 25 лет
        'scope_repair_26_50'=> NULL,                      // - по домам от 26 до 50 лет
        'scope_repair_51_75'=> NULL,                      // - по домам от 51 до 75 лет
        'scope_repair_more_76'=> NULL,                    // - по домам 76 лет и более
        'scope_repair_date_alarm'=> NULL,                 // - по аварийным домам
        'scope_improvement'=> NULL,                       // Объем работ по ремонту за отчетный период, тыс.руб.
        'scope_improvement_less_25'=> NULL,               // - по домам до 25 лет
        'scope_improvement_26_50'=> NULL,                 // - по домам от 26 до 50 лет
        'scope_improvement_51_75'=> NULL,                 // - по домам от 51 до 75 лет
        'scope_improvement_more_76'=> NULL,               // - по домам 76 лет и более
        'scope_improvement_date_alarm'=> NULL,            // - по аварийным домам
        'volume_attracted_funds'=> NULL,                  // Объем привлеченных средств за отчетный период, тыс.руб.
        'volume_attracted_funds_subsidies'=> NULL,        // - Субсидии
        'volume_attracted_funds_credits'=> NULL,          // - Кредиты
        'volume_attracted_funds_financing_leasing'=> NULL,// - Финансирование по договорам лизинга
        'volume_attracted_funds_financing_energy'=> NULL, // - Финансирование по энергосервисным договорам
        'volume_attracted_funds_earmarked'=> NULL,        // - Целевые взносы жителей
        'volume_attracted_funds_other'=> NULL,            // - Другие источники
        'paid_ru_indications'=> NULL,                     // Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.
        'paid_ru_indications_heating'=> NULL,             // - отопление
        'paid_ru_indications_electricity'=> NULL,         // - электричество
        'paid_ru_indications_gas'=> NULL,                 // - газ
        'paid_ru_indications_hot_water_supply'=> NULL,    // - горячее водоснабжение
        'paid_ru_indications_cold_water'=> NULL,          // - холодное водоснабжение
        'paid_ru_accounts'=> NULL,                        //  Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.
        'paid_ru_accounts_heating'=> NULL,                // - отопление
        'paid_ru_accounts_electricity'=> NULL,            // - электричество
        'paid_ru_accounts_gas'=> NULL,                   // - газ
        'paid_ru_accounts_hot_water_supply'=> NULL,       // - горячее водоснабжение
        'paid_ru_accounts_cold_water'=> NULL             // - холодное водоснабжение
    );

    protected $table = 'manager_service_mkd';

    public static function getOne($manager_id)
    {
        try{
            $post = ManagerServiceMKD::where('manager_id','=',$manager_id)->firstOrFail();
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function add($data)
    {
        try{
            $post = ManagerServiceMKD::create($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function edit($manager_id,$data)
    {
        try{
            $post = ManagerServiceMKD::where('manager_id','=',$manager_id)->update($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }
}