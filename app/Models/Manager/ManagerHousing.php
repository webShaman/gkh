<?php namespace App\Models\Manager;
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.05.15
 * Time: 20:46
 */

use Illuminate\Database\Eloquent\Model;


class ManagerHousing extends Model {

    public static $unguarded=true;

    protected $attributes = array(
    'number_peoples'=> NULL ,                                       // Число жителей в обслуживаемых домах
    'number_houses_end_time'=> NULL ,                               // Количество домов под управлением на отчетную дату
    'number_houses_end_time_tsg'=> NULL ,                           // - обслуживаемых ТСЖ
    'number_houses_end_time_tsg_and_organization'=> NULL ,          // - обслуживаемых по договору между ТСЖ и управляющей организацией
    'number_houses_end_time_tsg_and_owner'=> NULL ,                 // - обслуживаемых по договору между собственниками и управляющей организацией
    'number_houses_end_time_open_competition'=> NULL ,              // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
    'number_houses_start_time'=> NULL ,                             // Количество домов под управлением на начало периода
    'number_houses_start_time_tsg'=> NULL ,                         // - обслуживаемых ТСЖ
    'number_houses_start_time_tsg_and_organization'=> NULL ,        // - обслуживаемых по договору между ТСЖ и управляющей организацией
    'number_houses_start_time_tsg_and_owner'=> NULL ,               // - обслуживаемых по договору между собственниками и управляющей организацией
    'number_houses_start_time_open_competition'=> NULL ,            // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
    'common_area_houses_reporting_date'=> NULL ,                    // Общая площадь домов под управлением на отчетную дату, включая жилые и нежилые помещения, а также помещения общего пользования, тыс.кв.м.
    'common_area_houses_reporting_date_less_25'=> NULL ,            // - по домам до 25 лет
    'common_area_houses_reporting_date_26_50'=> NULL ,              // - по домам от 26 до 50 лет
    'common_area_houses_reporting_date_51_75'=> NULL ,              // - по домам от 51 до 75 лет
    'common_area_houses_reporting_date​_more_76'=> NULL ,            // - по домам 76 лет и более
    'common_area_houses_reporting_date_house_alarm'=> NULL ,        // - по аварийным домам
    'common_area_houses_start_period'=> NULL ,                      // Площадь домов под управлением на начало периода, тыс.кв.м.
    'common_area_houses_start_period_concluded_contract'=> NULL ,   // - изменение площади по заключенным договорам
    'common_area_houses_start_period_terminate_contract'=> NULL ,   // - изменение площади по расторгнутым договорам
    'average_term_service'=> NULL ,                                 // Средний срок обслуживания МКД, лет
    'average_term_service_less_25'=> NULL ,                         // - по домам до 25 лет
    'average_term_service_26_50'=> NULL ,                           // - по домам от 26 до 50 лет
    'average_term_service_51_75'=> NULL ,                           //  - по домам от 51 до 75 лет
    'average_term_service_more_76'=> NULL ,                         // - по домам 76 лет и более
    'average_term_service_house_alarm'=> NULL                       // - по аварийным домам
    );

    protected $table = 'manager_housing';

    public static function getOne($manager_id)
    {
        try{
            $post = ManagerHousing::where('manager_id','=',$manager_id)->firstOrFail();
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function getShortInfoForOne($manager_id)
    {
        try{
            $post = ManagerHousing::where('manager_id','=',$manager_id)->get(['number_houses_end_time','common_area_houses_reporting_date']);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function add($data)
    {
        try{
            $post = ManagerHousing::create($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function edit($manager_id,$data)
    {
        try{
            $post = ManagerHousing::where('manager_id','=',$manager_id)->update($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }
}