<?php namespace App\Models\Manager;
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.05.15
 * Time: 20:52
 */

use Illuminate\Database\Eloquent\Model;


class ManagerFinancialHighlights extends Model {

    public static $unguarded=true;

    protected $attributes = array(

        'start_time_reporting_period'=>NULL,                            //Дата начала отчетного периода
        'end_time_reporting_period'=>NULL,                              //Дата конца отчетного периода
        'total_debt'=>NULL,                                             //Общая задолженность управляющей организации (индивидуального предпринимателя) перед ресурсоснабжающими организациями за коммунальные ресурсы, руб.
        'total_debt_heat_energy'=>NULL,                                 //— Общая задолженность по тепловой энергии, руб.
        'total_debt_heat_energy_for_heating'=>NULL,                     //— Общая задолженность по тепловой энергии для нужд отопления, руб.
        'total_debt_heat_energy_for_water'=>NULL,                       //— Общая задолженность по тепловой энергии для нужд горячего водоснабжения, руб.
        'total_debt_hot_water_supply'=>NULL,                            //— Общая задолженность по горячей воде, руб.
        'total_debt_cold_water'=>NULL,                                  //— Общая задолженность по холодной воде, руб.
        'total_debt_sanitation'=>NULL,                                  //— Общая задолженность по водоотведению, руб.
        'total_debt_gas'=>NULL,                                         //— Общая задолженность по поставке газа, руб.
        'total_debt_electricity'=>NULL,                                 //— Общая задолженность по электрической энергии, руб.
        'total_debt_for_other_resources'=>NULL,                         //— Общая задолженность по прочим ресурсам (услугам), руб.

        'income_service'=> NULL,                                       // Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб.
        'income_service_houses_less_25'=> NULL,                        // - по домам до 25 лет
        'income_service_houses_26_50'=> NULL,                          // - по домам от 26 до 50 лет
        'income_service_houses_51_75'=> NULL,                          // - по домам от 51 до 75 лет
        'income_service_houses_more_76'=> NULL,                        // - по домам 76 лет и более
        'income_service_houses_alarm'=> NULL,                          // - по аварийным домам
        'income_common_property'=> NULL,                               // Сумма доходов, полученных от использования общего имущества за отчетный период, тыс.руб.
        'income_common_property_less_25'=> NULL,                       // - по домам до 25 лет
        'income_common_property_26_50'=> NULL,                         // - по домам от 26 до 50 лет
        'income_common_property_51_75'=> NULL,                         // - по домам от 51 до 75 лет
        'income_common_property_more_76'=> NULL,                       // - по домам 76 лет и более
        'income_common_property_alarm'=> NULL,                         // - по аварийным домам
        'income_utilities'=> NULL,                                     // Доход, полученный за отчетный период от предоставления коммунальных услуг без учета коммунальных ресурсов, поставленных
                                                                       // потребителям непосредственно поставщиками по прямым договорам, тыс.руб.
        'income_utilities_heating'=> NULL,                             // - отопление
        'income_utilities_electricity'=> NULL,                         // - электричество
        'income_utilities_gas'=> NULL,                                 // - газ
        'income_utilities_hot_water_supply'=> NULL,                    // - горячее водоснабжение
        'income_utilities_cold_water'=> NULL,                          // - холодное водоснабжение
        'income_utilities_sanitation'=> NULL,                          // - водоотведение
        'costs_service'=> NULL,                                        // Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб.
        'costs_service_less_25'=> NULL,                                // - по домам до 25 лет
        'costs_service_26_50'=> NULL,                                  // - по домам от 26 до 50 лет
        'costs_service_51_75'=> NULL,                                  // - по домам от 51 до 75 лет
        'costs_service_more_76'=> NULL,                                // - по домам 76 лет и более
        'costs_service_alarm'=> NULL,                                  // - по аварийным домам
        'costs_contract_service'=> NULL,                               // Выплаты по искам по договорам управления за отчетный период, тыс.руб.
        'costs_contract_service_damage'=> NULL,                        // - иски по компенсации нанесенного ущерба
        'costs_contract_service_unused_service'=> NULL,                // - иски по снижению платы в связи с неоказанием услуг
        'costs_contract_service_non_delivery_resources'=> NULL,        // - иски по снижению платы в связи с недопоставкой ресурсов
        'costs_resources_organization'=> NULL,                         // Выплаты по искам ресурсоснабжающих организаций за отчетный период, тыс.руб.
        'costs_resources_organization_heating'=> NULL,                 // - отопление
        'costs_resources_organization_electricity'=> NULL,             // - электричество
        'costs_resources_organization_gas'=> NULL,                     // - газ
        'costs_resources_organization_hot_water_supply'=> NULL,        // - горячее водоснабжение
        'costs_resources_organization_cold_water'=> NULL,              // - холодное водоснабжение
        'costs_resources_organization_sanitation'=> NULL,              // - водоотведение
        'net_assets'=> NULL
    );

    protected $table = 'manager_financial_highlights';

    public static function getOne($manager_id)
    {
        try{
            $post = ManagerFinancialHighlights::where('manager_id','=',$manager_id)->firstOrFail();
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function add($data)
    {
        try{
            $post = ManagerFinancialHighlights::create($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function edit($manager_id,$data)
    {
        try{
            $post = ManagerFinancialHighlights::where('manager_id','=',$manager_id)->update($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }
}