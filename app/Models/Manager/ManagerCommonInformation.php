<?php namespace App\Models\Manager;
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.05.15
 * Time: 20:49
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;


class ManagerCommonInformation extends Model {

    public static $unguarded=true;

    protected $attributes = array(
        'area_id'=>1,
        'map'=> NULL,                                           // Карта

        'address_dispatch_service'=> NULL,                      //— Адрес диспетчерской службы
        'contact_telephone_dispatch_service'=> NULL,            //— Контактные телефоны диспетчерской службы
        'mode_operation_dispatch_service'=> NULL,               //— Режим работы диспетчерской службы
        'fax'=> NULL,                                           //Факс
        'number_houses'=> NULL,                                 //Количество домов, находящихся в управлении , ед.
        'area_houses'=> NULL,                                   //Площадь домов, находящихся в управлении, кв. м

        'responsible_for_filling'=> NULL,                       // Ответственный за заполнение
        'full_name' => NULL,                                    // Полное наименование
        'short_name'=> NULL,                                    // Краткое наименование
        'organization_form'=> NULL,                             // Организационная форма
        'director'=> NULL,                                      // Руководитель
        'inn'=> NULL,                                           // ИНН
        'ogrn'=> NULL,                                          // ОГРН или ОГРНИП
        'date_ogrn'=> NULL,                                     // Дата присвоения ОГРН (ОГРНИП)
        'name_register_organ'=> NULL,                           // Наименование органа, принявшего решение о регистрации
        'legal_address'=> NULL,                                 // Юридический адрес
        'physical_address'=> NULL,                              // Фактический адрес
        'mailing_address'=> NULL,                               // Почтовый адрес
        'time_working'=> NULL,                                  // Режим работы
        'phone'=> NULL,                                         // Телефон
        'email_address'=> NULL,                                 // Электронный адрес
        'internet_site'=> NULL,                                 // Интернет сайт
        'share_in_authorized_capital_subject​​'=> NULL,           // Доля участия в уставном капитале Субъекта РФ, %
        'share_in_authorized_capital_municipal'=> NULL,         // Доля участия в уставном капитале муниципального образования, %
        'additional_information'=> NULL,                        // Дополнительная информация
        'self_regulatory_organizations'=> NULL,                 // Сведения об участии в саморегулируемых организациях
        'number_subject_where_organization_working'=> NULL,     // Количество Субъектов РФ, в которых организация осуществляет свою деятельность
        'number_municipal_where_organization_working'=> NULL,   // Количество муниципальных образований, в которых организация осуществляет свою деятельность
        'number_office'=> NULL,                                 // Количество офисов обслуживания граждан
        'regular_staffing'=> NULL,                              // Штатная численность на отчетную дату, чел.
        'regular_staffing_administrative'=> NULL,               // - административный персонал, чел.
        'regular_staffing_engineers'=> NULL,                    // - инженеры, чел.
        'regular_staffing_working'=> NULL,                      // - рабочий персонал, чел.
        'laid_off_during_period'=> NULL,                        // Уволено за отчетный период, чел.
        'laid_off_during_period_administrative'=> NULL,         // - административный персонал, чел.
        'laid_off_during_period_engineers'=> NULL,              // - инженеры, чел.
        'laid_off_during_period_working'=> NULL,                // - рабочий персонал, чел.
        'number_accidents'=> NULL,                              // Число несчастных случаев за отчетный период
        'number_administrative_responsibility'=> NULL,          // Число случаев привлечения организации к административной ответственности
        'copies_document_administrative_responsibility'=> NULL, // Копии документов о применении мер административного воздействия, а также мер, принятых для устранения нарушений, повлекших применение административных санкций
        'members_of_board'=> NULL,                              // Члены правления ТСЖ или ЖСК
        'members_audit_commission'=> NULL,                      // Члены ревизионной комиссии
        'more_information'=> NULL                               // Дополнительные сведения в произвольной форме
    );

    protected $table = 'manager_common_info';


    public static function getOne($id)
    {
        try{
            $manager = ManagerCommonInformation::where('id','=',$id)->firstOrFail();
        } catch (\Exception $e){
            return $e;
        }
        return $manager;
    }

    public static function get_Manager_Id_By_Inn($inn)
    {
        try{
            $short_info = ManagerCommonInformation::where('inn','like',"%$inn%")->get(['id']);
        } catch (\Exception $e){
            return $e;
        }
        return $short_info;
    }

    public static function get_Manager_Id_By_ShortName($ShortName)
    {
        try{
            $short_info = ManagerCommonInformation::where('short_name','like',"%$ShortName%")->get(['id']);
        } catch (\Exception $e){
            return $e;
        }
        return $short_info;
    }

    public static function get_Manager_Id_By_FullName($FullName)
    {
        try{
            $short_info = ManagerCommonInformation::where('full_name','like',"%$FullName%")->get(['id']);
        } catch (\Exception $e){
            return $e;
        }
        return $short_info;
    }


    public static function getShortInfoForOne($manager_id)
    {
        try{
            $short_info = ManagerCommonInformation::where('id','=',$manager_id)->get(['id','short_name','inn','regular_staffing','number_houses','area_houses']);
        } catch (\Exception $e){
            return $e;
        }
        return $short_info;
    }

    public static function getAllId()
    {
        try{
            $all_id = ManagerCommonInformation::get(['id']);
        } catch (\Exception $e){
            return $e;
        }
        return $all_id;
    }

    public static function add($data)
    {
        try{
             $post = ManagerCommonInformation::create($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function edit($id,$data)
    {
        try{
            $post = ManagerCommonInformation::where('id','=',$id)->update($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }
}