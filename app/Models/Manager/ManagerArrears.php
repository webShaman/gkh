<?php namespace App\Models\Manager;
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.05.15
 * Time: 20:53
 */

use Illuminate\Database\Eloquent\Model;


class ManagerArrears extends Model {

    public static $unguarded=true;

    protected $attributes = array(
        'past_account_owner_service_current_date'=> NULL,                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб.
        'past_account_owner_service_current_date_less_25'=> NULL,            // - по домам до 25 лет
        'past_account_owner_service_current_date_26_50'=> NULL,              // - по домам от 26 до 50 лет
        'past_account_owner_service_current_date_51_75'=> NULL,              // - по домам от 51 до 75 лет
        'past_account_owner_service_current_date_more_76'=> NULL,            // - по домам 76 лет и более
        'past_account_owner_service_current_date_alarm'=> NULL,              // - по аварийным домам
        'past_account_owner_service_start_period'=> NULL,                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению на начало отчетного периода, тыс.руб.
        'past_account_owner_utilities_current_date'=> NULL,                  // Просроченная задолженность собственников помещений и иных лиц, .... , за коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
        'past_account_owner_utilities_current_date_heating'=> NULL,          // - отопление
        'past_account_owner_utilities_current_date_electricity'=> NULL,      // - электричество
        'past_account_owner_utilities_current_date_gas'=> NULL,              // - газ
        'past_account_owner_utilities_current_date_hot_water_supply'=> NULL, // - горячее водоснабжение
        'past_account_owner_utilities_current_date_cold_water'=> NULL,       // - холодное водоснабжение
        'past_account_owner_utilities_current_date_sanitation'=> NULL,       // - водоотведение
        'past_account_owner_utilities_start_period'=> NULL,                  // Просроченная задолженность собственников помещений и иных лиц, .... за коммунальные услуги на начало отчетного периода, тыс.руб.
        'past_account_organ_utilities_current_date'=> NULL,                  // Просроченная задолженность организации за предоставленные коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
        'past_account_organ_utilities_current_date_heating'=> NULL,          // - отопление
        'past_account_organ_utilities_current_date_electricity'=> NULL,      // - электричество
        'past_account_organ_utilities_current_date_gas'=> NULL,              // - газ
        'past_account_organ_utilities_current_date_hot_water_supply'=> NULL, // - горячее водоснабжение
        'past_account_organ_utilities_current_date_cold_water'=> NULL,       // - холодное водоснабжение
        'past_account_organ_utilities_current_date_sanitation'=> NULL,       // - водоотведение
        'amount_collected_debt_service'=> NULL,                              //  Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
        //  проживающих в помещениях на законных основаниях за услуги по управлению, тыс.руб.
        'amount_collected_debt_service_less_25'=> NULL,                      // - по домам до 25 лет
        'amount_collected_debt_service_26_50'=> NULL,                        // - по домам от 26 до 50 лет
        'amount_collected_debt_service_51_75'=> NULL,                        // - по домам от 51 до 75 лет
        'amount_collected_debt_service_more_76'=> NULL,                      // - по домам 76 лет и более
        'amount_collected_debt_service_alarm'=> NULL,                        // - по аварийным домам
        'amount_collected_debt_utilities'=> NULL,                            // Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
        // проживающих в помещениях на законных основаниях за предоставленные коммунальные услуги, тыс.руб.
        'amount_collected_debt_utilities_heating'=> NULL,                    // - отопление
        'amount_collected_debt_utilities_electricity'=> NULL,                // - электричество
        'amount_collected_debt_utilities_gas'=> NULL,                        // - газ
        'amount_collected_debt_utilities_hot_water_supply'=> NULL,           // - горячее водоснабжение
        'amount_collected_debt_utilities_cold_water'=> NULL,                 // - холодное водоснабжение
        'amount_collected_debt_utilities_sanitation'=> NULL                  // - водоотведение
    );

    protected $table = 'manager_arrears';

    public static function getOne($manager_id)
    {
        try{
            $post = ManagerArrears::where('manager_id','=',$manager_id)->firstOrFail();
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function add($data)
    {
        try{
            $post = ManagerArrears::create($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }

    public static function edit($manager_id,$data)
    {
        try{
            $post = ManagerArrears::where('manager_id','=',$manager_id)->update($data);
        } catch (\Exception $e){
            return $e;
        }
        return $post;
    }
}