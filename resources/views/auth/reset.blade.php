@extends('auth.template')

@section('contentPassword')

                <h2>Сменить пароль</h2>
                <div class="uk-panel">
                    <form class="uk-form" role="form" method="POST" action="{{ url('/password/reset') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="token" value="{{ $token }}">


                        <div class="uk-form-row">
                            <label class="uk-form-label">Ваш E-mail <span class="tm-star">*</span></label>
                            <div class="uk-form-controls">
                                <input type="email" class="uk-width-1-2" name="email" value="{{ old('email') }}">
                                <div class="uk-form-help-block uk-text-small">Длина пароля не менее 6 символов</div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label">Новый пароль <span class="tm-star">*</span></label>
                            <div class="uk-form-controls">
                                <input type="password" class="uk-width-1-2" name="password" >
                                <div class="uk-form-help-block uk-text-small">Длина пароля не менее 6 символов</div>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label">Подтверждение нового пароля <span class="tm-star">*</span></label>
                            <div class="uk-form-controls">
                                <input type="password" class="uk-width-1-2" name="password_confirmation" >
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <button type="submit" class="uk-button uk-button-success">Сохранить</button>
                            <span class="uk-margin-left"><span class="tm-star">*</span>  —  Поля, обязательные для заполнения.	</span>
                        </div>
                    </form>
                </div>

@endsection
