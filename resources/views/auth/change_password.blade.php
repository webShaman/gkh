@extends('auth.template')

@section('contentPassword')

    <h2>Сменить пароль</h2>
    <div class="uk-panel">
        <form class="uk-form" role="form" method="POST" action="{{ url('/change/password') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="uk-form-row">
                <label class="uk-form-label">Старый пароль <span class="tm-star">*</span></label>
                <div class="uk-form-controls">
                    <input type="password" name="old_password" id="form-s-ip" placeholder="Введите пароль" class="uk-form-width-medium">
                    <div class="uk-form-help-block uk-text-small">Длина пароля не менее 6 символов</div>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Новый пароль <span class="tm-star">*</span></label>
                <div class="uk-form-controls">
                    <input type="password" name="password" id="form-s-ip" placeholder="Введите пароль" class="uk-form-width-medium">
                    <div class="uk-form-help-block uk-text-small">Длина пароля не менее 6 символов</div>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Подтверждение нового пароля <span class="tm-star">*</span></label>
                <div class="uk-form-controls">
                    <input type="password" name="password_confirmation" id="form-s-ip" placeholder="Повторите пароль" class="uk-form-width-medium">
                </div>
            </div>
            <div class="uk-form-row">
                <button class="uk-button uk-button-success" type="submit">Сохранить</button>
                <span class="uk-margin-left"><span class="tm-star">*</span>  —  Поля, обязательные для заполнения.	</span>
            </div>
        </form>
    </div>


@endsection