@extends('layout')

@section('content')
<div class="uk-container uk-container-center">
    <h1>Личный кабинет</h1>

    @if (count($errors) > 0)
    <div class="uk-alert uk-alert-warning" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
        <p>Вы ввели неверные данные</p>
        @foreach ($errors->keys() as $key)
            @if ($key != 'name' && $key != 'email' && $key != 'password')
                @foreach ($errors->get($key,null) as $error)
                    <p>{{ $error }}</p>
                @endforeach
            @endif
        @endforeach
    </div>
    @endif
    <div class="uk-margin-bottom">Внимание! При регистрации на портале "Реформа ЖКХ" Вы создаете учетную запись пользователя. Для регистрации организации с целью раскрытия информации, после подтверждения регистрации как пользователя, Вам необходимо будет подать заявку на регистрацию организации в Личном кабинете пользователя.</div>
    <form class="uk-form uk-form-stacked" role="form" method="POST" action="{{ url('/auth/register') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-name">Ваш логин*</label>
            <div class="uk-form-controls">
             <!--   <input type="text" id="form-name" placeholder="Введите ваш логин" class="uk-form-width-medium"> -->
            </div>
            <p class="uk-form-help-block uk-text-small">Не применяйте в нем знаков пунктуации, за исключением точек, знаков переноса и подчеркивания.</p>
        </div>
        <div class="uk-form-row">
            @if ($errors->has('email'))
            <div class="uk-alert uk-alert-warning" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                @foreach ($errors->get('email',null) as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            <label class="uk-form-label" for="form-s-ip">Адрес электронной почты (email) *</label>
            <div class="uk-form-controls">
                <input type="email" name="email" value="{{ old('email') }}" id="form-s-ip" placeholder="Введие ваш email" class="uk-form-width-medium">
            </div>
            <p class="uk-form-help-block uk-text-small">Все почтовые сообщения с сайта будут отсылаться на этот адрес. Адрес электронной почты не будет публиковаться и будет использован только по вашему желанию: для восстановления пароля или для получения новостей и уведомлений по электронной почте.</p>
        </div>
        <div class="uk-form-row">
            @if ($errors->has('name'))
            <div class="uk-alert uk-alert-warning" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                @foreach ($errors->get('name',null) as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            <label class="uk-form-label" for="form-s-ip">Полное имя (ФИО) *</label>
            <div class="uk-form-controls">
                <input type="text" name="name" value="{{ old('name') }}" id="form-s-ip" placeholder="Введите ваше полное имя" class="uk-form-width-medium">
            </div>
            <p class="uk-form-help-block uk-text-small">Как к вам могут обращаться.</p>
        </div>
        <div class="uk-form-row">
            @if ($errors->has('password'))
            <div class="uk-alert uk-alert-warning" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                @foreach ($errors->get('password',null) as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            <label class="uk-form-label" for="form-s-ip">Пароль *</label>
            <div class="uk-form-controls">
                <input type="password" name="password" id="form-s-ip" placeholder="Введите пароль" class="uk-form-width-medium">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-ip">Повторите пароль *</label>
            <div class="uk-form-controls">
                <input type="password" name="password_confirmation" id="form-s-ip" placeholder="Повторите пароль" class="uk-form-width-medium">
            </div>
        </div>
        <div class="uk-form-row">
            // Тут будет капча от гугл<br/>
            // <a href="https://www.google.com/recaptcha/intro/index.html">https://www.google.com/recaptcha/intro/index.html</a>
        </div>
        <div class="uk-form-row">
            <button class="uk-button" type="submit">Зарегистрироваться</button>
        </div>
    </form>
</div>

<!--
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
-->
@endsection
