@extends('layout')

@section('content')
<div class="tm-personal-account">
    <div class="uk-container uk-container-center">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <h1>Личный кабинет</h1>
        <div class="uk-grid">
            <div class="uk-width-1-4">
                <ul class="uk-nav uk-nav-side uk-nav-parent-icon" data-uk-nav>
                    <li class="uk-parent"><a href="#">Управляющая компания</a>
                        <ul class="uk-nav-sub">
                            <li><a href="/personal/mymanager/add_or_edit">Добавить новую компанию</a>
                            <li><a href="/personal/mymanager">Редактировать компанию</a>
                        </ul>
                    </li>
                    <li class="uk-parent"><a href="#">Дома компании</a>
                        <ul class="uk-nav-sub">
                            <li><a href="/personal/house/add">Добавить новый дом</a>
                            <li><a href="/personal/myhouse">Редактировать дом</a>
                        </ul>
                    </li>
                    <li class="uk-nav-divider"></li>
                    <li @if(isset($page)) @if($page=="change/personal_data") class="uk-active" @endif @endif><a href="/change/personal">Персональные данные</a></li>
                    <li @if(isset($page)) @if($page=="change/password") class="uk-active" @endif @endif><a href="/change/password">Сменить пароль</a></li>
                    <li><a href="{{ url('/auth/logout') }}">Выйти <i class="uk-icon-sign-out"></i></a></li>
                </ul>
            </div>
			<div lang="uk-width-3-4">
				@yield('contentPassword')
			</div>

        </div>
    </div>
</div>

@endsection