@extends('layout')

@section('content')

<div class="uk-container uk-container-center">
    <h1>Личный кабинет</h1>

    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form class="uk-form uk-form-stacked"  role="form" method="POST" action="{{ url('/password/email') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-s-ip">Адрес электронной почты (email) *</label>
            <div class="uk-form-controls">
                <input type="email" name="email" value="{{ old('email') }}" id="form-s-ip" placeholder="Введие ваш email" class="uk-form-width-medium">
            </div>
        </div>
        <div class="uk-form-row">
            <button class="uk-button" type="submit">Отправить ссылку на сброс пароля</button>
        </div>
    </form>
</div>

<!--
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Send Password Reset Link
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
-->
@endsection
