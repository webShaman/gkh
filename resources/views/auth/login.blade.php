@extends('layout')

@section('content')


<div class="tm-auth">
    <div class="uk-container uk-container-center">
        <h1>Авторизация</h1>
        @if (count($errors) > 0)
        <div class="uk-alert uk-alert-danger" data-uk-alert>
            <a href="" class="uk-alert-close uk-close"></a>
            <p>Вы ввели неверный логин или пароль.</p>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
        </div>
        @endif
        <div class="uk-grid" data-uk-grid-match="{target:'.uk-panel'}">
            <div class="uk-width-1-2">
                <div class="uk-panel uk-panel-box uk-panel-box-secondary">
                    <h3 class="uk-panel-title">Я зарегистрированный пользователь</h3>
                    <form class="uk-form" role="form" method="POST" action="{{ url('/auth/login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="form-login">Ваш E-Mail</label>
                            <input type="email" name="email" id="form-login" placeholder="Введите ваш E-Mail адрес" class="uk-width-1-1" value="{{ old('email') }}">
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="form-password">Пароль</label>
                            <div>
                                <input type="password" id="form-password" placeholder="Введите ваш пароль" class="uk-width-1-2" name="password">
                                <span class="uk-form-help-block uk-margin-left"><a href="{{ url('/password/email') }}">Забыли пароль?</a></span>
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <input type="checkbox" name="remember">
                            <label class="uk-form-label">Сохранить пароль</label>
                        </div>
                        <div class="uk-form-row">
                            <button type="submit" class="uk-button uk-button-success">Войти</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="uk-width-1-2">
                <div class="uk-panel uk-panel-box uk-panel-box-secondary">
                    <h3 class="uk-panel-title">Я новый пользователь</h3>
                    <p>Для продолжения оформления заказа Вам необходимо заполнить минимальный набор полей.</p>
                    <p>Это займет всего пару минут.</p>
                    <a class="uk-button uk-button-success" href="{{ url('/auth/register') }}">Зарегистрироваться</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
