@extends('auth.template')

@section('contentPassword')


    <h2>Персональные данные</h2>
    <div class="uk-panel">
        <form class="uk-form" role="form" method="POST" action="{{ url('/change/personal') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="uk-form-row">
                <label class="uk-form-label">Фамилия, имя, отчество <span class="tm-star">*</span></label>
                <div class="uk-form-controls">
                    <input type="text" name="name" @if(isset($name)) value="{{ $name }}" @else value="{{ old('name') }}" @endif id="form-s-ip" placeholder="Введите ваше полное имя" class="uk-width-1-2">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">E-mail <span class="tm-star">*</span></label>
                <div class="uk-form-controls">
                    <input type="email" name="email" id="form-login" placeholder="Введите ваш E-Mail адрес" class="uk-width-1-2" @if(isset($email)) value="{{ $email }}" @else value="{{ old('email') }}" @endif>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Введите пароль <span class="tm-star">*</span></label>
                <div class="uk-form-controls">
                    <input type="password" name="password" id="form-s-ip" placeholder="Введите пароль" class="uk-form-width-medium">
                    <div class="uk-form-help-block uk-text-small">Длина пароля не менее 6 символов</div>
                </div>
            </div>
            <div class="uk-form-row">
                <button class="uk-button uk-button-success">Сохранить</button>
                <span class="uk-margin-left"><span class="tm-star">*</span>  —  Поля, обязательные для заполнения.	</span>
            </div>
        </form>
    </div>


@endsection