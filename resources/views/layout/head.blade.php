<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
    <title>{{ $title  or 'Нижегородский портал ЖКХ' }}</title>
    <meta name="description" content="{{ $description or 'Нижегородский портал ЖКХ' }}">
    <meta name="keywords" content="{{ $keywords or 'Нижегородский портал ЖКХ'}}">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <meta name="robots" content="{{ $robots or ''}}">
    <meta name="viewport" content="width=1140">
    <meta name="author" content="www.webshaman.net">
	<meta name="copyright" content="webshaman">
	<!-- style -->
	<link rel="stylesheet" href="/css/uikit.almost-flat.min.css" />
    <link rel="stylesheet" href="/css/components/datepicker.almost-flat.min.css" />
    <link rel="stylesheet" href="/css/components/form-select.almost-flat.min.css" />
    <link rel="stylesheet" href="/css/components/slideshow.min.css" />
    <link rel="stylesheet" href="/css/components/slidenav.min.css" />
    <link rel="stylesheet" href="/css/components/form-file.min.css" />
    <link rel="stylesheet" href="/css/custom.css" />
</head>
<body>
<div class="tm-header-shadow">
    <div class="tm-header">
        <div class="uk-container uk-container-center">
            <div class="tm-headerbar uk-clearfix uk-vertical-align">
                <div class="uk-vertical-align-middle uk-width-1-1">

                    <div class="uk-float-left">
                        <div class="uk-vertical-align">
                            <a href="/">
                                <div class="uk-vertical-align-middle">
                                    <img src="/images/logo.png" width="100" height="100" alt="Нижегородский портал ЖКХ" title="Нижегородский портал ЖКХ" style="margin-top: -5px;">
                                </div>
                                <div class="uk-vertical-align-middle">
                                    <div class="uk-panel uk-margin-small-left">
                                        <div class="tm-logo-1">Нижегородский портал</div>
                                        <div class="tm-logo-2">ЖКХ</div>
                                    </div>
                                </div>
                                <div class="uk-vertical-align-middle">
                                    <div class="uk-panel tm-logo-3">
                                        Ассоциация организаций<br/>
                                        жилищно-коммунального хозяйства<br/>
                                        города Нижнего Новгорода
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="uk-float-right">
                        <div class="uk-vertical-align">
                            <div class="uk-vertical-align-middle">
                                <div class="uk-panel tm-header-phone">
                                    <div class="uk-h3">Бесплатный звонок по России</div>
                                    <div class="uk-h1">8 800 775 76 77</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-float-right">
                        <div class="uk-vertical-align">
                            <div class="uk-vertical-align-middle">
                                <div class="uk-panel tm-header-user">
                                    <ul class="uk-subnav">
                                        @if(Auth::check())
                                        <li><a class="cabinet" href="/personal">Личный кабинет</a>
                                        @else
	                                    <li><a class="cabinet" href="#auth-modal" data-uk-modal>Личный кабинет</a>
                                        @endif
                                        <li><a class="city" href="#">Нижний Новгород</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tm-navbar">
        <nav class="uk-navbar">
            <div class="uk-container uk-container-center">
                <ul class="uk-navbar-nav">
                    @foreach( $menu as $name => $element)
                    <li @if ($element['active']) class="uk-active" @endif><a href="/{{ $element['route'] }}">{{ $element['name_ru'] }}</a></li>
                    @endforeach
                </ul>
            </div>
        </nav>
    </div>
</div>
<div class="tm-main">
    <div class="tm-breadcrumb">
        <div class="uk-container uk-container-center">
            <ul class="uk-breadcrumb">
                @if ($breadcrumb != null)
                    @foreach($breadcrumb as $value)
                        @if ($value['count'] == 0)
                            <li><span>{{$value['name_ru']}}</span></li>
                        @else
                            <li><a href='/{{$value['route']}}'>{{$value['name_ru']}}</a></li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>