@extends('layout')

@section('content')
<div class="uk-container uk-container-center">
	<h1>Кинозал ЖКХ</h1>
    <div class="uk-grid" data-uk-grid-margin>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/aio1D2Qz4PI" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/X_uDboXXWT4" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/fyOryV_kmVs" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/AjXVp_3jm7g" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/8pSIB2yL9Cc" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/lO39h_-OiXE" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/JabwXmWzMrw" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/FyRjJZXWK3E" frameborder="0" allowfullscreen></iframe></div>
	    <div class="uk-width-1-3"><iframe width="333" height="222" src="https://www.youtube.com/embed/mjrS0PByWR8" frameborder="0" allowfullscreen></iframe></div>
    </div>
</div>

@stop