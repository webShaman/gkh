@extends('layout')

@section('content')
<div class="tm-directs">
    <div class="uk-container uk-container-center">
	    <div class="uk-vertical-align uk-clearfix tm-directs-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-margin-remove">Управляющие организации Нижнего Новгорода</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href="/search/manager"><i class="uk-icon-search"></i> Найти свою организацию</a></a></div>
		    </div>
		</div>
        <div class="uk-grid">
            <div class="uk-width-2-3">
				<div class="uk-grid uk-grid-divider">
                    <div class="uk-width-1-2">
	                    <a href="/mymanager/avtozavodsky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/avtozavod.png" width="70" height="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Автозаводский<br/>район</div>
			                </div>
		                </a>
	                </div>
	                <div class="uk-width-1-2">
	                    <a href="/mymanager/kanavinsky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/kanavinskiy.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Канавинский<br/>район</div>
			                </div>
		                </a>
	                </div>
				</div>
				<hr class="uk-grid-divider">
				<div class="uk-grid uk-grid-divider">
					<div class="uk-width-1-2">
	                    <a href="/mymanager/leninsky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/leninskiy.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Ленинский<br/>район</div>
			                </div>
		                </a>
	                </div>
	                <div class="uk-width-1-2">
	                    <a href="/mymanager/moscowsky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/moskovskiy.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Московский<br/>район</div>
			                </div>
		                </a>
	                </div>
				</div>
				<hr class="uk-grid-divider">
				<div class="uk-grid uk-grid-divider">
					<div class="uk-width-1-2">
	                    <a href="/mymanager/nizhegosky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/nizhegorodskiy.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Нижегородский<br/>район</div>
			                </div>
		                </a>
	                </div>
	                <div class="uk-width-1-2">
	                    <a href="/mymanager/prioksky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/priokskiy.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Приокский<br/>район</div>
			                </div>
		                </a>
	                </div>
				</div>
				<hr class="uk-grid-divider">
				<div class="uk-grid uk-grid-divider">
					<div class="uk-width-1-2">
	                    <a href="/mymanager/sovietsky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/sovetskiy.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Советский<br/>район</div>
			                </div>
		                </a>
	                </div>
	                <div class="uk-width-1-2">
	                    <a href="/mymanager/sormovsky/">
		                    <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-vertical-align">
			                    <img src="/images/sormovo.png" width="70" height="70">
			                    <div class="uk-vertical-align-middle uk-margin-left">Сормовский<br/>район</div>
			                </div>
		                </a>
	                </div>
				</div>


            </div>
            <div class="uk-width-1-3">
	            <p>В список <b>город Нижний Новгород</b> включены данные о <b>{{ $number_managers or 0 }}</b> Управляющих организациях</p>
	            <table class="tm-table-strip uk-margin-bottom">
                    <tbody>
                        <tr>
                            <td><span>обслуживающих</span></td>
                            <td><span class="uk-text-bold">{{ $number_all_houses or 0 }} дома</span></td>
                        </tr>
                        <tr>
                            <td><span>общей площадью</span></td>
                            <td><span class="uk-text-bold">{{ $total_area or 0 }} м<sup>2</sup></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Стандарт раскрытия</h3>
                    В соответствии с Постановлением Правительства РФ от 23.09.2010 года № 731 "Об утверждении стандарта раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами" организации обязаны раскрывать информацию о своей деятельности путем публикации ее на сайтах в сети Интернет, предназначенных для этих целей.
                    <div class="uk-text-right uk-margin-small-top"><a href="/standard"><u>Перейти к раскрытию</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Техническая поддержка</h3>
                    В случае возникновения трудностей в работе с порталом обратитесь в Службу технической поддержки по адресу электронной почты: <b>info@zhkh-nn.ru</b>
                    <div class="uk-text-right uk-margin-small-top"><a href="/support"><u>Подробнее</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Горячая линия</h3>
                    Для получения консультации по раскрытию информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, а также по вопросам капитального ремонта обратитесь на Горячую линию Ассоциации ЖКХ города Нижнего Новгорода по телефону <b>8 (800) 775 76 77</b> в рабочие дни с 9 до 19 ч. (время московское)
                </div>
            </div>
        </div>
    </div>
</div>
@stop