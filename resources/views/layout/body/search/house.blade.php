
<table class="uk-table uk-table-striped">
    <thead>
        <tr>
            <th class="uk-width-2-3">Адрес</th>
            <th class="uk-width-2-3">Управляющая организация</th>
        </tr>
    </thead>
    <tbody>
        @if ($homes)
            @foreach($homes as $home)
                <tr>
                    <td>
                        <a href="/myhouse/{{ $home['area']['name'] }}/{{$home['id']}}">
                            г. Нижний Новгород, {{ $home['area']['name_ru'] }} район, ул {{ $home['address'] }}
                        </a>
                    </td>
                    <td>
                        @if($home['manager']) <a class="uk-text-black" href="">{{ $home['manager'] }}</a>
                        @else нет данных
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="2">значений не найденно</td>
            </tr>
        @endif
    </tbody>
</table>

@if ($render)
    {!! $render !!}
@endif
