
<table id="Table" class="uk-table uk-table-striped">
    <thead>
        <tr>
            <th class="uk-width-1-3">Наименование</th>
            <th>ИНН</th>
            <th>Фактический адрес</th>
            <th>Количество домов под управлением</th>
        </tr>
    </thead>
    <tbody>
    @if (isset($managers) && count($managers))
    @foreach($managers as $manager)
    <tr>
        <td><a href="/mymanager/{{ $manager['id'] }}/{{ $manager['id'] }}">{{ $manager['short_name'] or "Нет данных" }}</a></td>
        <td>{{ $manager['inn'] or "Нет данных" }}</td>
        <td>{{ $manager['common_area_houses_reporting_date'] or "Нет данных" }}</td>
        <td>{{ $manager['number_houses_end_time'] or "Нет данных" }}</td>
    </tr>
    @endforeach
    @endif
    </tbody>
</table>
<div class="uk-clearfix">
    <div class="uk-float-left">
        <ul id="Action" class="uk-pagination">
            <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
            <li class="uk-active"><span>1</span></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><span>...</span></li>
            <li><a href="#">20</a></li>
            <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
        </ul>
    </div>
    <div class="uk-float-right">
        <ul id="Number" class="uk-subnav">
            <li class="uk-text-muted">Отображать по</li>
            <li class="uk-active"><a >10</a></li>
            <li><a >20</a></li>
            <li><a >50</a></li>
            <li><a >100</a></li>
        </ul>
    </div>
</div>


