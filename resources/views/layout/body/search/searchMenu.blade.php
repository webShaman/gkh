<div class="tm-search-home">
    <ul class="uk-subnav" data-uk-switcher="{connect:'#search-home'}">
        <li @if(isset($page)) @if($page=="house") class="uk-active" @endif  @endif ><a href="#">Найти свой дом</a></li>
        <li @if(isset($page)) @if($page=="manager") class="uk-active" @endif @endif ><a href="#">Найти управляющего своего дома</a></li>
    </ul>
    <ul id="search-home" class="uk-switcher">
        <li>
            <form class="uk-form" action="{{ route('house') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-5-6">

                        <div class="uk-form-icon uk-width-1-1">
                            <i class="uk-icon-search"></i>
                            <input type="text" placeholder="Введите свой субъект и адрес дома" class="uk-form-large uk-width-1-1" name="search">
                        </div>
                        <p class="uk-form-help-block uk-text-muted">Например: улица Ильинская, 12</p>
                    </div>

                    <div class="uk-width-1-6">
                        <button class="uk-button uk-button-large uk-width-1-1">Найти</button>
                    </div>
                </div>
            </form>
        </li>
        <li>
            <form class="uk-form" action="{{ url('/search/manager') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-5-6">
                        <div class="uk-form-icon uk-width-1-1">
                            <i class="uk-icon-search"></i>
                            <input type="text" name="search" placeholder="Введите ИНН или наименование организации" class="uk-form-large uk-width-1-1">
                        </div>
                        <p class="uk-form-help-block uk-text-muted">Например: ООО "Жилищник"</p>
                    </div>
                    <div class="uk-width-1-6">
                        <button class="uk-button uk-button-large uk-width-1-1">Найти</button>
                    </div>
                </div>
            </form>
        </li>
    </ul>
</div>