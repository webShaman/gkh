@extends('layout')

@section('content')

<div class="uk-container uk-container-center">
	<h1>Техническая поддержка</h1>
	<p>По вопросам технической поддержки Портала вы можете обратиться по адресу электронной почты <a href="mailto:info@zhkh-nn.ru">info@zhkh-nn.ru</a></p>
	<p>При отправке сообщения на info@zhkh-nn.ru в обязательном порядке необходимо указать:</p>
	<ul>
		<li>Контактную информацию для обратной связи;</li>
		<li>Реквизиты организации при наличии;</li>
		<li>Детальное описание проблемы;</li>
		<li>Порядок ваших действий;</li>
		<li>Скриншоты проблемы;</li>
	</ul>
	<p>Служба технической поддержки Портала также может уточнить необходимую для решения вопроса информацию.</p>
	<p>В случае повторных обращений в Службу технической поддержки Портала убедительная просьба оставлять историю переписки внутри обращения.</p>
</div>

@stop