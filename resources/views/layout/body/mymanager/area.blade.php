@extends('layout')

@section('content')
<div class="tm-area">
    <div class="uk-container uk-container-center">
	    <div class="uk-vertical-align uk-clearfix tm-directs-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-margin-remove">Управляющие организации {{ $area_ru_rod }} района</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href="/search/manager"><i class="uk-icon-search"></i> Найти свою организацию</a></a></div>
		    </div>
		</div>
        <div class="uk-grid">
            <div class="uk-width-2-3">
                <table id="Table" class="uk-table uk-table-hover">
                    <thead>
	                    <tr>
	                        <th class="uk-width-1-3">Название</th>
	                        <th>ИНН</th>
	                        <th>Количество домов</th>
	                        <th class="tm-nowrap">Площадь, м<sup>2</sup></th>
<!-- 	                        <th width="120">Рейтинг</th> -->
	                    </tr>
                    </thead>
                    <tbody>
                    @if (isset($managers) && count($managers))
                        @foreach($managers as $manager)
                        <tr>
                            <td><a href="/mymanager/{{ $area }}/{{ $manager['id'] }}">{{ $manager['short_name'] or "Нет данных" }}</a></td>
                            <td>{{ $manager['inn'] or "Нет данных" }}</td>
                            <td>{{ $manager['number_houses_end_time'] or "Нет данных" }}</td>
                            <td>{{ $manager['common_area_houses_reporting_date'] or "Нет данных" }}</td>
<!--
                            <td>
                                <div class="tm-box-star">
                                    <i class="uk-icon-star uk-icon-small"></i>
                                    <i class="uk-icon-star-half-empty uk-icon-small"></i>
                                    <i class="uk-icon-star-o uk-icon-small"></i>
                                    <i class="uk-icon-star-o uk-icon-small"></i>
                                    <i class="uk-icon-star-o uk-icon-small"></i>
                                </div>
                            </td>
-->
                        </tr>
                        @endforeach
                    @else
                    <p>Нет Управляющих организаций</p>
                    @endif

                    </tbody>
                </table>
                <ul id="Action" class="uk-pagination">
                    <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                    <li class="uk-active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><span>...</span></li>
                    <li><a href="#">20</a></li>
                    <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                </ul>
                
<!--
                <div class="uk-clearfix">
                    <div class="uk-float-left">
                        <ul id="Action" class="uk-pagination">
                            <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                            <li class="uk-active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                    <div class="uk-float-right">
                        <ul id="Number" class="uk-subnav">
                            <li class="uk-text-muted">Отображать по</li>
                            <li class="uk-active"><a >10</a></li>
                            <li><a >20</a></li>
                            <li><a >50</a></li>
                            <li><a >100</a></li>
                        </ul>
                    </div>
                </div>
-->
            </div>
            <div class="uk-width-1-3">
	            <p>В список <b>района {{ $area_ru }}</b> включены данные о <b>{{ $number_managers or 0 }}</b> Управляющих организациях</p>
	            <table class="tm-table-strip uk-margin-bottom">
                    <tbody>
                        <tr>
                            <td><span>обслуживающих</span></td>
                            <td><span class="uk-text-bold">{{ $number_all_houses or 0 }} дома</span></td>
                        </tr>
                        <tr>
                            <td><span>общей площадью</span></td>
                            <td><span class="uk-text-bold">{{ $total_area or 0 }} м<sup>2</sup></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Стандарт раскрытия</h3>
                    В соответствии с Постановлением Правительства РФ от 23.09.2010 года № 731 "Об утверждении стандарта раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами" организации обязаны раскрывать информацию о своей деятельности путем публикации ее на сайтах в сети Интернет, предназначенных для этих целей.
                    <div class="uk-text-right uk-margin-small-top"><a href="/standard"><u>Перейти к раскрытию</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Справка</h3>
                    Справочная информация для пользователей включает: инструкции, видеоуроки, часто задаваемые вопросы и ответы на них. Перед обращением в Службу поддержки просим убедиться, что интересующая информация не представлена в данном разделе
                    <div class="uk-text-right uk-margin-small-top"><a href="/help"><u>Перейти в раздел Справка</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Техническая поддержка</h3>
                    В случае возникновения трудностей в работе с порталом обратитесь в Службу технической поддержки по адресу электронной почты: <b>info@zhkh-nn.ru</b>
                    <div class="uk-text-right uk-margin-small-top"><a href="/support"><u>Подробнее</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Горячая линия</h3>
                    Для получения консультации по раскрытию информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, а также по вопросам капитального ремонта обратитесь на Горячую линию Ассоциации ЖКХ города Нижнего Новгорода по телефону <b>8 (800) 775 76 77</b> в рабочие дни с 9 до 19 ч. (время московское)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection