@extends('layout')

@section('content')
<div class="tm-manager">
    <div class="uk-container uk-container-center">
		<div class="uk-vertical-align uk-clearfix tm-area-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-h1 uk-margin-remove">{{ $manager_common_info->short_name  or "Нет данных"  }}</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href="/search/manager"><i class="uk-icon-search"></i> Найти свою организацию</a></div>
		    </div>
		</div>
		<div class="uk-grid" data-uk-grid-match="{target:'.uk-panel'}">
			<div class="uk-width-1-3">
				<div class="uk-panel uk-thumbnail">
					<script type="text/javascript" charset="utf-8" src="{{ $manager_common_info->map  or 'Нет данных'  }}"></script>
				</div>
			</div>
			<div class="uk-width-1-3">
				<div class="uk-panel uk-panel-box">
					<ul class="uk-list uk-list-space">
						<li>
							<b>Руководитель:</b>
							<div>{{ $manager_common_info->director  or "Нет данных"  }}</div>
						</li>
						<li>
							<b>Фактический адрес:</b>
							<div>{{ $manager_common_info->physical_address  or "Нет данных"  }}</div>
						</li>
						<li>
							<b>Телефон и e-mail:</b>
							<div>{{ $manager_common_info->phone  or "Нет данных"  }},  {{ $manager_common_info->email_address  or "Нет данных"  }}</div>
						</li>
						<li>
							<b>Ответственный за заполнение:</b>
							<div>{{ $manager_common_info->responsible_for_filling or "Нет данных"  }}</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="uk-width-1-3">
<!--
				<div class="uk-panel uk-panel-box tm-box-rating">
					<h3 class="uk-panel-title">Рейтинг <span class="uk-text-muted">(НА 23.04.2015)</span></h3>
					<div class="tm-box-star uk-margin">
						<i class="uk-icon-star uk-icon-large"></i>
						<i class="uk-icon-star-half-empty uk-icon-large"></i>
						<i class="uk-icon-star-o uk-icon-large"></i>
						<i class="uk-icon-star-o uk-icon-large"></i>
						<i class="uk-icon-star-o uk-icon-large"></i>
					</div>
					<table class="tm-table-strip">
						<tbody>
							<tr>
								<td><span class="uk-text-bold">Удовлетворительный</span></td>
								<td><span class="uk-text-bold">34</span></td>
							</tr>
							<tr>
								<td><span>Масштаб деятельности</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Финансовая устойчивость</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Эффективность</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Репутация</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Прозрачность</span></td>
								<td><span>34</span></td>
							</tr>
						</tbody>
					</table>
				</div>
-->
			</div>
		</div>
		<ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#manager-table'}">
            <li class="uk-active" ><a href="#">Список МКД</a></li>
            <li><a href="#">Текущая анкета</a></li>
<!--             <li><a href="#">Рейтинг</a></li> -->
<!--             <li><a href="#">Архив анкет</a></li> -->
        </ul>
        <ul id="manager-table" class="uk-switcher">
            <li>
            	<ul class="uk-tab" data-uk-tab="{connect:'#manager-table-mkd'}">
                    <li class="uk-active"><a href="#">Дома под управлением</a></li>
                    <li><a href="#">Дома, обслуживание которых завершено</a></li>
                </ul>
                <ul id="manager-table-mkd" class="uk-switcher uk-margin">
                    <li>
                    	<table id="Table" class="uk-table uk-table-striped">
		                    <thead>
			                    <tr>
			                        <th>Адрес</th>
			                        <th>Площадь, м<sup>2</sup></th>
			                        <th>Год ввода в эксплуатацию</th>
			                    </tr>
		                    </thead>
		                    <tbody>
                            @if ($grids)
                                @foreach($grids as $element)
                                <tr>
                                    <td><a href="/myhouse/{{ $element['area'][0]->name }}/{{ $element['id'] }}">г. Нижний Новгород, {{ $element['area_ru'][0]->name_ru }} р-н, ул. {{ $element['address'] }}</a></td>
                                    <td>{{ $element['total_area'] or 'не указанно'}}</td>
                                    <td>{{ $element['year'] or 'не указанно'}}</td>
                                </tr>
                                @endforeach
                            @endif
		                    </tbody>
                    	</table>
                    	<div class="uk-clearfix">
	                    	<div class="uk-float-left">
		                        <ul id="Action" class="uk-pagination">
		                            <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
		                            <li class="uk-active"><span>1</span></li>
		                            <li><a href="#">2</a></li>
		                            <li><a href="#">3</a></li>
		                            <li><a href="#">4</a></li>
		                            <li><span>...</span></li>
		                            <li><a href="#">20</a></li>
		                            <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
		                        </ul>
		                    </div>
						    <div class="uk-float-right">
							    <ul id="Number" class="uk-subnav">
								    <li class="uk-text-muted">Отображать по</li>
								    <li class="uk-active"><a >10</a></li>
								    <li><a >20</a></li>
								    <li><a >50</a></li>
								    <li><a >100</a></li>
								</ul>
						    </div>
						</div>
                    </li>
                    <li>
                    	<table class="uk-table uk-table-striped">
		                    <thead>
			                    <tr>
			                        <th class="uk-width-1-3">Адрес</th>
			                        <th>Площадь, м<sup>2</sup></th>
			                        <th>Год ввода в эксплуатацию</th>
			                        <th>Количество жителей</th>
			                        <th>Дата начала управления</th>
			                    </tr>
		                    </thead>
		                    <tbody>
		                    	<tr>
			                    	<td>Домов нет в списке</td>
			                    	<td></td>
			                    	<td></td>
			                    	<td></td>
									<td></td>
		                    	</tr>
		                    </tbody>
                    	</table>
                    </li>
                </ul>
            </li>
            <li>
            	<div class="uk-clearfix uk-margin-bottom">
				    <div class="uk-float-right"><a href="">Версия для печати</a></div>
				    <div class="uk-float-left"><b>Последнее изменение анкеты:</b> 17.03.2015</div>
				</div>
            	<ul class="uk-tab" data-uk-tab="{connect:'#manager-table-profile'}">
                    <li class="uk-active"><a href="#">Общие сведения об организации</a></li>
                    <li><a href="#">Сведения о лицензии</a></li>
                    <li><a href="#">Основные финансовые показатели</a></li>
                    <li><a href="#">Нарушения</a></li>
                </ul>
                <ul id="manager-table-profile" class="uk-switcher uk-margin">
<!-- ОБЩИЕ СВЕДЕНИЯ ОБ ОРГАНИЗАЦИИ -->	           
	                <li>
                    	<table class="tm-table-strip">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-1-2"><span>Фирменное наименование юридического лица <br/>(согласно уставу организации)</span></td>
                                    <td><span>{{ $manager_common_info->full_name  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Сокращенное наименование</span></td>
                                    <td><span>{{ $manager_common_info->short_name  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Организационно-правовая форма</span></td>
                                    <td><span>{{ $manager_common_info->organization_form  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>ФИО руководителя</span></td>
                                    <td><span>{{ $manager_common_info->director  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Идентификационный номер налогоплательщика (ИНН)</span></td>
                                    <td><span>{{ $manager_common_info->inn  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Основной государственный регистрационный номер / основной государственный регистрационный номер индивидуального предпринимателя (ОГРН/ ОГРНИП)</span></td>
                                    <td><span>{{ $manager_common_info->ogrn  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Место государственной регистрации юридического лица <br/>(адрес юридического лица)</span></td>
                                    <td><span>{{ $manager_common_info->legal_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td><span>Адрес фактического местонахождения органов управления</span></td>
                                    <td><span>{{ $manager_common_info->physical_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>9</span></td>
                                    <td><span>Почтовый адрес</span></td>
                                    <td><span>{{ $manager_common_info->mailing_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>10</span></td>
                                    <td><span>Режим работы, в т. ч. часы личного приема граждан</span></td>
                                    <td><span>{{ $manager_common_info->time_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>11</span></td>
                                    <td class="uk-text-bold"><span>Сведения о работе диспетчерской службы:</span></td>
                                    <td><span></span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Адрес диспетчерской службы</span></td>
                                    <td><span>{{ $manager_common_info->address_dispatch_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Контактные телефоны диспетчерской службы</span></td>
                                    <td><span>{{ $manager_common_info->contact_telephone_dispatch_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Режим работы диспетчерской службы</span></td>
                                    <td><span>{{ $manager_common_info->mode_operation_dispatch_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>12</span></td>
                                    <td><span>Контактные телефоны</span></td>
                                    <td><span>{{ $manager_common_info->phone  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>13</span></td>
                                    <td><span>Факс</span></td>
                                    <td><span>{{ $manager_common_info->fax  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>14</span></td>
                                    <td><span>Адрес электронной почты (при наличии)</span></td>
                                    <td><span>{{ $manager_common_info->email_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>15</span></td>
                                    <td><span>Официальный сайт в сети Интернет (при наличии)</span></td>
                                    <td><span>{{ $manager_common_info->internet_site  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>16</span></td>
                                    <td><span>Доля участия субъекта Российской Федерации <br/>в уставном капитале организации</span></td>
                                    <td><span>{{ $manager_common_info->share_in_authorized_capital_subject​​  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>17</span></td>
                                    <td><span>Доля участия муниципального образования в уставном капитале организации</span></td>
                                    <td><span>{{ $manager_common_info->share_in_authorized_capital_municipal  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>18</span></td>
                                    <td><span>Количество домов, находящихся в управлении , ед.</span></td>
                                    <td><span>{{ $manager_common_info->number_houses  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>19</span></td>
                                    <td><span>Площадь домов, находящихся в управлении, кв. м</span></td>
                                    <td><span>{{ $manager_common_info->area_houses  or "Нет данных"  }}</span></td>
                                </tr>
								<tr>
	                                <td><span>20</span></td>
                                    <td class="uk-text-bold"><span>Штатная численность (определяется по количеству заключенных трудовых договоров), в т .ч. административный персонал, инженеры, рабочие, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Штатная численность административного персонала, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing_administrative  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Штатная численность инженеров, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing_engineers  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Штатная численность рабочих, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>21</span></td>
                                    <td><span>Сведения о членстве управляющей организации, товарищества или кооператива в саморегулируемой организации.</span></td>
                                    <td><span>{{ $manager_common_info->self_regulatory_organizations  or "Нет данных"  }}</span></td>
                                </tr>
                            </tbody>
                    	</table>
	                </li>
<!-- / ОБЩИЕ СВЕДЕНИЯ ОБ ОРГАНИЗАЦИИ -->

<!-- СВЕДЕНИЯ О ЛИЦЕНЗИИ -->
					<li>
						<table class="uk-table">
						    <thead>
						        <tr>
						            <th>Номер лицензии</th>
						            <th>Дата получения лицензии</th>
						            <th>Орган, выдавший лицензию</th>
						            <th>Документ лицензии</th>
						        </tr>
						    </thead>
						    <tbody>


                            @if ( isset($tables_one) && count($tables_one))
                                @foreach ($tables_one as $row)
                                    <tr>
                                        <td>   {!! $row->number_license !!} </td>
                                        <td>   {!! $row->date_receipt_license !!}   </td>
                                        <td>   {!! $row->issuing_authority !!}   </td>
                                        <td><a href="{!! $row->documents_license !!}">документ лицензии</a></td>
                                    </tr>
                                @endforeach
                            @endif

						    </tbody>
						</table>
					</li>	
<!-- / СВЕДЕНИЯ О ЛИЦЕНЗИИ -->   
            
<!-- ОСНОВНЫЕ ФИНАНСОВЫЕ ПОКАЗАТЕЛИ	 -->                
	                <li>
	                	<table class="tm-table-strip">
                            <tbody>
	                            <tr>
	                                <td><span></span></td>
                                    <td><span>Дата начала отчетного периода</span></td>
                                    <td><span>{{ $manager_financial_highlights->start_time_reporting_period  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>Дата конца отчетного периода</span></td>
                                    <td><span>{{ $manager_financial_highlights->end_time_reporting_period  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-1-2"><span>Сведения о доходах, полученных за оказание услуг по управлению многоквартирными домами (по данным раздельного учета доходов и расходов), руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Сведения о расходах, понесенных в связи с оказанием услуг по управлению многоквартирными домами (по данным раздельного учета доходов и расходов), руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td class="uk-text-bold"><span>Общая задолженность управляющей организации (индивидуального предпринимателя) перед ресурсоснабжающими организациями за коммунальные ресурсы, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по тепловой энергии, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_heat_energy  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по тепловой энергии для нужд отопления, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_heat_energy_for_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по тепловой энергии для нужд горячего водоснабжения, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_heat_energy_for_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по горячей воде, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по холодной воде, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по водоотведению, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_sanitation  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по поставке газа, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_gas or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по электрической энергии, руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_electricity or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Общая задолженность по прочим ресурсам (услугам), руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->total_debt_for_other_resources or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td colspan="2" class="uk-text-left dotted-none">
	                                    <span class="uk-text-bold">Годовая бухгалтерская отчетность</span>
	                                    <div class="uk-text-muted uk-margin-small-top">Сведения в приложенных файлах.</div>
	                                    <div class="uk-panel tm-panel-box-table uk-width-1-2">
		                                    <ol class="uk-list-space">
			                                    <li><a href="">?????</a></li>
			                                    <li><a href="">????</a></li>
		                                    </ol>
	                                    </div>
	                                </td>
                                </tr>
                            </tbody>
	                	</table>
	                </li>
<!-- / ОСНОВНЫЕ ФИНАНСОВЫЕ ПОКАЗАТЕЛИ	 -->

<!-- НАРУШЕНИЯ -->
					<li> 
						Нарушения
					</li>
<!-- / НАРУШЕНИЯ -->
                </ul>
            </li>
<!--
            <li>
            	<table class="uk-table uk-table-hover">
                    <thead>
                        <tr>
		                    <th rowspan="2">Наименование показателя (группы показателей)</th>
		                    <th class="tm-nowrap">Значение показателя</th>
		                    <th colspan="2">Количество баллов</th>
		                </tr>
		                <tr>
		                    <th>По организации</th>
		                    <th>Максимальный балл</th>
		                    <th>По организации</th>
		                </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Общая оценка деятельности</td>
                            <td>
								<i class="uk-icon-star uk-icon-medium"></i>
								<i class="uk-icon-star-half-empty uk-icon-medium"></i>
								<i class="uk-icon-star-o uk-icon-medium"></i>
								<i class="uk-icon-star-o uk-icon-medium"></i>
								<i class="uk-icon-star-o uk-icon-medium"></i>
							</td>
                            <td>80.50</td>
                            <td>64.50</td>
                        </tr>
                        <tr>
	                        <td colspan="4" class="uk-text-bold">МАСШТАБ ДЕЯТЕЛЬНОСТИ</td>
                        </tr>
                        <tr>
	                        <td>Доход от управления, тыс. руб.</td>
	                        <td>23 810.60</td>
	                        <td>2.00</td>
	                        <td>2.00</td>
                        </tr>
                        <tr>
	                        <td>Доход от предоставления коммунальных услуг, тыс. руб. <span class="uk-text-muted">(не включает в себя обороты по поставке коммунальных ресурсов жителям непосредственно поставщикам таких ресурсов)</span></td>
	                        <td>0.00</td>
	                        <td>2.00</td>
	                        <td>0.50</td>
                        </tr>
                        <tr>
	                        <td>Общая площадь домов под управлением, тыс.кв.м. <span class="uk-text-muted">(включая жилые и нежилые помещения, а также помещения общего пользования)</span></td>
	                        <td>176.75</td>
	                        <td>2.00</td>
	                        <td>2.00</td>
                        </tr>
                        <tr>
	                        <td>Изменение общей площади домов за год, %</td>
	                        <td>76.70</td>
	                        <td>2.00</td>
	                        <td>1.50</td>
                        </tr>
                        <tr>
	                        <td>Количество домов под управлением</td>
	                        <td>23.00</td>
	                        <td>2.00</td>
	                        <td>1.50</td>
                        </tr>
                        <tr>
	                        <td>Изменение количества домов за год, %</td>
	                        <td>64.29</td>
	                        <td>2.00</td>
	                        <td>1.50</td>
                        </tr>
                        <tr>
	                        <td>Региональная сеть</td>
	                        <td>1,1,2</td>
	                        <td>2.00</td>
	                        <td>1.00</td>
                        </tr>
                        <tr>
	                        <td>Численность персонала</td>
	                        <td>54</td>
	                        <td>2.00</td>
	                        <td>2.00</td>
                        </tr>
                        <tr>
	                        <td colspan="4" class="uk-text-bold">ФИНАНСОВАЯ УСТОЙЧИВОСТЬ</td>
                        </tr>
                        <tr>
	                        <td>Прибыль от деятельности по управлению <span class="uk-text-muted">(в процентах от дохода)</span></td>
	                        <td>-3.38</td>
	                        <td>3.00</td>
	                        <td>1.00</td>
                        </tr>
                        <tr>
	                        <td>Просроченная задолженность жителей за услуги (работы) по управлению <span class="uk-text-muted">(в процентах от среднемесячного дохода от управления, по возрастанию)</span></td>
	                        <td>12.06</td>
	                        <td>4.00</td>
	                        <td>2.00</td>
                        </tr>
                        <tr>
	                        <td>Просроченная задолженность жителей за предоставленные коммунальные услуги <span class="uk-text-muted">(в процентах от среднемесячного дохода от предоставления коммунальных услуг, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>4.00</td>
	                        <td>4.00</td>
                        </tr>
                        <tr>
	                        <td>Просроченная задолженность организации перед поставщиками ресурсов <span class="uk-text-muted">(в процентах от среднемесячного дохода от предоставления коммунальных услуг, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Чистые активы, тыс. руб. <span class="uk-text-muted">(капитал)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td colspan="4" class="uk-text-bold">ЭФФЕКТИВНОСТЬ ДЕЯТЕЛЬНОСТИ</td>
                        </tr>
                        <tr>
	                        <td>Объем выполненных работ по ремонту и модернизации <span class="uk-text-muted">(в процентах от дохода от управления)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Объем выполненных работ по благоустройству <span class="uk-text-muted">(в процентах от дохода от управления)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Объем привлеченных средств на ремонт, модернизацию и благоустройство <span class="uk-text-muted">(кроме средств поступающих в составе доходов по договорам управления, в процентах от объема выполненных работ)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Объем ресурсов потребляемых по показаниям общедомовых приборов учета <span class="uk-text-muted">(в процентах от общей суммы платежей за коммунальные услуги)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Объем ресурсов потребляемых на общедомовые нужды <span class="uk-text-muted">(в процентах от общей суммы платежей за коммунальные услуги, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Сумма доходов, полученных от использования общего имущества <span class="uk-text-muted">(в процентах от доходов от управления)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Сумма взысканной просроченной задолженности жителей за услуги (работы) по управлению <span class="uk-text-muted">(в процентах к размеру такой просроченной задолженности на начало года)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Сумма взысканной просроченной задолженности жителей за коммунальные услуги <span class="uk-text-muted">(в процентах к размеру такой просроченной задолженности на начало года)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td colspan="4" class="uk-text-bold">РЕПУТАЦИЯ</td>
                        </tr>
                        <tr>
	                        <td>Средний срок обслуживания домов, лет</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Выплаты по искам собственников и жильцов <span class="uk-text-muted">(за период с начала года, в процентах от среднемесячного дохода от управления, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Выплаты по искам ресурсоснабжающих организаций <span class="uk-text-muted">(за период с начала года, в процентах от среднемесячного дохода от предоставления коммунальных услуг, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Текучесть кадров <span class="uk-text-muted">(увольнения, в процентах от численности сотрудников, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Число несчастных случаев среди сотрудников <span class="uk-text-muted">(за период с начала года, в процентах от численности сотрудников, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Число случаев привлечения организации к административной ответственности <span class="uk-text-muted">(в процентах от числа обслуживаемых домов, по возрастанию)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td colspan="4" class="uk-text-bold">ПРОЗРАЧНОСТЬ</td>
                        </tr>
                        <tr>
	                        <td>Полнота раскрытия дополнительных сведений об организации <span class="uk-text-muted">(число групп показателей)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                        <tr>
	                        <td>Полнота раскрытия сведений в разрезе многоквартирных домов <span class="uk-text-muted">(число групп показателей)</span></td>
	                        <td>0.00</td>
	                        <td>0.00</td>
	                        <td>0.00</td>
                        </tr>
                    </tbody>
                </table>
                <p class="uk-margin-large-top"><span class="uk-text-bold">Внимание.</span> <i>Данные предоставлены управляющей организацией. Ответственность за корректность предоставленных данных несет управляющая организация.</i></p>
            </li>
-->
<!--
            <li>
            	архив анкет
            </li>
-->
        </ul>
    </div>
</div>
@endsection