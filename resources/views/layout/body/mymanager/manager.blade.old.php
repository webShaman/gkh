@extends('layout')

@section('content')
<div class="tm-home">
    <div class="uk-container uk-container-center">
		<div class="uk-vertical-align uk-clearfix tm-area-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-h1 uk-margin-remove">{{ $manager_common_info->short_name  or "Нет данных"  }}</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href="{{ url('/search/manager') }}"><i class="uk-icon-search"></i> Найти свою организацию</a></div>
		    </div>
		</div>
		<div class="uk-grid" data-uk-grid-match="{target:'.uk-panel'}">
			<div class="uk-width-1-3">
				<div class="uk-panel uk-thumbnail">
					<script type="text/javascript" charset="utf-8" src="{{ $manager_common_info->map  or 'Нет данных'  }}"></script>
				</div>
			</div>
			<div class="uk-width-1-3">
				<div class="uk-panel uk-panel-box">
					<ul class="uk-list uk-list-space">
						<li>
							<b>Руководитель:</b>
							<div>{{ $manager_common_info->director  or "Нет данных"  }}</div>
						</li>
						<li>
							<b>Фактический адрес:</b>
							<div>{{ $manager_common_info->physical_address  or "Нет данных"  }}</div>
						</li>
						<li>
							<b>Телефон и e-mail:</b>
							<div>{{ $manager_common_info->phone  or "Нет данных"  }} ,  {{ $manager_common_info->email_address  or "Нет данных"  }}</div>
						</li>
						<li>
							<b>Ответственный за заполнение:</b>
							<div>{{ $manager_common_info->responsible_for_filling or "Нет данных"  }}</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="uk-width-1-3">
				<div class="uk-panel uk-panel-box tm-box-rating">
					<h3 class="uk-panel-title">Рейтинг <span class="uk-text-muted">(НА 23.04.2015)</span></h3>
					<div class="tm-box-star uk-margin">
						<i class="uk-icon-star uk-icon-large"></i>
						<i class="uk-icon-star-half-empty uk-icon-large"></i>
						<i class="uk-icon-star-o uk-icon-large"></i>
						<i class="uk-icon-star-o uk-icon-large"></i>
						<i class="uk-icon-star-o uk-icon-large"></i>
					</div>
					<table class="tm-table-strip">
						<tbody>
							<tr>
								<td><span class="uk-text-bold">Удовлетворительный</span></td>
								<td><span class="uk-text-bold">34</span></td>
							</tr>
							<tr>
								<td><span>Масштаб деятельности</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Финансовая устойчивость</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Эффективность</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Репутация</span></td>
								<td><span>34</span></td>
							</tr>
							<tr>
								<td><span>Прозрачность</span></td>
								<td><span>34</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#manager-table'}">
            <li class="uk-active" ><a href="#">Список МКД</a></li>
            <li><a href="#">Текущая анкета</a></li>
            <li><a href="#">Рейтинг</a></li>
<!--             <li><a href="#">Архив анкет</a></li> -->
        </ul>
        <ul id="manager-table" class="uk-switcher">
            <li>
            	<ul class="uk-tab" data-uk-tab="{connect:'#manager-table-mkd'}">
                    <li class="uk-active"><a href="#">Дома под управлением</a></li>
                    <li><a href="#">Дома, обслуживание которых завершено</a></li>
                </ul>
                <ul id="manager-table-mkd" class="uk-switcher uk-margin">
                    <li>
                    	<table id="Table" class="uk-table uk-table-striped">
		                    <thead>
			                    <tr>
			                        <th class="uk-width-1-3">Адрес</th>
			                        <th>Площадь, м<sup>2</sup></th>
			                        <th>Год ввода в эксплуатацию</th>
			                        <th>Количество жителей</th>
			                        <th>Дата начала управления</th>
			                    </tr>
		                    </thead>
		                    <tbody>
                            @if ($grids)
                                @foreach($grids as $element)
                                <tr>
                                    <td><a href="/myhouse/{{ $element['area'][0]->name }}/{{ $element['id'] }}">г. Нижний Новгород, {{ $element['area_ru'][0]->name_ru }} р-н, ул. {{ $element['address'] }}</a></td>
                                    <td>{{ $element['year'] or 'не указанно'}}</td>
                                    <td>{{ $element['total_area'] or 'не указанно'}}</td>
                                    <td>{{ $element['residents'] or 'не указанно'}}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            @endif
		                    </tbody>
                    	</table>
                    	<div class="uk-clearfix">
	                    	<div class="uk-float-left">
		                        <ul id="Action" class="uk-pagination">
		                            <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
		                            <li class="uk-active"><span>1</span></li>
		                            <li><a href="#">2</a></li>
		                            <li><a href="#">3</a></li>
		                            <li><a href="#">4</a></li>
		                            <li><span>...</span></li>
		                            <li><a href="#">20</a></li>
		                            <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
		                        </ul>
		                    </div>
						    <div class="uk-float-right">
							    <ul id="Number" class="uk-subnav">
								    <li class="uk-text-muted">Отображать по</li>
								    <li class="uk-active"><a >10</a></li>
								    <li><a >20</a></li>
								    <li><a >50</a></li>
								    <li><a >100</a></li>
								</ul>
						    </div>
						</div>
                    </li>
                    <li>
                    	<table class="uk-table uk-table-striped">
		                    <thead>
			                    <tr>
			                        <th class="uk-width-1-3">Адрес</th>
			                        <th>Площадь, м<sup>2</sup></th>
			                        <th>Год ввода в эксплуатацию</th>
			                        <th>Количество жителей</th>
			                        <th>Дата начала управления</th>
			                    </tr>
		                    </thead>
		                    <tbody>
		                    	<tr>
			                    	<td>Домов нет в списке</td>
			                    	<td></td>
			                    	<td></td>
			                    	<td></td>
									<td></td>
		                    	</tr>
		                    </tbody>
                    	</table>
                    </li>
                </ul>
            </li>
            <li>
            	<div class="uk-clearfix uk-margin-bottom">
				    <div class="uk-float-right"><a href="">Версия для печати</a></div>
				    <div class="uk-float-left">Последнее изменение анкеты: 17.03.2015</div>
				</div>
            	<ul class="uk-tab" data-uk-tab="{connect:'#manager-table-profile'}">
                    <li class="uk-active"><a href="#">Общие сведения об организации</a></li>
                    <li><a href="#">Жилищный фонд</a></li>
                    <li><a href="#">Основные финансовые показатели</a></li>
                    <li><a href="#">Задолженности</a></li>
                    <li><a href="#">Деятельность по управлению МКД</a></li>
                </ul>
                <ul id="manager-table-profile" class="uk-switcher uk-margin">
	                <li>
	                	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
                    	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-1-2"><span>Полное наименование <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->full_name  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Краткое наименование <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->short_name  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Организационная форма</span></td>
                                    <td><span>{{ $manager_common_info->organization_form  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Руководитель <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->director  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>ИНН (подробную информацию по ИНН 
можно получить на сайте http://egrul.nalog.ru/)</span></td>
                                    <td><span>{{ $manager_common_info->inn  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>ОГРН или ОГРНИП</span></td>
                                    <td><span>{{ $manager_common_info->ogrn  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>- Дата присвоения ОГРН (ОГРНИП)</span></td>
                                    <td><span>{{ $manager_common_info->date_ogrn  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>- Наименование органа, принявшего решение о регистрации</span></td>
                                    <td><span>{{ $manager_common_info->name_register_organ  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Юридический адрес</span></td>
                                    <td><span>{{ $manager_common_info->legal_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td><span>Фактический адрес <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->physical_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>9</span></td>
                                    <td><span>Почтовый адрес <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->mailing_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>10</span></td>
                                    <td><span>Режим работы <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->time_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>11</span></td>
                                    <td><span>Телефон <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->phone  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>12</span></td>
                                    <td><span>Электронный адрес <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->email_address  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>13</span></td>
                                    <td><span>Интернет сайт <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->internet_site  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>14</span></td>
                                    <td><span>Доля участия в уставном капитале Субъекта РФ, %</span></td>
                                    <td><span>{{ $manager_common_info->share_in_authorized_capital_subject​​  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>15</span></td>
                                    <td><span>Доля участия в уставном капитале муниципального образования, %</span></td>
                                    <td><span>{{ $manager_common_info->share_in_authorized_capital_municipal  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>16</span></td>
                                    <td colspan="2"><span class="uk-text-bold">Дополнительная информация</span>
                                        <div class="uk-text-muted uk-margin-small-top">{{ $manager_common_info->additional_information  or "Нет данных"  }}</div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>17</span></td>
                                    <td colspan="2"><span class="uk-text-bold">Сведения об участии в саморегулируемых организациях или в объединениях <br/>ТСЖ и ЖСК и наличии сертификатов соответствия стандартам обслуживания <span class="uk-badge uk-badge-danger">731</span></span>
                                        <div class="uk-text-muted uk-margin-small-top">{{ $manager_common_info->self_regulatory_organizations  or "Нет данных"  }}</div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>18</span></td>
                                    <td><span>Количество Субъектов РФ, в которых организация осуществляет свою деятельность</span></td>
                                    <td><span>{{ $manager_common_info->number_subject_where_organization_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>19</span></td>
                                    <td><span>Количество муниципальных образований, в которых организация осуществляет свою деятельность</span></td>
                                    <td><span>{{ $manager_common_info->number_municipal_where_organization_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>20</span></td>
                                    <td><span>Количество офисов обслуживания граждан</span></td>
                                    <td><span>{{ $manager_common_info->number_office  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>21</span></td>
                                    <td><span>Штатная численность на отчетную дату, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; административный персонал, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing_administrative  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; инженеры, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing_engineers  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; рабочий персонал, чел.</span></td>
                                    <td><span>{{ $manager_common_info->regular_staffing_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>22</span></td>
                                    <td><span>Уволено за отчетный период, чел.</span></td>
                                    <td><span>{{ $manager_common_info->laid_off_during_period  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; административный персонал, чел.</span></td>
                                    <td><span>{{ $manager_common_info->laid_off_during_period_administrative  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; инженеры, чел.</span></td>
                                    <td><span>{{ $manager_common_info->laid_off_during_period_engineers  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; рабочий персонал, чел.</span></td>
                                    <td><span>{{ $manager_common_info->laid_off_during_period_working  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>23</span></td>
                                    <td><span>Число несчастных случаев за отчетный период</span></td>
                                    <td><span>{{ $manager_common_info->number_accidents  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>24</span></td>
                                    <td><span>Число случаев привлечения организации к административной ответственности <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->number_administrative_responsibility  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>25</span></td>
                                    <td colspan="2">
	                                    <span class="uk-text-bold">Копии документов о применении мер административного воздействия, а также мер, <br/>принятых для устранения нарушений, повлекших применение административных санкций <span class="uk-badge uk-badge-danger">731</span></span>
	                                    <div class="uk-text-muted uk-margin-small-top">Сведения в приложенных файлах.</div>
	                                    <div class="uk-panel tm-panel-box-table uk-width-1-2">
		                                    <ol class="uk-list-space">
			                                    <li><a href="">ссылка на скачку документа</a></li>
			                                    <li><a href="">ссылка на скачку документа</a></li>
		                                    </ol>
	                                    </div>
	                                </td>
                                </tr>
                                <tr>
                                    <td colspan="3"><span class="uk-text-bold uk-h4">Дополнительно для товариществ или кооперативов:</span></td>
                                </tr>
                                <tr>
	                                <td><span>26</span></td>
                                    <td><span>Члены правления ТСЖ или ЖСК <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->members_of_board  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>27</span></td>
                                    <td><span>Члены ревизионной комиссии <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_common_info->members_audit_commission  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>28</span></td>
                                    <td colspan="2"><span>Дополнительные сведения в произвольной форме</span>
                                        <div class="uk-text-muted uk-margin-small-top">{{ $manager_common_info->more_information  or "Нет данных"  }}</div>
                                    </td>
                                </tr>
                            </tbody>
                    	</table>
                    	<p class="tm-text-black">Данные предоставлены управляющей организацией. Ответственность за корректность предоставленных данных несет управляющая организация.</p>
	                </li>
	                <li>
	                	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
	                	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-2-3"><span>Число жителей в обслуживаемых домах </span></td>
                                    <td><span>{{ $manager_housing->number_peoples  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Количество домов под управлением на отчетную дату <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_housing->number_houses_end_time  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых ТСЖ</span></td>
                                    <td><span>{{ $manager_housing->number_houses_end_time_tsg  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых по договору между ТСЖ и управляющей организацией</span></td>
                                    <td><span>{{ $manager_housing->number_houses_end_time_tsg_and_organization  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых по договору между собственниками и управляющей организацией</span></td>
                                    <td><span>{{ $manager_housing->number_houses_end_time_tsg_and_owner  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых по результатам открытого конкурса органов местного самоуправления</span></td>
                                    <td><span>{{ $manager_housing->number_houses_end_time_open_competition  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Количество домов под управлением на начало периода</span></td>
                                    <td><span>{{ $manager_housing->number_houses_start_time  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых ТСЖ</span></td>
                                    <td><span>{{ $manager_housing->number_houses_start_time_tsg  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых по договору между ТСЖ и управляющей организацией</span></td>
                                    <td><span>{{ $manager_housing->number_houses_start_time_tsg_and_organization  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых по договору между собственниками и управляющей организацией</span></td>
                                    <td><span>{{ $manager_housing->number_houses_start_time_tsg_and_owner  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; обслуживаемых по результатам открытого конкурса органов местного самоуправления</span></td>
                                    <td><span>{{ $manager_housing->number_houses_start_time_open_competition  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Общая площадь домов под управлением на отчетную дату, включая жилые и нежилые помещения, а также помещения общего пользования, тыс.кв.м. <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_reporting_date  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_reporting_date_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_reporting_date_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_reporting_date_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_reporting_date​_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_reporting_date_house_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Площадь домов под управлением на начало периода, тыс.кв.м.</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_start_period  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; изменение площади по заключенным договорам</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_start_period_concluded_contract  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; изменение площади по расторгнутым договорам</span></td>
                                    <td><span>{{ $manager_housing->common_area_houses_start_period_terminate_contract  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Средний срок обслуживания МКД, лет</span></td>
                                    <td><span>{{ $manager_housing->average_term_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_housing->average_term_service_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_housing->average_term_service_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_housing->average_term_service_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_housing->average_term_service_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_housing->average_term_service_house_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                            </tbody>
	                	</table>
	                	<p class="tm-text-black">Данные предоставлены управляющей организацией. Ответственность за корректность предоставленных данных несет управляющая организация.</p>
	                </li>
	                <li>
	                	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
	                	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-2-3"><span>Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб. <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service_houses_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service_houses_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service_houses_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service_houses_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_service_houses_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Сумма доходов, полученных от использования общего имущества за отчетный период, тыс.руб. </span></td>
                                    <td><span>{{ $manager_financial_highlights->income_common_property  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_common_property_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_common_property_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_common_property_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_common_property_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_common_property_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Доход, полученный за отчетный период от предоставления коммунальных услуг без учета коммунальных ресурсов, поставленных потребителям непосредственно поставщиками по прямым договорам, тыс.руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</span></td>
                                    <td><span>{{ $manager_financial_highlights->income_utilities_sanitation  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб. <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_service_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Выплаты по искам по договорам управления за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_contract_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иски по компенсации нанесенного ущерба</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_contract_service_damage  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иски по снижению платы в связи с неоказанием услуг</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_contract_service_unused_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иски по снижению платы в связи с недопоставкой ресурсов</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_contract_service_non_delivery_resources  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Выплаты по искам ресурсоснабжающих организаций за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</span></td>
                                    <td><span>{{ $manager_financial_highlights->costs_resources_organization_sanitation  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Чистые активы УО, тыс.руб. </span></td>
                                    <td><span>{{ $manager_financial_highlights->net_assets  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td><span class="uk-text-bold">Годовая бухгалтерская отчетность <span class="uk-badge uk-badge-danger">731</span></span></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><span class="uk-text-bold uk-h4">Дополнительно для товариществ или кооперативов:</span></td>
                                </tr>
                                <tr>
	                                <td><span>9</span></td>
                                    <td><span class="uk-text-bold">Сметы доходов и расходов ТСЖ или ЖСК <span class="uk-badge uk-badge-danger">731</span></span></td>
                                </tr>
                                <tr>
	                                <td><span>10</span></td>
                                    <td><span class="uk-text-bold">Отчет о выполнении сметы доходов и расходов <span class="uk-badge uk-badge-danger">731</span></span></td>
                                </tr>
                                <tr>
	                                <td><span>11</span></td>
                                    <td><span class="uk-text-bold">Протоколы общих собраний членов товарищества или кооператива, заседаний правления и ревизионной комиссии <span class="uk-badge uk-badge-danger">731</span></span></td>
                                </tr>
                                <tr>
	                                <td><span>12</span></td>
                                    <td><span class="uk-text-bold">Заключения ревизионной комиссии (ревизора) товарищества или кооператива по результатам проверки годовой бухгалтерской (финансовой) отчетности <span class="uk-badge uk-badge-danger">731</span></span></td>
                                </tr>
                                <tr>
	                                <td><span>13</span></td>
                                    <td><span class="uk-text-bold">Аудиторские заключения <span class="uk-badge uk-badge-danger">731</span></span></td>
                                </tr>
                            </tbody>
	                	</table>
	                	<p class="tm-text-black">Данные предоставлены управляющей организацией. Ответственность за корректность предоставленных данных несет управляющая организация.</p>
	                </li>
	                <li>
	                	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
	                	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-2-3"><span>Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях, за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб. </span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_current_date  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_current_date_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_current_date_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_current_date_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_current_date_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_current_date_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>2</span></td>
                                    <td class="uk-width-2-3"><span>Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях, за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб. </span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_service_start_period  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>3</span></td>
                                    <td class="uk-width-2-3"><span>Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях, за коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб. </span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_current_date_sanitation  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>4</span></td>
                                    <td class="uk-width-2-3"><span>Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях за коммунальные услуги на начало отчетного периода, тыс.руб. </span></td>
                                    <td><span>{{ $manager_arrears->past_account_owner_utilities_start_period  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>5</span></td>
                                    <td class="uk-width-2-3"><span>Просроченная задолженность организации за предоставленные коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</span></td>
                                    <td><span>{{ $manager_arrears->past_account_organ_utilities_current_date_sanitation  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>6</span></td>
                                    <td class="uk-width-2-3"><span>Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях за услуги по управлению, тыс.руб.</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_service  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_service_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_service_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_service_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_service_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_service_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>7</span></td>
                                    <td class="uk-width-2-3"><span>Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях за предоставленные коммунальные услуги, тыс.руб.</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</span></td>
                                    <td><span>{{ $manager_arrears->amount_collected_debt_utilities_sanitation  or "Нет данных"  }}</span></td>
                                </tr>
                            </tbody>
	                	</table>
	                	<p class="tm-text-black">Данные предоставлены управляющей организацией. Ответственность за корректность предоставленных данных несет управляющая организация.</p>
	                </li>
	                <li>
	                	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
	                	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-2-3"><span>Объем работ по ремонту за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_repair  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_repair_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_repair_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_repair_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_repair_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_repair_date_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Объем работ по благоустройству за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_improvement  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам до 25 лет</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_improvement_less_25  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 26 до 50 лет</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_improvement_26_50  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам от 51 до 75 лет</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_improvement_51_75  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по домам 76 лет и более</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_improvement_more_76  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; по аварийным домам</span></td>
                                    <td><span>{{ $manager_service_mkd->scope_improvement_date_alarm  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Объем привлеченных средств за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Субсидии</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds_subsidies  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Кредиты</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds_credits  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Финансирование по договорам лизинга</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds_financing_leasing  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Финансирование по энергосервисным договорам</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds_financing_energy  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Целевые взносы жителей</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds_earmarked  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; Другие источники</span></td>
                                    <td><span>{{ $manager_service_mkd->volume_attracted_funds_other  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_indications  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_indications_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_indications_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_indications_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_indications_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_indications_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_accounts  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_accounts_heating  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_accounts_electricity  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_accounts_gas  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_accounts_hot_water_supply  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</span></td>
                                    <td><span>{{ $manager_service_mkd->paid_ru_accounts_cold_water  or "Нет данных"  }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td colspan="2">
	                                    <span class="uk-text-bold">Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб. <span class="uk-badge uk-badge-danger">731</span></span> <div class="uk-text-muted uk-margin-small-top">В приложенном файле. </div>
                                    	<div class="uk-panel tm-panel-box-table uk-width-1-2">
		                                    <ol class="uk-list-space">
			                                    <li><a href="">ссылка на скачку документа</a></li>
			                                    <li><a href="">ссылка на скачку документа</a></li>
		                                    </ol>
	                                    </div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td colspan="2">
	                                    <span class="uk-text-bold">Стоимость услуг <span class="uk-badge uk-badge-danger">731</span></span> <div class="uk-text-muted uk-margin-small-top">Размер платы указан в приложенных файлах </div>
                                    	<div class="uk-panel tm-panel-box-table uk-width-1-2">
		                                    <ol class="uk-list-space">
			                                    <li><a href="">ссылка на скачку документа</a></li>
			                                    <li><a href="">ссылка на скачку документа</a></li>
		                                    </ol>
	                                    </div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td colspan="2">
	                                    <span class="uk-text-bold">Тарифы <span class="uk-badge uk-badge-danger">731</span></span> <div class="uk-text-muted uk-margin-small-top">Тарифы на коммунальные услуги указаны в приложенных файлах.</div>
                                    	<div class="uk-panel tm-panel-box-table uk-width-1-2">
		                                    <ol class="uk-list-space">
			                                    <li><a href="">ссылка на скачку документа</a></li>
			                                    <li><a href="">ссылка на скачку документа</a></li>
		                                    </ol>
	                                    </div>
                                    </td>
                                </tr>
                            </tbody>
	                	</table>
	                	<p class="tm-text-black">Данные предоставлены управляющей организацией. Ответственность за корректность предоставленных данных несет управляющая организация.</p>
	                </li>
                </ul>
            </li>
            <li>
            	<table class="uk-table uk-table-striped">
                    <thead>
                        <tr>
		                    <th rowspan="2">Наименование показателя (группы показателей)</th>
		                    <th class="tm-nowrap">Значение показателя</th>
		                    <th colspan="2">Количество баллов</th>
		                </tr>
		                <tr>
		                    <th>По организации</th>
		                    <th>Максимальный балл</th>
		                    <th>По организации</th>
		                </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Table Data</td>
                            <td>Table Data</td>
                            <td>Table Data</td>
                            <td>Table Data</td>
                        </tr>

                    </tbody>
                </table>
            </li>
<!--
            <li>
            	архив анкет
            </li>
-->
        </ul>
    </div>
</div>
@endsection