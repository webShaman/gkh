@extends('layout')

@section('content')
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-4-10">
				<ul class="uk-list uk-list-space tm-main-list-sections">
					<li>
						<a href="/about">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-1"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Нижегородский портал ЖКХ</h3>
										<p>Интернет-ресурс, содержащий актуальную информацию о ходе реформы ЖКХ</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
					<li>
						<a href="/myhouse">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-2"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Узнайте всё о своём доме</h3>
										<p>Подробная информация о доме и деятельности управляющей организации в нем: оказываемых услугах и выполняемых работах</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
					<li>
						<a href="/mymanager">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-3"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Мой управляющий</h3>
										<p>Сведения об организациях, осуществляющих деятельность в сфере управления многоквартирными домами</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
					<li>
						<a href="/overhaul">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-4"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Капитальный ремонт</h3>
										<p>Информация о запланированных и выполненных работах по каждому дому в рамках Нижегородской программы капитального ремонта</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
					<li>
						<a href="/territory">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-6"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Территория жизни</h3>
			                    		<p>Актуальные сведения о застройщиках и новостройках на территории города Нижнего Новгорода</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
					<li>
						<a href="/cinema">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-5"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Кинозал ЖКХ</h3>
			                    		<p>Смотрите все самое интересное о ЖКХ</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
					<li>
						<a href="/exemplary">
							<div class="uk-panel uk-clearfix uk-vertical-align">
			                    <div class="uk-vertical-align-middle uk-width-1-4">
				                    <div class="tm-ico-main tm-ico-main-7"></div>
				                </div>
			                    <div class="uk-vertical-align-middle uk-width-3-4">
				                    <div class="uk-panel">
				                    	<h3>Дома образцового содержания</h3>
			                    		<p>Самые благоустроенные дома города Нижнего Новгорода</p>
				                    </div>
			                    </div>
			                </div>
						</a>
					</li>
				</ul>
			</div>
			<div class="uk-width-6-10">
				@include('/layout/body/search/searchMenu', array('menu' => $menu))
				<div class="uk-grid uk-margin-large-top">
					<div class="uk-width-1-2">
						<div class="uk-panel uk-panel-header tm-news-home">
                            <h3 class="uk-panel-title">Новости ЖКХ</h3>
                            <ul class="uk-list uk-list-space">
                                <li>
                                    <div class="uk-clearfix">
                                        <img class="uk-thumbnail uk-align-medium-left" src="/images/news/1.jpg" width="100" height="100" alt="">
                                        <div class="uk-article-meta"><i class="uk-icon-clock-o"></i> 27 августа 2015</div>
                                        <a href="/news">Высокие тарифы на ЖКУ считают проблемой 73% россиян</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-clearfix">
                                        <img class="uk-thumbnail uk-align-medium-left" src="/images/news/2.jpg" width="100" height="100" alt="">
                                        <div class="uk-article-meta"><i class="uk-icon-clock-o"></i> 25 августа 2015</div>
                                        <a href="/news">«Муниципальную недвижимость» ликвидируют</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-clearfix">
                                        <img class="uk-thumbnail uk-align-medium-left" src="/images/news/3.jpg" width="100" height="100" alt="">
                                        <div class="uk-article-meta"><i class="uk-icon-clock-o"></i> 24 августа 2015</div>
                                        <a href="/news">Плата за пользование жилым помещением установлена с 1 сентября</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-clearfix">
                                        <img class="uk-thumbnail uk-align-medium-left" src="/images/news/4.jpg" width="100" height="100" alt="">
                                        <div class="uk-article-meta"><i class="uk-icon-clock-o"></i> 23 августа 2015</div>
                                        <a href="/news">Доля расходов на ЖКУ в регионе составляет 8,4%</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-clearfix">
                                        <img class="uk-thumbnail uk-align-medium-left" src="/images/news/5.jpg" width="100" height="100" alt="">
                                        <div class="uk-article-meta"><i class="uk-icon-clock-o"></i> 22 августа 2015</div>
                                        <a href="/news">Нижегородец пострадал от удара электротоком около ЛЭП</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-clearfix">
                                        <img class="uk-thumbnail uk-align-medium-left" src="/images/news/6.jpg" width="100" height="100" alt="">
                                        <div class="uk-article-meta"><i class="uk-icon-clock-o"></i> 20 августа 2015</div>
                                        <a href="/news">Регионы сами определят дату начала отопительного периода</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
					</div>
					<div class="uk-width-1-2 tm-main-sidebar-panel-box">
						<div class="uk-panel uk-panel-box">
		                    <h3 class="uk-panel-title">Стандарт раскрытия</h3>
		                    В соответствии с Постановлением Правительства РФ от 23.09.2010 года № 731 "Об утверждении стандарта раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами" организации обязаны раскрывать информацию о своей деятельности путем публикации ее на сайтах в сети Интернет, предназначенных для этих целей.
		                    <div class="uk-text-right uk-margin-small-top"><a href="/standard"><u>Перейти к раскрытию</u></a></div>
		                </div>
		                <div class="uk-panel uk-panel-box">
		                    <h3 class="uk-panel-title">Справка</h3>
		                    Справочная информация для пользователей включает: инструкции, видеоуроки, часто задаваемые вопросы и ответы на них. Перед обращением в Службу поддержки просим убедиться, что интересующая информация не представлена в данном разделе
		                    <div class="uk-text-right uk-margin-small-top"><a href="/help"><u>Перейти в раздел Справка</u></a></div>
		                </div>
		                <div class="uk-panel uk-panel-box">
		                    <h3 class="uk-panel-title">Техническая поддержка</h3>
		                    В случае возникновения трудностей в работе с порталом обратитесь в Службу технической поддержки по адресу электронной почты: <b>info@zhkh-nn.ru</b>
		                    <div class="uk-text-right uk-margin-small-top"><a href="/support"><u>Подробнее</u></a></div>
		                </div>
		                <div class="uk-panel uk-panel-box">
		                    <h3 class="uk-panel-title">Горячая линия</h3>
		                    Для получения консультации по раскрытию информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, а также по вопросам капитального ремонта обратитесь на Горячую линию Ассоциации ЖКХ города Нижнего Новгорода по телефону <b>8 (800) 775 76 77</b> в рабочие дни с 9 до 19 ч. (время московское)
		                </div>
					</div>
				</div>
			</div>
		</div>
    </div>
@stop