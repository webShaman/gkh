<div class='uk-form-row uk-text-bold'>
    Фундамент
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'> - Тип фундамента</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[foundation_type]', $structuralElements['foundation_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row uk-text-bold'>
    Стены и перекрытия
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Тип перекрытий</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[overlap_type]', $structuralElements['overlap_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Материал несущих стен</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[material_bearing_walls]', $structuralElements['material_bearing_walls'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Подвал
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Площадь подвала по полу, кв.м</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[basement_area]', $structuralElements['basement_area'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Мусоропроводы
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Тип мусоропровода</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[chutes_type]', $structuralElements['chutes_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Количество мусоропроводов, ед.</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[chutes_number]', $structuralElements['chutes_number'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Фасады
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>- Тип фасада</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[facade_type]', $structuralElements['facade_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Крыши
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Тип крыши</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[roof_type]', $structuralElements['roof_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Тип кровли</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[loft_type]', $structuralElements['loft_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Иное оборудование / конструктивный элемент
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Вид оборудования / конструктивного элемента</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[equipment_type]', $structuralElements['equipment_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - Описание дополнительного оборудования / конструктивного элемента</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_structural_elements[equipment_description]', $structuralElements['equipment_description'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
