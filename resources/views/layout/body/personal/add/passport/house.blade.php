
<div class='uk-form-row'>
    <label class='uk-form-label'>Район</label>
    <div class='uk-form-controls'>
        {!! Form::select('home[area_id]',  Home::$area_id, $home['area_id']) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>Улица и номер дома</label>
    <div class='uk-form-controls'>
        {!! Form::text('home[address]', $home['address'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>ИНН управляющей организации</label>
    <div class='uk-form-controls'>
        {!! Form::text('home[manager_inn]', $home['manager_inn'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>Конструктор карт</label>
    <div class='uk-form-controls'>
        {!! Form::textarea('home[map]', $home['map'], array('cols' => '30', 'rows' => '5', 'class' => 'uk-width-1-1')) !!}
        <p class="uk-form-help-block uk-text-small"><a target="_blank" href="https://tech.yandex.ru/maps/tools/constructor/">Конструктор карт</a>, размер: 397х310 (правится в коде скрипта)</p>
    </div>
</div>
