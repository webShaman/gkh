<div class='uk-form-row'>
    <label class='uk-form-label'>Год постройки</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[year_built]', $generalInformation['year_built'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>Год ввода в эксплуатацию</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[year_commissioning]', $generalInformation['year_commissioning'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>Количество жителей</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[residents_number]', $generalInformation['residents_number'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>Серия, тип проекта</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[series]', $generalInformation['series'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class='uk-form-row'>
    <label class='uk-form-label'>Тип дома</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[type]', $generalInformation['type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Описание местоположения</label>
    <div class='uk-form-controls'>
        {!! Form::textarea('homes_passport_general_information[description_position]', $generalInformation['description_position'], array('cols' => '30', 'rows' => '5', 'class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Индивидуальное наименование дома</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[name_house]', $generalInformation['name_house'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Способ формирования фонда капитального ремонта</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[method_overhaul]', $generalInformation['method_overhaul'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Количество этажей:
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - наибольшее, ед.</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[floors_number_max]', $generalInformation['floors_number_max'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - наименьшее, ед.</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[floors_number_min]', $generalInformation['floors_number_min'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Количество подъездов, ед</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[entrances_number]', $generalInformation['entrances_number'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Количество лифтов, ед</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[lifts_number]', $generalInformation['lifts_number'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Количество помещений, в том числе:</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[rooms_number]', $generalInformation['rooms_number'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - жилых, ед.</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[rooms_number_living]', $generalInformation['rooms_number_living'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - нежилых, ед.</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[rooms_number_no_living]', $generalInformation['rooms_number_no_living'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Общая площадь дома, в том числе, кв.м:</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[total_area]', $generalInformation['total_area'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - общая площадь жилых помещений, кв.м</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[total_area_living]', $generalInformation['total_area_living'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - общая площадь нежилых помещений, кв.м</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[total_area_no_living]', $generalInformation['total_area_no_living'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - общая площадь помещений, входящих в состав общего имущества, кв.м</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[total_area_common_property]', $generalInformation['total_area_common_property'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Общие сведения о земельном участке, на котором расположен многоквартирный дом:
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - площадь земельного участка, входящего в состав общего имущества в многоквартирном доме, кв.м</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[parcel_area]', $generalInformation['parcel_area'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - площадь парковки в границах земельного участка, кв.м</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[parcel_parking_area]', $generalInformation['parcel_parking_area'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>кадастровый номер</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[cadastral_number]', $generalInformation['cadastral_number'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Класс энергетической эффективности</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[energy_efficiency_class]', $generalInformation['energy_efficiency_class'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row uk-text-bold'>
    Общие сведения о земельном участке, на котором расположен многоквартирный дом:
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - детская площадка</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[beautification_playground_children]', $generalInformation['beautification_playground_children'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - спортивная площадка</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[beautification_playground_sports]', $generalInformation['beautification_playground_sports'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'> - другое</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[beautification_other]', $generalInformation['beautification_other'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
<div class='uk-form-row'>
    <label class='uk-form-label'>Дополнительная информация</label>
    <div class='uk-form-controls'>
        {!! Form::text('homes_passport_general_information[additional_information]', $generalInformation['additional_information'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>
