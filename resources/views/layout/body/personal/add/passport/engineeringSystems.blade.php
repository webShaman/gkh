<div class="uk-form-row uk-text-bold">
    Система электроснабжения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы электроснабжения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[electrical_type]', $engineeringSystems['electrical_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Количество вводов в дом, ед.</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[electronic_number_entries]', $engineeringSystems['electronic_number_entries'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система теплоснабжения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы теплоснабжения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[heating_type]', $engineeringSystems['heating_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система горячего водоснабжения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы горячего водоснабжения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[hot_water_type]', $engineeringSystems['hot_water_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система холодного водоснабжения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы холодного водоснабжения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[cold_water_type]', $engineeringSystems['cold_water_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система водоотведения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы водоотведения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[drainage_type]', $engineeringSystems['drainage_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Объем выгребных ям, куб. м.</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[cesspools_volume]', $engineeringSystems['cesspools_volume'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система газоснабжения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы газоснабжения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[gas_supply_type]', $engineeringSystems['gas_supply_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система вентиляции
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы вентиляции</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[ventilation_type]', $engineeringSystems['ventilation_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система пожаротушения
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы пожаротушения</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[fire_extinguishing_type]', $engineeringSystems['fire_extinguishing_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

<div class="uk-form-row uk-text-bold">
    Система водостоков
</div>

<div class="uk-form-row">
    <label class="uk-form-label">  - Тип системы водостоков</label>
    <div class="uk-form-controls">
        {!! Form::text('homes_passport_engineering_systems[drains_type]', $engineeringSystems['drains_type'], array('class' => 'uk-width-1-1')) !!}
    </div>
</div>

                            