@extends('layout')

@section('content')

<div class="personal-add">
    <div class="uk-container uk-container-center">
	    <h1>Добавление дома</h1>
	    @if (isset($massage))
	        <div class="uk-alert uk-alert-success" data-uk-alert>
		        <a href="" class="uk-alert-close uk-close"></a>
		        <p>{{ $massage }}</p>
		    </div>
        @else

        {!! Form::open(array('metod' => 'post', 'action' => 'Gkh\PersonalController@addHouse')) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            @if (isset($home['homes']['id'] ))
                <input type="hidden" name="edit" value="{{ $home['homes']['id'] }}"/>
            @endif
        <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#home_menu'}">
            <li class="uk-active" ><a href="#">Паспорт</a></li>
            <li><a href="#">Управление</a></li>
            <li><a href="#">Отчеты по управлению</a></li>
            <li><a href="#">История управления</a></li>
        </ul>
        <ul id="home_menu" class="uk-switcher">
            <li>
                <div class="uk-grid">
                    <div class="uk-width-medium-1-4">
                        <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#passport'}">
                            <li class="uk-active"><a href="#">Общие сведения</a></li>
                            <li><a href="#">Конструктивные элементы дома</a></li>
                            <li><a href="#">Инженерные системы</a></li>
                            <li><a href="#">Лифты</a></li>
                            <li><a href="#">Приборы учета</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-medium-2-3">
                        <ul id="passport" class="uk-switcher">
                            <li>
                                <div class="uk-form uk-form-horizontal">
                                    @include('/layout/body/personal/add/passport/house', array('home' => $home['homes']))
                                    @include('/layout/body/personal/add/passport/generalInformation', array('generalInformation' => $home['homes_passport_general_information']))
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                    @include('/layout/body/personal/add/passport/structuralElements', array('structuralElements' => $home['homes_passport_structural_elements']))
                                    {{--{!! $forms['homes_passport_structural_elements'] !!}--}}
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
	                                 @include('/layout/body/personal/add/passport/engineeringSystems', array('engineeringSystems' => $home['homes_passport_engineering_systems']))
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">4</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">5</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid">
                    <div class="uk-width-medium-1-4">
                        <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#management'}">
                            <li class="uk-active"><a href="#">Управляющая организация</a></li>
                            <li><a href="#">Выполняемые работы (услуги)</a></li>
                            <li><a href="#">Коммунальные услуги</a></li>
                            <li><a href="#">Общее имущество</a></li>
                            <li><a href="#">Сведения о капитальном ремонте</a></li>
                            <li><a href="#">Общие собрания</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-medium-3-4">
                        <ul id="management" class="uk-switcher">
                            <li>
                                <div class="uk-form uk-form-horizontal">
                                    <div class="uk-form-row">
                                        <label class="uk-form-label">1</label>
                                        <div class="uk-form-controls">
                                            <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">2</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">3</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">4</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">5</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid">
                    <div class="uk-width-medium-1-4">
                        <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#management_reports'}">
                            <li class="uk-active"><a href="#">Общая информация</a></li>
                            <li><a href="#">Выполненные работы</a></li>
                            <li><a href="#">Претензии по качеству работ</a></li>
                            <li><a href="#">Коммунальные услуги</a></li>
                            <li><a href="#">Объемы по коммунальным услугам</a></li>
                            <li><a href="#">Претензионно-исковая работа</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-medium-3-4">
                        <ul id="management_reports" class="uk-switcher">
                            <li>
                                <div class="uk-form uk-form-horizontal">
                                    <div class="uk-form-row">
                                        <label class="uk-form-label">1</label>
                                        <div class="uk-form-controls">
                                            <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">2</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">3</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">4</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                            <li>
                                 <div class="uk-form uk-form-horizontal">
                                     <div class="uk-form-row">
                                         <label class="uk-form-label">5</label>
                                         <div class="uk-form-controls">
                                             <input type="text" placeholder="Состояние дома" class="uk-width-1-1">
                                         </div>
                                     </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        @if (isset($home))
            <button class="uk-button uk-button-success uk-button-large uk-width-1-1 uk-margin-large-top" type="submit" >Сохранить</button>
        @else
            <button class="uk-button uk-button-success uk-button-large uk-width-1-1 uk-margin-large-top" type="submit" >Добавить</button>
        @endif
        {!! Form::close() !!}
        @endif
    </div>
</div>
@endsection