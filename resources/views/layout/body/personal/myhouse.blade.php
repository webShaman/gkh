@extends('layout')

@section('content')
    <div class="uk-container uk-container-center">
        <table class="uk-table uk-table-striped">
            <thead>
                <tr>
                    <th class="uk-width-1-3">Название</th>
                    <th>Год ввода в<br/>эксплуатацию</th>
                    <th>Площадь,<br/>м<sup>2</sup></th>
                    <th>Число<br/>жителей</th>
                    <th>Управляющая<br/>организация</th>
                    <th>действия</th>
                </tr>
            </thead>

            <a  href="/personal/house/add">Добавить дом</a>

            <tbody>
                @if ($grid)
                    @foreach($grid as $element)
                        <tr>
                            <td><a href="/myhouse/{{ $element['area']['name'] }}/{{ $element['id'] }}">г. Нижний Новгород, {{ $element['area']['name_ru'] }} р-н, ул. {{ $element['name'] }}</a></td>
                            <td>{{ $element['year'] or 'не указанно'}}</td>
                            <td>{{ $element['total_area'] or 'не указанно'}}</td>
                            <td>{{ $element['residents'] or 'не указанно'}}</td>
                            <td>{{ $element['manager']->short_name or 'не указанно'}}</td>
                            <td><a href="/personal/house/add/{{ $element['id'] }}">редактировать</a><br/><a>удалить</a></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">значений не найденно</td>
                    </tr>
                @endif
            </tbody>
        </table>
        @if ($render)
            {!! $render !!}
        @endif
    </div>

@endsection