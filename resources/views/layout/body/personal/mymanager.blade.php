@extends('layout')

@section('content')
<div class="tm-area">
    <div class="uk-container uk-container-center">
        <div class="uk-vertical-align uk-clearfix tm-directs-title">
            <div class="uk-width-3-4 uk-vertical-align-middle">
                <div class="uk-float-left"><h1 class="uk-margin-remove">Управляющие организации</h1></div>
            </div>
            <div class="uk-width-1-4 uk-vertical-align-middle">
               
            </div>
        </div>

        <table id="Table" class="uk-table uk-table-striped">
            <thead>
            <tr>
                <th class="uk-width-1-3">Название</th>
                <th>ИНН</th>
                <th>Количество<br/>домов</th>
                <th>Площадь,<br/>м<sup>2</sup></th>
                <th>Человек</th>
<!--                         <th width="120">Рейтинг</th> -->
                <th width="100">Действия</th>
            </tr>
            </thead>

            <a  href="/personal/mymanager/add_or_edit/">Добавить управляющую организацию</a>

            <tbody>
            @if (isset($managers) && count($managers))
                @foreach($managers as $manager)
                <tr>
                    <td><a href="/personal/mymanager/add_or_edit/{{ $manager['id'] }}">{{ $manager['short_name'] or "Нет данных" }}</a></td>
                    <td>{{ $manager['inn'] or "Нет данных" }}</td>
                    <td>{{ $manager['number_houses_end_time'] or "Нет данных" }}</td>
                    <td>{{ $manager['common_area_houses_reporting_date'] or "Нет данных" }}</td>
                    <td>{{ $manager['regular_staffing'] or "Нет данных" }}</td>
<!--
                    <td>
                        <div class="tm-box-star">
                            <i class="uk-icon-star uk-icon-small"></i>
                            <i class="uk-icon-star-half-empty uk-icon-small"></i>
                            <i class="uk-icon-star-o uk-icon-small"></i>
                            <i class="uk-icon-star-o uk-icon-small"></i>
                            <i class="uk-icon-star-o uk-icon-small"></i>
                        </div>
                    </td>
-->
                    <td><a  href="/personal/mymanager/add_or_edit/{{ $manager['id'] }}">Редактировать</a></td>
                </tr>
                @endforeach
            @else
            <p>Нет Управляющих организаций</p>
            @endif

            </tbody>
        </table>
        <div class="uk-clearfix">
            <div class="uk-float-left">
                <ul id="Action" class="uk-pagination">
                    <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                    <li class="uk-active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><span>...</span></li>
                    <li><a href="#">20</a></li>
                    <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                </ul>
            </div>
            <div class="uk-float-right">
                <ul id="Number" class="uk-subnav">
                    <li class="uk-text-muted">Отображать по</li>
                    <li class="uk-active"><a >10</a></li>
                    <li><a >20</a></li>
                    <li><a >50</a></li>
                    <li><a >100</a></li>
                </ul>
            </div>
        </div>

    </div>
</div>
@endsection