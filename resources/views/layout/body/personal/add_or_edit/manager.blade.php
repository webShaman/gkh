@extends('layout')

@section('content')

<div class="personal-add">
<div class="uk-container uk-container-center">
    <h1>@if(isset($manager_common_info))
            {{ $manager_common_info->short_name or "Добавление организации" }}
        @else
            Добавление организации
        @endif
    </h1>
    @if(isset($resultSaveData))
        @if($resultSaveData=='true')
            <div class="uk-alert uk-alert-success" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p>Данны успешно сохранены. Спасибо.</p>
            </div>
        @else
            <div class="uk-alert uk-alert-error" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p>Ошибка сохранения данных. Пожалуйста введите правильно данные.</p>
            </div>
        @endif
    @endif
    <div class="uk-grid">
        <div class="uk-width-medium-1-4">
            <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tab-left-content'}">
                <li class="uk-active"><a href="#">Общие сведения об организации</a></li>
                <li><a href="#">Сведения о лицензии</a></li>
                <li><a href="#">Основные финансовые показатели</a></li>
                <li><a href="#">Нарушения</a></li>
                <li><a href="#">Рейтинг</a></li>
            </ul>
        </div>
        <div class="uk-width-medium-3-4">

            {!! Form::open([]) !!}

            <ul id="tab-left-content" class="uk-switcher">

			<!-- ОБЩИЕ СВЕДЕНИЯ ОБ ОРГАНИЗАЦИИ -->
            <li>
	            <div class="uk-form uk-form-horizontal">
<!--
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Город</label>
	                    <div class="uk-form-controls">
	                        <select>
	                <option>Нижний Новгород</option>
	                        </select>
	                    </div>
	                </div>
-->
<!--
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Район</label>
	                    <div class="uk-form-controls">
	                        <select>
	                            <option>Автозаводский</option>
	                            <option>Канавинский</option>
	                            <option>Ленинский</option>
	                            <option>Московский</option>
	                            <option>Нижегородский</option>
	                            <option>Приокский</option>
	                            <option>Советский</option>
	                            <option>Сормовский</option>
	                        </select>
	                    </div>
	                </div>
-->
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Карта</label>
	                    <div class="uk-form-controls">
	                        {!! Form::textarea('map',$manager_common_info->map, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                        <p class="uk-form-help-block uk-text-small"><a target="_blank" href="https://tech.yandex.ru/maps/tools/constructor/">Конструктор карт</a>, размер: 323х310 (правится в коде скрипта)</p>
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Руководитель</label>
	                    <div class="uk-form-controls">
	                        {!! Form::textarea('director', $manager_common_info->director, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Электронный адрес</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('email_address', $manager_common_info->email_address, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Ответственный за заполнение</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('responsible_for_filling',$manager_common_info->responsible_for_filling, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Фирменное наименование юридического лица (согласно уставу организации)</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('full_name',$manager_common_info->full_name, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Сокращенное наименование</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('short_name', $manager_common_info->short_name, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Организационно-правовая форма</label>
	                    <div class="uk-form-controls">
	                        {!! Form::textarea('organization_form', $manager_common_info->organization_form, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Идентификационный номер налогоплательщика (ИНН)</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('inn', $manager_common_info->inn, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Основной государственный регистрационный номер / основной государственный регистрационный номер индивидуального предпринимателя (ОГРН/ ОГРНИП)</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('ogrn', $manager_common_info->ogrn, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Место государственной регистрации юридического лица (адрес юридического лица)</label>
	                    <div class="uk-form-controls">
                            {!! Form::textarea('legal_address', $manager_common_info->legal_address, ['class'=>'uk-width-1-1', 'cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Адрес фактического местонахождения органов управления</label>
	                    <div class="uk-form-controls">
                            {!! Form::textarea('physical_address', $manager_common_info->physical_address, ['class'=>'uk-width-1-1', 'cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Почтовый адрес</label>
	                    <div class="uk-form-controls">
	                        {!! Form::textarea('mailing_address', $manager_common_info->mailing_address, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Режим работы, в т. ч. часы личного приема граждан</label>
	                    <div class="uk-form-controls">
	                        {!! Form::textarea('time_working', $manager_common_info->time_working, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-text-bold uk-margin">Сведения о работе диспетчерской службы:</div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Адрес диспетчерской службы</label>
	                    <div class="uk-form-controls">
                            {!! Form::textarea('address_dispatch_service', $manager_common_info->address_dispatch_service, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Контактные телефоны диспетчерской службы</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('contact_telephone_dispatch_service', $manager_common_info->contact_telephone_dispatch_service, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Режим работы диспетчерской службы</label>
	                    <div class="uk-form-controls">
                            {!! Form::textarea('mode_operation_dispatch_service', $manager_common_info->mode_operation_dispatch_service, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Контактные телефоны</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('phone', $manager_common_info->phone, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Факс</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('fax', $manager_common_info->fax, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Официальный сайт в сети Интернет (при наличии)</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('internet_site', $manager_common_info->internet_site, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Доля участия субъекта Российской Федерации в уставном капитале организации</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('share_in_authorized_capital_subject​​', $manager_common_info->share_in_authorized_capital_subject​​, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Доля участия муниципального образования в уставном капитале организации</label>
	                    <div class="uk-form-controls">
	                        {!! Form::text('share_in_authorized_capital_municipal', $manager_common_info->share_in_authorized_capital_municipal, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Количество домов, находящихся в управлении , ед.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('number_houses', $manager_common_info->number_houses, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Площадь домов, находящихся в управлении, кв. м</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('area_houses', $manager_common_info->area_houses, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label uk-text-bold">Штатная численность (определяется по количеству заключенных трудовых договоров), в т .ч. административный персонал, инженеры, рабочие, чел.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('regular_staffing', $manager_common_info->regular_staffing, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Штатная численность административного персонала, чел.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('regular_staffing_administrative', $manager_common_info->regular_staffing_administrative, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Штатная численность инженеров, чел.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('regular_staffing_engineers', $manager_common_info->regular_staffing_engineers, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Штатная численность рабочих, чел.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('regular_staffing_working', $manager_common_info->regular_staffing_working, [ 'class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Сведения о членстве управляющей организации, товарищества или кооператива в саморегулируемой организации.</label>
	                    <div class="uk-form-controls">
                            {!! Form::textarea('self_regulatory_organizations', $manager_common_info->self_regulatory_organizations, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'5', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	            </div>
            </li>

            <!-- СВЕДЕНИЯ О ЛИЦЕНЗИИ -->
            <li>
                <table class="uk-table">
				    <thead>
				        <tr>
				            <th>Номер лицензии</th>
				            <th>Дата получения лицензии</th>
				            <th>Орган, выдавший лицензию</th>
				            <th>Документ лицензии</th>
				        </tr>
				    </thead>
				    <tbody id="change_table">
                    @if ( isset($tables_one) && count($tables_one))
                        <?php $row_num=1; ?>
                        @foreach ($tables_one as $row)
				            <tr>
				                <td>
                                    {!! Form::text("number_license_$row_num", $row->number_license, ['autocomplete' => 'off' ]) !!}
				                </td>
                                <td>
                                    {!! Form::text("date_receipt_license_$row_num", $row->date_receipt_license, ['autocomplete' => 'off' ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("issuing_authority_$row_num", $row->issuing_authority, ['autocomplete' => 'off' ]) !!}
                                </td>
                                <td><div class="uk-form-file uk-text-primary">приложить документ{!! Form::file("documents_license_$row_num", Null, ['autocomplete' => 'off' ]) !!}</div></td>
				            </tr>
                            <?php $row_num=$row_num+1; ?>
                        @endforeach
                    @endif
				    </tbody>
				</table>
				<a id="add_row" class="uk-button uk-button-primary"><i class="uk-icon-plus-circle"></i> добавить лицензию</a>
            </li>

            <!-- ОСНОВНЫЕ ФИНАНСОВЫЕ ПОКАЗАТЕЛИ -->
            <li>
                <div class="uk-form uk-form-horizontal">
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Дата начала отчетного периода</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('start_time_reporting_period', $manager_financial_highlights->start_time_reporting_period, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Дата конца отчетного периода</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('end_time_reporting_period', $manager_financial_highlights->end_time_reporting_period, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Сведения о доходах, полученных за оказание услуг по управлению многоквартирными домами (по данным раздельного учета доходов и расходов), руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('income_service', $manager_financial_highlights->income_service, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Сведения о расходах, понесенных в связи с оказанием услуг по управлению многоквартирными домами (по данным раздельного учета доходов и расходов), руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('costs_service', $manager_financial_highlights->costs_service, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label uk-text-bold">Общая задолженность управляющей организации (индивидуального предпринимателя) перед ресурсоснабжающими организациями за коммунальные ресурсы, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt', $manager_financial_highlights->total_debt, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">- Общая задолженность по тепловой энергии, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_heat_energy', $manager_financial_highlights->total_debt_heat_energy, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по тепловой энергии для нужд отопления, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_heat_energy_for_heating', $manager_financial_highlights->total_debt_heat_energy_for_heating, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по тепловой энергии для нужд горячего водоснабжения, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_heat_energy_for_water', $manager_financial_highlights->total_debt_heat_energy_for_water, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по горячей воде, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_hot_water_supply', $manager_financial_highlights->total_debt_hot_water_supply, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по холодной воде, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_cold_water', $manager_financial_highlights->total_debt_cold_water, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по водоотведению, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_sanitation', $manager_financial_highlights->total_debt_sanitation, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по поставке газа, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_gas', $manager_financial_highlights->total_debt_gas, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по электрической энергии, руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_electricity', $manager_financial_highlights->total_debt_electricity, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">— Общая задолженность по прочим ресурсам (услугам), руб.</label>
	                    <div class="uk-form-controls">
                            {!! Form::text('total_debt_for_other_resources', $manager_financial_highlights->total_debt_for_other_resources, ['class'=>'uk-width-1-1', 'autocomplete' => 'off' ]) !!}
	                    </div>
	                </div>
	                <div class="uk-form-row">
	                    <label class="uk-form-label">Годовая бухгалтерская отчетность</label>
	                    <div class="uk-form-controls">
	                        ??? прикладываем файлики
	                    </div>
	                </div>
	                
	            </div>
	        </li>

            <!-- НАРУШЕНИЯ -->
            <li>
                <div class="uk-form uk-form-horizontal">
	                ????
                </div>
            </li>
            <!-- РЕЙТИНГ -->
            <li>
                    Рейтинг.....
            </li>
            </ul>
            {!! Form::submit('Сохранить',['class'=>'uk-button uk-button-success uk-button-large uk-width-1-1 uk-margin-large-top']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>

@endsection