@extends('layout')

@section('content')

<div class="personal-add">
<div class="uk-container uk-container-center">
    <h1>Добавление организации</h1>
    @if(isset($resultSaveData))
        @if($resultSaveData=='true')
            <div class="uk-alert uk-alert-success" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p>Данны успешно сохранены. Спасибо.</p>
            </div>
        @else
            <div class="uk-alert uk-alert-error" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p>Ошибка сохранения данных. Пожалуйста введите правильно данные.</p>
            </div>
        @endif
    @endif
    <p>Знаком <span class="uk-text-danger uk-text-small">"731"</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
    <div class="uk-grid">
        <div class="uk-width-medium-1-3">
            <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tab-left-content'}">
                <li class="uk-active"><a href="#">Общие сведения об организации</a></li>
                <li><a href="#">Жилищный фонд</a></li>
                <li><a href="#">Основные финансовые показатели</a></li>
                <li><a href="#">Задолженности</a></li>
                <li><a href="#">Деятельность по управлению МКД</a></li>
                <li><a href="#">Рейтинг</a></li>
            </ul>
        </div>
        <div class="uk-width-medium-2-3">

            {!! Form::open([]) !!}

            <ul id="tab-left-content" class="uk-switcher">

<!-- ОБЩИЕ СВЕДЕНИЯ ОБ ОРГАНИЗАЦИИ -->
            <li>


            <div class="uk-form uk-form-horizontal">


                <div class="uk-form-row">
                    <label class="uk-form-label">Город</label>
                    <div class="uk-form-controls">
                        <select>
                <option>Нижний Новгород</option>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Район</label>
                    <div class="uk-form-controls">
                        <select>
                            <option>Автозаводский</option>
                            <option>Канавинский</option>
                            <option>Ленинский</option>
                            <option>Московский</option>
                            <option>Нижегородский</option>
                            <option>Приокский</option>
                            <option>Советский</option>
                            <option>Сормовский</option>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Карта</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('map',$manager_common_info->map, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Вставить скрипт', 'autocomplete' => 'off' ]) !!}
                        <p class="uk-form-help-block uk-text-small"><a target="_blank" href="https://tech.yandex.ru/maps/tools/constructor/">Конструктор карт</a>, размер: 323х310 (правится в коде скрипта)</p>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Ответственный за заполнение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('responsible_for_filling',$manager_common_info->responsible_for_filling, [ 'class'=>'uk-width-1-1','placeholder' => 'Ответственный за заполнение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Полное наименование</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('full_name',$manager_common_info->full_name, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Полное наименование', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Краткое наименование</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('short_name', $manager_common_info->short_name, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Краткое наименование', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Организационная форма</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('organization_form', $manager_common_info->organization_form, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Организационная форма', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Руководитель</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('director', $manager_common_info->director, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Руководитель', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">ИНН</label>
                    <div class="uk-form-controls">
                        {!! Form::text('inn', $manager_common_info->inn, [ 'class'=>'uk-width-1-1','placeholder' => 'ИНН', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> ОГРН или ОГРНИП</label>
                    <div class="uk-form-controls">
                        {!! Form::text('ogrn', $manager_common_info->ogrn, ['class'=>'uk-width-1-1', 'placeholder' => 'ОГРН или ОГРНИП', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- дата присвоения ОГРН (ОГРНИП)</label>
                    <div class="uk-form-controls">
                        {!! Form::text('date_ogrn', $manager_common_info->date_ogrn, [ 'class'=>'uk-width-1-1','placeholder' => 'Дата присвоения ОГРН (ОГРНИП)', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- наименование органа, принявшего решение о регистрации</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('name_register_organ', $manager_common_info->name_register_organ, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Наименование органа, принявшего решение о регистрации', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Юридический адрес</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('legal_address', $manager_common_info->legal_address, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Юридический адрес', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Фактический адрес</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('physical_address', $manager_common_info->physical_address, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Фактический адрес', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Почтовый адрес</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('mailing_address', $manager_common_info->mailing_address, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Почтовый адрес', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Режим работы</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('time_working', $manager_common_info->time_working, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Режим работы', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Телефон</label>
                    <div class="uk-form-controls">
                        {!! Form::text('phone', $manager_common_info->phone, ['class'=>'uk-width-1-1', 'placeholder' => 'Телефон', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Электронный адрес</label>
                    <div class="uk-form-controls">
                        {!! Form::text('email_address', $manager_common_info->email_address, ['class'=>'uk-width-1-1', 'placeholder' => 'Электронный адрес', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Интернет сайт</label>
                    <div class="uk-form-controls">
                        {!! Form::text('internet_site', $manager_common_info->internet_site, ['class'=>'uk-width-1-1', 'placeholder' => 'Интернет сайт', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Доля участия в уставном капитале Субъекта РФ, %</label>
                    <div class="uk-form-controls">
                        {!! Form::text('share_in_authorized_capital_subject​​', $manager_common_info->share_in_authorized_capital_subject​​, [ 'class'=>'uk-width-1-1','placeholder' => 'Доля участия в уставном капитале Субъекта РФ, %', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Доля участия в уставном капитале муниципального образования, %</label>
                    <div class="uk-form-controls">
                        {!! Form::text('share_in_authorized_capital_municipal', $manager_common_info->share_in_authorized_capital_municipal, [ 'class'=>'uk-width-1-1','placeholder' => 'Доля участия в уставном капитале муниципального образования, %', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Дополнительная информация</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('additional_information', $manager_common_info->additional_information, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Дополнительная информация', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Сведения об участии в саморегулируемых организациях или в объединениях ТСЖ и ЖСК и наличии сертификатов соответствия стандартам обслуживания</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('self_regulatory_organizations', $manager_common_info->self_regulatory_organizations, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Сведения об участии в саморегулируемых...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Количество Субъектов РФ, в которых организация осуществляет свою деятельность</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_subject_where_organization_working', $manager_common_info->number_subject_where_organization_working, ['class'=>'uk-width-1-1', 'placeholder' => 'Количество Субъектов РФ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Количество муниципальных образований, в которых организация осуществляет свою деятельность</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_municipal_where_organization_working', $manager_common_info->number_municipal_where_organization_working, [ 'class'=>'uk-width-1-1','placeholder' => 'Количество муниципальных образований...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Количество офисов обслуживания граждан</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_office', $manager_common_info->number_office, [ 'class'=>'uk-width-1-1','placeholder' => 'Количество офисов обслуживания граждан', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Штатная численность на отчетную дату, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('regular_staffing', $manager_common_info->regular_staffing, ['class'=>'uk-width-1-1', 'placeholder' => 'Штатная численность на отчетную дату, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- административный персонал, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('regular_staffing_administrative', $manager_common_info->regular_staffing_administrative, ['class'=>'uk-width-1-1', 'placeholder' => 'административный персонал, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- инженеры, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('regular_staffing_engineers', $manager_common_info->regular_staffing_engineers, ['class'=>'uk-width-1-1', 'placeholder' => 'инженеры, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- рабочий персонал, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('regular_staffing_working', $manager_common_info->regular_staffing_working, ['class'=>'uk-width-1-1', 'placeholder' => 'рабочий персонал, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Уволено за отчетный период, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('laid_off_during_period', $manager_common_info->laid_off_during_period, ['class'=>'uk-width-1-1', 'placeholder' => 'Уволено за отчетный период, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- административный персонал, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('laid_off_during_period_administrative', $manager_common_info->laid_off_during_period_administrative, [ 'class'=>'uk-width-1-1','placeholder' => 'административный персонал, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- инженеры, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('laid_off_during_period_engineers', $manager_common_info->laid_off_during_period_engineers, [ 'class'=>'uk-width-1-1','placeholder' => 'инженеры, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- рабочий персонал, чел.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('laid_off_during_period_working', $manager_common_info->laid_off_during_period_working, [ 'class'=>'uk-width-1-1','placeholder' => 'рабочий персонал, чел.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Число несчастных случаев за отчетный период</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_accidents', $manager_common_info->number_accidents, [ 'class'=>'uk-width-1-1','placeholder' => 'Число несчастных случаев за отчетный период', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Число случаев привлечения организации к административной ответственности </label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_administrative_responsibility', $manager_common_info->number_administrative_responsibility, ['class'=>'uk-width-1-1', 'placeholder' => 'Число случаев привлечения...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Копии документов о применении мер административного воздействия, а также мер, принятых для устранения нарушений, повлекших применение административных санкций предприятие к административной ответственности за отчетный период не привлекалось</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('copies_document_administrative_responsibility', $manager_common_info->copies_document_administrative_responsibility, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Копии документов о применении мер административного воздействия...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Члены правления ТСЖ или ЖСК</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('members_of_board', $manager_common_info->members_of_board, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Члены правления ТСЖ или ЖСК', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Члены ревизионной комиссии</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('members_audit_commission', $manager_common_info->members_audit_commission, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Члены ревизионной комиссии', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Дополнительные сведения в произвольной форме</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('more_information', $manager_common_info->more_information, [ 'class'=>'uk-width-1-1','cols'=>'30', 'rows'=>'3','placeholder' => 'Дополнительные сведения в произвольной форме', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>


                </div>


                </li>

                <!-- ЖИЛИЩНЫЙ ФОНД -->
                <li>


                <div class="uk-form uk-form-horizontal">


                <div class="uk-form-row">
                    <label class="uk-form-label">Число жителей в обслуживаемых домах</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_peoples', $manager_housing->number_peoples, ['class'=>'uk-width-1-1', 'placeholder' => 'Число жителей в обслуживаемых домах ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Количество домов под управлением на отчетную дату</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_end_time', $manager_housing->number_houses_end_time, [ 'class'=>'uk-width-1-1','placeholder' => 'Количество домов под управлением на отчетную дату ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых ТСЖ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_end_time_tsg', $manager_housing->number_houses_end_time_tsg, ['class'=>'uk-width-1-1', 'placeholder' => 'обслуживаемых ТСЖ ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых по договору между ТСЖ и управляющей организацией</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_end_time_tsg_and_organization', $manager_housing->number_houses_end_time_tsg_and_organization, [ 'class'=>'uk-width-1-1','placeholder' => 'обслуживаемых по договору между ТСЖ ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых по договору между собственниками и управляющей организацией</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_end_time_tsg_and_owner', $manager_housing->number_houses_end_time_tsg_and_owner, ['class'=>'uk-width-1-1', 'placeholder' => 'обслуживаемых по договору между собственниками ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых по результатам открытого конкурса органов местного самоуправления</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_end_time_open_competition', $manager_housing->number_houses_end_time_open_competition, [ 'class'=>'uk-width-1-1','placeholder' => 'обслуживаемых по результатам открытого конкурса ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Количество домов под управлением на начало периода</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_start_time', $manager_housing->number_houses_start_time, ['class'=>'uk-width-1-1', 'placeholder' => 'Количество домов под управлением на начало периода ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых ТСЖ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_start_time_tsg', $manager_housing->number_houses_start_time_tsg, ['class'=>'uk-width-1-1', 'placeholder' => 'обслуживаемых ТСЖ ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых по договору между ТСЖ и управляющей организацией</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_start_time_tsg_and_organization', $manager_housing->number_houses_start_time_tsg_and_organization, ['class'=>'uk-width-1-1', 'placeholder' => 'обслуживаемых по договору между ТСЖ ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых по договору между собственниками и управляющей организацией</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_start_time_tsg_and_owner', $manager_housing->number_houses_start_time_tsg_and_owner, [ 'class'=>'uk-width-1-1','placeholder' => 'обслуживаемых по договору между собственниками ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- обслуживаемых по результатам открытого конкурса органов местного самоуправления</label>
                    <div class="uk-form-controls">
                        {!! Form::text('number_houses_start_time_open_competition', $manager_housing->number_houses_start_time_open_competition, ['class'=>'uk-width-1-1', 'placeholder' => 'обслуживаемых по результатам ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Общая площадь домов под управлением на отчетную дату, включая жилые и нежилые помещения, а также помещения общего пользования, тыс.кв.м.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_reporting_date', $manager_housing->common_area_houses_reporting_date, ['class'=>'uk-width-1-1', 'placeholder' => 'Общая площадь домов  ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_reporting_date_less_25', $manager_housing->common_area_houses_reporting_date_less_25, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам до 25 лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_reporting_date_26_50', $manager_housing->common_area_houses_reporting_date_26_50, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 26 до 50 лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_reporting_date_51_75', $manager_housing->common_area_houses_reporting_date_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_reporting_date_more_76', $manager_housing->common_area_houses_reporting_date_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_reporting_date_house_alarm', $manager_housing->common_area_houses_reporting_date_house_alarm, ['class'=>'uk-width-1-1', 'placeholder' => 'по аварийным домам ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Площадь домов под управлением на начало периода, тыс.кв.м.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_start_period', $manager_housing->common_area_houses_start_period, ['class'=>'uk-width-1-1', 'placeholder' => 'Площадь домов под управлением ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- изменение площади по заключенным договорам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_start_period_concluded_contract', $manager_housing->common_area_houses_start_period_concluded_contract, ['class'=>'uk-width-1-1', 'placeholder' => 'изменение площади по заключенным договорам ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- изменение площади по расторгнутым договорам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('common_area_houses_start_period_terminate_contract', $manager_housing->common_area_houses_start_period_terminate_contract, [ 'class'=>'uk-width-1-1','placeholder' => 'изменение площади по расторгнутым договорам ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Средний срок обслуживания МКД, лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('average_term_service', $manager_housing->average_term_service, [ 'class'=>'uk-width-1-1','placeholder' => 'Средний срок обслуживания МКД, лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('average_term_service_less_25', $manager_housing->average_term_service_less_25, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам до 25 лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('average_term_service_26_50', $manager_housing->average_term_service_26_50, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам от 26 до 50 лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('average_term_service_51_75', $manager_housing->average_term_service_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('average_term_service_more_76', $manager_housing->average_term_service_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('average_term_service_house_alarm', $manager_housing->average_term_service_house_alarm, ['class'=>'uk-width-1-1', 'placeholder' => 'по аварийным домам ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>


                </div>


                </li>

                <!-- ОСНОВНЫЕ ФИНАНСОВЫЕ ПОКАЗАТЕЛИ -->
                <li>


                <div class="uk-form uk-form-horizontal">


                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_service', $manager_financial_highlights->income_service  , ['class'=>'uk-width-1-1', 'placeholder' => 'Доходы, полученные за оказание услуг ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_service_houses_less_25', $manager_financial_highlights->income_service_houses_less_25, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_service_houses_26_50', $manager_financial_highlights->income_service_houses_26_50, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_service_houses_51_75', $manager_financial_highlights->income_service_houses_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_service_houses_more_76', $manager_financial_highlights->income_service_houses_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_service_houses_alarm', $manager_financial_highlights->income_service_houses_alarm, ['class'=>'uk-width-1-1', 'placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Сумма доходов, полученных от использования общего имущества за отчетный период, тыс.руб. </label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_common_property', $manager_financial_highlights->income_common_property, ['class'=>'uk-width-1-1', 'placeholder' => 'Сумма доходов, полученных от использования общего имущества ... ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_common_property_less_25', $manager_financial_highlights->income_common_property_less_25, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_common_property_26_50', $manager_financial_highlights->income_common_property_26_50, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_common_property_51_75', $manager_financial_highlights->income_common_property_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_common_property_more_76', $manager_financial_highlights->income_common_property_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_common_property_alarm', $manager_financial_highlights->income_common_property_alarm, ['class' => 'uk-width-1-1', 'placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Доход, полученный за отчетный период от предоставления коммунальных услуг без учета коммунальных ресурсов, поставленных потребителям непосредственно поставщиками по прямым договорам, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities', $manager_financial_highlights->income_utilities, ['class'=>'uk-width-1-1', 'placeholder' => 'Доход, полученный за отчетный период от предоставления коммунальных услуг ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities_heating', $manager_financial_highlights->income_utilities_heating, ['class'=>'uk-width-1-1', 'placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities_electricity', $manager_financial_highlights->income_utilities_electricity, ['class'=>'uk-width-1-1', 'placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities_gas', $manager_financial_highlights->income_utilities_gas, ['class'=>'uk-width-1-1', 'placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities_hot_water_supply', $manager_financial_highlights->income_utilities_hot_water_supply, ['class'=>'uk-width-1-1', 'placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities_cold_water', $manager_financial_highlights->income_utilities_cold_water, ['class'=>'uk-width-1-1', 'placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- водоотведение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('income_utilities_sanitation', $manager_financial_highlights->income_utilities_sanitation, [ 'class'=>'uk-width-1-1','placeholder' => 'водоотведение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб. </label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_service', $manager_financial_highlights->costs_service, [ 'class'=>'uk-width-1-1','placeholder' => 'Расходы, полученные в связи с оказанием услуг ...', 'autocomplete' => 'off' ]) !!}<
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_service_less_25', $manager_financial_highlights->costs_service_less_25, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_service_26_50', $manager_financial_highlights->costs_service_26_50, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_service_51_75', $manager_financial_highlights->costs_service_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_service_more_76', $manager_financial_highlights->costs_service_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_service_alarm', $manager_financial_highlights->costs_service_alarm, ['class'=>'uk-width-1-1', 'placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Выплаты по искам по договорам управления за отчетный период, тыс.руб. </label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_contract_service', $manager_financial_highlights->costs_contract_service, ['class'=>'uk-width-1-1', 'placeholder' => 'Выплаты по искам по договорам ... ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- иски по компенсации нанесенного ущерба</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_contract_service_damage', $manager_financial_highlights->costs_contract_service_damage, ['class'=>'uk-width-1-1', 'placeholder' => 'иски по компенсации нанесенного ущерба', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- иски по снижению платы в связи с неоказанием услуг</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_contract_service_unused_service', $manager_financial_highlights->costs_contract_service_unused_service, ['class'=>'uk-width-1-1', 'placeholder' => 'иски по снижению платы в связи с неоказанием услуг', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- иски по снижению платы в связи с недопоставкой ресурсов</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_contract_service_non_delivery_resources', $manager_financial_highlights->costs_contract_service_non_delivery_resources, [ 'class'=>'uk-width-1-1','placeholder' => 'иски по снижению платы в связи с недопоставкой ресурсов', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Выплаты по искам ресурсоснабжающих организаций за отчетный период, тыс.руб. </label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization', $manager_financial_highlights->costs_resources_organization, [ 'class'=>'uk-width-1-1','placeholder' => 'Выплаты по искам ресурсоснабжающих организаций ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization_heating', $manager_financial_highlights->costs_resources_organization_heating, [ 'class'=>'uk-width-1-1','placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization_electricity', $manager_financial_highlights->costs_resources_organization_electricity, [ 'class'=>'uk-width-1-1','placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization_gas', $manager_financial_highlights->costs_resources_organization_gas, [ 'class'=>'uk-width-1-1','placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization_hot_water_supply', $manager_financial_highlights->costs_resources_organization_hot_water_supply, ['class'=>'uk-width-1-1', 'placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization_cold_water', $manager_financial_highlights->costs_resources_organization_cold_water, ['class'=>'uk-width-1-1', 'placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- водоотведение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('costs_resources_organization_sanitation', $manager_financial_highlights->costs_resources_organization_sanitation, ['class'=>'uk-width-1-1', 'placeholder' => 'водоотведение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Чистые активы УО, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('net_assets', $manager_financial_highlights->net_assets, ['class'=>'uk-width-1-1', 'placeholder' => 'Чистые активы УО, тыс.руб.', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span>  Годовая бухгалтерская отчетность</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Годовая бухгалтерская отчетность" class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Сметы доходов и расходов ТСЖ или ЖСК</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Сметы доходов и расходов ТСЖ или ЖСК" class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Отчет о выполнении сметы доходов и расходов</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Отчет о выполнении сметы доходов и расходов" class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Протоколы общих собраний членов товарищества или кооператива, заседаний правления и ревизионной комиссии</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Протоколы общих собраний членов ..." class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Заключения ревизионной комиссии (ревизора) товарищества или кооператива по результатам проверки годовой бухгалтерской (финансовой) отчетности</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Заключения ревизионной комиссии (ревизора) товарищества ..." class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Аудиторские заключения </label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Аудиторские заключения" class="uk-width-1-1"></textarea>
                    </div>
                </div>


                </div>


                </li>

                <!-- ЗАДОЛЖЕННОСТИ -->
                <li>


                <div class="uk-form uk-form-horizontal">


                <div class="uk-form-row">
                    <label class="uk-form-label">Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях, за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_current_date', $manager_arrears->past_account_owner_service_current_date, [ 'class'=>'uk-width-1-1','placeholder' => 'Просроченная задолженность собственников помещений ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_current_date_less_25', $manager_arrears->past_account_owner_service_current_date_less_25, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_current_date_26_50', $manager_arrears->past_account_owner_service_current_date_26_50, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_current_date_51_75', $manager_arrears->past_account_owner_service_current_date_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_current_date_more_76', $manager_arrears->past_account_owner_service_current_date_more_76, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_current_date_alarm', $manager_arrears->past_account_owner_service_current_date_alarm, [ 'class'=>'uk-width-1-1','placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях, за оказанные услуги по управлению на начало отчетного периода, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_service_start_period', $manager_arrears->past_account_owner_service_start_period, ['class'=>'uk-width-1-1', 'placeholder' => 'Просроченная задолженность собственников помещений ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях, за коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date', $manager_arrears->past_account_owner_utilities_current_date, [ 'class'=>'uk-width-1-1','placeholder' => 'Просроченная задолженность собственников помещений ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date_heating', $manager_arrears->past_account_owner_utilities_current_date_heating, ['class'=>'uk-width-1-1', 'placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date_electricity', $manager_arrears->past_account_owner_utilities_current_date_electricity, ['class'=>'uk-width-1-1', 'placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date_gas', $manager_arrears->past_account_owner_utilities_current_date_gas, [ 'class'=>'uk-width-1-1','placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date_hot_water_supply', $manager_arrears->past_account_owner_utilities_current_date_hot_water_supply, [ 'class'=>'uk-width-1-1','placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date_cold_water', $manager_arrears->past_account_owner_utilities_current_date_cold_water, [ 'class'=>'uk-width-1-1','placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- водоотведение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_current_date_sanitation', $manager_arrears->past_account_owner_utilities_current_date_sanitation, [ 'class'=>'uk-width-1-1','placeholder' => 'водоотведение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Просроченная задолженность собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях за коммунальные услуги на начало отчетного периода, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_owner_utilities_start_period', $manager_arrears->past_account_owner_utilities_start_period, ['class'=>'uk-width-1-1', 'placeholder' => 'Просроченная задолженность собственников помещений ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Просроченная задолженность организации за предоставленные коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date', $manager_arrears->past_account_organ_utilities_current_date, ['class'=>'uk-width-1-1', 'placeholder' => 'Просроченная задолженность организации ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date_heating', $manager_arrears->past_account_organ_utilities_current_date_heating, ['class'=>'uk-width-1-1', 'placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date_electricity', $manager_arrears->past_account_organ_utilities_current_date_electricity, [ 'class'=>'uk-width-1-1','placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date_gas', $manager_arrears->past_account_organ_utilities_current_date_gas, ['class'=>'uk-width-1-1', 'placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date_hot_water_supply', $manager_arrears->past_account_organ_utilities_current_date_hot_water_supply, [ 'class'=>'uk-width-1-1','placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date_cold_water', $manager_arrears->past_account_organ_utilities_current_date_cold_water, ['class'=>'uk-width-1-1', 'placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- водоотведение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('past_account_organ_utilities_current_date_sanitation', $manager_arrears->past_account_organ_utilities_current_date_sanitation, [ 'class'=>'uk-width-1-1','placeholder' => 'водоотведение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях за услуги по управлению, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_service', $manager_arrears->amount_collected_debt_service, [ 'class'=>'uk-width-1-1','placeholder' => 'Сумма взысканной за отчетный период ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_service_less_25', $manager_arrears->amount_collected_debt_service_less_25, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_service_26_50', $manager_arrears->amount_collected_debt_service_26_50, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_service_51_75', $manager_arrears->amount_collected_debt_service_51_75, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_service_more_76', $manager_arrears->amount_collected_debt_service_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_service_alarm', $manager_arrears->amount_collected_debt_service_alarm, ['class'=>'uk-width-1-1', 'placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или проживающих в помещениях на законных основаниях за предоставленные коммунальные услуги, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities', $manager_arrears->amount_collected_debt_utilities, ['class'=>'uk-width-1-1', 'placeholder' => 'Сумма взысканной за отчетный период ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities_heating', $manager_arrears->amount_collected_debt_utilities_heating, [ 'class'=>'uk-width-1-1','placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities_electricity', $manager_arrears->amount_collected_debt_utilities_electricity, ['class'=>'uk-width-1-1', 'placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities_gas', $manager_arrears->amount_collected_debt_utilities_gas, ['class'=>'uk-width-1-1', 'placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities_hot_water_supply', $manager_arrears->amount_collected_debt_utilities_hot_water_supply, ['class'=>'uk-width-1-1', 'placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities_cold_water', $manager_arrears->amount_collected_debt_utilities_cold_water, ['class'=>'uk-width-1-1', 'placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- водоотведение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('amount_collected_debt_utilities_sanitation', $manager_arrears->amount_collected_debt_utilities_sanitation, [ 'class'=>'uk-width-1-1','placeholder' => 'водоотведение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>


                </div>


                </li>

                <!-- ДЕЯТЕЛЬНОСТЬ ПО УПРАВЛЕНИЮ МКД -->
                <li>


                <div class="uk-form uk-form-horizontal">


                <div class="uk-form-row">
                    <label class="uk-form-label">Объем работ по ремонту за отчетный период, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_repair', $manager_service_mkd->scope_repair, ['class'=>'uk-width-1-1', 'placeholder' => 'Объем работ по ремонту ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_repair_less_25', $manager_service_mkd->scope_repair_less_25, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_repair_26_50', $manager_service_mkd->scope_repair_26_50, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_repair_51_75', $manager_service_mkd->scope_repair_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_repair_more_76', $manager_service_mkd->scope_repair_more_76, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_repair_date_alarm', $manager_service_mkd->scope_repair_date_alarm, ['class'=>'uk-width-1-1', 'placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Объем работ по благоустройству за отчетный период, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_improvement', $manager_service_mkd->scope_improvement, ['class'=>'uk-width-1-1', 'placeholder' => 'Объем работ по благоустройству ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам до 25 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_improvement_less_25', $manager_service_mkd->scope_improvement_less_25, [ 'class'=>'uk-width-1-1','placeholder' => 'по домам до 25 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 26 до 50 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_improvement_26_50', $manager_service_mkd->scope_improvement_26_50, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 26 до 50 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам от 51 до 75 лет</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_improvement_51_75', $manager_service_mkd->scope_improvement_51_75, ['class'=>'uk-width-1-1', 'placeholder' => 'по домам от 51 до 75 лет', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по домам 76 лет и более</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_improvement_more_76', $manager_service_mkd->scope_improvement_more_76, [ 'class'=>'uk-width-1-1','placeholder' => '"по домам 76 лет и более', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- по аварийным домам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('scope_improvement_date_alarm', $manager_service_mkd->scope_improvement_date_alarm, [ 'class'=>'uk-width-1-1','placeholder' => 'по аварийным домам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Объем привлеченных средств за отчетный период, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds', $manager_service_mkd->volume_attracted_funds, [ 'class'=>'uk-width-1-1','placeholder' => 'Объем привлеченных средств ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- Субсидии</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds_subsidies', $manager_service_mkd->volume_attracted_funds_subsidies, ['class'=>'uk-width-1-1', 'placeholder' => 'Субсидии', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- Кредиты</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds_credits',$manager_service_mkd->volume_attracted_funds_credits, [ 'class'=>'uk-width-1-1','placeholder' => 'Кредиты', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- Финансирование по договорам лизинга</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds_financing_leasing', $manager_service_mkd->volume_attracted_funds_financing_leasing, ['class'=>'uk-width-1-1', 'placeholder' => 'Финансирование по договорам лизинга', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- Финансирование по энергосервисным договорам</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds_financing_energy', $manager_service_mkd->volume_attracted_funds_financing_energy, ['class'=>'uk-width-1-1', 'placeholder' => 'Финансирование по энергосервисным договорам', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- Целевые взносы жителей</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds_earmarked', $manager_service_mkd->volume_attracted_funds_earmarked, [ 'class'=>'uk-width-1-1','placeholder' => 'Целевые взносы жителей', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- Другие источники</label>
                    <div class="uk-form-controls">
                        {!! Form::text('volume_attracted_funds_other', $manager_service_mkd->volume_attracted_funds_other, [ 'class'=>'uk-width-1-1','placeholder' => 'Другие источники', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_indications', $manager_service_mkd->paid_ru_indications, [ 'class'=>'uk-width-1-1','placeholder' => 'Оплачено КУ по показаниям общедомовых ПУ ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_indications_heating', $manager_service_mkd->paid_ru_indications_heating, [ 'class'=>'uk-width-1-1','placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_indications_electricity', $manager_service_mkd->paid_ru_indications_electricity, ['class'=>'uk-width-1-1', 'placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_indications_gas', $manager_service_mkd->paid_ru_indications_gas, [ 'class'=>'uk-width-1-1','placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_indications_hot_water_supply', $manager_service_mkd->paid_ru_indications_hot_water_supply, [ 'class'=>'uk-width-1-1','placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_indications_cold_water', $manager_service_mkd->paid_ru_indications_cold_water, [ 'class'=>'uk-width-1-1','placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_accounts', $manager_service_mkd->paid_ru_accounts, [ 'class'=>'uk-width-1-1','placeholder' => 'Оплачено КУ по счетам на общедомовые ...', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- отопление</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_accounts_heating', $manager_service_mkd->paid_ru_accounts_heating, [ 'class'=>'uk-width-1-1','placeholder' => 'отопление', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- электричество</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_accounts_electricity', $manager_service_mkd->paid_ru_accounts_electricity, [ 'class'=>'uk-width-1-1','placeholder' => 'электричество', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- газ</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_accounts_gas', $manager_service_mkd->paid_ru_accounts_gas, [ 'class'=>'uk-width-1-1','placeholder' => 'газ', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- горячее водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_accounts_hot_water_supply', $manager_service_mkd->paid_ru_accounts_hot_water_supply, [ 'class'=>'uk-width-1-1','placeholder' => 'горячее водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">- холодное водоснабжение</label>
                    <div class="uk-form-controls">
                        {!! Form::text('paid_ru_accounts_cold_water', $manager_service_mkd->paid_ru_accounts_cold_water, [ 'class'=>'uk-width-1-1','placeholder' => 'холодное водоснабжение', 'autocomplete' => 'off' ]) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Проект договора управления</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Проект договора управления" class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Стоимость услуг</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Стоимость услуг" class="uk-width-1-1"></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label"><span class="uk-text-danger uk-text-small">"731"</span> Тарифы</label>
                    <div class="uk-form-controls">
                        <textarea cols="30" rows="3" placeholder="Тарифы" class="uk-width-1-1"></textarea>
                    </div>
                </div>


                </div>


                </li>

            <!-- РЕЙТИНГ -->
                <li>
                    Рейтинг.....
                </li>
            </ul>
            {!! Form::submit('Сохранить',['class'=>'uk-button uk-button-success uk-button-large uk-width-1-1 uk-margin-large-top']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>

@endsection