@extends('layout')

@section('content')

    <div class="uk-container uk-container-center">


        <div class="uk-grid uk-margin-large-top">
            <div class="uk-width-1-1">
                @include('/layout/body/search/searchMenu', array('menu' => $menu))
                <h1>Результаты поиска</h1>
                @if ($page == 'house') @include('/layout/body/search/house', array('menu' => $menu)) @endif
                @if ($page == 'manager') @include('/layout/body/search/manager', array('menu' => $menu)) @endif
            </div>
        </div>
    </div>

@endsection