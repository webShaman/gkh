@extends('layout')

@section('content')
<div class="tm-home">
    <div class="uk-container uk-container-center">
        <div class="uk-vertical-align uk-clearfix tm-area-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-h2 uk-margin-remove"><span>Анкета дома</span> г. Нижний Новгород, Сормовский р-н, ул. Исполкома, д. 11</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href=""><i class="uk-icon-search"></i> Найти свой дом</a></div>
		    </div>
		</div>
		<div class="uk-grid">
			<div class="uk-width-4-10">
				<div class="uk-thumbnail"><img src="https://api-maps.yandex.ru/services/constructor/1.0/static/?sid=5_X7s2xCkc6Gt_R3_8JbznOLvOpjQ11v&width=421&height=270" alt=""/></div>
			</div>
			<div class="uk-width-6-10">
				<h3 class="uk-margin-remove">Домом управляет: <a href="">ОАО "ДК Приокского района"</a></h3>
				<h3 class="uk-margin-small-top uk-margin-bottom-remove">Состояние дома: <span>Исправный</span></h3>
				<hr class="uk-article-divider uk-margin">
				<div class="uk-grid uk-grid-medium">
					<div class="uk-width-1-2">
						<div class="uk-panel">
							<table class="tm-table-strip">
	                            <tbody>
	                                <tr>
	                                    <td><span>Общая площадь помещений</span></td>
	                                    <td><span class="uk-text-bold">{{$home['total_area'] or 'Нет данных' }}</span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Кадастровый номер участка</span></td>
	                                    <td><span class="uk-text-bold">{{$home['cadastral_number'] or 'Нет данных' }}</span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Год ввода в эксплуатацию</span></td>
	                                    <td><span class="uk-text-bold">{{$home['year_commissioning'] or 'Нет данных' }}</span></td>
	                                </tr>
	                            </tbody>
	                        </table>
						</div>
					</div>
					<div class="uk-width-1-2">
						<div class="uk-panel">
							<table class="tm-table-strip">
	                            <tbody>
	                                <tr>
	                                    <td><span>Последнее изменение анкеты</span></td>
	                                    <td><span class="uk-text-bold">нет данных</span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Дата начала <br/>обслуживания дома</span></td>
	                                    <td><span class="uk-text-bold"> ??? </span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Плановая дата прекращения обслуживания дома</span></td>
	                                    <td><span class="uk-text-bold"> ??? </span></td>
	                                </tr>
	                            </tbody>
	                        </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#home-table'}">
            <li class="uk-active" ><a href="#">Паспорт</a></li>
            <li><a href="#">Управление</a></li>
            <li><a href="#">Финансы</a></li>
            <li><a href="#">История управления</a></li>
        </ul>
        <ul id="home-table" class="uk-switcher">
            <li>
            	<ul class="uk-tab" data-uk-tab="{connect:'#home-table-passport'}">
                    <li class="uk-active"><a href="#">Общая характеристика</a></li>
                    <li><a href="#">Конструктивные элементы дома</a></li>
                    <li><a href="#">Инженерные системы</a></li>
                    <li><a href="#">Лифты</a></li>
                </ul>
                <ul id="home-table-passport" class="uk-switcher uk-margin">
                    <li>
                    	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
                    	<div class="uk-grid">
                        	<div class="uk-width-1-2">
	                        	<table class="tm-table-strip">
		                            <tbody>
		                                <tr>
			                                <td width="30"><span>1</span></td>
		                                    <td><span>Серия, тип проекта</span></td>
		                                    <td><span>{{$home['series'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>2</span></td>
		                                    <td><span>Описание местоположения</span></td>
		                                    <td><span>{{$home['description_position'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>3</span></td>
		                                    <td><span>Индивидуальное наименование дома</span></td>
		                                    <td><span>{{$home['individual_name'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>4</span></td>
		                                    <td><span>Тип жилого дома</span></td>
		                                    <td><span>{{$home['type_home'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>5</span></td>
		                                    <td><span>Год ввода в эксплуатацию</span></td>
		                                    <td><span>{{$home['year_commissioning'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>6</span></td>
		                                    <td><span>Материал стен</span></td>
		                                    <td><span>{{$home['wall_material'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>7</span></td>
		                                    <td><span>Тип перекрытий</span></td>
		                                    <td><span>{{$home['type_floors'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>8</span></td>
		                                    <td><span>Этажность</span></td>
		                                    <td><span>{{$home['number_storeys'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>9</span></td>
		                                    <td><span>Количество подъездов</span></td>
		                                    <td><span>{{$home['number_entrances'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>10</span></td>
		                                    <td><span>Количество лифтов</span></td>
		                                    <td><span>{{$home['number_lifts'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>11</span></td>
		                                    <td><span>Общая площадь, м<sup>2</sup> <span class="uk-badge uk-badge-danger">731</span></span></td>
		                                    <td><span>{{$home['total_area'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>12</span></td>
		                                    <td><span class="uk-text-bold">Площадь жилых помещений всего, м<sup>2</sup></span></td>
		                                    <td><span class="uk-text-bold">{{$home['residential_area'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span></span></td>
		                                    <td><span>&mdash; Частная</td>
		                                    <td><span>{{$home['residential_area_private'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span></span></td>
		                                    <td><span>&mdash; Муниципальная</td>
		                                    <td><span>{{$home['residential_area_municipal'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span></span></td>
		                                    <td><span>&mdash; Государственная</td>
		                                    <td><span>{{$home['residential_area_state'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>13</span></td>
		                                    <td><span>Площадь нежилых помещений, м<sup>2</sup></span></td>
		                                    <td><span>{{$home['​​residential_non_area'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>14</span></td>
		                                    <td><span>Площадь участка, м<sup>2</sup></span></td>
		                                    <td><span>{{$home['land_area'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>15</span></td>
		                                    <td><span>Площадь придомовой территории, м<sup>2</sup></span></td>
		                                    <td><span>{{$home['territory_area'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>16</span></td>
		                                    <td><span>Инвентарный номер</span></td>
		                                    <td><span>{{$home['inventory_number'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>17</span></td>
		                                    <td><span>Кадастровый номер участка</span></td>
		                                    <td><span>{{$home['cadastral_number'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>18</span></td>
		                                    <td><span>Количество квартир</span></td>
		                                    <td><span>{{$home['apartments_number'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>19</span></td>
		                                    <td><span>Количество жителей</span></td>
		                                    <td><span>{{$home['residents_number'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>20</span></td>
		                                    <td><span>Количество лицевых счетов</span></td>
		                                    <td><span>{{$home['accounts_number'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>21</span></td>
		                                    <td class="dotted-none uk-text-left" colspan="2">
			                                    <span class="uk-text-bold">Конструктивные особенности дома <span class="uk-badge uk-badge-danger">731</span></span> 
			                                    <div class="uk-text-muted uk-margin-small-top">{{$home['design_features'] or 'Нет данных' }}</div>
		                                    </td>
		                                </tr>
		                                <tr>
			                                <td><span>22</span></td>
		                                    <td class="dotted-none uk-text-left" colspan="2"><span class="uk-text-bold">Удельная тепловая характеристика здания</span></td>
		                                </tr>
		                                <tr>
			                                <td><span></span></td>
		                                    <td><span>&mdash; фактический удельный расход, Вт/М3Сград</td>
		                                    <td><span>{{$home['thermal_characteristic_actual'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span></span></td>
		                                    <td><span>&mdash; нормативный удельный расход, Вт/М3Сград</td>
		                                    <td><span>{{$home['thermal_characteristic_regulatory'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>23</span></td>
		                                    <td><span>Класс энергоэффективности</span></td>
		                                    <td><span>{{$home['energy_efficiency'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>24</span></td>
		                                    <td><span>Дата проведения энергетического аудита</span></td>
		                                    <td><span>{{$home['date_energy_audit'] or 'Нет данных' }}</span></td>
		                                </tr>
		                                <tr>
			                                <td><span>25</span></td>
		                                    <td><span>Дата начала приватизации</span></td>
		                                    <td><span>{{$home['date_privatization'] or 'Нет данных' }}</span></td>
		                                </tr>
		                        </table>
                        	</div>
                        	<div class="uk-width-1-2">
	                        	<div class="uk-panel uk-panel-box">
		                        	<table class="tm-table-strip">
			                            <tbody>
			                                <tr>
			                                    <td><span>Общая степень износа <span class="uk-badge uk-badge-danger">731</span></span></td>
			                                    <td><span>{{$home['wear'] or 'Нет данных' }}</span></td>
			                                </tr>
			                                <tr>
			                                    <td><span>Степень износа фундамента <span class="uk-badge uk-badge-danger">731</span></span></td>
			                                    <td><span>{{$home['wear_foundation'] or 'Нет данных' }}</span></td>
			                                </tr>
			                                <tr>
			                                    <td><span>Степень износа несущих стен <span class="uk-badge uk-badge-danger">731</span></span></td>
			                                    <td><span>{{$home['wear_wall'] or 'Нет данных' }}</span></td>
			                                </tr>
			                                <tr>
			                                    <td><span>Степень износа перекрытий <span class="uk-badge uk-badge-danger">731</span></span></td>
			                                    <td><span>{{$home['wear_overlap'] or 'Нет данных' }}</span></td>
			                                </tr>
			                            </tbody>
			                        </table>
	                        	</div>
                        	</div>
                    	</div>
                    </li>
                    <li>
                    	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
	                            <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Фасад</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-2-3"><span>Площадь фасада общая, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_total'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>2</span></td>
                                    <td><span>Площадь фасада оштукатуренная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_plastered'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>3</span></td>
                                    <td><span>Площадь фасада неоштукатуренная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_not_plastered'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>4</span></td>
                                    <td><span>Площадь фасада панельная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_panel'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>5</span></td>
                                    <td><span>Площадь фасада, облицованная плиткой, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_tiles'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>6</span></td>
                                    <td><span>Площадь фасада, облицованная сайдингом, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_siding'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>7</span></td>
                                    <td><span>Площадь фасада деревянная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_wooden'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>8</span></td>
                                    <td><span>Площадь утепленного фасада с отделкой декоративной штукатуркой, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_thermal_finish'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>9</span></td>
                                    <td><span>Площадь утепленного фасада с отделкой плиткой, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_thermal_tiles'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>10</span></td>
                                    <td><span>Площадь утепленного фасада с отделкой сайдингом, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_thermal_siding'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>11</span></td>
                                    <td><span>Площадь отмостки, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_otmostka'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>12</span></td>
                                    <td><span>Площадь остекления мест общего пользования (дерево) , м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_glazing_common'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>13</span></td>
                                    <td><span>Площадь остекления мест общего пользования (пластик) , м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_glazing_public'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>14</span></td>
                                    <td><span>Площадь индивидуального остекления (дерево) , м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_individual_glazing_wood'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>15</span></td>
                                    <td><span>Площадь индивидуального остекления (пластик) , м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_individual_glazing_plastic'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>16</span></td>
                                    <td><span>Площадь металлических дверных заполнений, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_metal_door'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>17</span></td>
                                    <td><span>Площадь иных дверных заполнений, м<sup>2</sup></span></td>
                                    <td><span>{{$home['facade_area_wood_door'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>18</span></td>
                                    <td><span>Год проведения последнего капитального ремонта</span></td>
                                    <td><span>{{$home['facade_year_renovation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Кровля</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Площадь кровли общая, м<sup>2</sup></span></td>
                                    <td><span>{{$home['roof_area_total'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Площадь кровли шиферная скатная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['roof_area_ramp_slate'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Площадь кровли металлическая скатная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['roof_area_ramp_metal'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Площадь кровли иная скатная, м<sup>2</sup></span></td>
                                    <td><span>{{$home['roof_area_ramp_different'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Площадь кровли плоская, м<sup>2</sup></span></td>
                                    <td><span>{{$home['roof_area_flat'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Год проведения последнего капитального ремонта кровли</span></td>
                                    <td><span>{{$home['roof_year_renovation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Подвал</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Сведения о подвале</span></td>
                                    <td><span>{{$home['basement_Information'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Площадь подвальных помещений (включая помещения подвала и техподполье, если оно требует ремонта) , м<sup>2</sup></span></td>
                                    <td><span>{{$home['basement_area_total'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Год проведения последнего капитального ремонта подвальных помещений</span></td>
                                    <td><span>{{$home['basement_year_renovation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Помещения общего пользования</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Площадь помещений общего пользования, м<sup>2</sup></span></td>
                                    <td><span>{{$home['common_area_total'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Год проведения последнего ремонта помещений общего пользования</span></td>
                                    <td><span>{{$home['common_year_renovation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Мусоропроводы</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Количество мусоропроводов в доме</span></td>
                                    <td><span>{{$home['chutes_number'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Год проведения последнего ремонта мусоропроводов</span></td>
                                    <td><span>{{$home['chutes_year_renovation'] or 'Нет данных' }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                    	<table class="tm-table-strip tm-table-strip-align">
                            <tbody>
	                            <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система отопления</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td class="uk-width-2-3"><span>Тип</span></td>
                                    <td><span>{{$home['heating_system_type'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Количество элеваторных узлов системы отопления</span></td>
                                    <td><span>{{$home['heating_number_elevator_units'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Длина трубопроводов системы отопления, м</span></td>
                                    <td><span>{{$home['heating_length_pipelines'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Год проведения последнего капитального ремонта системы отопления</span></td>
                                    <td><span>{{$home['heating_year_overhaul'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Количество точек ввода отопления</span></td>
                                    <td><span>{{$home['heating_number_dot_in'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Количество узлов управления отоплением</span></td>
                                    <td><span>{{$home['heating_number_control_units'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Количество общедомовых приборов учета отопления</span></td>
                                    <td><span>{{$home['heating_number_obschedomovyh'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td><span>Отпуск отопления производится</span></td>
                                    <td><span>{{$home['heating_leave'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система горячего водоснабжения</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Тип</span></td>
                                    <td><span>{{$home['hotwater_system_type'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Длина трубопроводов системы горячего водоснабжения, м</span></td>
                                    <td><span>{{$home['hotwater_length_pipelines'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Год проведения последнего капитального ремонта системы горячего водоснабжения</span></td>
                                    <td><span>{{$home['hotwater_year_overhaul'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Количество точек ввода горячей воды</span></td>
                                    <td><span>{{$home['hotwater_number_dot_in'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Количество узлов управления поставкой горячей воды</span></td>
                                    <td><span>{{$home['hotwater_number_control_units'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Количество общедомовых приборов учета горячей воды</span></td>
                                    <td><span>{{$home['hotwater_number_obschedomovyh'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Отпуск горячей воды производится</span></td>
                                    <td><span>{{$home['hotwater_leave'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система холодного водоснабжения</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Тип</span></td>
                                    <td><span>{{$home['coldwater_system_type'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Длина трубопроводов системы холодного водоснабжения, м</span></td>
                                    <td><span>{{$home['coldwater_length_pipelines'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Год проведения последнего капитального ремонта системы холодного водоснабжения</span></td>
                                    <td><span>{{$home['coldwater_year_overhaul'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Количество точек ввода холодной воды</span></td>
                                    <td><span>{{$home['coldwater_number_dot_in'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Количество общедомовых приборов учета холодной воды</span></td>
                                    <td><span>{{$home['coldwater_number_obschedomovyh'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Отпуск холодной воды производится</span></td>
                                    <td><span>{{$home['coldwater_leave'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система водоотведения (канализации)</span></td>
                                </tr>
                                 <tr>
	                                <td><span>1</span></td>
                                    <td><span>Тип</span></td>
                                    <td><span>{{$home['sewage_system_type'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Длина трубопроводов системы водоотведения, м</span></td>
                                    <td><span>{{$home['sewage_length_pipelines'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Год проведения последнего капитального ремонта системы водоотведения (канализации)</span></td>
                                    <td><span>{{$home['sewage_year_overhaul'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система электроснабжения</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Система электроснабжения</span></td>
                                    <td><span>{{$home['electrical'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Длина сетей в местах общего пользования, м</span></td>
                                    <td><span>{{$home['electrical_length_commons'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Год проведения последнего капремонта системы электроснабжения</span></td>
                                    <td><span>{{$home['electrical_year_overhaul'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Количество точек ввода электричества</span></td>
                                    <td><span>{{$home['electrical_number_dot_in'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Количество общедомовых приборов учета электричества</span></td>
                                    <td><span>{{$home['electrical_number_obschedomovyh'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Отпуск электричества производится</span></td>
                                    <td><span>{{$home['electrical_leave'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система газоснабжения</span></td>
                                </tr>
                                <tr>
	                                <td><span>1</span></td>
                                    <td><span>Вид системы газоснабжения</span></td>
                                    <td><span>{{$home['gas_system_type'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Длина сетей соответствующих требованиям</span></td>
                                    <td><span>{{$home['gas_length_compliant'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Длина сетей не соответствующих требованиям</span></td>
                                    <td><span>{{$home['gas_length_non_compliant'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Год проведения последнего капремонта системы газоснабжения</span></td>
                                    <td><span>{{$home['gas_year_overhaul'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Количество точек ввода газа</span></td>
                                    <td><span>{{$home['gas_number_dot_in'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span>Количество общедомовых приборов учета газа</span></td>
                                    <td><span>{{$home['gas_number_obschedomovyh'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Отпуск газа производится</span></td>
                                    <td><span>{{$home['gas_leave'] or 'Нет данных' }}</span></td>
                                </tr>
                            </tbody>
                    	</table>
                    </li>
                    <li>Лифты. {{$home['elevators'] or 'Нет данных' }}</li>
                </ul>
            </li>
            <li>
            	<p class="uk-text-muted">Знаком <span class="uk-badge uk-badge-danger">731</span> обозначены поля, обязательные к раскрытию согласно Стандарту раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, утвержденному Постановлением Правительства РФ от 23.09.2010 N 731.</p>
            	<div class="uk-grid">
	            	<div class="uk-width-1-2">
                        <h3>Карточка договор с управляющей организацией</h3>
                        <table class="tm-table-strip">
                            <tbody>
	                            <tr>
	                                <td><span>1</span></td>
                                    <td class="dotted-none uk-text-left" colspan="2"><span class="uk-text-bold">Тип договора управления</span> <div class="uk-text-muted uk-margin-small-top">{{$home['management_type_contract'] or 'Нет данных' }}</div></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>2</span></td>
                                    <td><span>{{$home['management_date_start'] or 'Нет данных' }}</span></td>
                                    <td><span>27.11.2006</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>3</span></td>
                                    <td><span>{{$home['management_date_end'] or 'Нет данных' }}</span></td>
                                    <td><span>01.01.2016</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td class="dotted-none uk-text-left" colspan="2">
	                                    <span class="uk-text-bold">Выполняемые работы <span class="uk-badge uk-badge-danger">731</span></span> 
	                                    <div class="uk-panel tm-panel-box-table">
		                                    <ol class="uk-list-space">
		                                        {{$home['management_performed_works'] or 'Нет данных' }}
			                                    {{--<li><a href="">ссылка на скачку документа</a></li>--}}

		                                    </ol>
	                                    </div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td class="dotted-none uk-text-left" colspan="2">
	                                    <span class="uk-text-bold">Выполнение обязательств <span class="uk-badge uk-badge-danger">731</span></span> 
	                                    <div class="uk-panel tm-panel-box-table">
		                                    <ol class="uk-list-space">
			                                    {{$home['management_execution_works'] or 'Нет данных' }}{{--<li><a href="">ссылка на скачку документа</a></li>--}}

		                                    </ol>
	                                    </div>
                                    </td>
                                </tr>
                                <tr>
	                                <td width="30"><span>6</span></td>
                                    <td><span>Примечание</span></td>
                                    <td><span>{{$home['management_note'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td class="dotted-none uk-text-left" colspan="2">
	                                    <span class="uk-text-bold">Стоимость услуг <span class="uk-badge uk-badge-danger">731</span></span> 
	                                    {{--<div class="uk-text-muted uk-margin-small-top">Нет данных</div>--}}
	                                    <div class="uk-panel tm-panel-box-table">
		                                    <ol class="uk-list-space">
			                                    {{$home['management_cost_services'] or 'Нет данных' }}
		                                    </ol>
	                                    </div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td class="dotted-none uk-text-left" colspan="2">
	                                    <span class="uk-text-bold">Средства ТСЖ или ЖСК <span class="uk-badge uk-badge-danger">731</span></span> 
	                                    <div class="uk-text-muted uk-margin-small-top">{{$home['management_means'] or 'Нет данных' }}</div>
                                    </td>
                                </tr>
                                <tr>
	                                <td><span>9</span></td>
                                    <td class="dotted-none uk-text-left" colspan="2">
	                                    <span class="uk-text-bold">Условия оказания услуг ТСЖ или ЖСК <span class="uk-badge uk-badge-danger">731</span></span> 
	                                    <div class="uk-text-muted uk-margin-small-top">{{$home['management_terms_services'] or 'Нет данных' }}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
	            	</div>
	            	<div class="uk-width-1-2">
                        <h3>Карточка договоров с РСО</h3>
                        <table class="tm-table-strip-none">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td width="230"><span class="uk-text-bold">Поставщик отопления <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{$home['rco_heating'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span class="uk-text-bold">Поставщик электричества <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{$home['rco_electricity'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span class="uk-text-bold">Поставщик газа <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{$home['rco_gas'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span class="uk-text-bold">Поставщик горячей воды <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{$home['rco_hot_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span class="uk-text-bold">Поставщик холодной воды <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{$home['rco_cold_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span class="uk-text-bold">Поставщик водоотведения <span class="uk-badge uk-badge-danger">731</span></span></td>
                                    <td><span>{{$home['rco_sanitation'] or 'Нет данных' }}</span></td>
                                </tr>
                            </tbody>
                        </table>
	            	</div>
            	</div>
            </li>
            <li>
            	<div class="uk-grid">
	            	<div class="uk-width-1-2">
		            	<h3>Управление общим имуществом</h3>
		            	<table class="tm-table-strip">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td><span>Доход от управления за отчетный период, тыс. руб.</span></td>
                                    <td width="100"><span>{{$home['rco_sanitation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>2</span></td>
                                    <td><span>Доход от управления общим имуществом за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_income_property'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>3</span></td>
                                    <td><span>Расходы на управление за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_costs'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>4</span></td>
                                    <td><span>Задолженность собственников за услуги управления за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_debt'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>5</span></td>
                                    <td><span>Взыскано с собственников за услуги управления за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_recover_owners'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>6</span></td>
                                    <td><span class="uk-text-bold">Выплаты по искам и договорам управления за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_payment'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иски по компенсации нанесенного ущерба</td>
                                    <td><span>{{$home['common_claims'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иски по снижению платы в связи с неоказанием услуг</td>
                                    <td><span>{{$home['common_claims_refusal'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иски по снижению платы в связи с недопоставкой ресурсов</td>
                                    <td><span>{{$home['common_claims_shipment'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>7</span></td>
                                    <td><span>Объем работ по ремонту за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_work_repair'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>8</span></td>
                                    <td><span>Объем работ по благоустройству за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_work_improvement'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span>9</span></td>
                                    <td><span class="uk-text-bold">Объем привлеченных средств за отчетный период, тыс. руб.</span></td>
                                    <td><span>{{$home['common_attracted_funds'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; субсидии</td>
                                    <td><span>{{$home['common_subsidy'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; кредиты</td>
                                    <td><span>{{$home['common_credits'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; финансирование по договорам лизинга</td>
                                    <td><span>{{$home['common_financing_lease'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; финансирование по энергосервисным договорам</td>
                                    <td><span>{{$home['common_financing_energy'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; целевые взносы жителей</td>
                                    <td><span>{{$home['common_contributions'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; иные источники</td>
                                    <td><span>{{$home['common_other_sources'] or 'Нет данных' }}</span></td>
                                </tr>
                            </tbody>
		            	</table>
	            	</div>
	            	<div class="uk-width-1-2">
	            		<h3>Коммунальные услуги</h3>
		            	<table class="tm-table-strip">
                            <tbody>
                                <tr>
	                                <td width="30"><span>1</span></td>
                                    <td><span class="uk-text-bold">Доход от поставки КУ за отчетный период, тыс. руб.</span></td>
                                    <td width="100"><span>{{$home['income'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</td>
                                    <td><span>{{$home['income_heating'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</td>
                                    <td><span>{{$home['income_electricity'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</td>
                                    <td><span>{{$home['income_gas'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</td>
                                    <td><span>{{$home['income_hot_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</td>
                                    <td><span>{{$home['income_cold_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</td>
                                    <td><span>{{$home['income_sanitation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>2</span></td>
                                    <td><span class="uk-text-bold">Задолженность собственников за КУ за отчетный период, тыс. руб.</span></td>
                                    <td width="100"><span>{{$home['debt'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</td>
                                    <td><span>{{$home['debt_heating'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</td>
                                    <td><span>{{$home['debt_electricity'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</td>
                                    <td><span>{{$home['debt_gas'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</td>
                                    <td><span>{{$home['debt_hot_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</td>
                                    <td><span>{{$home['debt_cold_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</td>
                                    <td><span>{{$home['debt_sanitation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>3</span></td>
                                    <td><span class="uk-text-bold">Взыскано с собственников за КУ за отчетный период, тыс. руб.</span></td>
                                    <td width="100"><span>{{$home['charged'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</td>
                                    <td><span>{{$home['charged_heating'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</td>
                                    <td><span>{{$home['charged_electricity'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</td>
                                    <td><span>{{$home['charged_gas'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</td>
                                    <td><span>{{$home['charged_hot_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</td>
                                    <td><span>{{$home['charged_cold_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</td>
                                    <td><span>{{$home['charged_sanitation'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>4</span></td>
                                    <td><span class="uk-text-bold">Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс. руб.</span></td>
                                    <td width="100"><span>{{$home['paid'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</td>
                                    <td><span>{{$home['paid_heating'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</td>
                                    <td><span>{{$home['paid_electricity'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</td>
                                    <td><span>{{$home['paid_gas'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</td>
                                    <td><span>{{$home['paid_hot_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</td>
                                    <td><span>{{$home['paid_cold_water'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</td>
                                    <td><span>{{$home['paid_resources'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td width="30"><span>5</span></td>
                                    <td><span class="uk-text-bold">Оплачено ресурсов по счетам на общедомовые нужды за отчетный период, тыс. руб.</span></td>
                                    <td width="100"><span>{{$home['paid_resources_heating'] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; отопление</td>
                                    <td><span>{{$home[''] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; электричество</td>
                                    <td><span>{{$home[''] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; газ</td>
                                    <td><span>{{$home[''] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; горячее водоснабжение</td>
                                    <td><span>{{$home[''] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; холодное водоснабжение</td>
                                    <td><span>{{$home[''] or 'Нет данных' }}</span></td>
                                </tr>
                                <tr>
	                                <td><span></span></td>
                                    <td><span>&mdash; водоотведение</td>
                                    <td><span>{{$home[''] or 'Нет данных' }}</span></td>
                                </tr>
                            </tbody>
		            	</table>
	            	</div>
            	</div>
            </li>
            <li>
            	<p class="uk-text-muted">В данном разделе представлена история управление данным многоквартирным домом. Так же вы можете просмотреть версию анкеты на важных этапах управления домом, таких как смена управляющей организации и закрытие отчетного периода.</p>
            	<div class="uk-panel uk-panel-box uk-width-2-3">
	            	<div class="uk-grid">
		            	<div class="uk-width-1-2">
			            	<div>27 Ноября 2006 г. - Настоящее время</div>
			            	<a href="">ОАО "ДК Приокского района"</a>
		            	</div>
		            	<div class="uk-width-1-2">
			            	<ul class="uk-nav uk-nav-side">
							    <li class="uk-active"><a href="#">Текущая анкета</a></li>
							    <li><a href="#">Архивная анкета за 2014 год - Закрытие отчетного периода</a></li>
							</ul>
		            	</div>
	            	</div>
            	</div>
            </li>
        </ul>
    </div>
</div>
@endsection