@extends('layout')

@section('content')
<div class="tm-area">
    <div class="uk-container uk-container-center">
	    <div class="uk-vertical-align uk-clearfix tm-area-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-margin-remove">Жилищный фонд {{ Area::getAreaOnName($area['name']) }} района</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href="/search/house"><i class="uk-icon-search"></i> Найти свой дом</a></div>
		    </div>
		</div>
        <div class="uk-grid">
            <div class="uk-width-2-3">
	            <table class="uk-table uk-table-hover">
				    <thead>
				        <tr>
				            <th class="uk-width-1-3">Название</th>
				            <th>Год ввода в эксплуатацию</th>
				            <th class="tm-nowrap">Площадь, м<sup>2</sup></th>
				            <th>Управляющая организация</th>
				        </tr>
				    </thead>
				    <tbody>
				        @if ($grid)
				            @foreach($grid as $element)
                                <tr>
                                    <td><a href="/myhouse/{{ $area['name'] }}/{{ $element['id'] }}">г. Нижний Новгород, {{ $area['name_ru'] }} р-н, ул. {{ $element['name'] }}</a></td>
                                    <td>{{ $element['year'] or 'не указанно'}}</td>
                                    <td>{{ $element['total_area'] or 'не указанно'}}</td>
                                    <td>{{ $element['manager']->short_name or 'не указанно'}}</td>
                                </tr>
                            @endforeach
				        @else
                            <tr>
                                <td colspan="5">значений не найденно</td>
                            </tr>
				        @endif
				    </tbody>
				</table>
                @if ($render)
                    {!! $render !!}
                @endif
            </div>
            <div class="uk-width-1-3">
				<p>Общая информация для <b>{{Area::getAreaOnName($area['name'])}} района</b></p>
	            <table class="tm-table-strip uk-margin-bottom">
                    <tbody>
                        <tr>
                            <td><span>домов</span></td>
                            <td><span class="uk-text-bold">{{ $numberHomes }} ед.</span></td>
                        </tr>
                        <tr>
                            <td><span>жилой площадью</span></td>
                            <td><span class="uk-text-bold">{{ $numberResidentialArea }} кв.м</span></td>
                        </tr>
                    </tbody>
                </table>
				
	            <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Стандарт раскрытия</h3>
                    В соответствии с Постановлением Правительства РФ от 23.09.2010 года № 731 "Об утверждении стандарта раскрытия информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами" организации обязаны раскрывать информацию о своей деятельности путем публикации ее на сайтах в сети Интернет, предназначенных для этих целей.
                    <div class="uk-text-right uk-margin-small-top"><a href="/standard"><u>Перейти к раскрытию</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Справка</h3>
                    Справочная информация для пользователей включает: инструкции, видеоуроки, часто задаваемые вопросы и ответы на них. Перед обращением в Службу поддержки просим убедиться, что интересующая информация не представлена в данном разделе
                    <div class="uk-text-right uk-margin-small-top"><a href="/help"><u>Перейти в раздел Справка</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Техническая поддержка</h3>
                    В случае возникновения трудностей в работе с порталом обратитесь в Службу технической поддержки по адресу электронной почты: <b>info@zhkh-nn.ru</b>
                    <div class="uk-text-right uk-margin-small-top"><a href="/support"><u>Подробнее</u></a></div>
                </div>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Горячая линия</h3>
                    Для получения консультации по раскрытию информации организациями, осуществляющими деятельность в сфере управления многоквартирными домами, а также по вопросам капитального ремонта обратитесь на Горячую линию Ассоциации ЖКХ города Нижнего Новгорода по телефону <b>8 (800) 775 76 77</b> в рабочие дни с 9 до 19 ч. (время московское)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection