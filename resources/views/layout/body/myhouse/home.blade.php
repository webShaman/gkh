@extends('layout')

@section('content')
<div class="tm-home">
    <div class="uk-container uk-container-center">
        <div class="uk-vertical-align uk-clearfix tm-area-title">
		    <div class="uk-width-3-4 uk-vertical-align-middle">
				<div class="uk-float-left"><h1 class="uk-h2 uk-margin-remove"><span>Анкета дома</span> г. Нижний Новгород, {{ Area::getNameRu_ById($home['homes']['area_id']) }} р-н, {{ $home['homes']['address'] }}</h1></div>
		    </div>
		    <div class="uk-width-1-4 uk-vertical-align-middle">
				<div class="uk-float-right"><a class="uk-button" href="/search/house"><i class="uk-icon-search"></i> Найти свой дом</a></div>
		    </div>
		</div>
		<div class="uk-grid">
			<div class="uk-width-4-10">
				<div class="uk-thumbnail"><script type="text/javascript" charset="utf-8" src="{!! $home['homes']['map']!!}"></script></div>
			</div>
			<div class="uk-width-6-10">
				<h3 class="uk-margin-remove">Домом управляет:
                    @if (!empty($manager))
                        <a href="/mymanager/avtozavodsky/{{$manager['id']}}">{{$manager['short_name']}}</a>
                    @else
                        организация не выбранна
                    @endif
                </h3>
				<hr class="uk-article-divider uk-margin">
				<div class="uk-grid uk-grid-medium">
					<div class="uk-width-1-2">
						<div class="uk-panel">
							<table class="tm-table-strip">
	                            <tbody>
	                                <tr>
	                                    <td><span>Общая площадь помещений</span></td>
	                                    <td><span class="uk-text-bold">{{$home['homes_passport_general_information']['total_area'] or 'Нет данных' }}</span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Кадастровый номер участка</span></td>
	                                    <td><span class="uk-text-bold">{{$home['homes_passport_general_information']['cadastral_number'] or 'Нет данных' }}</span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Год ввода в эксплуатацию</span></td>
	                                    <td><span class="uk-text-bold">{{$home['homes_passport_general_information']['year_commissioning'] or 'Нет данных' }}</span></td>
	                                </tr>
	                            </tbody>
	                        </table>
						</div>
					</div>
					<div class="uk-width-1-2">
						<div class="uk-panel">
							<table class="tm-table-strip">
	                            <tbody>
	                                <tr>
	                                    <td><span>Последнее изменение анкеты</span></td>
	                                    <td><span class="uk-text-bold">нет данных</span></td>
	                                </tr>
	                                <tr>
	                                    <td><span>Дата начала <br/>обслуживания дома</span></td>
	                                    <td><span class="uk-text-bold"> ??? </span></td>
	                                </tr>
	                            </tbody>
	                        </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#home_menu'}">
            <li class="uk-active" ><a href="#">Паспорт</a></li>
            <li><a href="#">Управление</a></li>
            <li><a href="#">Отчеты по управлению</a></li>
            <li><a href="#">История управления</a></li>
        </ul>
        <ul id="home_menu" class="uk-switcher">
            <li>
            	<ul class="uk-tab" data-uk-tab="{connect:'#passport'}">
                    <li class="uk-active"><a href="#">Общая сведения</a></li>
                    <li><a href="#">Конструктивные элементы дома</a></li>
                    <li><a href="#">Инженерные системы</a></li>
                    <li><a href="#">Лифты</a></li>
                    <li><a href="#">Приборы учета</a></li>
                </ul>
                <ul id="passport" class="uk-switcher uk-margin">
                    <li>
	                	<table class="tm-table-strip">
	                        <tbody>
                                @include('/layout/body/myhouse/passport/generalInformation', array('generalInformation' => $home['homes_passport_general_information']))
                            </tbody>
	                    </table>
                    </li>
                    <li>
                    	<table class="tm-table-strip">
                            <tbody>
                                @include('/layout/body/myhouse/passport/structuralElements', array('structuralElements' => $home['homes_passport_structural_elements']))
                            </tbody>
                        </table>
                    </li>
                    <li>
                    	<table class="tm-table-strip">
                            <tbody>
	                            @include('/layout/body/myhouse/passport/engineeringSystems', array('engineeringSystems' => $home['homes_passport_engineering_systems']))
                            </tbody>
                    	</table>
                    </li>
                    <li>
                    	<table class="uk-table tm-table-red">
						    <thead>
						        <tr>
						            <th>Номер лифта</th>
						            <th>Номер подъезда</th>
						            <th>Тип лифта</th>
						            <th>Год ввода в эксплуатацию</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td>???</td>
						            <td>???</td>
						            <td>???</td>
						            <td>???</td>
						        </tr>
						    </tbody>
						</table>
                    </li>
                    <li>
                    	<table class="uk-table tm-table-red">
						    <thead>
						        <tr>
						            <th>Вид коммунального ресурса</th>
						            <th>Наличие прибора учета</th>
						            <th>Единица измерения</th>
						            <th>Дата ввода в эксплуатацию</th>
						            <th>Тип прибора учета</th>
						            <th>Дата поверки / замены прибора учета</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td>???</td>
						            <td>???</td>
						            <td>???</td>
						            <td>???</td>
						            <td>???</td>
						            <td>???</td>
						        </tr>
						    </tbody>
						</table>
                    </li>
                </ul>
            </li>
            <li>
				<ul class="uk-tab" data-uk-tab="{connect:'#management'}">
                    <li class="uk-active"><a href="#">Управляющая организация</a></li>
                    <li><a href="#">Выполняемые работы (услуги)</a></li>
                    <li><a href="#">Коммунальные услуги</a></li>
                    <li><a href="#">Общее имущество</a></li>
                    <li><a href="#">Сведения о капитальном ремонте</a></li>
                    <li><a href="#">Общие собрания</a></li>
                </ul>
                <table class="uk-table tm-table-head-grey uk-margin-bottom-remove">
				    <tbody>
				        <tr>
				            <td class="uk-width-2-4"><div class="uk-text-bold uk-text-large">Текущая управляющая организация:</div> Данные за период управления:</td>
				            <td><div>
				                    @if (!empty($manager))
				                        <a href="/mymanager/avtozavodsky/{{$manager['id']}}">{{$manager['short_name']}}</a>
                                    @else
                                        организация не выбранна
                                    @endif
                                </div>
                                <div>??????</div>
                            </td>
				        </tr>
				    </tbody>
				</table>
                <ul id="management" class="uk-switcher">
	                <li>
						<table class="tm-table-strip">
                            <tbody>
                                {!! $forms['homes_manager_organization'] !!}
                                <tr>
	                                <td><span></span></td>
                                    <td colspan="2" class="uk-text-left dotted-none">
	                                    <span class="uk-text-muted">Копия договора управления</span>
	                                    <div class="uk-panel tm-panel-box-table uk-width-1-2">
		                                    <ol class="uk-list-space">
			                                    <li><a href="">?????</a></li>
			                                    <li><a href="">????</a></li>
		                                    </ol>
	                                    </div>
	                                </td>
                                </tr>
						    </tbody>
						</table>
	                </li>
	                <li>
						<table class="uk-table tm-table-red uk-margin-top-remove">
						    <thead>
						        <tr>
						            <th class="uk-width-2-3">Наименование работы (услуги)</th>
						            <th>Годовая плановая стоимость <br/>работ (услуг) (руб.)</th>
						            <th class="uk-width-1-10"></th>
						        </tr>
						    </thead>
						    <tbody>
						        {!! $forms['homes_manager_work_performed'] !!}
						    </tbody>
						</table>
	                </li>
	                <li>
						<table class="uk-table tm-table-red uk-margin-top-remove">
						    <thead>
						        <tr>
						            <th>Вид коммунальной услуги</th>
						            <th>Факт предоставления услуги</th>
						            <th>Тариф (цена) (руб.)</th>
									<th>Единица измерения</th>
									<th>Лицо, осуществляющее поставку<br/>коммунального ресурса</th>
						            <th class="uk-width-1-10"></th>
						        </tr>
						    </thead>
                            {!! $forms['homes_manager_public_service'] !!}
						</table>
						    
	                </li>
	                <li>
                        <table class="tm-table-strip">
                            <tbody>
                                {!! $forms['homes_manager_common_property'] !!}
                            </tbody>
                        </table>
                    </li>
	                <li>
                        <table class="tm-table-strip">
                            <tbody>
                                {!! $forms['homes_manager_repair'] !!}
                            </tbody>
                        </table>
                    </li>
	                <li>Не заполнено</li>
                </ul>
            </li>
            <li>
            	<ul class="uk-tab" data-uk-tab="{connect:'#management_reports'}">
                    <li class="uk-active"><a href="#">Общая информация</a></li>
                    <li><a href="#">Выполненные работы</a></li>
                    <li><a href="#">Претензии по качеству работ</a></li>
                    <li><a href="#">Коммунальные услуги</a></li>
                    <li><a href="#">Объемы по коммунальным услугам</a></li>
                    <li><a href="#">Претензионно-исковая работа</a></li>
                </ul>
                <table class="uk-table tm-table-head-grey uk-margin-bottom-remove">
				    <tbody>
				        <tr>
				            <td class="uk-width-2-4"><div class="uk-text-bold uk-text-large">Текущая управляющая организация:</div> Данные за период управления:</td>
				            <td><div>
                                    @if (!empty($manager))
                                        <a href="/mymanager/avtozavodsky/{{$manager['id']}}">{{$manager['short_name']}}</a>
                                    @else
                                        организация не выбранна
                                    @endif
                                </div>
                                <div>??????</div>
                            </td>
				        </tr>
				    </tbody>
				</table>
                <ul id="management_reports" class="uk-switcher">
	                <li>
	                	<table class="tm-table-strip">
                            <tbody>
	                           {!! $forms['homes_report_information'] !!}
						    </tbody>
						</table>
	                </li>
	                <li>
	                	<table class="uk-table tm-table-red uk-margin-top-remove">
						    <thead>
						        <tr>
						            <th class="uk-width-1-3">Наименование работы</th>
						            <th>Годовая фактическая<br/>стоимость работ/услуг (руб.)</th>
						            <th>Количество работ (ед.)</th>
						            <th class="uk-width-1-10"></th>
						        </tr>
						    </thead>
						    <tbody>
							    {!! $forms['homes_report_work_performed'] !!}
							</tbody>
						</table>
	                </li>
	                <li>
	                	<table class="tm-table-strip">
                            <tbody>
	                            {!! $forms['homes_report_claim'] !!}
						    </tbody>
						</table>
	                </li>
	                <li>
	                	<table class="tm-table-strip">
                            <tbody>
	                            {!! $forms['homes_report_utilities'] !!}
						    </tbody>
						</table>
	                </li>
	                <li>
	                	<table class="uk-table tm-table-red uk-margin-top-remove">
						    <thead>
						        <tr>
						            <th>Вид коммунальной услуги</th>
						            <th>Факт предоставления</th>
						            <th>Единица измерения</th>
						            <th>Начислено потребителям (руб.)</th>
						            <th class="uk-width-1-10"></th>
						        </tr>
						    </thead>
						    <tbody>
							    {!! $forms['homes_report_public_service'] !!}
						    </tbody>
	                	</table>
	                </li>
	                <li>
	                	<table class="tm-table-strip">
	                        <tbody>
	                            {!! $forms['homes_report_claim_work'] !!}
						    </tbody>
						</table>
	                </li>
                </ul>
            </li>
            <li>Раздел временно недоступен.</li>
        </ul>
    </div>
</div>
<div id="service_1m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_2m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_3m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_4m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_5m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_6m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_7m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_8m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="service_9m" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Год предоставления работы <br/>/ услуги</th>
		            <th class="uk-text-center">Годовая плановая стоимость <br/>работ (услуг) (руб)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="drainage_history" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата начала действия установленного размера стоимости услуги (работы)</th>
		            <th class="uk-text-center">Единица измерения</th>
		            <th class="uk-text-center">Тариф, установленный для потребителей, (руб.)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="drainage_act" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата нормативно-правового акта</th>
		            <th class="uk-text-center">Номер нормативно-правового акта</th>
		            <th class="uk-text-center">Наименование принявшего акт органа</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="cold_history" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата начала действия установленного размера стоимости услуги (работы)</th>
		            <th class="uk-text-center">Единица измерения</th>
		            <th class="uk-text-center">Тариф, установленный для потребителей, (руб.)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="cold_act" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата нормативно-правового акта</th>
		            <th class="uk-text-center">Номер нормативно-правового акта</th>
		            <th class="uk-text-center">Наименование принявшего акт органа</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="hot_history" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата начала действия установленного размера стоимости услуги (работы)</th>
		            <th class="uk-text-center">Единица измерения</th>
		            <th class="uk-text-center">Тариф, установленный для потребителей, (руб.)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="hot_act" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата нормативно-правового акта</th>
		            <th class="uk-text-center">Номер нормативно-правового акта</th>
		            <th class="uk-text-center">Наименование принявшего акт органа</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="heating_history" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата начала действия установленного размера стоимости услуги (работы)</th>
		            <th class="uk-text-center">Единица измерения</th>
		            <th class="uk-text-center">Тариф, установленный для потребителей, (руб.)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="heating_act" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата нормативно-правового акта</th>
		            <th class="uk-text-center">Номер нормативно-правового акта</th>
		            <th class="uk-text-center">Наименование принявшего акт органа</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="electroanalgesia_history" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата начала действия установленного размера стоимости услуги (работы)</th>
		            <th class="uk-text-center">Единица измерения</th>
		            <th class="uk-text-center">Тариф, установленный для потребителей, (руб.)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="electroanalgesia_act" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата нормативно-правового акта</th>
		            <th class="uk-text-center">Номер нормативно-правового акта</th>
		            <th class="uk-text-center">Наименование принявшего акт органа</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="gas_history" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">История стоимости услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата начала действия установленного размера стоимости услуги (работы)</th>
		            <th class="uk-text-center">Единица измерения</th>
		            <th class="uk-text-center">Тариф, установленный для потребителей, (руб.)</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
<div id="gas_act" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги</div>
        <table class="uk-table">
		    <thead>
		        <tr>
		            <th class="uk-text-center">Дата нормативно-правового акта</th>
		            <th class="uk-text-center">Номер нормативно-правового акта</th>
		            <th class="uk-text-center">Наименование принявшего акт органа</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		            <td class="uk-text-center">????</td>
		        </tr>
		    </tbody>
		</table>
    </div>
</div>
@endsection