
<tr>
    <td><span>1</span></td>
    <td><span>Год постройки</span></td>
    <td><span>{{$generalInformation['year_built'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>2</span></td>
    <td><span>Год ввода в эксплуатацию</span></td>
    <td><span>{{$generalInformation['year_commissioning'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>2</span></td>
    <td><span>Количество жителей</span></td>
    <td><span>{{$generalInformation['residents_number'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>3</span></td>
    <td><span>Серия, тип проекта</span></td>
    <td><span>{{$generalInformation['series'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>4</span></td>
    <td><span>Тип дома</span></td>
    <td><span>{{$generalInformation['type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>5</span></td>
    <td><span>Описание местоположения</span></td>
    <td><span>{{$generalInformation['description_position'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>6</span></td>
    <td><span>Индивидуальное наименование дома</span></td>
    <td><span>{{$generalInformation['name_house'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>7</span></td>
    <td><span>Способ формирования фонда капитального ремонта</span></td>
    <td><span>{{$generalInformation['method_overhaul'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>8</span></td>
    <td><span class="uk-text-bold">Количество этажей:</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>наибольшее, ед.</span></td>
    <td><span>{{$generalInformation['floors_number_max'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>наименьшее, ед.</span></td>
    <td><span>{{$generalInformation['floors_number_min'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>9</span></td>
    <td><span>Количество подъездов, ед</span></td>
    <td><span>{{$generalInformation['entrances_number'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>10</span></td>
    <td><span>Количество лифтов, ед</span></td>
    <td><span>{{$generalInformation['lifts_number'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>11</span></td>
    <td><span>Количество помещений, в том числе:</span></td>
    <td><span>{{$generalInformation['rooms_number'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>жилых, ед.</span></td>
    <td><span>{{$generalInformation['rooms_number_living'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>нежилых, ед.</span></td>
    <td><span>{{$generalInformation['rooms_number_no_living'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>12</span></td>
    <td><span>Общая площадь дома, в том числе, кв.м:</span></td>
    <td><span>{{$generalInformation['total_area'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>общая площадь жилых помещений, кв.м</span></td>
    <td><span>{{$generalInformation['total_area_living'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>общая площадь нежилых помещений, кв.м</span></td>
    <td><span>{{$generalInformation['total_area_no_living'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>общая площадь помещений, входящих в состав общего имущества, кв.м</span></td>
    <td><span>{{$generalInformation['total_area_common_property'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>13</span></td>
    <td><span>Общие сведения о земельном участке, на котором расположен многоквартирный дом:</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>площадь земельного участка, входящего в состав общего имущества в многоквартирном доме, кв.м</span></td>
    <td><span>{{$generalInformation['parcel_area'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>площадь парковки в границах земельного участка, кв.м</span></td>
    <td><span>{{$generalInformation['parcel_parking_area'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>14</span></td>
    <td><span>кадастровый номер</span></td>
    <td><span>{{$generalInformation['cadastral_number'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>15</span></td>
    <td><span>Класс энергетической эффективности</span></td>
    <td><span>{{$generalInformation['energy_efficiency_class'] or "нет данных" }}</span></td>
</tr>
<tr>
    <td><span>16</span></td>
    <td><span>Общие сведения о земельном участке, на котором расположен многоквартирный дом:</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>детская площадка</span></td>
    <td><span>{{$generalInformation['beautification_playground_children'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>спортивная площадка</span></td>
    <td><span>{{$generalInformation['beautification_playground_sports'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>другое</span></td>
    <td><span>{{$generalInformation['beautification_other'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>17</span></td>
    <td><span>Дополнительная информация</span></td>
    <td><span>{{$generalInformation['additional_information'] or "нет данных"}}</span></td>
</tr>