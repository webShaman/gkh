<tr>
    <td><span>1</span></td>
    <td><span class="uk-text-bold">Фундамент</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Тип фундамента</span></td>
    <td><span>{{$structuralElements['foundation_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>2</span></td>
    <td><span class="uk-text-bold">Стены и перекрытия</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Тип перекрытий</span></td>
    <td><span>{{$structuralElements['overlap_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Материал несущих стен</span></td>
    <td><span>{{$structuralElements['material_bearing_walls'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>3</span></td>
    <td><span class="uk-text-bold">Подвал</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Площадь подвала по полу, кв.м</span></td>
    <td><span>{{$structuralElements['basement_area'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>4</span></td>
    <td><span class="uk-text-bold">Мусоропроводы</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Тип мусоропровода</span></td>
    <td><span>{{$structuralElements['chutes_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Количество мусоропроводов, ед.</span></td>
    <td><span>{{$structuralElements['chutes_number'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>5</span></td>
    <td><span class="uk-text-bold">Фасады</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Тип фасада</span></td>
    <td><span>{{$structuralElements['facade_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>6</span></td>
    <td><span class="uk-text-bold">Крыши</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Тип крыши</span></td>
    <td><span>{{$structuralElements['roof_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Тип кровли.</span></td>
    <td><span>{{$structuralElements['loft_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>7</span></td>
    <td><span class="uk-text-bold">Иное оборудование / конструктивный элемент</span></td>
    <td><span></span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Вид оборудования / конструктивного элемента</span></td>
    <td><span>{{$structuralElements['equipment_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span> - </span></td>
    <td><span>Описание дополнительного оборудования / конструктивного элемента</span></td>
    <td><span>{{$structuralElements['equipment_description'] or "нет данных"}}</span></td>
</tr>