<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система электроснабжения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы электроснабжения</span></td>
    <td><span>{{$engineeringSystems['electrical_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td><span>2</span></td>
    <td><span>Количество вводов в дом, ед.</span></td>
    <td><span>{{$engineeringSystems['electronic_number_entries'] or 'Нет данных' }}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система теплоснабжения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы теплоснабжения</span></td>
    <td><span>{{$engineeringSystems['heating_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система горячего водоснабжения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы горячего водоснабжения</span></td>
    <td><span>{{$engineeringSystems['hot_water_type'] or 'Нет данных' }}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система холодного водоснабжения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы холодного водоснабжения</span></td>
    <td><span>{{$engineeringSystems['cold_water_type'] or 'Нет данных' }}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система водоотведения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы водоотведения</span></td>
    <td><span>{{$engineeringSystems['drainage_type'] or 'Нет данных' }}</span></td>
</tr>
<tr>
    <td><span>2</span></td>
    <td><span>Объем выгребных ям, куб. м.</span></td>
    <td><span>{{$engineeringSystems['cesspools_volume'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система газоснабжения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы газоснабжения</span></td>
    <td><span>{{$engineeringSystems['gas_supply_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система вентиляции</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы вентиляции</span></td>
    <td><span>{{$engineeringSystems['ventilation_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система пожаротушения</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы пожаротушения</span></td>
    <td><span>{{$engineeringSystems['fire_extinguishing_type'] or "нет данных"}}</span></td>
</tr>
<tr>
    <td class="dotted-none uk-text-left" colspan="3"><span class="uk-text-bold uk-h3">Система водостоков</span></td>
</tr>
<tr>
    <td><span>1</span></td>
    <td><span>Тип системы водостоков</span></td>
    <td><span>{{$engineeringSystems['drains_type'] or "нет данных"}}</span></td>
</tr>



