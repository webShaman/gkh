@extends('layout')

@section('content')

<div class="uk-container uk-container-center">
	<h1>Дома образцового содержания</h1>
	<p class="uk-text-justify uk-article-lead uk-text-warning">Нижегородский рейтинг организаций, осуществляющих управление многоквартирными домами, находится в стадии отбора участников и предварительного голосования.</p>
	<p class="uk-text-justify">В этом разделе содержится информация о награжденных знаком отличия «Нижегородская слава ЖКХ» руководителях товариществ собственников жилья, советов многоквартирных домов, жилищно-строительных кооперативов и управляющих компаний за выдающиеся успехи в управлении и содержании многоквартирного дома, внедрении современных энергосберегающих и энергоэффективных технологий, создании комфортных и безопасных условий проживания граждан, развитии института ответственных собственников жилья.</p>
<p>Принимаем заявки на участие.</p>
</div>

@stop