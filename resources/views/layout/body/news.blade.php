@extends('layout')

@section('content')

    <div class="uk-container uk-container-center">
        <div class="uk-grid">
	        <div class="uk-width-3-4">
		        
		        <div class="uk-article">
                    <h3 class="uk-article-title">Шумных соседей могут оштрафовать на 2000 рублей</h1>
                    <p class="uk-article-meta">26 июня 2015</p>
                    <p class="uk-article-lead">Депутаты приняли изменения в КоАП 25 июня.</p>
					<a href="#">Подробнее</a>
                </div>
                
                <div class="uk-article">
                    <h3 class="uk-article-title">«Нижновэнерго» выявило крупное хищение электроэнергии</h1>
                    <p class="uk-article-meta">26 июня 2015</p>
                    <p class="uk-article-lead">Возбуждено уголовное дело по факту хищения электроэнергии на 6 млн рублей.</p>
					<a href="#">Подробнее</a>
                </div>
                
                <div class="uk-article">
                    <h3 class="uk-article-title">Тарифы на электроэнергию вырастут с 1 июля</h1>
                    <p class="uk-article-meta">26 июня 2015</p>
                    <p class="uk-article-lead">РСТ опубликовала тарифы на электроэнергию для населения и дачников.</p>
					<a href="#">Подробнее</a>
                </div>
                
                <div class="uk-article">
                    <h3 class="uk-article-title">Стоимость проезда не повысят в 2015 году</h1>
                    <p class="uk-article-meta">25 июня 2015</p>
                    <p class="uk-article-lead">Олег Кондрашов сообщил о состоянии общественного транспорта в Нижнем Новгороде.</p>
					<a href="#">Подробнее</a>
                </div>
                
                <div class="uk-article">
                    <h3 class="uk-article-title">Свалка обнаружена в зоне пойменных озер на Артемовских лугах</h1>
                    <p class="uk-article-meta">25 июня 2015</p>
                    <p class="uk-article-lead">Несанкционированная свалка мусора и строительных отходов обнаружена в водоохранной зоне.</p>
					<a href="#">Подробнее</a>
                </div>
                
                <div class="uk-article">
                    <h3 class="uk-article-title">Размер взноса на капремонт не изменится ближайшие три года</h1>
                    <p class="uk-article-meta">25 июня 2015</p>
                    <p class="uk-article-lead">Депутаты обсудили итоги мониторинга реализации регионального закона о капремонте.</p>
					<a href="#">Подробнее</a>
                </div>
                
	        </div>
	        <div class="uk-width-1-4"></div>
        </div>
        
    </div>

@stop