</div>
<div class="tm-partners">
	<div class="uk-container uk-container-center">
		<hr class="uk-grid-divider">
		<div class="uk-grid">
			<div class="uk-width-1-10">
				<img src="/images/nino.png" class="gerb-nino" width="" alt="">
			</div>
			<div class="uk-width-9-10">
				<ul class="uk-grid uk-grid-width-1-5 tm-partners-list" data-uk-grid-margin>
				    <li><a target="_blank" href="http://www.government-nnov.ru/">Правительство <br/>Нижегородской области</a></li>
				    <li><a target="_blank" href="http://mingkh.government-nnov.ru/">Министерство ЖКХ и ТЭК Нижегородской области</a></li>
				    <li><a target="_blank" href="http://www.admgor.nnov.ru/">Администрация города <br/>Нижнего Новгорода</a></li>
				    <li><a target="_blank" href="http://www.admgor.nnov.ru/vlast/administratsiya-goroda/deps/dep-gil/">Департамент жилья и инженерной инфраструктуры</a></li>
				    <li><a target="_blank" href="http://www.admgor.nnov.ru/vlast/adm-rayon/">Администрации районов <br/>города Нижнего Новгорода</a></li>
				    <li><a target="_blank" href="http://www.rstno.ru/">Региональная служба по тарифам Нижегородской области</a></li>
				    <li><a target="_blank" href="http://www.gzhinn.ru/">Государственная жилищная инспекция Нижегородской области</a></li>
				    <li><a target="_blank" href="http://www.iatn.nnov.ru/">Инспекция административно-технического надзора</a></li>
				    <li><a target="_blank" href="http://www.52.rospotrebnadzor.ru/">Управление Роспотребнадзора по Нижегородской области</a></li>
				    <li><a target="_blank" href="http://www.fkrnnov.ru/">Фонд капитального ремонта МКД Нижегородской области</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<footer class="tm-footer">
    <div class="uk-container uk-container-center">
        <div class="uk-clearfix">
		    <div class="uk-float-left">
			    © 2015<br/>
				«Нижегородский портал ЖКХ»<br/> 
				Все права защищены.
		    </div>
		    <div class="uk-float-left tm-margin-left">
			    <u>Почтовый адрес:</u><br/>
				603006, г. Н.Новгород, ул. Семашко, 37<br/>
				Бизнес Центр «Семашко 37», этаж 3
		    </div>
		    <div class="uk-float-left tm-margin-left">
			    <u>Справочная служба:</u><br/>
				тел.: 8 800 775 76 77 <br/>
				e-mail: info@zhkh-nn.ru
		    </div>
		    <div class="uk-float-left tm-margin-left">
			    <u>Правовая поддержка:</u><br/>
				тел./факс: +7 (831) 2-333-911<br/>
				e-mail: portal@usod-group.ru
		    </div>
		    <div class="uk-float-right"></div>
		</div>
		<hr class="uk-grid-divider">
		<div class="uk-clearfix">
		    <div class="uk-float-left">
			    <ul class="uk-subnav uk-subnav-line">
				    <li><a href="/about">О проекте</a></li>
	                <li><a href="/siterules">Правила пользования сайтом</a></li>
	                <li><a href="/policy">Политика конфиденциальности</a></li>
	                <li><a href="#">Карта сайта</a></li>
	                <li class="uk-text-muted">
	                	<!-- Yandex.Metrika informer -->
						<a href="https://metrika.yandex.ru/stat/?id=31335943&amp;from=informer"
						target="_blank" rel="nofollow"><img src="https://mc.yandex.ru/informer/31335943/3_1_FFFFFFFF_FFFFFFFF_0_pageviews"
						style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:31335943,lang:'ru'});return false}catch(e){}" /></a>
						<!-- /Yandex.Metrika informer -->
	                </li>
	            </ul>
		    </div>
		    <div class="uk-float-right tm-webshaman"><a href="#">создание сайта / webshaman</a></div>
		</div>
    </div>
</footer>
<div id="auth-modal" class="uk-modal">
    <div class="uk-modal-dialog">
	    <a class="uk-modal-close uk-close"></a>
	    <div class="uk-modal-header">
		    <h2>Вход в личный кабинет</h2>
	    </div>
	    <form class="uk-form" role="form" method="POST" action="/auth/login">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <div class="uk-form-row">
                <label class="uk-form-label " for="form-login">Ваш E-Mail</label>
                <input type="email" id="form-login" placeholder="Введите ваш E-Mail адрес" class="uk-width-1-1" name="email" value="{{ old('email') }}">
            </div>
	        <div class="uk-form-row">
                <label class="uk-form-label" for="form-password">Пароль</label>
                <div>
                    <input type="password" id="form-password" placeholder="Введите ваш пароль" class="uk-width-1-2" name="password">
                    <span class="uk-form-help-block uk-margin-left"><a href="/password/email">Забыли пароль?</a></span>
                </div>
            </div>
            <div class="uk-form-row">
	            <input type="checkbox" name="remember" >
                <label class="uk-form-label">Сохранить пароль</label>
            </div>
	        <div class="uk-form-row">
	        	<button class="uk-button uk-button-success">Войти</button>
	        </div>
		</form>
        <div class="uk-modal-footer uk-text-center"><a href="/auth/register">Регистрация для новых пользователей</a></div>
    </div>
</div>
<script src="/js/jquery-2.1.3.js"></script>
<script src="/js/uikit.min.js"></script>
<script src="/js/components/slideshow.min.js"></script>
<script src="/js/components/datepicker.min.js"></script>
<script src="/js/components/form-select.min.js"></script>
<script src="/js/components/slideshow.min.js"></script>
<script src="/js/table.js"></script>
<script src="/js/add_row.js"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31335943 = new Ya.Metrika({
                    id:31335943,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31335943" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
