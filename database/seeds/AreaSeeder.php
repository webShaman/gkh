<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 31.05.15
 * Time: 14:59
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Area;
use App\Models\Home;

class AreaSeeder extends Seeder
{
    public function run()
    {
        DB::table('areas')->delete();
        DB::table('areas')->create(array(
            array('id' => '1','name' => Area::AVT, 'name_ru' => 'Автозаводский', 'name_ru_rod' => 'Автозаводского', 'name_ru_dat' => 'Автозаводскому'),
            array('id' => '2','name' => Area::KAN, 'name_ru' => 'Канавинский', 'name_ru_rod' => 'Канавинского', 'name_ru_dat' => 'Канавинскому'),
            array('id' => '3','name' => Area::LEN, 'name_ru' => 'Ленинский', 'name_ru_rod' => 'Ленинского', 'name_ru_dat' => 'Ленинскому'),
            array('id' => '4','name' => Area::MOS, 'name_ru' => 'Московский', 'name_ru_rod' => 'Московского', 'name_ru_dat' => 'Московскому'),
            array('id' => '5','name' => Area::NIZ, 'name_ru' => 'Нижегородский', 'name_ru_rod' => 'Нижегородского', 'name_ru_dat' => 'Нижегородскому'),
            array('id' => '6','name' => Area::PRI, 'name_ru' => 'Приокский', 'name_ru_rod' => 'Приокского', 'name_ru_dat' => 'Приокскому'),
            array('id' => '7','name' => Area::SOV, 'name_ru' => 'Советский', 'name_ru_rod' => 'Советского', 'name_ru_dat' => 'Советскому'),
            array('id' => '8','name' => Area::SOR, 'name_ru' => 'Сормовский', 'name_ru_rod' => 'Сормовского', 'name_ru_dat' => 'Сормовскому'),
        ));
    }
}
