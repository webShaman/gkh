<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesUtilityBills extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_utility_bills', function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('income')->nullable();                     //    Доход от поставки КУ за отчетный период, тыс. руб.
            $table->char('income_heating')->nullable();             //    отопление
            $table->char('income_electricity')->nullable();         //    электричество
            $table->char('income_gas')->nullable();                 //    газ
            $table->char('income_hot_water')->nullable();           //    горячее водоснабжение
            $table->char('income_cold_water')->nullable();          //    холодное водоснабжение
            $table->char('income_sanitation')->nullable();          //    водоотведение
            $table->char('debt')->nullable();                       //    Задолженность собственников за КУ за отчетный период, тыс. руб.
            $table->char('debt_heating')->nullable();               //    отопление
            $table->char('debt_electricity')->nullable();           //    электричество
            $table->char('debt_gas')->nullable();                   //    газ
            $table->char('debt_hot_water')->nullable();             //    горячее водоснабжение
            $table->char('debt_cold_water')->nullable();            //    холодное водоснабжение
            $table->char('debt_sanitation')->nullable();            //    водоотведение
            $table->char('charged')->nullable();                    //    Взыскано с собственников за КУ за отчетный период, тыс. руб.
            $table->char('charged_heating')->nullable();            //    отопление
            $table->char('charged_electricity')->nullable();        //    электричество
            $table->char('charged_gas')->nullable();                //    газ
            $table->char('charged_hot_water')->nullable();          //    горячее водоснабжение
            $table->char('charged_cold_water')->nullable();         //    холодное водоснабжение
            $table->char('charged_sanitation')->nullable();         //    водоотведение
            $table->char('paid')->nullable();                       //    Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс. руб.
            $table->char('paid_heating')->nullable();               //    отопление
            $table->char('paid_electricity')->nullable();           //    электричество
            $table->char('paid_gas')->nullable();                   //    газ
            $table->char('paid_hot_water')->nullable();             //    горячее водоснабжение
            $table->char('paid_cold_water')->nullable();            //    холодное водоснабжение
            $table->char('paid_resources')->nullable();             //    Оплачено ресурсов по счетам на общедомовые нужды за отчетный период, тыс. руб.
            $table->char('paid_resources_heating')->nullable();     //    отопление
            $table->char('paid_resources_electricity')->nullable(); //    электричество
            $table->char('paid_resources_gas')->nullable();         //    газ
            $table->char('paid_resources_hot_water')->nullable();   //    горячее водоснабжение
            $table->char('paid_resources_cold_water')->nullable();  //    холодное водоснабжение

            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_utility_bills');
	}

}
