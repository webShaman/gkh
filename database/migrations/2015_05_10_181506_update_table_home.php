<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableHome extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('homes', function(Blueprint $table)
        {
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });

        Schema::table('homes_passport_general', function(Blueprint $table)
        {
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });

        Schema::table('homes_structural_elements', function(Blueprint $table)
        {
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });

        Schema::table('homes_engineering_systems', function(Blueprint $table)
        {
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });

        Schema::table('homes_elevator', function(Blueprint $table)
        {
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
