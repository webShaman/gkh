<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Report\PublicService;

class CreateTableHomesReportPublicService extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(PublicService::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // колонки
            $table->char('view_public')->nullable();            //    Вид коммунальной услуги
            $table->char('fact_providing')->nullable();         //    Факт предоставления услуги
            $table->char('unit_measure')->nullable();           //    Единица измерения
            $table->char('charges_customers')->nullable();      //    Начислено потребителям (руб.)

            $table->char('total_consumption')->nullable();      //    1 Общий объем потребления, нат. показ.
            $table->char('paid_consumers')->nullable();         //    2	Оплачено потребителями, руб.
            $table->char('debt_consumers')->nullable();         //    3	Задолженность потребителей, руб.
            $table->char('accrued_supplier')->nullable();       //    4	Начислено поставщиком (поставщиками) коммунального ресурса, руб.
            $table->char('paid_supplier')->nullable();          //    5	Оплачено поставщику (поставщикам) коммунального ресурса, руб.
            $table->char('payable_suppliers')->nullable();      //    6	Задолженность перед поставщиком (поставщиками) коммунального ресурса, руб.
            $table->char('fines')->nullable();                  //    7	Суммы пени и штрафов, уплаченные поставщику(поставщикам) коммунального ресурса, руб.

            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
