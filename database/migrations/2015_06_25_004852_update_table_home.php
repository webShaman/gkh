<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home;
use App\Models\Home\Passport\GeneralInformation;

class UpdateTableHome extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
        Schema::table(Home::TABLE_NAME, function (Blueprint $table) {
            $table->text('map')->nullable();
        });

        Schema::table(GeneralInformation::TABLE_NAME, function (Blueprint $table) {
            $table->string('year_commissioning')->change();
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
