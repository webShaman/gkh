<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Passport\EngineeringSystems;

class CreateTableHomesPassportEgineeringSystems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(EngineeringSystems::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('electrical_type')->nullable();            //    Тип системы электроснабжения
            $table->char('electronic_number_entries')->nullable();  //    Количество вводов в дом, ед.
            $table->char('heating_type')->nullable();               //    Тип системы теплоснабжения
            $table->char('foundation_type')->nullable();            //    Тип системы горячего водоснабжения
            $table->char('hot_water_type')->nullable();             //    Тип системы холодного водоснабжения
            $table->char('drainage_type')->nullable();              //    Тип системы водоотведения
            $table->char('cesspools_volume')->nullable();           //    Объем выгребных ям, куб. м.
            $table->char('gas_supply_type')->nullable();            //    Тип системы газоснабжения
            $table->char('ventilation_type')->nullable();           //    Тип системы вентиляции
            $table->char('fire_extinguishing_type')->nullable();    //    Тип системы пожаротушения
            $table->char('drains_type')->nullable();                //    Тип системы водостоков
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
