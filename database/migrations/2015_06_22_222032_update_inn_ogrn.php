<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInnOgrn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('manager_common_info', function(Blueprint $table)
        {
            $table->dropColumn('inn');
            $table->dropColumn('ogrn');
        });

        Schema::table('manager_common_info', function(Blueprint $table)
        {
            $table->double('inn')->nullable();
            $table->double('ogrn')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
