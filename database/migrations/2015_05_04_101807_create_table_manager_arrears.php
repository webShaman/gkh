<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManagerArrears extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('manager_arrears', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с районом
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('manager_common_info');
            // поля
            $table->double('past_account_owner_service_current_date')->nullable();                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению, накопленная за весь период обслуживания на отчетную дату, тыс.руб.
            $table->double('past_account_owner_service_current_date_less_25')->nullable();            // - по домам до 25 лет
            $table->double('past_account_owner_service_current_date_26_50')->nullable();              // - по домам от 26 до 50 лет
            $table->double('past_account_owner_service_current_date_51_75')->nullable();              // - по домам от 51 до 75 лет
            $table->double('past_account_owner_service_current_date_more_76')->nullable();            // - по домам 76 лет и более
            $table->double('past_account_owner_service_current_date_alarm')->nullable();              // - по аварийным домам
            $table->double('past_account_owner_service_start_period')->nullable();                    // Просроченная задолженность собственников помещений и иных лиц, .... , за оказанные услуги по управлению на начало отчетного периода, тыс.руб.
            $table->double('past_account_owner_utilities_current_date')->nullable();                  // Просроченная задолженность собственников помещений и иных лиц, .... , за коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
            $table->double('past_account_owner_utilities_current_date_heating')->nullable();          // - отопление
            $table->double('past_account_owner_utilities_current_date_electricity')->nullable();      // - электричество
            $table->double('past_account_owner_utilities_current_date_gas')->nullable();              // - газ
            $table->double('past_account_owner_utilities_current_date_hot_water_supply')->nullable(); // - горячее водоснабжение
            $table->double('past_account_owner_utilities_current_date_cold_water')->nullable();       // - холодное водоснабжение
            $table->double('past_account_owner_utilities_current_date_sanitation')->nullable();       // - водоотведение
            $table->double('past_account_owner_utilities_start_period')->nullable();                  // Просроченная задолженность собственников помещений и иных лиц, .... за коммунальные услуги на начало отчетного периода, тыс.руб.
            $table->double('past_account_organ_utilities_current_date')->nullable();                  // Просроченная задолженность организации за предоставленные коммунальные услуги, накопленная за весь период обслуживания на текущую дату, тыс.руб.
            $table->double('past_account_organ_utilities_current_date_heating')->nullable();          // - отопление
            $table->double('past_account_organ_utilities_current_date_electricity')->nullable();      // - электричество
            $table->double('past_account_organ_utilities_current_date_gas')->nullable();              // - газ
            $table->double('past_account_organ_utilities_current_date_hot_water_supply')->nullable(); // - горячее водоснабжение
            $table->double('past_account_organ_utilities_current_date_cold_water')->nullable();       // - холодное водоснабжение
            $table->double('past_account_organ_utilities_current_date_sanitation')->nullable();       // - водоотведение
            $table->double('amount_collected_debt_service')->nullable();                              //  Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
                                                                                          //  проживающих в помещениях на законных основаниях за услуги по управлению, тыс.руб.
            $table->double('amount_collected_debt_service_less_25')->nullable();                      // - по домам до 25 лет
            $table->double('amount_collected_debt_service_26_50')->nullable();                        // - по домам от 26 до 50 лет
            $table->double('amount_collected_debt_service_51_75')->nullable();                        // - по домам от 51 до 75 лет
            $table->double('amount_collected_debt_service_more_76')->nullable();                      // - по домам 76 лет и более
            $table->double('amount_collected_debt_service_alarm')->nullable();                        // - по аварийным домам
            $table->double('amount_collected_debt_utilities')->nullable();                            // Сумма взысканной за отчетный период просроченной задолженности собственников помещений и иных лиц, пользующихся или
                                                                                          // проживающих в помещениях на законных основаниях за предоставленные коммунальные услуги, тыс.руб.
            $table->double('amount_collected_debt_utilities_heating')->nullable();                    // - отопление
            $table->double('amount_collected_debt_utilities_electricity')->nullable();                // - электричество
            $table->double('amount_collected_debt_utilities_gas')->nullable();                        // - газ
            $table->double('amount_collected_debt_utilities_hot_water_supply')->nullable();           // - горячее водоснабжение
            $table->double('amount_collected_debt_utilities_cold_water')->nullable();                 // - холодное водоснабжение
            $table->double('amount_collected_debt_utilities_sanitation')->nullable();                 // - водоотведение
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('manager_arrears');
	}

}
