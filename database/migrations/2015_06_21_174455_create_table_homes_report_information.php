<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Report\Information;

class CreateTableHomesReportInformation extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(Information::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // колонки
            $table->char('charges_begin')->nullable();                    //    1 Ававнсовые платежи потребителей (на начало периода), руб.
            $table->char('carryover_begin')->nullable();                  //    2	Переходящие остатки денежных средств (на начало периода), руб.
            $table->char('debt_begin')->nullable();                       //    3	Задолженность потребителей (на начало периода), руб.
            //    4	Начислено за услуги (работы) по содержанию и текущему ремонту, в том числе:
            $table->char('charged_contents')->nullable();                 //    — за содержание дома, руб.
            $table->char('charged_maintenance')->nullable();              //    — за текущий ремонт, руб.
            $table->char('charged_management')->nullable();               //    — за услуги управления, руб.
            //    5	Получено денежных средств, в том числе:
            $table->char('obtained_cash_owners')->nullable();             //    — денежных средств от собственников/нанимателей помещений, руб.
            $table->char('obtained_contributions_owners')->nullable();    //    — целевых взносов от собственников/нанимателей помещений, руб.
            $table->char('obtained_subsidies_owners')->nullable();        //    — субсидий, руб.
            $table->char('obtained_common_property_owners')->nullable();  //    — денежных средств от использования общего имущества, руб.
            $table->char('obtained_other_income')->nullable();            //    — прочие поступления, руб.
            $table->char('total_cash')->nullable();                       //    6	Всего денежных средств с учетом остатков, руб.
            $table->char('charges_end')->nullable();                      //    7	Авансовые платежи потребителей (на конец периода), руб.
            $table->char('carryover_end')->nullable();                    //    8	Переходящие остатки денежных средств (на конец периода), руб.
            $table->char('debt_end')->nullable();                         //    9	Задолженность потребителей (на конец периода), руб.

            $table->char('info')->nullable(); //  информация
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
