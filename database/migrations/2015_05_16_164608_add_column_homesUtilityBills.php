<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHomesUtilityBills extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('homes_utility_bills', function(Blueprint $table) {
            $table->char('paid_resources_sanitation')->nullable();  //    водоотведение
            $table->char('paid_sanitation')->nullable();            //    водоотведение
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
