<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Passport\Elevators;

class CreateTableHomesPassportElevators extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(Elevators::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('elevator_number')->nullable();        //    Номер лифта
            $table->char('entrance_number')->nullable();        //    Номер подъезда
            $table->char('lift_type')->nullable();              //    Тип лифта
            $table->date('year_commissioning')->nullable();     //    Год ввода в эксплуатацию
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
