<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesCardRCO extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_card_rco', function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->text('rco_heating')->nullable();        //    Поставщик отопления
            $table->date('rco_electricity')->nullable();    //    Поставщик электричества
            $table->date('rco_gas')->nullable();            //    Поставщик газа
            $table->text('rco_hot_water')->nullable();      //    Поставщик горячей воды
            $table->text('rco_cold_water')->nullable();     //    Поставщик холодной воды
            $table->text('rco_sanitation')->nullable();     //    Поставщик водоотведения
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_card_rco');
	}

}
