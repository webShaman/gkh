<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Management\PublicService;

class CreateTableHomesManagerPublicService extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PublicService::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('view_public')->nullable();            //    Вид коммунальной услуги
            $table->char('fact_providing')->nullable();         //    Факт предоставления услуги
            $table->char('tariff')->nullable();                 //    Тариф (цена) (руб.)
            $table->char('unit_measure')->nullable();           //    Единица измерения
            $table->char('person')->nullable();                 //    Лицо, осуществляющее поставку коммунального ресурса
            // дополнительные поля
            $table->char('inn')->nullable();                        //    1	ИНН лица, осуществляющего поставку коммунального ресурса
            $table->char('additional_information')->nullable();     //    2	Дополнительная информация о лице, осуществляющего поставку коммунального ресурса
            $table->char('base_services')->nullable();              //    3	Основание предоставления услуги
            $table->char('details_contract')->nullable();           //    4	Реквизиты договора на поставку коммунального ресурса
            $table->char('act_tariff')->nullable();                 //    5	Нормативно-правовой акт, устанавливающий тариф
            $table->date('date_start_tariff')->nullable();          //    6	Дата начала действия тарифа
            $table->char('differentiation')->nullable();            //    7	Описание дифференциации тарифов в случаях, предусмотренных законодательством Российской Федерации о государственном регулировании цен (тарифов)
            $table->char('cost_history')->nullable();               //    8	История стоимости услуги
            //    9	Норматив потребления коммунальной услуги в жилых помещениях:
            $table->char('dwellings_utilities')->nullable();        //    — Норматив потребления коммунальной услуги в жилых помещениях
            $table->char('dwellings_unit')->nullable();             //    — Единица измерения норматива потребления услуги
            $table->char('dwellings_advanced')->nullable();         //    — Дополнительно
            //    10	Норматив потребления коммунальной услуги на общедомовые нужды:
            $table->char('obschedomovye_utilities')->nullable();    //    — Норматив потребления коммунальной услуги на общедомовые нужды
            $table->char('obschedomovye_unit')->nullable();         //    — Единица измерения норматива потребления услуги
            $table->char('obschedomovye_advanced')->nullable();     //    — Дополнительно
            $table->char('regulatory_act')->nullable();             //    11	Нормативно-правовой акт, устанавливающий норматив потребления коммунальной услуги
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
