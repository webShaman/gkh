<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManagerServiceMkd extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('manager_service_mkd', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с районом
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('manager_common_info');
            // поля
            $table->double('scope_repair')->nullable();                            // Объем работ по ремонту за отчетный период, тыс.руб.
            $table->double('scope_repair_less_25')->nullable();                    // - по домам до 25 лет
            $table->double('scope_repair_26_50')->nullable();                      // - по домам от 26 до 50 лет
            $table->double('scope_repair_51_75')->nullable();                      // - по домам от 51 до 75 лет
            $table->double('scope_repair_more_76')->nullable();                    // - по домам 76 лет и более
            $table->double('scope_repair_date_alarm')->nullable();                 // - по аварийным домам
            $table->double('scope_improvement')->nullable();                       // Объем работ по ремонту за отчетный период, тыс.руб.
            $table->double('scope_improvement_less_25')->nullable();               // - по домам до 25 лет
            $table->double('scope_improvement_26_50')->nullable();                 // - по домам от 26 до 50 лет
            $table->double('scope_improvement_51_75')->nullable();                 // - по домам от 51 до 75 лет
            $table->double('scope_improvement_more_76')->nullable();               // - по домам 76 лет и более
            $table->double('scope_improvement_date_alarm')->nullable();            // - по аварийным домам
            $table->double('volume_attracted_funds')->nullable();                  // Объем привлеченных средств за отчетный период, тыс.руб.
            $table->double('volume_attracted_funds_subsidies')->nullable();        // - Субсидии
            $table->double('volume_attracted_funds_credits')->nullable();          // - Кредиты
            $table->double('volume_attracted_funds_financing_leasing')->nullable();// - Финансирование по договорам лизинга
            $table->double('volume_attracted_funds_financing_energy')->nullable(); // - Финансирование по энергосервисным договорам
            $table->double('volume_attracted_funds_earmarked')->nullable();        // - Целевые взносы жителей
            $table->double('volume_attracted_funds_other')->nullable();            // - Другие источники
            $table->double('paid_ru_indications')->nullable();                     // Оплачено КУ по показаниям общедомовых ПУ за отчетный период, тыс.руб.
            $table->double('paid_ru_indications_heating')->nullable();             // - отопление
            $table->double('paid_ru_indications_electricity')->nullable();         // - электричество
            $table->double('paid_ru_indications_gas')->nullable();                 // - газ
            $table->double('paid_ru_indications_hot_water_supply')->nullable();    // - горячее водоснабжение
            $table->double('paid_ru_indications_cold_water')->nullable();          // - холодное водоснабжение
            $table->double('paid_ru_accounts')->nullable();                        //  Оплачено КУ по счетам на общедомовые нужды за отчетный период, тыс.руб.
            $table->double('paid_ru_accounts_heating')->nullable();                // - отопление
            $table->double('paid_ru_accounts_electricity')->nullable();            // - электричество
            $table->double('paid_ru_accounts_gas')->nullable();                    // - газ
            $table->double('paid_ru_accounts_hot_water_supply')->nullable();       // - горячее водоснабжение
            $table->double('paid_ru_accounts_cold_water')->nullable();             // - холодное водоснабжение
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('manager_service_mkd');
	}

}
