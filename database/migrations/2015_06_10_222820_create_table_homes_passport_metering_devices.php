<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Passport\MeteringDevices;

class CreateTableHomesPassportMeteringDevices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(MeteringDevices::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('resources_public_type')->nullable();      //    Вид коммунального ресурса
            $table->char('meters_installed')->nullable();           //    Наличие прибора учета
            $table->char('unit_measure')->nullable();               //    Единица измерения
            $table->char('date_start_up')->nullable();              //    Дата ввода в эксплуатацию
            $table->char('metering_device_type')->nullable();       //    Тип прибора учета
            $table->date('date_checking_replacing')->nullable();    //    Дата поверки / замены прибора учета
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
