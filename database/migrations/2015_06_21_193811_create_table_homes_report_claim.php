<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Report\Claim;

/**
 * Class CreateTableHomesReportClaim
 */
class CreateTableHomesReportClaim extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(Claim::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // колонки
            $table->char('number_incoming')->nullable();          //    1	Количество поступивших претензий, ед.
            $table->char('number_satisfied')->nullable();         //    2	Количество удовлетворенных претензий, ед.
            $table->char('number_denied')->nullable();            //    3	Количество претензий, в удовлетворении которых отказано, ед.
            $table->char('recalculation')->nullable();            //    4	Сумма произведенного перерасчета, руб.

            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
