<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Report\Utilities;

class CreateTableHomesReportUtilities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(Utilities::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // колонки
            $table->char('advance_payments_start')->nullable();         //    1 Авансовые платежи потребителей (на начало периода), руб.
            $table->char('carryover_payments_start')->nullable();       //    2	Переходящие остатки денежных средств (на начало периода), руб.
            $table->char('debt_start')->nullable();                     //    3	Задолженность потребителей (на начало периода), руб.
            $table->char('advance_payments_end')->nullable();           //    4	Авансовые платежи потребителей (на конец периода), руб.
            $table->char('carryover_payments_end')->nullable();         //    5	Переходящие остатки денежных средств (на конец периода), руб.
            $table->char('debt_end')->nullable();                       //    6	Задолженность потребителей (на конец периода), руб.
            $table->char('claim_number_incoming')->nullable();          //    7	Количество поступивших претензий, ед.
            $table->char('claim_number_satisfied')->nullable();         //    8	Количество удовлетворенных претензий, ед.
            $table->char('claim_number_denied')->nullable();            //    9	Количество претензий, в удовлетворении которых отказано, ед.
            $table->char('recalculation')->nullable();                  //    10	Сумма произведенного перерасчета, руб.

            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
