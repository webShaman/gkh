<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Management\WorkPerformed;

class CreateTableHomesManagerManagerOrganization extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(WorkPerformed::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('maintenance_premises')->nullable();         // Работы по содержанию помещений, входящих в состав общего имущества в многоквартирном доме
            $table->char('export_waste')->nullable();                 // Работы по обеспечению вывоза бытовых отходов
            $table->char('maintenance_elevator')->nullable();         // Работы по содержанию и ремонту лифта (лифтов) в многоквартирном доме
            $table->char('disinfestation')->nullable();               // Проведение дератизации и дезинсекции помещений, входящих в состав общего имущества в многоквартирном доме
            $table->char('structural_elements')->nullable();          // Работы по содержанию и ремонту конструктивных элементов (несущих конструкций и ненесущих конструкций) многоквартирных домов
            $table->char('maintenance_equipment')->nullable();        // Работы по содержанию и ремонту оборудования и систем инженерно-технического обеспечения, входящих в состав общего имущества в многоквартирном доме)
            $table->char('content_land')->nullable();                 // Работы по содержанию земельного участка с элементами озеленения и благоустройства, иными объектами, предназначенными для обслуживания и эксплуатации многоквартирного дома
            $table->char('eliminate_accidents')->nullable();          // Обеспечение устранения аварий на внутридомовых инженерных системах в многоквартирном доме
            $table->char('management')->nullable();                   // Работы (услуги) по управлению многоквартирным домом
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
