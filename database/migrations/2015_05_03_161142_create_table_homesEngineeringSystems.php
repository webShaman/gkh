<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHomesEngineeringSystems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_engineering_systems', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // отопления
            $table->char('heating_system_type', 50);            // Тип
            $table->integer('heating_number_elevator_units');   // Количество элеваторных узлов системы отопления
            $table->integer('heating_length_pipelines');        // Длина трубопроводов системы отопления
            $table->char('heating_year_overhaul', 4);           // Год проведения последнего капитального ремонта системы отопления
            $table->integer('heating_number_dot_in');           // Количество точек ввода отопления
            $table->integer('heating_number_control_units');    // Количество узлов управления отоплением
            $table->integer('heating_number_obschedomovyh');    // Количество общедомовых приборов учета отопления
            $table->char('heating_leave', 50);                  // Отпуск отопления производится
            // Система горячего водоснабжения
            $table->char('hotwater_system_type', 50);           // Тип
            $table->integer('hotwater_length_pipelines');       // Длина трубопроводов системы горячего водоснабжения
            $table->char('hotwater_year_overhaul', 4);          // Год проведения последнего капитального ремонта системы горячего водоснабжения
            $table->integer('hotwater_number_dot_in');          // Количество точек ввода горячей воды
            $table->integer('hotwater_number_control_units');   // Количество узлов управления поставкой горячей воды
            $table->integer('hotwater_number_obschedomovyh');   // Количество общедомовых приборов учета горячей воды
            $table->char('hotwater_leave', 50);                 // Отпуск горячей воды производится
            // Система холодного водоснабжения
            $table->char('coldwater_system_type', 50);          // Тип
            $table->integer('coldwater_length_pipelines');      // Длина трубопроводов системы холодного водоснабжения
            $table->char('coldwater_year_overhaul', 4);         // Год проведения последнего капитального ремонта системы холодного водоснабжения
            $table->integer('coldwater_number_dot_in');         // Количество точек ввода холодного воды
            $table->integer('coldwater_number_obschedomovyh');  // Количество общедомовых приборов учета холодной воды
            $table->char('coldwater_leave', 50);                // Отпуск холодной воды производится
            // Система водоотведения (канализации)
            $table->char('sewage_system_type', 50);             // Тип
            $table->integer('sewage_length_pipelines');         // Длина трубопроводов системы холодного водоснабжения
            $table->char('sewage_year_overhaul', 4);            // Год проведения последнего капитального ремонта системы холодного
            // Система электроснабжения
            $table->char('electrical', 50);                     // Система электроснабжения
            $table->integer('electrical_length_commons');       // Длина сетей в местах общего пользования
            $table->char('electrical_year_overhaul', 4);        // Год проведения последнего капремонта системы электроснабжения
            $table->integer('electrical_number_dot_in');        // Количество точек ввода электричества
            $table->integer('electrical_number_obschedomovyh'); // Количество общедомовых приборов учета электричества
            $table->char('electrical_leave', 50);               // Отпуск электричества производитсяводоснабжения
            // Система газоснабжения
            $table->char('gas_system_type', 50);       // Вид системы газоснабжения
            $table->integer('gas_length_compliant');    // Длина сетей соответствующих требованиям
            $table->integer('gas_length_non_compliant');    // Длина сетей не соответствующих требованиям
            $table->char('gas_year_overhaul', 4);    // Год проведения последнего капремонта системы газоснабжения
            $table->integer('gas_number_dot_in');    // Количество точек ввода газа
            $table->integer('gas_number_obschedomovyh');    // Количество общедомовых приборов учета газа
            $table->char('gas_leave', 50);    // Отпуск газа производится
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_engineering_systems');
	}

}
