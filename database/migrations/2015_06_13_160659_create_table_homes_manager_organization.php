<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Management\Organization;

class CreateTableHomesManagerOrganization extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(Organization::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->date('date_start')->nullable();                     //  1  Дата начала управления
            $table->char('ground_control')->nullable();                 //  2  Основание управления
            // 3	Документ, подтверждающий выбранный способ управления:
            $table->char('document_name')->nullable();                  // - Наименование документа
            $table->char('date_number_document')->nullable();           // - Дата и номер документа
            // 4	Договор управления:
            $table->date('date_contract')->nullable();                  // - Дата заключения договора управления
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
