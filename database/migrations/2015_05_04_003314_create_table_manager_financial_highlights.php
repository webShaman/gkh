<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManagerFinancialHighlights extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('manager_financial_highlights', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с районом
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('manager_common_info');
            // поля
            $table->double('income_service')->nullable();                                       // Доходы, полученные за оказание услуг по управлению многоквартирными домами, тыс.руб.
            $table->double('income_service_houses_less_25')->nullable();                        // - по домам до 25 лет
            $table->double('income_service_houses_26_50')->nullable();                          // - по домам от 26 до 50 лет
            $table->double('income_service_houses_51_75')->nullable();                          // - по домам от 51 до 75 лет
            $table->double('income_service_houses_more_76')->nullable();                        // - по домам 76 лет и более
            $table->double('income_service_houses_alarm')->nullable();                          // - по аварийным домам
            $table->double('income_common_property')->nullable();                               // Сумма доходов, полученных от использования общего имущества за отчетный период, тыс.руб.
            $table->double('income_common_property_less_25')->nullable();                       // - по домам до 25 лет
            $table->double('income_common_property_26_50')->nullable();                         // - по домам от 26 до 50 лет
            $table->double('income_common_property_51_75')->nullable();                         // - по домам от 51 до 75 лет
            $table->double('income_common_property_more_76')->nullable();                       // - по домам 76 лет и более
            $table->double('income_common_property_alarm')->nullable();                         // - по аварийным домам
            $table->double('income_utilities')->nullable();                                     // Доход, полученный за отчетный период от предоставления коммунальных услуг без учета коммунальных ресурсов, поставленных
                                                                                    // потребителям непосредственно поставщиками по прямым договорам, тыс.руб.
            $table->double('income_utilities_heating')->nullable();                             // - отопление
            $table->double('income_utilities_electricity')->nullable();                         // - электричество
            $table->double('income_utilities_gas')->nullable();                                 // - газ
            $table->double('income_utilities_hot_water_supply')->nullable();                    // - горячее водоснабжение
            $table->double('income_utilities_cold_water')->nullable();                          // - холодное водоснабжение
            $table->double('income_utilities_sanitation')->nullable();                          // - водоотведение
            $table->double('costs_service')->nullable();                                        // Расходы, полученные в связи с оказанием услуг по управлению многоквартирными домами, тыс.руб.
            $table->double('costs_service_less_25')->nullable();                                // - по домам до 25 лет
            $table->double('costs_service_26_50')->nullable();                                  // - по домам от 26 до 50 лет
            $table->double('costs_service_51_75')->nullable();                                  // - по домам от 51 до 75 лет
            $table->double('costs_service_more_76')->nullable();                                // - по домам 76 лет и более
            $table->double('costs_service_alarm')->nullable();                                  // - по аварийным домам
            $table->double('costs_contract_service')->nullable();                               // Выплаты по искам по договорам управления за отчетный период, тыс.руб.
            $table->double('costs_contract_service_damage')->nullable();                        // - иски по компенсации нанесенного ущерба
            $table->double('costs_contract_service_unused_service')->nullable();                // - иски по снижению платы в связи с неоказанием услуг
            $table->double('costs_contract_service_non_delivery_resources')->nullable();        // - иски по снижению платы в связи с недопоставкой ресурсов
            $table->double('costs_resources_organization')->nullable();                         // Выплаты по искам ресурсоснабжающих организаций за отчетный период, тыс.руб.
            $table->double('costs_resources_organization_heating')->nullable();                 // - отопление
            $table->double('costs_resources_organization_electricity')->nullable();             // - электричество
            $table->double('costs_resources_organization_gas')->nullable();                     // - газ
            $table->double('costs_resources_organization_hot_water_supply')->nullable();        // - горячее водоснабжение
            $table->double('costs_resources_organization_cold_water')->nullable();              // - холодное водоснабжение
            $table->double('costs_resources_organization_sanitation')->nullable();              // - водоотведение
            $table->double('net_assets')->nullable();                                           // Чистые активы УО, тыс.руб.
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('manager_financial_highlights');
	}

}
