<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesCommonProperty extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_common_property', function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('common_income')->nullable();              //    Доход от управления за отчетный период, тыс. руб.
            $table->char('common_income_property')->nullable();     //    Доход от управления общим имуществом за отчетный период, тыс. руб.
            $table->char('common_costs')->nullable();               //    Расходы на управление за отчетный период, тыс. руб.
            $table->char('common_debt')->nullable();                //    Задолженность собственников за услуги управления за отчетный период, тыс. руб.
            $table->char('common_recover_owners')->nullable();      //    Взыскано с собственников за услуги управления за отчетный период, тыс. руб.
            $table->char('common_payment')->nullable();             //    Выплаты по искам и договорам управления за отчетный период, тыс. руб.
            $table->char('common_claims')->nullable();              //    иски по компенсации нанесенного ущерба
            $table->char('common_claims_refusal')->nullable();      //    иски по снижению платы в связи с неоказанием услуг
            $table->char('common_claims_shipment')->nullable();     //    иски по снижению платы в связи с недопоставкой ресурсов
            $table->char('common_work_repair')->nullable();         //    Объем работ по ремонту за отчетный период, тыс. руб.
            $table->char('common_work_improvement')->nullable();    //    Объем работ по благоустройству за отчетный период, тыс. руб.
            $table->char('common_attracted_funds')->nullable();     //    Объем привлеченных средств за отчетный период, тыс. руб.
            $table->char('common_subsidy')->nullable();             //    субсидии
            $table->char('common_credits')->nullable();             //    кредиты
            $table->char('common_financing_lease')->nullable();     //    финансирование по договорам лизинга
            $table->char('common_financing_energy')->nullable();    //    финансирование по энергосервисным договорам
            $table->char('common_contributions')->nullable();       //    целевые взносы жителей
            $table->char('common_other_sources')->nullable();       //    иные источники
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_common_property');
	}

}
