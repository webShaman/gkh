<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Passport\EngineeringSystems;

class UpdateTablePassportEfineringSystem extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table(EngineeringSystems::TABLE_NAME, function(Blueprint $table) {
            $table->renameColumn('foundation_type', 'cold_water_type');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
