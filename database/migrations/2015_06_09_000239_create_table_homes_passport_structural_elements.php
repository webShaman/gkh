<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Passport\StructuralElements;

class CreateTableHomesPassportStructuralElements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(StructuralElements::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('foundation_type')->nullable();                        //    Тип фундамента
            $table->char('overlap_type')->nullable();                           //    Тип перекрытий
            $table->char('material_bearing_walls')->nullable();                 //    Материал несущих стен
            $table->char('basement_area')->nullable();                          //    Площадь подвала по полу, кв.м
            $table->char('chutes_type')->nullable();                            //    Тип мусоропровода
            $table->char('chutes_number')->nullable();                          //    Количество мусоропроводов, ед.
            $table->char('facade_type')->nullable();                            //    Тип фасада
            $table->char('roof_type')->nullable();                              //    Тип крыши
            $table->char('loft_type')->nullable();                              //    Тип кровли
            $table->char('equipment_type')->nullable();                         //    Вид оборудования / конструктивного элемента
            $table->char('equipment_description')->nullable();                  //    Описание дополнительного оборудования / конструктивного элемента
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
