<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Report\WorkPerformed;

class CreateTableHomesReportWorkPerformed extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(WorkPerformed::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // колонки
            $table->char('name')->nullable();         //    Наименование работы
            $table->char('cost')->nullable();         //    Годовая фактическая стоимость работ/услуг (руб.)
            $table->char('number')->nullable();      //    Количество работ (ед.)

            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
