<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Passport\GeneralInformation;

class CreateTableHomesPassportGeneralInformation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(GeneralInformation::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->date('year_built')->nullable();                             //    Год постройки
            $table->date('year_commissioning')->nullable();                     //    Год ввода в эксплуатацию
            $table->char('series', 50)->nullable();                             //    Серия, тип проекта
            $table->char('type', 50)->nullable();                               //    Тип дома
            $table->char('description_position')->nullable();                   //    Описание местоположения
            $table->char('name_house')->nullable();                             //    Индивидуальное наименование дома
            $table->char('method_overhaul')->nullable();                        //    Способ формирования фонда капитального ремонта
            $table->integer('floors_number_max')->nullable();                   //    наибольшее количество помещений, ед.
            $table->integer('floors_number_min')->nullable();                   //    наименьшее количество помещений, ед.
            $table->integer('entrances_number')->nullable();                    //    Количество подъездов, ед
            $table->integer('lifts_number')->nullable();                        //    Количество лифтов, ед
            $table->integer('rooms_number')->nullable();                        //    Количество помещений, в том числе:
            $table->integer('rooms_number_living')->nullable();                 //    жилых, ед.
            $table->integer('rooms_number_no_living')->nullable();              //    нежилых, ед.
            $table->char('total_area')->nullable();                             //    Общая площадь дома, в том числе, кв.м:
            $table->char('total_area_living')->nullable();                      //    общая площадь жилых помещений, кв.м
            $table->char('total_area_no_living')->nullable();                   //    общая площадь нежилых помещений, кв.м
            $table->char('total_area_common_property')->nullable();             //    общая площадь помещений, входящих в состав общего имущества, кв.м
            $table->char('parcel_area')->nullable();                            //    площадь земельного участка, входящего в состав общего имущества в многоквартирном доме, кв.м
            $table->char('parcel_parking_area')->nullable();                    //    площадь парковки в границах земельного участка, кв.м
            $table->char('cadastral_number')->nullable();                       //    кадастровый номер
            $table->char('energy_efficiency_class')->nullable();                //    Класс энергетической эффективности
            $table->char('beautification_playground_children')->nullable();     //    детская площадка
            $table->char('beautification_playground_sports')->nullable();       //    спортивная площадка
            $table->char('beautification_other')->nullable();                   //    другое
            $table->char('additional_information')->nullable();                 //    Дополнительная информация
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
