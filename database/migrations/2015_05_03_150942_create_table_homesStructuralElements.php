<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHomesStructuralElements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_structural_elements', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            //Фасад
            $table->float('facade_area_total');             // Площадь фасада общая
            $table->float('facade_area_plastered');         // Площадь фасада оштукатуренная
            $table->float('facade_area_not_plastered');     // Площадь фасада неоштукатуренная
            $table->float('facade_area_panel');             // Площадь фасада панельная
            $table->float('facade_area_tiles');             // Площадь фасада, облицованная плиткой
            $table->float('facade_area_siding');            // Площадь фасада, облицованная сайдингом
            $table->float('facade_area_wooden');            // Площадь фасада деревянная
            $table->float('facade_area_thermal_finish');    // Площадь утепленного фасада с отделкой декоративной штукатуркой
            $table->float('facade_area_thermal_tiles');     // Площадь утепленного фасада с отделкой плиткой
            $table->float('facade_area_thermal_siding');    // Площадь утепленного фасада с отделкой сайдингом
            $table->float('facade_area_otmostka');          // Площадь отмостки
            $table->float('facade_area_glazing_common');    // Площадь остекления мест общего пользования (дерево)
            $table->float('facade_area_glazing_public');    // Площадь остекления мест общего пользования (пластик)
            $table->float('facade_area_individual_glazing_wood');  // Площадь индивидуального остекления (дерево)
            $table->float('facade_area_individual_glazing_plastic');  // Площадь индивидуального остекления (пластик)
            $table->float('facade_area_metal_door');        // Площадь металлических дверных заполнений
            $table->float('facade_area_wood_door');         // Площадь иных дверных заполнений
            $table->char('facade_year_renovation', 4);      // Год проведения последнего капитального ремонта
            // Кровля
            $table->float('roof_area_total');               // Площадь кровли общая
            $table->float('roof_area_ramp_slate');          // Площадь кровли шиферная скатная
            $table->float('roof_area_ramp_metal');          // Площадь кровли металлическая скатная
            $table->float('roof_area_ramp_different');      // Площадь кровли иная скатная
            $table->float('roof_area_flat');                // Площадь кровли плоская
            $table->char('roof_year_renovation', 4);        // Год проведения последнего капитального ремонта кровли
            // Подвал
            $table->text('basement_Information');           // Сведения о подвале
            $table->float('basement_area_total');           // Площадь подвальных помещений
            $table->char('basement_year_renovation', 4);    // Год проведения ремонта
            // Помещения общего пользования
            $table->float('common_area_total');             // Площадь помещений общего пользования
            $table->char('common_year_renovation', 4);      // Год проведения последнего ремонта помещений общего пользования
            // Мусоропроводы
            $table->integer('chutes_number');      // Количество мусоропроводов в доме	Нет данных
            $table->char('chutes_year_renovation', 4);      // Год проведения последнего ремонта мусоропроводов
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_structural_elements');
	}

}
