<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesCardManagement extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_card_management', function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->text('management_type_contract')->nullable();       //    Тип договора управления
            $table->date('management_date_start')->nullable();          //    Дата начала обслуживания дома
            $table->date('management_date_end')->nullable();            //    Плановая дата прекращения обслуживания дома
            $table->text('management_performed_works')->nullable();     //    Выполняемые работы
            $table->text('management_execution_works')->nullable();     //    Выполнение обязательств
            $table->text('management_note')->nullable();                //    Примечание
            $table->text('management_cost_services')->nullable();       //    Стоимость услуг
            $table->text('management_means')->nullable();               //    Средства ТСЖ или ЖСК
            $table->text('management_terms_services')->nullable();      //    Условия оказания услуг ТСЖ или ЖСК
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_card_management');
	}

}
