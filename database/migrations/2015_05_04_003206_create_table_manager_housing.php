<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManagerHousing extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('manager_housing', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с районом
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('manager_common_info');
            // поля
            $table->integer('number_peoples')->nullable();                                       // Число жителей в обслуживаемых домах
            $table->integer('number_houses_end_time')->nullable();                               // Количество домов под управлением на отчетную дату
            $table->integer('number_houses_end_time_tsg')->nullable();                           // - обслуживаемых ТСЖ
            $table->integer('number_houses_end_time_tsg_and_organization')->nullable();          // - обслуживаемых по договору между ТСЖ и управляющей организацией
            $table->integer('number_houses_end_time_tsg_and_owner')->nullable();                 // - обслуживаемых по договору между собственниками и управляющей организацией
            $table->integer('number_houses_end_time_open_competition')->nullable();              // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
            $table->integer('number_houses_start_time')->nullable();                             // Количество домов под управлением на начало периода
            $table->integer('number_houses_start_time_tsg')->nullable();                         // - обслуживаемых ТСЖ
            $table->integer('number_houses_start_time_tsg_and_organization')->nullable();        // - обслуживаемых по договору между ТСЖ и управляющей организацией
            $table->integer('number_houses_start_time_tsg_and_owner')->nullable();               // - обслуживаемых по договору между собственниками и управляющей организацией
            $table->integer('number_houses_start_time_open_competition')->nullable();            // - обслуживаемых по результатам открытого конкурса органов местного самоуправления
            $table->double('common_area_houses_reporting_date')->nullable();                     // Общая площадь домов под управлением на отчетную дату, включая жилые и нежилые помещения, а также помещения общего пользования, тыс.кв.м.
            $table->double('common_area_houses_reporting_date_less_25')->nullable();             // - по домам до 25 лет
            $table->double('common_area_houses_reporting_date_26_50')->nullable();               // - по домам от 26 до 50 лет
            $table->double('common_area_houses_reporting_date_51_75')->nullable();               // - по домам от 51 до 75 лет
            $table->double('common_area_houses_reporting_date​_more_76')->nullable();             // - по домам 76 лет и более
            $table->double('common_area_houses_reporting_date_house_alarm')->nullable();         // - по аварийным домам
            $table->double('common_area_houses_start_period')->nullable();                       // Площадь домов под управлением на начало периода, тыс.кв.м.
            $table->double('common_area_houses_start_period_concluded_contract')->nullable();    // - изменение площади по заключенным договорам
            $table->double('common_area_houses_start_period_terminate_contract')->nullable();    // - изменение площади по расторгнутым договорам
            $table->double('average_term_service')->nullable();                                  // Средний срок обслуживания МКД, лет
            $table->double('average_term_service_less_25')->nullable();                          // - по домам до 25 лет
            $table->double('average_term_service_26_50')->nullable();                            // - по домам от 26 до 50 лет
            $table->double('average_term_service_51_75')->nullable();                            //  - по домам от 51 до 75 лет
            $table->double('average_term_service_more_76')->nullable();                          // - по домам 76 лет и более
            $table->double('average_term_service_house_alarm')->nullable();                      // - по аварийным домам
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('manager_housing');
	}

}
