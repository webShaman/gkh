<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('homes_passport_general', function(Blueprint $table)
        {
            $table->string('series', 50)->nullable()->change();
            $table->string('description_position', 50)->nullable()->change();
            $table->string('individual_name', 50)->nullable()->change();
            $table->integer('year_commissioning')->nullable()->change();
            $table->string('wall_material', 50)->nullable()->change();
            $table->string('type_floors', 50)->nullable()->change();
            $table->date('year_commissioning')->nullable()->change();
            $table->string('wall_material', 50)->nullable()->change();
            $table->string('type_floors', 50)->nullable()->change();
            $table->integer('number_storeys')->nullable()->change();
            $table->integer('number_entrances')->nullable()->change();
            $table->integer('number_lifts')->nullable()->change();
            $table->float('total_area')->nullable()->change();
            $table->float('residential_area')->nullable()->change();
            $table->float('residential_area_private')->nullable()->change();
            $table->float('residential_area_municipal')->nullable()->change();
            $table->float('residential_area_state')->nullable()->change();
            $table->float('​​residential_non_area')->nullable()->change();
            $table->float('land_area')->nullable()->change();
            $table->float('territory_area')->nullable()->change();
            $table->string('inventory_number', 20)->nullable()->change();
            $table->string('cadastral_number', 20)->nullable()->change();
            $table->integer('apartments_number')->nullable()->change();
            $table->integer('residents_number')->nullable()->change();
            $table->integer('accounts_number')->nullable()->change();
            $table->text('design_features')->nullable()->change();
            $table->float('thermal_characteristic')->nullable()->change();
            $table->float('thermal_characteristic_actual')->nullable()->change();
            $table->float('thermal_characteristic_regulatory')->nullable()->change();
            $table->string('energy_efficiency', 10)->nullable()->change();
            $table->dateTime('date_energy_audit')->nullable()->change();
            $table->dateTime('date_privatization')->nullable()->change();
            $table->integer('wear')->nullable()->change();
            $table->integer('wear_foundation')->nullable()->change();
            $table->integer('wear_wall')->nullable()->change();
            $table->integer('wear_overlap')->nullable()->change();
        });

        Schema::table('homes_structural_elements', function(Blueprint $table)
        {
            $table->float('facade_area_total')->nullable()->change();
            $table->float('facade_area_plastered')->nullable()->change();
            $table->float('facade_area_not_plastered')->nullable()->change();
            $table->float('facade_area_panel')->nullable()->change();
            $table->float('facade_area_tiles')->nullable()->change();
            $table->float('facade_area_siding')->nullable()->change();
            $table->float('facade_area_wooden')->nullable()->change();
            $table->float('facade_area_thermal_finish')->nullable()->change();
            $table->float('facade_area_thermal_tiles')->nullable()->change();
            $table->float('facade_area_thermal_siding')->nullable()->change();
            $table->float('facade_area_otmostka')->nullable()->change();
            $table->float('facade_area_glazing_common')->nullable()->change();
            $table->float('facade_area_glazing_public')->nullable()->change();
            $table->float('facade_area_individual_glazing_wood')->nullable()->change();
            $table->float('facade_area_individual_glazing_plastic')->nullable()->change();
            $table->float('facade_area_metal_door')->nullable()->change();
            $table->float('facade_area_wood_door')->nullable()->change();
            $table->string('facade_year_renovation', 4)->nullable()->change();
            // Кровля
            $table->float('roof_area_total')->nullable()->change();
            $table->float('roof_area_ramp_slate')->nullable()->change();
            $table->float('roof_area_ramp_different')->nullable()->change();
            $table->float('roof_area_ramp_metal')->nullable()->change();
            $table->float('roof_area_flat')->nullable()->change();
            $table->string('roof_year_renovation', 4)->nullable()->change();
            // Подвал
            $table->text('basement_Information')->nullable()->change();
            $table->float('basement_area_total')->nullable()->change();
            $table->string('basement_year_renovation', 4)->nullable()->change();
            // Помещения общего пользования
            $table->float('common_area_total')->nullable()->change();
            $table->string('common_year_renovation', 4)->nullable()->change();
            // Мусоропроводы
            $table->integer('chutes_number')->nullable()->change();
            $table->string('chutes_year_renovation', 4)->nullable()->change();
        });

        Schema::table('homes_engineering_systems', function(Blueprint $table)
        {
            // отопления
            $table->string('heating_system_type', 50)->nullable()->change();
            $table->integer('heating_number_elevator_units')->nullable()->change();
            $table->integer('heating_length_pipelines')->nullable()->change();
            $table->string('heating_year_overhaul', 4)->nullable()->change();
            $table->integer('heating_number_dot_in')->nullable()->change();
            $table->integer('heating_number_control_units')->nullable()->change();
            $table->integer('heating_number_obschedomovyh')->nullable()->change();
            $table->string('heating_leave', 50)->nullable()->change();
            // Система горячего водоснабжения
            $table->string('hotwater_system_type', 50)->nullable()->change();
            $table->integer('hotwater_length_pipelines')->nullable()->change();
            $table->string('hotwater_year_overhaul', 4)->nullable()->change();
            $table->integer('hotwater_number_dot_in')->nullable()->change();
            $table->integer('hotwater_number_control_units')->nullable()->change();
            $table->integer('hotwater_number_obschedomovyh')->nullable()->change();
            $table->string('hotwater_leave', 50)->nullable()->change();
            // Система холодного водоснабжения
            $table->string('coldwater_system_type', 50)->nullable()->change();
            $table->integer('coldwater_length_pipelines')->nullable()->change();
            $table->string('coldwater_year_overhaul', 4)->nullable()->change();
            $table->integer('coldwater_number_dot_in')->nullable()->change();
            $table->integer('coldwater_number_obschedomovyh')->nullable()->change();
            $table->string('coldwater_leave', 50)->nullable()->change();
            // Система водоотведения (канализации)
            $table->string('sewage_system_type', 50)->nullable()->change();
            $table->integer('sewage_length_pipelines')->nullable()->change();
            $table->string('sewage_year_overhaul', 4)->nullable()->change();
            // Система электроснабжения
            $table->string('electrical', 50)->nullable()->change();
            $table->integer('electrical_length_commons')->nullable()->change();
            $table->string('electrical_year_overhaul', 4)->nullable()->change();
            $table->integer('electrical_number_dot_in')->nullable()->change();
            $table->integer('electrical_number_obschedomovyh')->nullable()->change();
            $table->string('electrical_leave', 50)->nullable()->change();
            // Система газоснабжения
            $table->string('gas_system_type', 50)->nullable()->change();
            $table->integer('gas_length_compliant')->nullable()->change();
            $table->integer('gas_length_non_compliant')->nullable()->change();
            $table->string('gas_year_overhaul', 4)->nullable()->change();
            $table->integer('gas_number_dot_in')->nullable()->change();
            $table->integer('gas_number_obschedomovyh')->nullable()->change();
            $table->string('gas_leave', 50)->nullable()->change();
        });

        Schema::table('homes_elevator', function(Blueprint $table) {
            $table->text('elevators')->nullable()->change();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
