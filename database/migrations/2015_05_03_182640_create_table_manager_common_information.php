<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManagerCommonInformation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('manager_common_info', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с районом
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')->references('id')->on('areas');
            // поля
            $table->char('full_name', 250)->nullable();                                     // Полное наименование
            $table->char('short_name', 150)->nullable();                                    // Краткое наименование
            $table->char('organization_form', 150)->nullable();                             // Организационная форма
            $table->char('director', 150)->nullable();                                      // Руководитель
            $table->integer('inn')->nullable();                                             // ИНН
            $table->integer('ogrn')->nullable();                                            // ОГРН или ОГРНИП
            $table->time('date_ogrn')->nullable();                                          // Дата присвоения ОГРН (ОГРНИП)
            $table->char('name_register_organ', 250)->nullable();                           // Наименование органа, принявшего решение о регистрации
            $table->char('legal_address', 250)->nullable();                                 // Юридический адрес
            $table->char('physical_address', 250)->nullable();                              // Фактический адрес
            $table->char('mailing_address',250)->nullable();                                // Почтовый адрес
            $table->char('time_working', 150)->nullable();                                  // Режим работы
            $table->char('phone',25)->nullable();                                           // Телефон
            $table->char('email_address', 30)->nullable();                                  // Электронный адрес
            $table->char('internet_site', 30)->nullable();                                  // Интернет сайт
            $table->double('share_in_authorized_capital_subject​​')->nullable();              // Доля участия в уставном капитале Субъекта РФ, %
            $table->double('share_in_authorized_capital_municipal')->nullable();            // Доля участия в уставном капитале муниципального образования, %
            $table->char('additional_information', 250)->nullable();                        // Дополнительная информация
            $table->char('self_regulatory_organizations', 250)->nullable();                 // Сведения об участии в саморегулируемых организациях
            $table->integer('number_subject_where_organization_working')->nullable();       // Количество Субъектов РФ, в которых организация осуществляет свою деятельность
            $table->integer('number_municipal_where_organization_working')->nullable();     // Количество муниципальных образований, в которых организация осуществляет свою деятельность
            $table->integer('number_office')->nullable();                                   // Количество офисов обслуживания граждан
            $table->integer('regular_staffing')->nullable();                                // Штатная численность на отчетную дату, чел.
            $table->integer('regular_staffing_administrative')->nullable();                 // - административный персонал, чел.
            $table->integer('regular_staffing_engineers')->nullable();                      // - инженеры, чел.
            $table->integer('regular_staffing_working')->nullable();                        // - рабочий персонал, чел.
            $table->integer('laid_off_during_period')->nullable();                          // Уволено за отчетный период, чел.
            $table->integer('laid_off_during_period_administrative')->nullable();           // - административный персонал, чел.
            $table->integer('laid_off_during_period_engineers')->nullable();                // - инженеры, чел.
            $table->integer('laid_off_during_period_working')->nullable();                  // - рабочий персонал, чел.
            $table->integer('number_accidents')->nullable();                                // Число несчастных случаев за отчетный период
            $table->integer('number_administrative_responsibility')->nullable();            // Число случаев привлечения организации к административной ответственности
            $table->char('copies_document_administrative_responsibility', 100)->nullable(); // Копии документов о применении мер административного воздействия, а также мер, принятых для устранения нарушений, повлекших применение административных санкций
            $table->char('members_of_board', 250)->nullable();                              // Члены правления ТСЖ или ЖСК
            $table->char('members_audit_commission', 250)->nullable();                      // Члены ревизионной комиссии
            $table->char('more_information', 250)->nullable();                              // Дополнительные сведения в произвольной форме
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('manager_common_info');
	}

}
