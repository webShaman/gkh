<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignHomeTomanager extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('homes', function(Blueprint $table)
        {
            // связываем с упарвляющей организацией
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('manager_common_info');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('homes', function(Blueprint $table)
        {
            // связываем с упарвляющей организацией
            $table->dropIndex('manager_id');
            $table->drop('manager_id');
        });
	}

}
