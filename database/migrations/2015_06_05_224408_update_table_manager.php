<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableManager extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('manager_common_info', function(Blueprint $table)
        {
            $table->char('address_dispatch_service', 250)->nullable();                              //— Адрес диспетчерской службы
            $table->char('contact_telephone_dispatch_service', 250)->nullable();                    //— Контактные телефоны диспетчерской службы
            $table->char('mode_operation_dispatch_service', 250)->nullable();                       //— Режим работы диспетчерской службы
            $table->char('fax', 50)->nullable();                                                    //Факс
            $table->integer('number_houses')->nullable();                                           //Количество домов, находящихся в управлении , ед.
            $table->double('area_houses')->nullable();                                              //Площадь домов, находящихся в управлении, кв. м
        });

        Schema::table('manager_financial_highlights', function(Blueprint $table)
        {
            $table->date('start_time_reporting_period', 250)->nullable();                            //Дата начала отчетного периода
            $table->date('end_time_reporting_period', 250)->nullable();                              //Дата конца отчетного периода
            $table->double('total_debt')->nullable();                                                //Общая задолженность управляющей организации (индивидуального предпринимателя) перед ресурсоснабжающими организациями за коммунальные ресурсы, руб.
            $table->double('total_debt_heat_energy')->nullable();                                    //— Общая задолженность по тепловой энергии, руб.
            $table->double('total_debt_heat_energy_for_heating')->nullable();                        //— Общая задолженность по тепловой энергии для нужд отопления, руб.
            $table->double('total_debt_heat_energy_for_water')->nullable();                          //— Общая задолженность по тепловой энергии для нужд горячего водоснабжения, руб.
            $table->double('total_debt_hot_water_supply')->nullable();                               //— Общая задолженность по горячей воде, руб.
            $table->double('total_debt_cold_water')->nullable();                                     //— Общая задолженность по холодной воде, руб.
            $table->double('total_debt_sanitation')->nullable();                                     //— Общая задолженность по водоотведению, руб.
            $table->double('total_debt_gas')->nullable();                                            //— Общая задолженность по поставке газа, руб.
            $table->double('total_debt_electricity')->nullable();                                    //— Общая задолженность по электрической энергии, руб.
            $table->double('total_debt_for_other_resources')->nullable();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
