<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DellForeignHomeToManager extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('homes', function(Blueprint $table)
        {
            // удаляем жесткую привязку
            $table->dropForeign('homes_manager_id_foreign');
        });


        Schema::table('homes', function(Blueprint $table)
        {
            // пока делаем пустым
            $table->integer('manager_id')->nullable()->change();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
