<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableManagerCommonInformatio extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('manager_common_info', function(Blueprint $table)
        {
            $table->char('map', 250)->nullable();                                           // Карта
            $table->char('responsible_for_filling', 100)->nullable();                       // Ответственный за заполнение
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
