<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHomesPassportGeneral extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homes_passport_general', function(Blueprint $table)
        {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // поля
            $table->char('series', 50);                 // Серия, тип проекта
            $table->char('description_position', 50);   // Описание местоположения
            $table->char('individual_name', 50);        // Индивидуальное наименование дома
            $table->char('type_home', 50);              // Тип жилого дома
            $table->integer('year_commissioning');   // Год ввода в эксплуатацию
            $table->char('wall_material', 50);          // Материал стен
            $table->char('type_floors', 50);            // Тип перекрытий
            $table->integer('number_storeys');       // Этажность
            $table->integer('number_entrances');     // Количество подъездов
            $table->integer('number_lifts');         // Количество лифтов
            $table->float('total_area');                // Общая площадь
            $table->float('residential_area');          // Площадь жилых помещений
            $table->float('residential_area_private');          // Частная
            $table->float('residential_area_municipal');        // Муниципальная
            $table->float('residential_area_state');            // Государственная
            $table->float('​​residential_non_area');      // Площадь нежилых помещений
            $table->float('land_area');                 // Площадь участка
            $table->float('territory_area');            // Площадь придомовой территории
            $table->char('inventory_number', 20);       // Инвентарный номер
            $table->char('cadastral_number', 20);       // Кадастровый номер участка
            $table->integer('apartments_number');    // Количество квартир
            $table->integer('residents_number');     // Количество жителей
            $table->integer('accounts_number');      // Количество лицевых счетов
            $table->text('design_features');            // Конструктивные особенности (текст)
            $table->float('thermal_characteristic');    // тепловая характеристика
            $table->float('thermal_characteristic_actual');     // тепловая характеристика
            $table->float('thermal_characteristic_regulatory'); // тепловая характеристика
            $table->char('energy_efficiency', 10);      // Класс энергоэффективности
            $table->dateTime('date_energy_audit');      // Дата энергетического аудита
            $table->dateTime('date_privatization');     // Дата начала приватизации
            $table->integer('wear');                 // Общая степень износа
            $table->integer('wear_foundation');              // Степень износа фундамента
            $table->integer('wear_wall');                    // Степень износа несущих стен
            $table->integer('wear_overlap');                 // Степень износа перекрытий
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('homes_passport_general');
	}

}
