<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Home\Report\ClaimWork;

class CreateTableHomesReportClaimWork extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create(ClaimWork::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');
            // связываем с домом
            $table->integer('home_id')->unsigned();
            $table->foreign('home_id')->references('id')->on('homes');
            // колонки
            $table->char('sent_claims')->nullable();                //    1	Направлено претензий потребителям-должникам
            $table->char('sent_suits')->nullable();                 //    2	Направлено исковых заявлений
            $table->char('obtained_funds')->nullable();             //    3	Получено денежных средств по результатам претензионно-исковой работы

            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
