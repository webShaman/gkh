<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAreaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('areas', function(Blueprint $table)
        {
            $table->char('name_ru_rod', 20)->nullable();                       // Родительный падеж
            $table->char('name_ru_dat', 20)->nullable();                       // Дательный падеж
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
