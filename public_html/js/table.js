/**
 * Created by Андрей on 27.05.15.
 */

var numberElementsAll = 0; // количество элементов
var curentPage=0; // текущая страница
var numberElementsOnPage=20;  // Количество элементов на странице

function drawNumberList(PageScrolling){
    var tags="";
    var oneDisplay1=0;
    var oneDisplay2=0;
    PageScrolling.innerHTML=tags;
    if(numberElementsOnPage<=numberElementsAll){
        if(curentPage==1){
            tags="<li class=\"uk-disabled\"><span>";
        }else{
            tags="<li><a>";
        }
        tags+="<i class=\"uk-icon-angle-double-left\"></i>";
        if(curentPage==1){
            tags+="</span></li>";
        }else{
            tags+="</a></li>";
        }

        for(var i=0; i<((numberElementsAll)/numberElementsOnPage); i=i+1){
            var Ni=i+1;

            if(Ni<(curentPage-3)&&i==0){
                tags+="<li><a >"+Ni+"</a></li>";
            }else if(Ni<(curentPage-2)&&oneDisplay1==0){
                tags+="<li><span>...</span></li>";
                oneDisplay1=1;
            }else if(Ni>=(curentPage-2) && Ni<=(curentPage+2)){
                tags+="<li";
                if(curentPage==Ni) {
                    tags+=" class=\"uk-active\" ";
                    tags+="><span >"+Ni+"</span>";
                }else{
                    tags+="><a >"+Ni+"</a>";
                }
                tags+="</li>";
            }else if(Ni>=(curentPage+2)&&(curentPage+3)<((numberElementsAll)/numberElementsOnPage)&&oneDisplay2==0){
                tags+="<li><span>...</span></li>";
                oneDisplay2=1;
            }else if(((numberElementsAll/numberElementsOnPage)-i)<=1){
                tags+="<li><a >"+Ni+"</a></li>";
            }
        }
        if(((numberElementsAll/numberElementsOnPage)-curentPage)<=0){
            tags+="<li class=\"uk-disabled\"><span>";
        }else{
            tags+="<li><a>";
        }
        tags+="<i class=\"uk-icon-angle-double-right\"></i>";
        if(((numberElementsAll/numberElementsOnPage)-curentPage)<=0){
            tags+="</span></li>";
        }else{
            tags+="</a></li>";
        }
    }
    PageScrolling.innerHTML=tags;
    var node;
    for (var i = 0; (node = PageScrolling.getElementsByTagName("a").item(i)); i++) {
        if (node.addEventListener) node.addEventListener("click", Display, false);
        else if (node.attachEvent) node.attachEvent("onclick", Display);
        //node.title = "Нажмите на кнопку";
    }
}

// суть скрипта
function Display(e) {
    var el = window.event ? window.event.srcElement : e.currentTarget;
    while (el.tagName.toLowerCase() != "li") el = el.parentNode;
    DisplayBody(el.parentNode,el);
}

function DisplayBody(ul,el) {
    if(el!=null){
        var li_a = el.firstChild;
        var li_a_in = li_a.innerHTML;
        var nextpage=0;
        if(! Number(li_a.innerHTML)){
            if(el.getElementsByTagName('i').item(0).className=="uk-icon-angle-double-left"){
                if(Number(curentPage)>1){
                    nextpage=curentPage-1;
                }
            }else if(el.getElementsByTagName('i').item(0).className=="uk-icon-angle-double-right"){
                if((numberElementsAll/numberElementsOnPage)>curentPage){
                    nextpage=curentPage+1;
                }
            }
        }else if(Number(li_a_in))
        {
            nextpage=Number(li_a_in);
        }else if(curentPage>1){
            nextpage=curentPage-1;
        }else{
            nextpage=curentPage;
        }
    }else{
        nextpage=1;
    }

    var dad = ul.parentNode.parentNode.parentNode.parentNode;
    var table = dad.getElementsByTagName("table").item(0);
    var node;
    var tbody=table.getElementsByTagName("tbody").item(0);
    for (var i = 0; (node = tbody.getElementsByTagName("tr").item(i)); i++) {
        if(i>=((nextpage-1)*numberElementsOnPage) && i<nextpage*numberElementsOnPage){
            node.style.display="";
        }else{
            node.style.display="none";
        }
    }
    curentPage=nextpage;
    drawNumberList(ul);
}


function NumberElem(e) {
    var el = window.event ? window.event.srcElement : e.currentTarget;
    while (el.tagName.toLowerCase() != "li") el = el.parentNode;
    var ul=el.parentNode;
    for (var i = 0; (node = ul.getElementsByTagName("a").item(i)); i++) {
        if(node.innerHTML == el.firstChild.innerHTML){
            node.parentNode.className="uk-active";
            numberElementsOnPage=node.innerHTML;
        }else{
            node.parentNode.className="";
        }
    }

    drawNumberList(ul.parentNode.parentNode.getElementsByTagName("ul").item(0));
    if(numberElementsOnPage<=numberElementsAll){
        DisplayBody(ul.parentNode.parentNode.getElementsByTagName("ul").item(0),ul.parentNode.parentNode.getElementsByTagName("ul").item(0).getElementsByTagName("li").item(1));
    }else{
        DisplayBody(ul.parentNode.parentNode.getElementsByTagName("ul").item(0),null);
    }

}


// ф-ция инициализации всего процесса
function init(e) {
    numberElementsOnPage=20;
    if (!document.getElementsByTagName) return;
    numberElementsAll= document.getElementById("Table").getElementsByTagName("tbody").item(0).getElementsByTagName("tr").length;
    curentPage=1;
    var PageScrolling = document.getElementById("Action");
    drawNumberList(PageScrolling);
    var node;
    for (var i = 0; (node = PageScrolling.getElementsByTagName("a").item(i)); i++) {
        if (node.addEventListener) node.addEventListener("click", Display, false);
        else if (node.attachEvent) node.attachEvent("onclick", Display);
        //node.title = "Нажмите на кнопку";
    }
    var NumberOnPage = document.getElementById("Number");
    for (var i = 0; (node = NumberOnPage.getElementsByTagName("li").item(i)); i++) {
        if (node.addEventListener) node.addEventListener("click", NumberElem, false);
        else if (node.attachEvent) node.attachEvent("onclick", NumberElem);
        //node.title = "Нажмите на кнопку";
    }

    DisplayBody(PageScrolling,PageScrolling.getElementsByTagName("li").item(1));
}

// запускаем ф-цию init() при возникновении события load
var root = window.addEventListener || window.attachEvent ? window : document.addEventListener ? document : null;
if (root){
    if (root.addEventListener) root.addEventListener("load", init, false);
    else if (root.attachEvent) root.attachEvent("onload", init);
}
